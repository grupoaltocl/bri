##Instalación

###Requisitos:

- Ruby 2.3.0
- Apache2 / httpd
- MySql
- Passenger

##Configuración:
###Ruby
Para configurar Ruby se recomienda utilizar **_rvm_** y configurar un alias de ambiente para la plataforma, así se evita cambiar dependencias de forma indeseada por otras plataformas en el mismo servidor.

Una vez configurado el alias con la versión 2.3.0, nos preocupamos de utilizar la versión configurada, por ejemplo:

```
rvm use 2.3.0@alto
```

Luego se debe instalar las gemas y dependencias del proyecto. Para esto se utiliza bundler, un gestor de gemas para aplicaciones en Ruby.

Para instalar bundler ejecutamos el siguiente comando:

```
gem install bundler
```

Dentro de la carpeta del proyecto instalamos las gemas necesarias extraidas del archivo Gemfile del proyecto ejecutando el siguiente comando:

```
bundle install
```

### Apache y Passenger

Para utilizar apache se debe configurar un VirtualHost, a continuación se deja uno de ejemplo:

```
<VirtualHost *:80>
    DocumentRoot /var/www/html/devaid/public
    ServerName bri.altosistema.com
    PassengerRuby /home/centos/.rvm/gems/ruby-2.3.0@alto/wrappers/ruby
    TimeOut 600
    <Directory "/var/www/html/devaid/public">
        AllowOverride all
        Allow from all
        Options -MultiViews
        Require all granted
    </Directory>
</VirtualHost>
```

En general se pueden seguir las [instrucciones](https://www.phusionpassenger.com/library/walkthroughs/deploy/ruby/ownserver/integration_mode.html) oficiales de Passenger.

###Proyecto

El proyecto también tiene configuraciones dinámicas dependiendo del ambiente de trabajo o servidor en donde esté instalado.

#### Base de datos

```
# config/database.yml
# 
# SQLite version 3.x
#   gem install sqlite3
#
#   Ensure the SQLite 3 gem is defined in your Gemfile
#   gem 'sqlite3'
#
default: &default
  adapter: mysql2
  username: user
  password: password
  host: localhost
  port: 3306
  database: db_name
  pool: 5
  timeout: 5000

development:
  adapter: mysql2
  username: user
  password: password
  host: localhost
  port: 3306
  database: db_name
  pool: 5
  timeout: 5000


production:
  adapter: mysql2
  encondig: utf8
  username: user
  password: password
  host: localhost
  port: 3306
  database: db_name
  pool: 5
  timeout: 5000
# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  <<: *default
  database: db/test.sqlite3

production:
  <<: *default
  database: db_name
```

####Secrets

```
# config/secrets.yml
#
# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rails secret` to generate a secure secret key.

# Make sure the secrets in this file are kept private
# if you're sharing your code publicly.

development:
  secret_key_base: SECRET_KEY_DEV

test:
  secret_key_base: SECRET_KEY_TEST

# Do not keep production secrets in the repository,
# instead read values from the environment.
production:
  secret_key_base: SECRET_KEY_PROD

```

####Cities (Facebook)

```
# config/cities.yml
cities:
  111806975511600:
    name: Arica
  109988942364338: 
    name: Iquique
  106221866081430: 
    name: Calama
  110061615681238: 
    name: Antofagasta
  108711402493416: 
    name: Copiapó
  105686679466017: 
    name: La Serena
  111908478835911: 
    name: Valparaiso
  112371848779363: 
    name: Santiago
  110300362332319: 
    name: Rancagua
  108445209187309: 
    name: Talca
  109177112433420: 
    name: Concepcion
  108019992553591: 
    name: Temuco
  108628905828413: 
    name: Valdivia
  115767185103622: 
    name: Puerto Montt
  108301162534806: 
    name: Chaiten
  111586475526184: 
    name: Coyhaique
  108224032538973: 
    name: Cochrane
  108817815816062: 
    name: Puerto Natales
  108424689189074: 
    name: Punta Arenas
```

#### FOG (S3 AWS)

``` 
# config/fog.yml
default: &default
    aws_access_key_id:    KEY_ID
    aws_secret_access_key:    SECRET_ACCESS_KEY
    aws_region:    AWS_REGION

staging:
    <<: *default
    directory: DIR_NAME

production:
    <<: *default
    directory: DIR_NAME

development:
    <<: *default
    directory: DIR_NAME
```

#### Proxy (Facebook Scraper)

```
# config/proxy.yml
ip: IPv4:PORT
```
## Subiendo nuevas versiones

Para subir nuevas versiones de la aplicación en las cuales los cambios tengan relevancia con la base de datos, es importante ejecutar las migraciones pertinentes para actualizar el schema de la base de datos.

Para eso se deben instalar las nuevas gemas con bundler y luego ejecutar el siguiente comando:

```
bundle exec rake assets:precompile db:migrate
```

Finalmente, cada vez que se actualice la aplicación es importante reiniciar apache para ver los cambios en producción.
