module ApplicationHelper
  # Returns the full title on a per-page basis.

  STATUS_CLEAN = 0
  STATUS_LOW = 1
  STATUS_MODERATE = 2
  STATUS_HIGH = 3
  STATUS_CRITICAL = 4

  USE_STATUS_UNDEFINED = 0
  USE_STATUS_USED = 1
  USE_STATUS_NEW = 2

  USE_TYPE_CATEGORY = 0
  USE_TYPE_BRAND = 1

  ACTIVATE_BY_NAME = 0
  ACTIVATE_BY_DESCRIPTION = 1
  ACTIVATE_BY_NAME_AND_DESCRIPTION = 2

  SEARCH_BRAND = 0
  SEARCH_CATEGORY = 1
  
  $colors = ['#dc3912','#ff9900','#109618','#990099','#0099c6','#dd4477','#66aa00','#b82e2e','#316395','#994499','#22aa99','#aaaa11','#6633cc','#e67300','#8b0707','#651067','#329262','#5574a6','#3b3eac','#b77322','#16d620','#b91383','#0011cc','#f4359e','#9c5935','#a9c413','#2a778d','#668d1c','#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00', '#b82e2e', '#316395', '#994499', '#22aa99', '#aaaa11', '#6633cc', '#e67300', '#8b0707', '#651067', '#329262', '#5574a6', '#3b3eac', '#b77322', '#16d620', '#b91383', '#f4359e', '#9c5935', '#a9c413', '#2a778d', '#668d1c', '#bea413', '#0c5922', '#743411']
  $slices = 
  def status_for(status)
    val = "Limpio"
    case status
      when ApplicationHelper::STATUS_CLEAN
        val = "Nulo"
      when ApplicationHelper::STATUS_LOW
        val = "Bajo"
      when ApplicationHelper::STATUS_MODERATE
        val = "Medio"
      when ApplicationHelper::STATUS_HIGH
        val = "Alto"
      when ApplicationHelper::STATUS_CRITICAL
        val = "Crítico"
    end
    val
  end

  # Returns the corresponding color for status
  def status_color(status)
    val = "green"
    case status
      when ApplicationHelper::STATUS_CLEAN
        val = "gray"
      when ApplicationHelper::STATUS_LOW
        val = "green"
      when ApplicationHelper::STATUS_MODERATE
        val = "yellow"
      when ApplicationHelper::STATUS_HIGH
        val = "orange"
      when ApplicationHelper::STATUS_CRITICAL
        val = "red"
    end
    val
  end
  
  # Returns the corresponding status choices
  def status_choices()
    [ApplicationHelper::STATUS_CLEAN,
      ApplicationHelper::STATUS_LOW,
      ApplicationHelper::STATUS_MODERATE,
      ApplicationHelper::STATUS_HIGH,
      ApplicationHelper::STATUS_CRITICAL].map{ |x| [status_for(x), x]}.to_h
  end


  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end


  def color_for(number)
    return "#000" if number.nil?
    val = number
    r = ("0" + ([(1-val)*100,1].min * 255).to_s(16)).slice(-3,2)
    g = "00"
    b = "00"
    return "##{r}#{g}#{b}"
  end

  # Returns the corresponding status
  def condition_for(status)
    val = "Desconocido"
    case status
      when ApplicationHelper::USE_STATUS_UNDEFINED
        val = "Desconocido"
      when ApplicationHelper::USE_STATUS_USED
        val = "Usado"
      when ApplicationHelper::USE_STATUS_NEW
        val = "Nuevo"
    end
    val
  end
  
  # Returns the corresponding status choices
  def condition_choices
    [ApplicationHelper::USE_STATUS_UNDEFINED,
      ApplicationHelper::USE_STATUS_USED,
      ApplicationHelper::USE_STATUS_NEW].map{ |x| [condition_for(x), x]}.to_h
  end

  def search_choices
    [ApplicationHelper::SEARCH_CATEGORY,
    ApplicationHelper::SEARCH_BRAND].map{ |x| [search_for(x), x]}.to_h
  end

  def search_for(status)
    val = ""
    case status
    when ApplicationHelper::SEARCH_CATEGORY
      val = "Por categoría"
    when ApplicationHelper::SEARCH_BRAND
      val = "Por marca"
    end
    val
  end

  def activate_for(status)
    val = "Nombre"
    case status
    when ApplicationHelper::ACTIVATE_BY_NAME
      val = "Nombre"
    when ApplicationHelper::ACTIVATE_BY_DESCRIPTION
      val = "Descripción"
    when ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION
      val = "Nombre y Descripción"
    end
    val
  end

  def activation_choices
    [ApplicationHelper::ACTIVATE_BY_NAME,
      ApplicationHelper::ACTIVATE_BY_DESCRIPTION,
      ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION].map{ |x| [activate_for(x), x]}.to_h
  end

  # Check use status
  def guess_condition(text)
    status_used = /^\s*(usad[oa][s]?.*)|(.*usad[oa][s]?)\s*$/i
    status_new = /^\s*(nuev[oa][s]?.*)|(.*nuev[oa][s]?)\s*$/i
    return ApplicationHelper::USE_STATUS_NEW if not text.match(status_new).nil?
    return ApplicationHelper::USE_STATUS_USED if not text.match(status_used).nil?
    ApplicationHelper::USE_STATUS_UNDEFINED
  end

  def hsv2rgb(hsv)
    puts "input #{hsv.join(',')}"
    return "#000"
    h = hsv[:hue]
    s = hsv[:sat]
    v = hsv[:val]

    rgb, i, data = []
    if s == 0 then
      rgb = [v,v,v]
    else
      h = h / 60
      i = h.floor
      data = [v*(1-s), v*(1-s*(h-i)), v*(1-s*(1-(h-i)))]
      case i
        when 0
          rgb = [v, data[2], data[0]]
        when 1
          rgb = [data[1], v, data[0]]
        when 2
          rgb = [data[0], v, data[2]]
        when 3
          rgb = [data[0], data[1], v]
        when 4
          rgb = [data[2], data[0], v]
        else
          rgb = [v, data[0], data[1]]
      end
    end
    result = rgb.map do |x|
      ("0" + (x*255).round.to_s(16)).slice(-2)
    end
    '#' + result.join('')
  end

  def active_and_valid_line_chart(pubs)
    [
      {name: "todas", data: pubs.count_active_by_day},
      {name: "vigentes", data: pubs.count_valid_by_day}
    ]
  end

  def exec_stolen_score_calculator
    puts "Entrando a la función calculo de stolen score".red
    exec("bundle exec rails r lib/spider/stolen_score_calculator.rb") if fork == nil
    puts "Terminando calculo de stolen score".red      
  end

  def exec_categorizer
    Configuration.first.update_column(:run_cat, true)
    
    puts "Entrando a la función calculo de categorizador".red
    exec("bundle exec rails r lib/spider/new_categorizer.rb >>"+ "./log/#{DateTime.now.strftime('%Y%m%d')}_spider_cat_manual.log 2>&1") if fork == nil
    # Open3.capture3("bundle exec rails r lib/spider/categorizer.rb") do |stdin, stdout, stderr, wait_thr|
    #   while line = stdout.gets
    #     puts line
    #   end
    #   exit_status = wait_thr.value
    #   puts exit_status
    # end    
    puts "Terminando calculo de categorizador".red
  end

  def update_publisher_to_max_status(pub)
    max_pub_publisher = pub.publisher.publications.order('publications.status DESC').where('publications.status IS NOT NULL').first
    if max_pub_publisher.present?
      if pub.publisher.status != max_pub_publisher.status
        pub.publisher.status!(max_pub_publisher.status)
        person = pub.publisher.person
        if not person.nil?
          max_publisher_status = person.publishers.order('publishers.status DESC').where('publishers.status IS NOT NULL').first
          if person.status != max_publisher_status.status
            person.status!(max_publisher_status.status)
          end
        end
        flash[:warning] = "El estado del publicador fue actualizado al mayor de sus publicaciones"
      end
    end
  end

def publisher_code(publisher)
  publisher = Publisher.find(publisher)
  code = '['
  if publisher.fb_page1?
    code << 'FB'+publisher.fb_page1[-4..-1] 
  else
    code << 'FB0000'
  end
  if publisher.phone1?
    code << '-PH'+publisher.phone1[-4..-1]
  else
    code << '-PH0000'
  end
  if publisher.county_raw?
    code << '-'+publisher.county_raw 
  else
    code << '-SC'
  end
  code << ']'
  code
end
def copy_aliases(type,aliases)
  if type.aliases && type.aliases.strip.size > 0
    aliases += type.aliases.strip.split(',').map { |x| x.strip.downcase }
  end
  return aliases
end
def copy_excludes(type,excludes)
  if type.excludes && type.excludes.strip.size > 0
    excludes += type.excludes.strip.split(',').map { |x| x.strip.downcase }
  end
  return excludes
end

def check_product(publication_name, publication_description = "", product)
  has_product_aliases = false
  has_product_aliases_description = false
  has_product_excludes = false
  has_product_excludes_description = false
  has_brand_aliases = false
  has_brand_aliases_description = false
  has_brand_excludes = false
  has_brand_excludes_description = false
  has_category_aliases = false
  has_category_aliases_description = false
  has_category_excludes = false
  has_category_excludes_description = false
  if publication_description.empty?
    # puts "Publication without description".yellow
    if product[:filter_by_category] # tiene filtrado por Rubro
      product[:product_excludes_category].each do |x|
        if publication_name.include? x
          puts "By NAME excluded by category #{x}".red
          has_category_excludes = true
          return false
        end
      end
    end
    if product[:filter_by_brand] # tiene filtrado por Marca
      product[:product_excludes_brand].each do |x|
        if publication_name.include? x
          puts "By NAME Excluded by brand #{x}".red
          has_brand_excludes = true
          return false
        end
      end
    end
    product[:excludes].each do |x|
      if publication_name.include? x
        puts "By NAME Excluded by product #{x}".red
        has_product_excludes = true
        return false
      end
    end
  else
    # puts "Publication with description".yellow
    # revision de excludes por nombre y descripcion
    if product[:filter_by_category] # Tiene filtrado por Rubro
      product[:product_excludes_category].each do |x|
        if publication_name.include? x
          puts "by NAME excluded by category #{x}"
          has_category_excludes = true
          return false
        end
        if publication_description.include? x
          puts "by DESCRIPTION excluded by category #{x}"
          has_category_excludes_description = true
          return false
        end
      end
    end
    if product[:filter_by_brand] 
      product[:product_excludes_brand].each do |x|
        if publication_name.include? x
          puts "by NAME excluded by brand #{x}"
          has_brand_excludes = true
          return false
        end
        if publication_description.include? x
          puts "by DESCRIPTION excluded by brand #{x}"
          has_brand_excludes_description = true
          return false
        end
      end
    end
    product[:excludes].each do |x|
      if publication_name.include? x
        puts "By NAME Excluded by product #{x}".red
        has_product_excludes = true
        return false
      end
      if publication_description.include? x
        puts "By DESCRIPTION Excluded by product #{x}".red
        has_brand_excludes_description = true
        return false
      end
    end
  end

  # puts "PUBLICATION CLEAN WITHOUT ANY EXCLUDE in brand".red
  # Revision de aliases

  # Aliases del producto
  product[:aliases].each do |x|
    if publication_name.include? x
      has_product_aliases = true
      break
    end
    if publication_description.include? x
      has_product_aliases_description = true
      break
    end
  end
  if publication_description.empty?
    product[:product_aliases_brand].each do |x|
      if publication_name.include? x
        has_brand_aliases = true
        break
      end
    end
    product[:product_aliases_category].each do |x|
      if publication_name.include? x
        has_category_aliases = true
        break
      end
    end
    if product[:product_aliases_category].empty?
      # puts "Category aliases empty".red
      has_category_aliases = true
    end
    if product[:product_aliases_brand].empty?
      # puts "Brand aliases empty".red
      has_brand_aliases = true
    end
  
    if product[:filter_by_category] && product[:filter_by_brand]
      if has_category_aliases && has_brand_aliases && has_product_aliases
        return true 
      else
        return false
      end
    end
    if product[:filter_by_brand]
      if has_brand_aliases && has_product_aliases
        return true
      else
        return false
      end
    end
    if has_product_aliases
      return true
    else
      return false
    end
  else
    product[:product_aliases_brand].each do |x|
      if publication_name.include? x
        has_brand_aliases = true
        break
      end
      if publication_description.include? x
        has_brand_aliases_description = true
        break
      end
    end
    product[:product_aliases_category].each do |x|
      if publication_name.include? x
        has_category_aliases = true
        break
      end
      if publication_description.include? x
        has_category_aliases_description = true
        break
      end
    end

    if product[:product_aliases_category].empty?
      # puts "Category aliases empty".red
      has_category_aliases = true
      has_category_aliases_description = true
    end
    if product[:product_aliases_brand].empty?
      # puts "Brand aliases empty".red
      has_brand_aliases = true
      has_brand_aliases_description = true
    end

    if product[:filter_by_category] && product[:filter_by_brand]
      if (has_category_aliases || has_category_aliases_description) && (has_brand_aliases || has_brand_aliases_description) && (has_product_aliases || has_product_aliases_description)
        return true
      else
        return false
      end
    end
    if product[:filter_by_brand]
      if (has_brand_aliases || has_brand_aliases_description) && (has_product_aliases || has_product_aliases_description)
        return true
      else
        return false
      end
    end
    if (has_product_aliases || has_product_aliases_description)
      return true
    else
      return false
    end
  end
end

def set_category(category)
  brand_in_category = []
  category.brands.each do |brand|
    brand_aliases = []
    brand_excludes = []
    # puts "SOY UNA MARCA GENERICA #{brand.name}" if brand.generic == true
    # puts "NO SOY UNA MARCA GENERICA #{brand.name}" if brand.generic == false
    brand_aliases = copy_aliases(brand,brand_aliases) 
    brand_excludes = copy_excludes(brand,brand_excludes)
    if brand.filter_by_category
      brand_aliases = copy_aliases(category,brand_aliases) if brand.generic == true
      brand_excludes = copy_excludes(category,brand_excludes)
      if not category.search_engine.empty?
        category.search_engine.each do |s_e|
          if s_e.search_excludes && s_e.search_excludes.strip.size > 0
            brand_excludes += s_e.search_excludes.split(',').map { |x| x.strip.downcase }
          end
        end
      end
    end
    products_in_brand = []
    brand.products.each do |product|
      product_aliases = []
      product_excludes = []
      product_aliases_brand = []
      product_excludes_brand = []
      product_aliases_category = []
      product_excludes_category = []
      product_aliases = copy_aliases(product, product_aliases)
      product_excludes = copy_excludes(product, product_excludes)
      if not product.base_product.nil?
        product_aliases = copy_aliases(product.base_product, product_aliases) 
        product_excludes = copy_excludes(product.base_product, product_excludes)
      end
      if product.filter_by_brand
        product_aliases_brand = copy_aliases(product.brand, product_aliases_brand) if product.generic == true
        product_excludes_brand = copy_excludes(product.brand, product_excludes_brand)
        if product.brand.filter_by_category
          product_aliases_category = copy_aliases(category, product_aliases_category) if brand.generic == true
          product_excludes_category = copy_excludes(category, product_excludes_category)
          # puts product_excludes_category
          if not category.search_engine.empty?
            puts "has search_engine"
            category.search_engine.each do |s_e|
              if s_e.search_excludes && s_e.search_excludes.strip.size > 0
                product_excludes_category += s_e.search_excludes.split(',').map { |x| x.strip.downcase }
              end
            end
            # puts product_excludes_category
          end
        end
      end
      prod = {
        obj: product,
        aliases: product_aliases,
        excludes: product_excludes,
        product_aliases_brand: product_aliases_brand,
        product_excludes_brand: product_excludes_brand,
        product_aliases_category: product_aliases_category,
        product_excludes_category: product_excludes_category,
        filter_by_brand: product.filter_by_brand,
        filter_by_category: product.brand.filter_by_category
      }
      products_in_brand << prod
    end
    brand = {
      obj: brand,
      aliases: brand_aliases,
      excludes: brand_excludes,
      products: products_in_brand
    }
    brand_in_category << brand
  end
  brand_in_category
end

def check_with_brand(publication_name, publication_description, brand_in_category, category)
  pub_products = []
  pub_brands = []
  brand_in_category.each do |brand|
    puts "Brand #{brand[:obj].name} Aliases empty".red if brand[:aliases].empty?
    has_brand_excludes = false
    has_brand_excludes_description = false
    has_brand_alias = false
    has_brand_alias_description = false
    brand[:excludes].each do |x|
      if publication_name.include? x
        puts "In brand #{brand[:obj].name} product exclude in NAME #{x}"
        has_brand_excludes = true
        break
      end
      if publication_description.include? x
        puts "In brand #{brand[:obj].name} product exclude in DESCRIPTION #{x}"
        has_brand_excludes_description = true
        break
      end
    end
    if category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION
      # puts "Filter by description"
      if has_brand_excludes_description || has_brand_excludes
        puts "HAS EXCLUDES IN NAME OR DESCRIPTION".red
        next
      end
    else
      # puts "Filter by name"
      if has_brand_excludes
        puts "HAS EXCLUDES IN NAME".red
        next
      end
    end
    brand[:aliases].each do |x|
      if publication_name.include? x
        has_brand_alias = true
        break
      end
      if publication_description.include? x
        has_brand_alias_description = true
        break
      end
    end
    if category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION
      if has_brand_alias_description || has_brand_alias
        pub_brands.push(brand[:obj]) unless pub_brands.include?(brand[:obj])
        brand[:products].each do |prod|
          if check_product(publication_name, publication_description, prod)
            pub_products << prod[:obj] 
          end
        end
      end
    else
      if has_brand_alias
        pub_brands.push(brand[:obj]) unless pub_brands.include?(brand[:obj])
        brand[:products].each do |prod|
          if check_product(publication_name, "", prod)
            pub_products << prod[:obj]
          end
        end
      end
    end
  end
  { pub_products: pub_products, pub_brands: pub_brands }
end

def set_products(publication, pub_products, pub_brands, category)
  has_products = false
  if pub_products.empty? && (not pub_brands.empty?)
    publication.products.delete_all
    pub_brands.each do |brand|
      # product = Product.find_or_initialize_by(name: brand.name + ' Otros')
      product = Product.find_or_initialize_by(name: brand.name.chomp('Otros').strip + ' Otros')
      brand.products << product rescue ActiveRecord::RecordNotUnique
      product.filter_by_brand = true
      product.generic = true
      product.save
      pub_products << product
    end
    if pub_products.size > 1
      generic_product = category.name.chomp('Otros').strip + ' Otros'
      pub_products.delete_if { |x| x.name == generic_product }
    end
    publication.products << pub_products
    has_products = true
  else
    if pub_products.size > 1
      generic_product = category.name.chomp('Otros').strip + ' Otros'
      pub_products.delete_if { |x| x.name == generic_product }
    end
    publication.products.delete_all
    publication.products << pub_products
    has_products = true
  end
  has_products
end

end
