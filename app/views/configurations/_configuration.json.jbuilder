json.extract! configuration, :id, :factor_a, :factor_e, :factor_f, :factor_g, :created_at, :updated_at
json.url configuration_url(configuration, format: :json)