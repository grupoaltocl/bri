json.extract! search_engine, :id, :name, :search_keywords, :search_excludes, :created_at, :updated_at
json.url search_engine_url(search_engine, format: :json)