json.extract! publication_snapshot, :id, :created_at, :updated_at
json.url publication_snapshot_url(publication_snapshot, format: :json)