json.extract! case_file, :id, :file, :description, :created_at, :updated_at
json.url case_file_url(case_file, format: :json)