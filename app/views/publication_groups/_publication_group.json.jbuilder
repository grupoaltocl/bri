json.extract! publication_group, :id, :name, :description, :created_at, :updated_at
json.url publication_group_url(publication_group, format: :json)