json.extract! person, :id, :uid, :name, :first_name, :last_name, :email1, :phone1, :note, :suitcase_id, :watch, :status, :created_at, :updated_at
json.url person_url(person, format: :json)