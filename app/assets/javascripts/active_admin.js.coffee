//= require jquery
//= require jquery_ujs
//= require best_in_place
//= require active_admin/base
//= require jquery-ui
//= require best_in_place.jquery-ui
//= require active_admin_scoped_collection_actions


#= require active_admin/base
#= require activeadmin_addons/all
#= require chartkick
$(document).ready ->
  ### Activating Best In Place ###
  
  jQuery('.best_in_place').best_in_place()
  
  $('.index_table tr').change ->
    a = 0
    $('.index_table tr').each ->
      if $(this).hasClass('selected')
        a += 1
      return
    $('.dropdown_menu_button').text 'Acciones en masa (' + a + ')'
    $('.dropdown_menu_list').children().each ->
      text = $(this).text()
      text = text.replace(/\(([^\)]+)\)/g, '')
      $(this).find('a').text text + '(' + a + ')'
      return
    return

  setTimeout (->
    $.get location.origin+'/admin/parameters/1.json', (value) ->
      if value['run_cat']
        console.log 'ESTOY CORRIENDO'
        $('.categorizer').css('background', 'red');
      else
        console.log 'NO ESTOY CORRIENDO'
        $('.categorizer').css('background', 'green');
      return
    return
  ), 3000

  setTimeout (->
    $('.filter_form').submit (event) ->
      # console.log 'something'
      event.preventDefault()
      $('input[name$=\'_blank]\']').each (i, el) ->
        if el.value == 'f' or el.value == 'false' or el.text == 'f' or el.text == 'false'
          console.log 'Seteando a falso'
          el.value = 'false'
          el.text = 'false'
        else
          console.log 'Seteando a true'
          el.value = 'true'
          el.text = 'true'
        return
      $('.filter_form').unbind('submit').submit()
    return
  ), 3000

  status_color = (status) ->
    val = 'green'
    switch status
      when '0'
        return 'gray'
      when '1'
        return 'green'
      when '2'
        return 'yellow'
      when '3'
        return 'orange'
      when '4'
        return 'red'
      else
        return 'green'
    return

  setTimeout (->
    $('.pub_status_on_success').bind 'ajax:success', (status) ->
      $(this).parent().parent().children('td.col.col-reviewed_at').children().children().removeClass("no")
      $(this).parent().parent().children('td.col.col-reviewed_at').children().children().addClass("active_green")
      $(this).parent().parent().children('td.col.col-reviewed_at').children().children().text("Sí")
      [
        'green'
        'yellow'
        'blue'
        'orange'
        'red'
      ].forEach ((x, y) ->
        $(this).removeClass x
        return
      ).bind(this)
      $(this).addClass status_color($(this).data().bipValue)
      return
    return

    $('#callbacks').multiSelect

    $('#callbacks').multiSelect
    afterSelect: (values) ->
        console.log values
      return
    afterDeselect: (values) ->
        console.log values
      return  


    $('.conf_customer_on_success').bind 'ajax:success', (status) ->
      console.log "hola"
      # $(this).parent().parent().children('td.col.col-cliente').children().children().removeClass("no")
      # $(this).parent().parent().children('td.col.col-cliente').children().children().addClass("active_green")
      # $(this).parent().parent().children('td.col.col-cliente').children().children().text("Sí")
    return
    
  ), 3000

  setTimeout (->
    $('.pub_group_status_on_success').bind 'ajax:success', (status) ->
      $(this).parent().parent().children('td.col.col-reviewed_at').children().children().removeClass("no")
      $(this).parent().parent().children('td.col.col-reviewed_at').children().children().addClass("active_green")
      $(this).parent().parent().children('td.col.col-reviewed_at').children().children().text("Sí")
      [
        'green'
        'yellow'
        'blue'
        'orange'
        'red'
      ].forEach ((x, y) ->
        $(this).removeClass x
        return
      ).bind(this)
      $(this).addClass status_color($(this).data().bipValue)
      return
    return
  ), 3000

return


# ---
