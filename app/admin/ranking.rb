include ApplicationHelper
ActiveAdmin.register_page "Ranking" do

  menu priority: 0, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    # render partial: 'dashboard'
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     # small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # def status_tag(status)
    #   case status
    #     when 0
    #       return 'green'
    #     when 1
    #       return 'yellow'
    #     when 3
    #       return 'blue'
    #     when 4
    #       return 'magenta'
    #     when 5
    #       return 'red'
    #     else
    #       return 'green'
    #   return      
    # end
    columns do
      options_bool = {true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
      topN = 100
      column do
        panel "Ranking de Usuario por cantidad publicaciones" do
          table do
            tr do
              th { "Top" }
              th { "Publicador/Persona" }
              th { "fuente" }
              th { "FB" }
              th { "Score" }
              th { "Riesgo" }
              th { "cantidad" }
              th { "no revisadas" }
              th { "todos" }
            end
            publisher_pubs = client_scoped(Publication.active.valid)
                              .select('publications.*, count(DISTINCT publications.id) as q, publisher_id')
                              .joins(:publisher).joins('LEFT JOIN people ON publishers.person_id = people.id')
                              .where('people.id IS NULL')
                              .group('publisher_id')
                              .order('q desc')
                              .limit(topN)
            publishers_ids = []
            publisher_pubs.each do |pub|
              publishers_ids << pub.publisher_id
            end
            publisher_pubs_not_reviewed = client_scoped(Publication.active.valid)
                                          .where('publications.publisher_id IN (?)', publishers_ids)
                                          .where('publications.reviewed_at IS NULL')
                                          .group('publications.publisher_id')
                                          .count
            publisher_all_pubs = client_scoped(Publication.active)
                                          .where('publications.publisher_id IN (?)', publishers_ids)
                                          .group('publications.publisher_id')
                                          .count

            people_pubs = client_scoped(Publication.active.valid)
                            .select('publications.*, count(DISTINCT publications.id) as q')
                            .joins(:publisher)
                            .joins('INNER JOIN people ON publishers.person_id = people.id')
                            .group('people.id')
                            .order('q desc')
                            .limit(topN)
            people_ids = []
            people_pubs.each do |pub|
              people_ids << pub.publisher.person
            end
            people_pubs_not_reviewed = client_scoped(Publication.active.valid)
                                        .joins(:publisher)
                                        .joins('INNER JOIN people ON publishers.person_id = people.id')
                                        .where('publications.reviewed_at IS NULL')
                                        .where('people.id in (?)', people_ids)
                                        .group('people.id')
                                        .count
            people_all_pubs = client_scoped(Publication.active)
                                        .joins(:publisher)
                                        .joins('INNER JOIN people ON publishers.person_id = people.id')
                                        .where('people.id in (?)', people_ids)
                                        .group('people.id')
                                        .count
            merge_pubs = publisher_pubs + people_pubs
            merge_pubs.sort_by(&:q).reverse[0..99].each_with_index.map do |rec, idx|

              if rec.nil? || rec.publisher.nil?
                next
              end
              
              if rec.publisher.person.nil? # Publisher
                publisher = rec.publisher
                tr do
                  # puts rec.id
                  td { idx+1 }
                  td { link_to("#{publisher.to_s.truncate(30)}", admin_publisher_path(publisher)) }
                  td { link_to("#{publisher.source.code}", admin_source_path(publisher.source)) }
                  td { publisher.fb_page1.present? ? link_to(options_bool[:true], publisher.fb_page1) : options_bool[:false] }
                  td { publisher.stolen_score.to_f.round(2)}
                  td { status_tag(status_for(publisher.status), status_color(publisher.status))}
                  td { rec.q.to_i }
                  td { publisher_pubs_not_reviewed[publisher.id].to_i }
                  td { publisher_all_pubs[publisher.id].to_i }
                  # td { client_scoped(rec.publisher.publications.active.valid).where('publications.reviewed_at IS NULL').count }
                end
              else # Person
                person = rec.publisher.person
                tr do
                  # puts rec.id
                  td { idx+1 }
                  td { link_to("#{person.to_s.truncate(30)}", admin_person_path(person)) }
                  td ""
                  td { person.fb_page1.present? ? link_to(options_bool[:true], person.fb_page1) : options_bool[:false] }
                  td { person.stolen_score.to_f.round(2) }
                  td { status_tag(status_for(person.status), status_color(person.status))}
                  td { rec.q.to_i }
                  td { people_pubs_not_reviewed[person.id].to_i }
                  td { people_all_pubs[person.id].to_i }
                  # td { people_pubs_not_reviewed[person.id] }
                  # td { client_scoped(Publication.in_person(person).active.valid).where('publications.reviewed_at IS NULL').count }
                end
              end
            end
          end
        end
      end

      column do
        panel "Últimas publicaciones (2 semanas) de publicadores riesgosos" do
          table do
            tr do
              th { "publicación" }
              th { "publicador" }
              th { "riesgo publicador" }
              th { "fecha" }
              th { "Multi" }
              th { "Precio" }
              th { "Score" }
              th { "Riesgo" }
              th { "Link" }
            end
            client_scoped(Publication.active).joins(:publisher)
                        .where("published_at > ?", Time.current - 2.weeks)
                        .order("publishers.status desc")
                        .order("stolen_score desc")
                        .order("published_at desc")
                        .uniq
                        .first(topN).map do |record|
              tr do
                td { link_to(record.name.truncate(20), admin_publication_path(record)) }
                td { link_to(record.publisher.to_s.truncate(20), admin_publisher_path(record.publisher)) }
                td { status_tag(status_for(record.publisher.status), status_color(record.publisher.status))}
                td record.published_at
                td { record.multi_prod.present? ? options_bool[:true] : options_bool[:false] }
                td number_to_currency record.price, locale: :es, separator: ",", delimiter: ".", precision: 0
                td record.stolen_score.to_f.round(2)
                td { status_tag(status_for(record.status), status_color(record.status))}
                td link_to t('app.goto'), record.url
              end
            end
          end
        end
      end

    end
    controller do
      def scoped_collection
        publications = Publication.all
        publications = client_scoped(publications)
      end
    end  
  end # content
end
