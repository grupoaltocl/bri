ActiveAdmin.register Customer do
  menu parent: 'Configuración', priority: 8, label: proc{ I18n.t("active_admin.customers") }

  permit_params :name, :description, :code, brand_ids: [], product_ids: []

  filter :name
  filter :code
  filter :brands, multiple: true, collection: -> { Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }
	filter :description
	filter :id

	member_action :toggle_active, method: :put do
		if session[:customer_ids].nil? 
		  session[:customer_ids] = [params[:id].to_i]
		else
			if session[:customer_ids].include?(params[:id].to_i)
			  session[:customer_ids].delete(params[:id].to_i)
			else
			  session[:customer_ids] << params[:id].to_i
			end
		end
		puts "#{session[:customer_ids]}".red
		redirect_to edit_admin_parameter_path(Configuration.first)
  end  

  index do
	
	selectable_column
	# id_column
	
	column :name do |record|
	  	link_to "#{record.name}", admin_customer_path(record)
	end

	column :code
	# column :brands do |record|
	# 	# puts "#{record.brands.name}".red
	# 	record.brands.each do |brand|
	# 		# puts "#{brand.name}".red
	# 		link_to "#{brand.name}", admin_brand_path(brand)
	# 	end
	  	
	# end
	# column :created_at
	column :updated_at
	actions
  end


  show do
		attributes_table do
		  row :id
		  row :name
		  row :code
		  row :description

		  row :created_at
		  row :updated_at
		end
		panel t("app.brand.other") do
		  table_for customer.brands.order(name: :asc) do
				column :brands do |brand|
				  link_to brand.name, admin_brand_path(brand)
				end
		  end
		end
		
		panel t("app.product.other") do
		  table_for customer.products.order(name: :asc) do
				column :products do |product|
				  link_to product, admin_product_path(product)
				end
		  end
		end


  end

  form do |f|
		f.semantic_errors # shows errors on :base

		f.inputs I18n.t("app.detail.other") do
		  f.input :name
		  f.input :code
			f.input :brands
			product = Product.all
			if session[:customer_ids].present?
				product = Product.in_customers(session[:customer_ids])
			end
		  f.input :products, :as => :select, multi: true, :collection => product.order(:name).map { |x| [x.to_s, x.id] }
		  f.input :description
		end
		f.actions
  end

end
