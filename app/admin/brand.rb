include ApplicationHelper
ActiveAdmin.register Brand do
	# config.create_another = false
  menu parent: 'Configuración', priority: 5, label: proc{ t("active_admin.brands") }
  permit_params :name, :aliases, :excludes, :aliases, :excludes, :filter_by_category, customer_ids: [], search_engine_attributes:[:name,:search_keywords,:search_excludes,:id,:parent_type,:keywords_ml,:keywords_yp,:keywords_mp,:active,:_destroy]
  filter :customers
	filter :products, as: :select, collection: -> { Product.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }
  filter :created_at
	filter :updated_at
	filter :id
	member_action :filter_by_category, method: :put do
    s_e = Brand.find(params[:id])
    if s_e.filter_by_category == false
      s_e.update_columns(filter_by_category: true)
    else
      s_e.update_columns(filter_by_category: false)
    end
    return '{ "status": "OK"}'
  end
  index do

	selectable_column
	column :name do |record|
	  link_to record.name, admin_brand_path(record)
	end
	column :aliases
	column :filter_by_category do |resource|
    options = {true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
    place_holder = resource.filter_by_category? ? options[:true] : options[:false]
    best_in_place resource, :filter_by_category, place_holder: place_holder, as: :checkbox, collection: options, url:  filter_by_category_admin_brand_path(resource)
  end
	column :customers do |record|
		x = []
	  record.customers.each do |customer|
		x << customer.code
	  end
	  x.join(", ").html_safe
	end
	column :search_engine do |resource|
		
		options = {true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
		resource.search_engine.nil? ? options[:false] : options[:true]
		
	end
	column t("active_admin.category") do |resource|
		Category.find_by_id(resource.category_id)
	end
	# column :created_at
	column :updated_at
	actions
  end

  show do
		# active_publications = brand.publications.active
		# inactive_publications = brand.publications.inactive
		attributes_table do
			row :id
			row "Nombre (No se incluye en la categorización)" do |resource|
				resource.name
			end
			row :aliases
			row :excludes
			row :search_engine
			row :filter_by_category
			# row "Criterio de búsqueda" do |record|
			# 	x = []        
			# 	record.search_engine.each do |prod|
			# 	  x << link_to("#{prod.to_s}", admin_search_engine_path(prod))
			# 	end
			# 	x.join(",").html_safe
			# end
			row :created_at
			row :updated_at
			row :category do |resource|
				Category.joins(:brands).where(brands: { id: resource }).first
			end
			
		end
		
		# div.panel t('app.stats.other') do
		#   attributes_table_for brand do # FIXME: hack
		# 			row :ratio_activation do |record|
		# 				inactive_count = inactive_publications.count
		# 				if not inactive_count.zero?
		# 					active_publications.count/inactive_count
		# 				else
		# 					0
		# 				end
		# 			end
		# 			row :active_valid do |record|
		# 				active_publications.count
		# 			end

		# 			row "Monto total" do |record|
		# 				number_to_currency active_publications.sum(:price), locale: :es, separator: ",", delimiter: ".", precision: 0
		# 			end

		# 			row "Precio promedio" do |record|
		# 				number_to_currency active_publications.average(:price), locale: :es, separator: ",", delimiter: ".", precision: 0
		# 			end

		#   end                                                                  
		# end  


		panel t("app.customer.other") do
			table_for brand.customers.order(name: :asc) do
				column :name do |customer|
				link_to customer.name, admin_customer_path(customer)
				end
			end
		end
		
		panel t("app.product.other") do
			table_for brand.products.order(name: :asc) do
				column :name do |product|
					link_to product, admin_product_path(product)
				end
				column :price do |product|
					number_to_currency product.price, locale: :es, separator: ",", delimiter: ".", precision: 0
				end
				column "Precio promedio" do |record|
					number_to_currency record.publications.average(:price), locale: :es, separator: ",", delimiter: ".", precision: 0
				end
				column :updated_at
				# column "records count" do |product|
				#   product.publications.count
				# end
			end
		end

		# panel "Indicadores" do
		# 	render partial: "dashboard", locals: {pubs: brand.publications}
		# end
		pubs = Publication.joins(products: :brand).where(brands: {id: resource.id})
		render partial: "publication_common", locals: {pubs: pubs, resource: resource, params_filter: {products_brand_id_in: resource.id}}			
	end

	form do |f|
		f.semantic_errors # shows errors on :base

		f.inputs t("app.detail.other") do
		  f.input :customers, as: :select, :collection => Customer.all.order(:name) 
		  f.input :name, :label => "Nombre (No se incluye en la categorización)"
		  f.input :aliases
			f.input :excludes
			f.input :filter_by_category
			f.inputs do
				if brand.search_engine.nil?
					1.times do
						f.object.build_search_engine
					end
				end
				f.has_many :search_engine, 
										new_record: false, 
										allow_destroy: true,
										heading: 'Criterios de búsqueda' do |t|
					t.input :name, :label => "Nombre (No se incluye en búsqueda)"
					t.input :search_keywords
					t.input :search_excludes
					t.input :keywords_ml
					t.input :keywords_yp
					t.input :keywords_mp
					t.input :active
					t.input :parent_type, :as => :hidden,:input_html => { :value => ApplicationHelper::USE_TYPE_BRAND }
				end
			end
		end

		f.actions
	end

	controller do 
		alias_method :delete_brand, :destroy
    def scoped_collection
      brands = Brand.all
      if session[:customer_ids].present?
        return brands.in_customers(session[:customer_ids])
				# return brands.customer.where('customers.id IN (?)', session[:customer_ids])
				
      end
      brands
		end
		def destroy
			category = Category.joins(:brands).where(brands: {id: resource.id}).first
			if category.nil?
				redirect_to collection_path, alert: t("app.alert.delete_brand")
			else
				brand = Brand.find_or_initialize_by(name: category.name.chomp('Otros').strip + ' Otros')
				resource.products.each do |prod|
					brand.products << prod rescue ActiveRecord::RecordNotUnique
				end
				brand.save
				category.brands << brand
				delete_brand
			end
		end
		def action_methods
			if session[:customer_ids].present?
				super - ['new'] 
			else
				super
			end
		end  		
  end

end
