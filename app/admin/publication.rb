include ApplicationHelper

ActiveAdmin.register Publication do
	
  scope proc { I18n.t('active_admin.active_valid') }, :default_order, default: true
  scope proc { I18n.t('active_admin.active_expired') }, :active_expired
  scope I18n.t("app.all"), :active_list

  menu priority: 2, label: proc{ t('active_admin.publications') }, 
                    :url    =>  proc{ admin_publications_path(scope: "default_order") }

  config.sort_order = ''
  config.batch_actions = true

  

  before_save do |pub|
    if pub.multi_prod_changed? || pub.price_changed? || pub.products_changed? || pub.active_changed?
        pub.manual_match = true
    end
    if pub.status_changed?
        pub.review!
        update_publisher_to_max_status(pub)
    end
    if pub.condition_changed?
      if pub.condition_change[1] == ApplicationHelper::USE_STATUS_USED
        pub.active = false
      end
    end

    if pub.publication_group_id_changed?
      pg = pub.publication_group
      if pg.present? && pg.publications.active.count != 0
        flash[:warning] = "El grupo ya tiene una publicación activa y sólo puede tener una. Publicación desactivada!"
        pub.update_columns(active: false)
        # pub.active = false
      end
    end
    
  end

  collection_action :launch_score_calculator, method: :get do
    # stdout, stderr, status, thread = Open3.capture3("bundle exec rails r lib/spider/stolen_score_calculator.rb")
    # puts "STDOUT #{stdout}"
    puts "Iniciando cálculo del score".red
    exec_stolen_score_calculator
    puts "redireccionando calculo del score".red
    redirect_to admin_launchers_path, notice: t('app.launch_stolen_score_calculator')
  end

  collection_action :launch_categorizer, method: :get do
    exec_categorizer
    redirect_to admin_launchers_path, notice: t('app.launch_categorizer')
  end

  # i18n_scope and header style are set via options
  # xls(:i18n_scope => [:activerecord ,:attributes, :publication],:header_style => {:weight => :bold, :color => :blue }) do 
 xls(:header_style => {:weight => :bold, :color => :blue }) do 
    whitelist
    column :id
    column :name
    column :products do |record|
      x = []        
      record.products.in_customers(session[:customer_ids]).each do |prod|
        x << prod.name
      end
      x.join(",").html_safe
    end    
  #   column('Marca Scraper') do |record|
  #    x = []
  #    record.brands.in_customers(session[:customer_ids]).each do |b|
  #      x << b.name
  #    end
  #    x.join(",").html_safe
  #  end 
    column('Marca Producto') do |record|
      x = []
      record.products.in_customers(session[:customer_ids]).each do |prod|
        x << prod.brand.name
      end
      x.join(",").html_safe
    end 
    column ('Producto marca') do |record|
      x = []
      record.products.in_customers(session[:customer_ids]).each do |prod|
        x << prod.to_s
      end
      x.join(",").html_safe
    end
    column :condition do |record|
      condition_for(record.condition)
    end
    column :price do |record|
       record.price.to_i
    end
    column :price2 do |record|
       record.price2.to_i
    end
    column :stolen_score
    column :status
    column "Riesgo" do |record|
      status_for(record.status)
    end
    column "Revisado" do |record|
      record.reviewed?
    end
    column "Fecha revisión" do |record|
      "#{record.reviewed_at}"
    end
    column :multi_prod do |record|
      if record.multi_prod
        true
      else
        false
      end
    end
    column :manual_match
    column "Comuna" do |record|
      record.county_raw
    end
    column "Región" do |record|
      record.state_raw
    end
    column "Ciudad" do |record|
      record.city_raw
    end
    column :published_at
    column "Día publicación" do |record|
      record.published_at.day if !record.published_at.nil?
    end
    column "Día semana publicación" do |record|
      record.published_at.wday if !record.published_at.nil?
    end
    column "Mes publicación" do |record|
      record.published_at.month if !record.published_at.nil?
    end
    column "Año publicación" do |record|
      record.published_at.year if !record.published_at.nil?
    end
    column "Vigente" do |record|
      sys_scraped_at = Configuration.first.sys_scraped_at
      if not record.last_seen_at.nil?
        if record.last_seen_at >= sys_scraped_at 
          true
        else
          false
        end
      else
        false
      end
    end
    column "Ultima vez visto" do |record|
      record.last_seen_at
    end
    column I18n.t("app.aprox_valid_time") do |record|
      if record.published_at.present? && record.last_seen_at.present?
        days = ((record.last_seen_at - record.published_at).to_f / 1.day).floor
        "#{days} día(s)"
      end
    end
    column :description
    column "ID Grupo" do |resource|
      group_id = ""
      group_id = "#{resource.publication_group.id}" if not resource.publication_group.nil?
      group_id      
    end
    column(:publication_group) do |resource|
      group_name = ""
      group_name = "#{resource.publication_group.name}" if not resource.publication_group.nil?
      group_name
    end
    column "ID Publicador" do |resource|
      "#{resource.publisher.id}"
    end
    column "Publicador" do |resource|
      publisher_name = t('app.unknown')
      publisher_name = "#{resource.publisher.name}" if not resource.publisher.nil?
      publisher_name
    end
    column("Valor riesgo publicador") do |resource|
      publisher_status = t('app.unknown')
      publisher_status = "#{resource.publisher.status}" if not resource.publisher.status.nil?
      publisher_status
    end    
    column("Riesgo publicador") do |resource|
      publisher_status = t('app.unknown')
      publisher_status = "#{status_for(resource.publisher.status)}" if not resource.publisher.status.nil?
      publisher_status
    end    
    column("Fuente publicador") do |resource|
      publisher_source = t('app.unknown')
      publisher_source = "#{resource.publisher.source.name}" if not resource.publisher.source.nil?
      publisher_source
    end
    column :url
    column "Snap" do |resource|
      snap = resource.publication_snapshots.first
      snap = "#{snap.snapshot.url}" if (snap.present? && snap.snapshot.present? && snap.snapshot.url.present?)
      snap
    end
    column :created_at
    column :updated_at
    column "Id caso" do |record|
      x = []        
      record.suitcases.each do |suitcase|
        x << suitcase.id
      end
      x.join("").html_safe      
      # suitcase_ids = "#{resource.suitcases.id}" if not resource.suitcases.present?
      # suitcase_ids
    end
    column "Caso" do |record|
      x = []        
      record.suitcases.each do |suitcase|
        x << suitcase.name
      end
      x.join("").html_safe        
      # suitcase_name = "#{resource.suitcases.name}" if not resource.suitcases.present?
      # suitcase_name
    end

#    after_filter { |sheet|
#      # todo
#    }
#
#    before_filter do |sheet|
#      # todo
#    end
 end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params(
    :name, :publisher_id, :published_at, :active,
    :description, :status, :publication_group_id,
    :stolen_score, :condition, :publication_snapshot,
    :multi_prod, :manual_match, :reviewed_at, :destroy,
    publication_snapshots_attributes: [:id, :name, :price, :_destroy],
    product_ids: [], suitcase_ids: []
    )
  member_action :toggle, method: [:get, :put] do
    if request.get?
      publication = Publication.find(params[:id])
      if publication.reviewed_at != nil
        publication.unreview!
        redirect_to admin_publication_path, notice: t('app.reviewed')
      else
        publication.review!
        redirect_to admin_publication_path, notice: t('app.unreviewed')
      end
    elsif request.put?
      publication = Publication.find(params[:id])
      publication.set_status(params[:publication]["status"])
      update_publisher_to_max_status(publication)
      publication.review!
      return '{ "status": "OK"}'
    end
  end
  
  member_action :set_multi, method: :put do
    publication = Publication.find(params[:id])
    if publication.multi_prod == false
      publication.multi!
      publication.manual!
    else
      publication.unmulti!
    end
    return '{ "status": "OK"}'
  end

  member_action :toggle_reviewed, method: :put do
    publication = Publication.find(params[:id])
    if publication.reviewed_at != nil
      publication.unreview!
    return '{ "status": "OK"}'
    # redirect_to admin_publication_path, notice: t('app.reviewed')
    else
      publication.review!
    return '{ "status": "OK"}'
    # redirect_to admin_publication_path, notice: t('app.unreviewed')
    end
  end

  action_item :action_review, only: [:show, :edit] do
    publication = Publication.find(params[:id])
    if publication.reviewed_at != nil
      link_to t('app.unmark_reviewed'), toggle_admin_publication_path, class: :reviewed
    else 
      link_to t('app.mark_reviewed'), toggle_admin_publication_path, class: :not_reviewed
    end
  end
  action_item :action_recategorize, only: [:show, :edit] do
    link_to "Recategorizar", pub_recategorize_admin_publication_path, class: :not_reviewed
  end

  member_action :pub_recategorize, method: :get do 
    publication = Publication.find(params[:id])
    publication.recategorize
    redirect_to admin_publication_path, notice: "Publicación recategorizada, revisar las notificaciones para ver los cambios"
  end

  member_action :action_inactive, method: :get do
    publication = Publication.find(params[:id])
    publication.deactivate!
    publication.manual!
    
    redirect_to :back, 
    notice: t('app.notice_publication_inactivated',
              pub: "<a href=\"#{admin_publication_inactive_path(publication)}\">#{publication.id}</a>",
              undo: "<a href=\"#{action_active_admin_publication_path(publication)}\">Deshacer</a>"
            )
  end
  
  member_action :action_active, method: :get do
    publication = Publication.find(params[:id])
    publication.activate!
    publication.manual!
    redirect_to :back, 
    notice: t('app.notice_publication_activated',
              pub: "<a href=\"#{admin_publication_path(publication)}\">#{publication.id}</a>",
              undo: "<a href=\"#{action_inactive_admin_publication_path(publication)}\">Deshacer</a>"
            )
  end
  member_action :toggle_active, method: :put do
    publication = Publication.find(params[:id])
    if publication.active
      publication.update_column(:active, false)
      return '{ "status": "OK"}'
    else
      publication.update_column(:active, true)
      return '{ "status": "OK"}'
      # redirect_to admin_publication_path, notice: t('app.unreviewed')
    end
  end

  filter :name_cont, label: "Contiene nombre" # filters: [:not_start,  :not_end, :not_cont]
  filter :name_not_cont, label: "No contiene nombre"
  filter :description_cont, label: "Contiene descripción"
  filter :description_not_cont, label: "No contiene descripción"
  filter :publisher_name, as: :string, label: I18n.t('app.publisher_name')
  filter :publisher_status, as: :select, collection: -> { status_choices }
  filter :published_at
  filter :publication_valid_in, as: :boolean, :label => "Vigente",collection: -> { [["Si", 1],["No", 0]] }
  filter :multi_prod
  filter :price
  filter :price2
  filter :stolen_score
  filter :status, :as => :select, multiple: true, collection: -> { status_choices }
  filter :reviewed_at_null, :as => :boolean, label: "Revisado"
  # filter :brands_customers_id, as: :select,  multiple: true, collection: -> { Customer.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }, label: I18n.t('app.customer.other')
  # filter :brands, as: :select,  multiple: true, label: I18n.t('app.brand.filter_brand'), collection: -> { 
  #     Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }
  #   }
  filter :products_brand_id, as: :select,  multiple: true, label: I18n.t('app.brand.filter_product'), collection: -> { 
    Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }
  }
  filter :products_base_product_id, as: :select, multiple: true, collection: -> { BaseProduct.client_scoped(session[:customer_ids]).order(:name) }
  filter :products_id, as: :select, multiple: true, collection: -> { Product.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }, label: I18n.t('app.product.other')
  filter :publisher_person_id, as: :select,  multiple: true, collection: -> { Person.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }, label: I18n.t('app.person.other')
  filter :publication_group, :as => :select, multiple: true
  filter :suitcases, as: :select, multiple: true, collection: -> { Suitcase.client_scoped(session[:customer_ids]).order(:name) }
  filter :county_raw
  filter :state_raw
  filter :city_raw
  filter :created_at
  filter :updated_at
  filter :manual_match
  filter :publisher_id
  filter :publisher_source_id, as: :select, multiple: true, collection: -> { Source.all.order(:name) }, label: I18n.t('app.source.other')
  filter :id


  index do

    selectable_column
    column :name do |record|
      span :class => "name" do
        link_to "#{truncate record.name, :length   => 30, :separator => /\w/, :omission => "..."}", admin_publication_path(record)
      end
    end
    column :publication_group do |record|
      if record.publication_group
        publication_group = record.publication_group
      end
      if not publication_group.nil?
        link_to '<span class="status_tag yes">Sí</span>'.html_safe, admin_publication_group_path(publication_group)
      else
        '<span class="status_tag no">No</span>'.html_safe
      end
    end   
    column :publisher do |record|
      if record.publisher.display_name != ""
        span link_to "#{record.publisher.display_name}", admin_publisher_path(record.publisher)
      else
        span link_to "#{record.publisher.to_s}", admin_publisher_path(record.publisher)
      end
    end
    column :publisher_status do |record|
      if record.publisher and record.publisher.status
        publisher_status = record.publisher.status
      end
      if not publisher_status.nil?
        status_tag(status_for(publisher_status), status_color(publisher_status))
      end
    end
    column :published_at
    column :valid do |res|
      sys_scraped_at = Configuration.first.sys_scraped_at
      options = {true: '<span class="status_tag sí active_green">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}

      is_valid = false
      if res.last_seen_at.present? && sys_scraped_at.present?
        is_valid = res.last_seen_at > sys_scraped_at
      end
      is_valid ? options[:true] : options[:false]
    end
    column t("app.product_brand.one") do |record|
      x = []
      record.products.each do |prod|
        # if prod.generic == false
        x << link_to("#{prod.to_s}", admin_product_path(prod))
        # end
      end
      x.join("").html_safe
      if x.count > 1
        "#{x[0]} (+#{x.count - 1})".html_safe
      else
        x[0]
      end
    end
    column :multi_prod do |resource|
      options = {true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
      place_holder = resource.multi? ? options[:true] : options[:false]
      best_in_place resource, :multi_prod, place_holder: place_holder, as: :checkbox, collection: options, url: set_multi_admin_publication_path(resource)
    end
    column :price do |record|
      number_to_currency record.price, locale: :es, separator: ",", delimiter: ".", precision: 0
    end
    column :price2 do |record|
      number_to_currency record.price2, locale: :es, separator: ",", delimiter: ".", precision: 0
    end
    column :stolen_score do |record|
      span :class => "score" do
        number_with_precision record.stolen_score, precision: 2
      end
    end
    column :status do |resource|
      options = [ApplicationHelper::STATUS_CLEAN,
                 ApplicationHelper::STATUS_LOW,
                 ApplicationHelper::STATUS_MODERATE,
                 ApplicationHelper::STATUS_HIGH,
                 ApplicationHelper::STATUS_CRITICAL].map{|x| [x, status_for(x)]}.to_h
      best_in_place resource, :status, place_holder: options[ApplicationHelper::STATUS_CLEAN], as: :select, collection: options, url: toggle_admin_publication_path(resource), :class => "status_tag #{status_color(resource.status)} pub_status_on_success"
    end 
    column :reviewed_at do |res|
      options = {true: '<span class="status_tag sí active_green">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
      place_holder = res.reviewed? ? options[:true] : options[:false]
      best_in_place res, :reviewed_at, place_holder: place_holder, as: :checkbox, collection: options, url: toggle_reviewed_admin_publication_path(res), :class => "pub_reviewed_at_on_success"
    end
    column :url do |record|
      snap = record.publication_snapshots.first
      [
        record.url.present? ? link_to('Link', record.url) : 'link',
        (snap.present? && snap.snapshot.present? && snap.snapshot.url.present?) ? link_to(' Snap', snap.snapshot.url) : ''
      ].join("").html_safe
    end
    
    actions defaults: false do  |record|
      item "Ver", admin_publication_path(record), class: 'member_link'
      item "Editar", edit_admin_publication_path(record), class: 'member_link'
      item "Desactivar", action_inactive_admin_publication_path(record), class: 'member_link'
    end
  end

  show do
  attributes_table do
    row :id
    row :name
    row :publication_group
    row :description
    row :publisher do |record|
      if not record.publisher.nil?
        span do
          pubs = record.publisher.publications
          q = client_scoped(pubs).active_valid.count
          link_to "(#{q}) #{record.publisher.to_s}", admin_publisher_path(record.publisher)
        end
      end
    end
    row :published_at
    row t("app.valid") do |resource|
      sys_scraped_at = Configuration.first.sys_scraped_at
      options = {true: '<span class="status_tag sí active_green">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
      is_valid = false
      if resource.last_seen_at.present? && sys_scraped_at.present?
        is_valid = resource.last_seen_at > sys_scraped_at
      end
      is_valid ? options[:true] : options[:false]
    end
    row :multi_prod
    row :price do |record|
      number_to_currency record.price, locale: :es, separator: ",", delimiter: ".", precision: 0
    end
    row :price2 do |record|
      number_to_currency record.price2, locale: :es, separator: ",", delimiter: ".", precision: 0
    end
    row :stolen_score do |record|
      number_with_precision record.stolen_score, precision: 2
    end 
    row :status do |record|
      status_tag(status_for(record.status), status_color(record.status))
    end
    row "Revisado" do |record|
      record.reviewed_at
    end
    row :url do |pub|
      div :class => "" do
        link_to pub.url, pub.url, target: "_blank"
      end
    end
    row :condition do |record|
      div :class => "condition" do
        condition_for(record.condition)
      end
    end
    row :county_raw
    row :state_raw
    row :city_raw
    row :manual_match
    row :active
    row :publication_group
    row :suitcase
    row :created_at
    row :updated_at
    row :search_engine do |pub|
      s_e = SearchEngine.find_by_id(pub.search_engine_id)
      if s_e.nil?
        "Publicación encontrada antes de los Criterios de búsqueda"
      else
        s_e
      end
    end
  end

div.panel t('app.publication_stats.one') do
  attributes_table_for resource do # FIXME: hack
    row t("app.aprox_valid_time") do |record|
      if resource.published_at.present? && resource.last_seen_at.present?
        days = ((resource.last_seen_at - resource.published_at).to_f / 1.day).floor
        "#{days} día(s)"
        # days = resource.last_seen_at.day - resource.published_at.day
        # "#{days} día(s)"
      end
    end
  end                                                                  
end  

  panel t('app.suitcase.other') do
    table_for publication.suitcases do
      column "name" do |c|
        c.name
      end
      # column :appointment_date
    end
  end 


  panel t('app.product.other') do
    table_for publication.products.order(name: :asc) do
    column :brand do |product|
        link_to product.brand, admin_brand_path(product.brand) if product.brand
    end
    column "Producto base" do |product|
        link_to product.name, admin_product_path(product)
    end
    end
  end
  panel t('app.snapshot.other') do
    table_for publication.publication_snapshots.order(updated_at: :desc) do
    column :updated_at
    column :name do |snapshot|
      span :class => "name" do
        truncate snapshot.name, :length   => 30, :separator => /\w/, :omission => "..."
      end
    end

    column :price do |snapshot|
      span :class => "price" do
        number_to_currency snapshot.price, locale: :es, separator: ",", delimiter: ".", precision: 0
      end
    end 
    column :description
    column :snapshot_at
    column :file do |snapshot|
      div :class => "url" do
        link_to t('app.download'), snapshot.snapshot.url if snapshot.snapshot.url
      end          
    end

    end
  end
  active_admin_comments
  end

  form do |f|
  f.semantic_errors # shows errors on :base

  f.inputs t('app.detail.other') do
  
    f.input :name, :input_html => { :readonly => true, :disabled => true }
    f.input :publication_group, as: :select, :collection => PublicationGroup.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } 
    f.input :description, as: :string, :input_html => { :readonly => true, :disabled => true }
    publisher = Publisher.name_order
		if session[:customer_ids].present?
			publisher = publisher.in_customers(session[:customer_ids])
		end
    f.input :publisher_id, as: :search_select, url: admin_publishers_path, fields: [:name], display_name: 'display_name', minimum_input_length: 3,
      order_by: 'name_asc'
    f.input :published_at, as: :datepicker, datepicker_options: { min_date: "2016-01-01", max_date: "+3Y" }, :input_html => { :readonly => true, :disabled => true }
    f.input :last_seen_at, as: :datepicker, datepicker_options: { min_date: "2016-01-01", max_date: "+3Y" }, :input_html => { :readonly => true, :disabled => true }
    f.input :multi_prod, :input_html => { :readonly => true, :disabled => true }
    f.input :price
    f.input :price2, :input_html => { :readonly => true, :disabled => true }
    f.input :stolen_score, :input_html => { :readonly => true, :disabled => true }
    f.input :status, :as => :select, :collection => status_choices
    f.input :reviewed_at, :as => :select, :label => "Revisado"
    f.input :url, :input_html => { :readonly => true, :disabled => true }
    f.input :condition, :as => :select, :collection => condition_choices()
    f.input :county_raw, :input_html => { :readonly => true, :disabled => true }
    f.input :state_raw, :input_html => { :readonly => true, :disabled => true }
    f.input :city_raw, :input_html => { :readonly => true, :disabled => true }
    f.input :manual_match
    f.input :active
    product = Product.all
		if session[:customer_ids].present?
			product = product.in_customers(session[:customer_ids])
		end
    f.input :products, :as => :select, multi: true, :collection => product.order(:name).map { |x| [x.to_s, x.id] }
    f.input :suitcases, :as => :select, multi: true, :collection => Suitcase.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }
  end

  f.actions
  end

  # batch_action :watch do |ids|
  #   batch_action_collection.find(ids).each do |pub|
  #     pub.watch!
  #   end
  #   redirect_to collection_path, alert: t("app.alert.batch_watch")
  # end

  # batch_action :unwatch do |ids|
  #   batch_action_collection.find(ids).each do |pub|
  #     pub.unwatch!
  #   end
   # redirect_to collection_path, alert: t("app.alert.batch_unwatch")
  # end
  # scoped_collection_action :review,  
  #                           title: 'Revisar', 
  #                           form: -> { 
  #                               {review: [['Si', Time.zone.now], ['No', nil]]}
  #                           } do
  #   puts params[:changes]                         
  #   puts scoped_collection_records                  
  #   scoped_collection_records.update_all(reviewed_at: params[:changes][:review])
  # end

  scoped_collection_action :scoped_collection_update,
                          title: 'Acciones en masa',
                           form: -> do
                            {
                              reviewed_at: [['Si', "#{DateTime.now.strftime('%F %T')}"], ['No', nil]],
                              force_scraper: [['Si', true],['No', false]],
                              price: 'text'
                            }
                          #  } do
    # scoped_collection_records.update_all(name: params[:changes][:name], reviewed_at: params[:changes][:review])
    # flash[:notice] = 'Name successfully changed.'
    # render nothing: true, status: :no_content, location: admin_publications_path
  end
  scoped_collection_action :desactivar, title: 'Desactivar' do
    scoped_collection_records.update_all(active: false, manual_match: true)
  end
  # scoped_collection_action :not_review,  title: 'No Revisado' do 
  #   puts "Estoy no revisando"
  #   puts scoped_collection_records
  #   scoped_collection_records.update_all(reviewed_at: nil)
  #   # flash[:notice] = t("app.alert.reviewed_at")
  # end
  batch_action :review do |ids|
    batch_action_collection.find(ids).each do |pub|
      pub.review!
    end
    redirect_to :back, alert: t("app.alert.reviewed_at")
  end

  batch_action :unreview do |ids|
    batch_action_collection.find(ids).each do |pub|
      pub.unreview!
    end
    redirect_to :back, alert: t("app.alert.unreviewed_at")
  end

  batch_action :used do |ids|
    batch_action_collection.find(ids).each do |pub|
      pub.set_condition(ApplicationHelper::USE_STATUS_USED)
      pub.deactivate!
      pub.manual!
    end
    redirect_to :back, alert: t("app.alert.used")
  end

  batch_action :force_scraper do |ids|
    batch_action_collection.find(ids).each do |pub|
      pub.update_attribute(:force_scraper, true)
    end
    redirect_to :back, alert: t("app.alert.force_scraper")
  end
  
  batch_action :deactivate do |ids|
    batch_action_collection.find(ids).each do |pub|
      pub.deactivate!
    end
    redirect_to :back, alert: t("app.alert.deactivate")
  end
  batch_action :to_publication_group, form: -> { 
    {publication_group: PublicationGroup.client_scoped(session[:customer_ids]).pluck(:name, :id)} } do |ids, inputs|
    pb = PublicationGroup.find(inputs["publication_group"])
    active_pub = pb.publications.where('publications.active = true').first
    puts "#{active_pub.id}".red if !active_pub.nil?
    ids.each do |id|
      pub = Publication.find(id)
      next if pub.nil?
      pb.publications << pub
      if !active_pub.nil?
        puts "pub: #{pub.id}, active_pub: #{active_pub.id}"
        if pub.id != active_pub.id
          puts "DESACTIVANDO #{pub.id}".red
          pub.update_column(:active, false)
        end
      else
        puts "entrando al else".red
        puts "pub: #{pub.id}, active_pub: #{ids[0]}"
        pub.deactivate! if pub.id != ids[0].to_i
      end
      # pub.save
    end
    redirect_to admin_publication_group_url(pb), notice: "Agregadas las publicaciones #{ids} a #{pb.name}"
  end
  batch_action :create_and_asign_to_publication_group, form: -> { 
    {name: :text} } do |ids, inputs|
    pb = PublicationGroup.find_or_initialize_by(name: inputs["name"])
    ids.each do |id|
      pub = Publication.find(id)
      next if pub.nil?
      pb.publications << pub
      # puts "pub: #{pub.id}, active_pub: #{ids[0]}"
      # pub.deactivate! if pub.id != ids[0].to_i
    end
    pb.update_publications_to_group
    pb.save!
    redirect_to admin_publication_group_url(pb), notice: "Agregadas las publicaciones #{ids} a #{pb.name} se activó por defecto #{ids[0]}"
  end
  controller do
    def action_methods
      if session[:customer_ids].present?
        super - ['new'] 
      else
        super
      end
    end      
    def scoped_collection
      publications = Publication.all
      publications = client_scoped(publications)
    end
    # def action_methods
    #   customer_session_setup(session[:customer_ids])
    # end
    def update
      @publication = Publication.find(params[:id])
      @publication.remember_products_hash
      update!
    end

  end

end
