include ApplicationHelper
ActiveAdmin.register Category do

  menu parent: 'Configuración', priority: 4, label: proc{ t("active_admin.category") }
  permit_params :name, :by_description, :aliases, :excludes, :by_used, brand_ids: [],search_engine_attributes:[:name,:search_keywords,:search_excludes,:keywords_ml,:keywords_mp,:keywords_yp,:id,:parent_type, :active,:_destroy]

  filter :name
#   filter :products, as: :select, collection: -> { Product.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }
  filter :created_at
	filter :updated_at
	filter :id

  index do

	selectable_column
		column :name
		column :aliases
    column t("app.brand.other") do |record|
			x = []
			record.brands.each do |prod|
				x << link_to(prod.to_s, admin_brand_path(prod))
			end
			x.join("").html_safe
			if x.count > 1
				"#{x[0]} (+#{x.count - 1})".html_safe
			else
				x[0]
			end
		end
		column :search_engine do |resource|
			options = {true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
			resource.search_engine.empty? ? options[:false] : options[:true]
			
		end		
		column :updated_at
		actions
  end

  show do
		# active_publications = brand.publications.active
		# inactive_publications = brand.publications.inactive
		attributes_table do
			row :id
			row "Nombre (No se incluye en categorización)" do |resource|
				resource.name
			end
			row t("app.brand.other") do |record|
				x = []
				record.brands.each do |prod|
				  x << link_to("#{prod.to_s}", admin_brand_path(prod))
				end
				x.join(",").html_safe
			end
			row :aliases
			row :excludes

			row "Criterios de búsqueda" do |record|
				x = []
				record.search_engine.each do |prod|
				  x << link_to("#{prod.to_s}", admin_search_engine_path(prod))
				end
				x.join(",").html_safe
			end
      row :by_description do |record|
        div :class => "condition" do
          activate_for(record.by_description)
        end
      end
			row :by_used
			row :created_at
			row :updated_at
		end
		
		# panel t("app.product.other") do
		# 	# Publication.joins(:products).joins(:brands).where('brands.category_id = ?', category.id)
		# 	table_for Product.joins(:brands).where('brands.category_id = ?', category.id).order(name: :asc) do
		# 		column :name do |product|
		# 			link_to product, admin_product_path(product)
		# 		end
		# 		column :price do |product|
		# 			number_to_currency product.price, locale: :es, separator: ",", delimiter: ".", precision: 0
		# 		end
		# 		column "Precio promedio" do |record|
		# 			number_to_currency record.publications.average(:price), locale: :es, separator: ",", delimiter: ".", precision: 0
		# 		end
		# 		column :updated_at
		# 		# column "records count" do |product|
		# 		#   product.publications.count
		# 		# end
		# 	end
		# end

		panel "Indicadores" do
			sys_scraped_at = Configuration.first.sys_scraped_at
			month_minus_2 = sys_scraped_at - 3.months
			render partial: "dashboard", locals: {pubs: Publication.joins(products: :brand).where('brands.category_id = ? AND publications.published_at > ?', category.id, month_minus_2)}
		end
		# pubs = client_scoped(resource.publications)
		# render partial: "publication_common", locals: {pubs: pubs, resource: resource, params_filter: {brands_id_in: resource.id}}			
	end

	form do |f|
		f.semantic_errors # shows errors on :base

		f.inputs t("app.detail.other") do
		f.input :name, :label => "Nombre (No se incluye en categorización)"
		f.input :brands, as: :select, :collection =>  		Brand.order(:name).map { |x| 
			if x.generic
				[x.to_s + '(Generico)', x.id] 
			else
				[x.to_s, x.id] 
			end
		} 
		 

		f.input :aliases
		f.input :excludes
		# f.input :by_description, as: :select
		f.input :by_description, :as => :select, :collection => activation_choices()
		f.input :by_used, as: :select

		f.inputs "Parámetros Fórmula Score" do
			f.input :factor_a
			f.input :disc_limit1
			f.input :disc_limit2
			f.input :disc_limit3		
			f.input :factor_b
			f.input :has_label1_ss
			f.input :factor_c
			f.input :has_label2_ss
			f.input :factor_d
			f.input :many_updates_ss 
			f.input :factor_e
			f.input :publisher_freq_ss
			f.input :factor_f
			# f.input :factor_g
		end		
		f.inputs "Criterios de búsqueda" do
			# 1.times do
			# 	f.object.search_engine.build
			#   end
			f.has_many :search_engine,
									allow_destroy: true,
									new_record: 'Añadir criterio de búsqueda',
									heading: 'Criterios de búsqueda' do |t|
				t.input :name, :label => "Nombre (No se incluye en búsqueda)"
				t.input :search_keywords
				t.input :keywords_ml
				t.input :keywords_yp
				t.input :keywords_mp
				t.input :search_excludes
				t.input :active
				t.input :parent_type, :as => :hidden,:input_html => { :value => ApplicationHelper::USE_TYPE_CATEGORY }
			end
		end
        #   f.input :brands, :as => :select, :collection	=> Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }  
		end

		f.actions
	end

  controller do 
    # nested_belongs_to :brands, :search_engine
    def scoped_collection
      categories = Category.all
      if session[:customer_ids].present?
        return categories.in_customers(session[:customer_ids])
      end
			categories
		end
	# 	def action_methods
	# 		if session[:customer_ids].present?
	# 			super - ['new'] 
	# 		else
	# 			super
	# 		end
	# 	end  		
  end

end
