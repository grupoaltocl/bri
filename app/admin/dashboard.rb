include ApplicationHelper
ActiveAdmin.register Publication, as: "statistic" do
  # config.paginate = false
  config.clear_action_items!
  config.batch_actions = false

  config.sort_order = ''
  # scope proc { I18n.t('active_admin.active') }, :active, default: true

  # menu parent: 'Configuración', priority: 1001, label: proc{ t("active_admin.statistic") }
  menu priority: 1, label: "Dashboard" 
	filter :name_cont, label: "Contiene nombre" # filters: [:not_start,  :not_end, :not_cont]
  filter :name_not_cont, label: "No contiene nombre"
  filter :description_cont, label: "Contiene descripción"
  filter :description_not_cont, label: "No contiene descripción"
  filter :publisher_name, as: :string, label: I18n.t('app.publisher_name')
  filter :publisher_status, as: :select, collection: -> { status_choices }
  filter :published_at
  filter :publication_valid_in, as: :boolean, :label => "Vigente",collection: -> { [["Si", 1],["No", 0]] }
  # filter :last_seen_at_gteq, :as => :boolean, collection: -> { 
  #     sys_scraped_at = Configuration.first.sys_scraped_at
  #     [["Si", sys_scraped_at],["Todos", "2099-01-01"]] 
  #   }, label: "Vigente"
  filter :multi_prod
  filter :price
  filter :stolen_score
  filter :status, :as => :select, multiple: true, collection: -> { status_choices }
  filter :reviewed_at_null, :as => :boolean, label: "Revisado"
  # filter :brands_customers_id, as: :select,  multiple: true, collection: -> { Customer.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }, label: I18n.t('app.customer.other')
  # filter :brands, as: :select,  multiple: true, label: I18n.t('app.brand.filter_brand'), collection: -> { 
  #   Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }
  # }
  filter :products_brand_id, as: :select,  multiple: true, label: I18n.t('app.brand.filter_product'), collection: -> { 
    Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }
  }
  filter :publisher_person_id, as: :select,  multiple: true, collection: -> { Person.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }, label: I18n.t('app.person.other')
  filter :products_base_product_id, as: :select, multiple: true, collection: -> { BaseProduct.client_scoped(session[:customer_ids]).order(:name) }
  filter :publication_group, :as => :select, multiple: true
  filter :suitcases, as: :select, multiple: true, collection: -> { Suitcase.client_scoped(session[:customer_ids]).order(:name) }
  filter :county_raw
  filter :state_raw
  filter :city_raw
  filter :created_at
  filter :updated_at
  filter :manual_match
  filter :publisher_id
  filter :publisher_source_id, as: :select, multiple: true, collection: -> { Source.all.order(:name) }, label: I18n.t('app.source.other')
  filter :id
  

  before_filter -> {
    sys_scraped_at = Configuration.first.sys_scraped_at
    month_minus_2 = sys_scraped_at - 2.months
    if not params[:q].present?
      params[:q] = { published_at_gteq_datetime: month_minus_2.strftime('%Y-%m-%d') }
    end
  }

  index :title =>'Dashboard' do
    panel "Publicaciones" do
    div class: "dashboard-title" do
      sum = 0
      collection.each do |pub|
        next if pub.nil?
        sum += pub.price if !pub.price.nil?
      end
      h2 "<strong>#{collection.count}</strong> publicaciones con un monto total de: <strong>#{number_to_currency(sum,locale: :es, separator: ",", delimiter: ".", precision: 0)}</strong>".html_safe
      if params[:q].present?
        if params[:q][:published_at_lteq_datetime].nil?
          h3 "desde #{params[:q][:published_at_gteq_datetime]} hasta #{Time.now.strftime('%Y-%m-%d')}"
        else
          h3 "desde #{params[:q][:published_at_gteq_datetime]} hasta #{params[:q][:published_at_lteq_datetime]}"
        end
      end
    end
    render partial: "dashboard", locals: {pubs: collection}
    end
  end

  xls(:header_style => {:weight => :bold, :color => :blue }) do 
      whitelist
      column :id
      column :name
      column :products do |record|
        x = []        
        record.products.in_customers(session[:customer_ids]).each do |prod|
          x << prod.name
        end
        x.join(",").html_safe
      end    
    #   column('Marca Scraper') do |record|
    #    x = []
    #    record.brands.in_customers(session[:customer_ids]).each do |b|
    #      x << b.name
    #    end
    #    x.join(",").html_safe
    #  end 
      column('Marca Producto') do |record|
        x = []
        record.products.in_customers(session[:customer_ids]).each do |prod|
          x << prod.brand.name
        end
        x.join(",").html_safe
      end 
      column ('Producto marca') do |record|
        x = []
        record.products.in_customers(session[:customer_ids]).each do |prod|
          x << prod.to_s
        end
        x.join(",").html_safe
      end
      column :condition do |record|
        condition_for(record.condition)
      end
      column :price do |record|
        record.price.to_i
      end
      column :price2 do |record|
        record.price2.to_i
      end
      column :stolen_score
      column :status
      column "Riesgo" do |record|
        status_for(record.status)
      end
      column "Revisado" do |record|
        record.reviewed?
      end
      column "Fecha revisión" do |record|
        "#{record.reviewed_at}"
      end
      column :multi_prod do |record|
        if record.multi_prod
          true
        else
          false
        end
      end
      column :manual_match
      column "Comuna" do |record|
        record.county_raw
      end
      column "Región" do |record|
        record.state_raw
      end
      column "Ciudad" do |record|
        record.city_raw
      end
      column :published_at
      column "Día publicación" do |record|
        record.published_at.day if !record.published_at.nil?
      end
      column "Día semana publicación" do |record|
        record.published_at.wday if !record.published_at.nil?
      end
      column "Mes publicación" do |record|
        record.published_at.month if !record.published_at.nil?
      end
      column "Año publicación" do |record|
        record.published_at.year if !record.published_at.nil?
      end
      column "Vigente" do |record|
        sys_scraped_at = Configuration.first.sys_scraped_at
        if record.last_seen_at >= sys_scraped_at 
          true
        else
          false
        end
      end
      column "Ultima vez visto" do |record|
        record.last_seen_at
      end
      column I18n.t("app.aprox_valid_time") do |record|
        if record.published_at.present? && record.last_seen_at.present?
          days = ((record.last_seen_at - record.published_at).to_f / 1.day).floor
          "#{days} día(s)"
        end
      end
      column :description
      column "ID Grupo" do |resource|
        group_id = ""
        group_id = "#{resource.publication_group.id}" if not resource.publication_group.nil?
        group_id      
      end
      column(:publication_group) do |resource|
        group_name = ""
        group_name = "#{resource.publication_group.name}" if not resource.publication_group.nil?
        group_name
      end
      column "ID Publicador" do |resource|
        "#{resource.publisher.id}"
      end
      column "Publicador" do |resource|
        publisher_name = t('app.unknown')
        publisher_name = "#{resource.publisher.name}" if not resource.publisher.nil?
        publisher_name
      end
      column("Valor riesgo publicador") do |resource|
        publisher_status = t('app.unknown')
        publisher_status = "#{resource.publisher.status}" if not resource.publisher.status.nil?
        publisher_status
      end    
      column("Riesgo publicador") do |resource|
        publisher_status = t('app.unknown')
        publisher_status = "#{status_for(resource.publisher.status)}" if not resource.publisher.status.nil?
        publisher_status
      end    
      column("Fuente publicador") do |resource|
        publisher_source = t('app.unknown')
        publisher_source = "#{resource.publisher.source.name}" if not resource.publisher.source.nil?
        publisher_source
      end
      column :url
      column "Snap" do |resource|
        snap = resource.publication_snapshots.first
        snap = "#{snap.snapshot.url}" if (snap.present? && snap.snapshot.present? && snap.snapshot.url.present?)
        snap
      end
      column :created_at
      column :updated_at
      column "Id caso" do |record|
        x = []        
        record.suitcases.each do |suitcase|
          x << suitcase.id
        end
        x.join("").html_safe      
        # suitcase_ids = "#{resource.suitcases.id}" if not resource.suitcases.present?
        # suitcase_ids
      end
      column "Caso" do |record|
        x = []        
        record.suitcases.each do |suitcase|
          x << suitcase.name
        end
        x.join("").html_safe        
        # suitcase_name = "#{resource.suitcases.name}" if not resource.suitcases.present?
        # suitcase_name
      end

  #    after_filter { |sheet|
  #      # todo
  #    }
  #
  #    before_filter do |sheet|
  #      # todo
  #    end
  end
  # render partial: "statistics", locals: {chart: chart}
  controller do 
    def scoped_collection
			publications = Publication.active
			publications = client_scoped(publications)
    end
    before_action only: :index do
      @per_page = 900000
    end

  end
  
end
