include ApplicationHelper
ActiveAdmin.register Suitcase do
 	menu priority: 6, label: proc{ I18n.t("active_admin.suitcases") }

	permit_params(
		:name, :description, :note,
		case_files_attributes: [ :file, :description ],
		person_ids: [], publisher_ids: [], publication_ids: [], case_file_ids: [],
		)

	filter :name
	filter :description
	# filter :people 
	filter :people, as: :select, collection: -> { Person.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }, label: I18n.t('app.person.other')
	filter :publication_groups, collection: -> { PublicationGroup.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }
	filter :created_at
	filter :updated_at
	filter :id

	index do
		selectable_column
		# id_column
		column :name
		column :description
		column :case_files do |record|
			record.case_files.count
		end
		column :updated_at
		actions
	end

  show do
		attributes_table do
			row :id
			row :name
			row :description
			row :note
			# row :name do |c|
			# 	link_to c.person_ids.to_s, admin_person_path(c)
			# end		
			row :people do |resource|
				# puts "#{resource.peoples}".red
				x = []
				resource.person_ids.each do |people|
					person = Person.find(people)
					x << link_to("#{person}", admin_person_path(person))
				end
				x.join("").html_safe
			end
			row :created_at
			row :updated_at
		end

		panel I18n.t("app.case_files.other") do
		  table_for suitcase.case_files do
			column :file do |case_file_link|
			  div :class => "url" do
				link_to t('app.download'), case_file_link.file.url if case_file_link.file.url
			  end          
			end
			column :description
			column :created_at
		  end
		end

		render partial: "person_table", locals: {people: suitcase.people}
		render partial: "publisher_table", locals: {publishers: suitcase.publishers.name_order}
		render partial: "publication_group_table", locals: {publication_groups: suitcase.publication_groups}
		render partial: "publication_table", locals: {pubs: suitcase.publications, params_filter: {}}

		pubs = client_scoped(Publication.all_in_suitcases([suitcase]))
		render partial: "publication_common", locals: {pubs: pubs, resource: resource, params_filter: {}}
		
		active_admin_comments
  end

  form do |f|
		f.semantic_errors # shows errors on :base

		f.inputs I18n.t("app.detail.other") do
		  f.input :name
		  f.input :description
		#   f.input :people, :collection => Person.all.map{ |p| ["#{p.first_name} #{p.last_name}", p.id] }
			# publisher = Publisher.all
			# if session[:customer_ids].present?
			# 	publisher = publisher.in_customers(session[:customer_ids])
			# end		
		  f.input :publishers, :collection => Publisher.client_scoped(session[:customer_ids]).name_order.map{ |p| ["#{p.name}", p.id] }
		  # f.input :publication_groups, :collection => PublicationGroup.all.map{ |p| ["#{p.name}", p.id] }
		  f.input :publications, :collection => client_scoped(Publication.all).order(:name).map{ |p| ["#{p.name}", p.id] }
		  f.input :note
		  f.inputs do
			f.has_many :case_files,
					 new_record: "Agregar archivos",
					 allow_destroy: true do |b|
						b.input :file, :as => :file
						b.input :description
					end
		  end
		end
		f.actions
  end

	controller do
  def action_methods
		if session[:customer_ids].present?
				super - ['new'] 
			else
				super
			end
		end  		
    def scoped_collection
			suitcases = Suitcase.all
			if session[:customer_ids].present?
				return Suitcase.all_in_customers(session[:customer_ids])
      end
      suitcases
    end
  end
end
