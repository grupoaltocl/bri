ActiveAdmin.register Publication, as: "launchers" do
  
    config.paginate = false
    config.batch_actions = false
    config.clear_action_items!
    before_filter :skip_sidebar!, :only => :index

    # scope proc { I18n.t('active_admin.active') }, :active, default: true

    # menu parent: 'Mantenedores', priority: 2001, label: proc{ t("active_admin.statistic") }
    menu parent: 'Configuración', priority: 2001, label: 'Lanzadores' 
    # menu false


    
    
    index title: proc{ I18n.t("active_admin.dashboard") }, :download_links => false, :paginate => false  do

        panel "Lanzadores" do
            columns do
                column { button_to 'Calcular Score', launch_score_calculator_admin_publications_path, method: :get }
                column { button_to 'Lanzar categorizador', launch_categorizer_admin_publications_path, method: :get }
            end
            
        end
    end
    controller do 
        def scoped_collection
                publications = Publication.limit(1)
                publications = client_scoped(publications)
        end
        before_action only: :all do
            @per_page = 10
        end

    end

end