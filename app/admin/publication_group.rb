include ApplicationHelper
ActiveAdmin.register PublicationGroup do
  menu priority: 4, label: proc{ I18n.t("active_admin.publication_groups") }

  permit_params(
	:name, :url, :publisher_id, :published_at,
	:description, :status, :publication_group_id,
	:stolen_score, :condition, :multi_prod, :reviewed_at,
	suitcase_ids: []
	)

  member_action :toggle, method: :put do
    pg = PublicationGroup.find(params[:id])
    pg.set_status(params[:publication_group]["status"])
    pg.review!
    return '{ "status": "OK"}'
  end

  member_action :toggle_reviewed, method: :put do
    pub = Publication.find(params[:id])
    publication_group = PublicationGroup.find(pub.publication_group.id)
    if publication_group.reviewed_at != nil
      publication_group.unreview!
      return '{ "status": "OK"}'
      # redirect_to admin_publication_path, notice: t('app.reviewed')
    else
      publication_group.review!
      return '{ "status": "OK"}'
      # redirect_to admin_publication_path, notice: t('app.unreviewed')
    end
  end

  filter :brands, multiple: true
  filter :name_cont, label: "Contiene nombre publicación" # filters: [:not_start,  :not_end, :not_cont]
	filter :name_not_cont, label: "No contiene nombre publicación"  
	filter :publisher_name, as: :string, label: I18n.t('app.publisher_name')
	# filter :publisher_name_cont, label: "Contiene nombre publicador" # filters: [:not_start,  :not_end, :not_cont]
	# filter :publisher_name_not_cont, label: "No contiene nombre publicador"
	filter :publisher_source_id, as: :select, collection: -> { Source.all.order(:name) }, label: I18n.t('app.source')
	filter :condition, :as => :select, collection: -> { condition_choices }
	filter :price
	filter :stolen_score
	filter :status, :as => :select, multiple: true, collection: -> { status_choices }
	filter :reviewed_at
  filter :suitcases, as: :select, multiple: true, collection: -> { Suitcase.client_scoped(session[:customer_ids]).order(:name) }
  filter :description
  filter :county_raw
  filter :state_raw
  filter :city_raw
  filter :published_at
  
  filter :created_at
  filter :updated_at
  filter :multi_prod
  filter :id



  index do
  
  selectable_column
  column :name do |record|
    pub = record.publications.active.first
    span :class => "name" do
      link_to "#{truncate record.name, :length   => 30, :separator => /\w/, :omission => "..."}", admin_publication_group_path(record)
    end
  end
  column :publisher do |record|
    pub = record.publications.active.first
    if pub.present?
      span link_to "#{pub.publisher.to_s}", admin_publisher_path(pub.publisher)
    end
  end
  column :publisher_status do |record|
    pub = record.publications.active.first
    if pub.present?
      publisher_status = pub.publisher.status
      if publisher_status.present?
      status_tag(status_for(publisher_status), status_color(publisher_status))
      end
    end
  end
  column :published_at do |record|
    pub = record.publications.active.first
    if pub.present?
      pub.published_at
    end
  end
  column :price do |record|
    pub = record.publications.active.first
    if pub.present?
      number_to_currency pub.price, locale: :es, separator: ",", delimiter: ".", precision: 0
    end
  end
  column :stolen_score do |record|
    pub = record.publications.active.first
    if pub.present?
      span :class => "score" do
        number_with_precision pub.stolen_score, precision: 2
      end
    end
  end

  column :status do |record|
    pub = record.publications.active.first
    if pub.present?
  # status_tag(status_for(record.status), status_color(record.status))
    options = [ApplicationHelper::STATUS_CLEAN,
    ApplicationHelper::STATUS_LOW,
    ApplicationHelper::STATUS_MODERATE,
    ApplicationHelper::STATUS_HIGH,
    ApplicationHelper::STATUS_CRITICAL].map{ |x| [x, status_for(x)]}.to_h
    best_in_place pub, :status, place_holder: options[ApplicationHelper::STATUS_CLEAN], as: :select, collection: options, url: toggle_admin_publication_path(pub), :class => "status_tag #{status_color(pub.status)} pub_group_status_on_success"
    end 
  end 

  column :reviewed_at do |record|
    pub = record.publications.active.first
    if pub.present?
    options = {true: '<span class="status_tag sí active_green">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
    place_holder = pub.reviewed? ? options[:true] : options[:false]
    best_in_place pub, :reviewed_at, place_holder: place_holder, as: :checkbox, collection: options, url: toggle_reviewed_admin_publication_group_path(pub), :class => "pub_reviewed_at_on_success"
    end
	end
  column :url do |record|
    pub = record.publications.active.first
    if pub.present?
    snap = pub.publication_snapshots.first
    [
      pub.url.present? ? link_to('Link', pub.url) : 'link',
      (snap.present? && snap.snapshot.present? && snap.snapshot.url.present?) ? link_to(' Snap', snap.snapshot.url) : ''
    ].join("").html_safe
    end
  end
	
	actions
  end

	show do
		
		attributes_table do
			row :id
      row :name
			
		end

    pubs = resource.publications.valid
    render partial: "publication_table_for_group", locals: {pubs: pubs, params_filter: {publication_group_id_equals: resource.id}}
    
    panel t('app.suitcase.other') do
      table_for publication_group.suitcases do
        column "name",include_blank: false do |c|
          c.name
        end
        # column :appointment_date
      end
    end
    
    # render partial: "publication_stats", locals: {pubs: client_scoped(pubs), resource: resource}
    # render partial: "dashboard", locals: {pubs: client_scoped(pubs)}

		active_admin_comments
  end

	form do |f|
			f.semantic_errors # shows errors on :base

			f.inputs t('app.detail.other') do
        f.input :name
        # f.input :url
        # f.input :condition, :as => :select, :collection => condition_choices()
        # f.input :publisher
        # f.input :published_at, as: :datepicker, datepicker_options: { min_date: "2016-01-01", max_date: "+3Y" }
        # f.input :last_seen_at, as: :datepicker, datepicker_options: { min_date: "2016-01-01", max_date: "+3Y" }
        # f.input :county_raw
        # f.input :state_raw
        # f.input :city_raw
        # f.input :description
        # f.input :multi_prod
				f.input :suitcases
			end

		# f.inputs t("app.metric.other") do
		# 	f.input :stolen_score, :input_html => { :readonly => true, :disabled => true }
		# 	f.input :status, :as => :select, :collection => status_choices
		# end
		panel I18n.t("app.publication_to_activate.one") do
			publication = client_scoped(publication_group.publications).each { |x| x.name }
			render partial: "active_relation_select", locals: {relations: publication, label: "Publicación"}
		end		
		f.actions
	end

	# batch_action :watch do |ids|
	# 		batch_action_collection.find(ids).each do |pub|
	# 		pub.watch!
	# 	end
	# 	redirect_to collection_path, alert: t("app.alert.batch_watch")
	# end

 #  	batch_action :unwatch do |ids|
	# 		batch_action_collection.find(ids).each do |pub|
	# 		pub.unwatch!
	# 	end
	# 	redirect_to collection_path, alert: t("app.alert.batch_unwatch")
	# end

	controller do 
		def scoped_collection
		publication_groups = PublicationGroup.all
		if session[:customer_ids].present?
				# publication_groups.joins(query).where('customers.id IN (?)', session[:customer_ids])
				return publication_groups.in_customers(session[:customer_ids])
		else
				publication_groups
		end
  end
  def action_methods
    if session[:customer_ids].present?
      super - ['new'] 
    else
      super
    end
  end  
		def update
		update! do |format|
			format.html do 
        pg = PublicationGroup.find(params[:id])
        if request.request_parameters['relation_selected'].present?
          flash[:warning] = "Se cambió la publicación activa!"
          pub = Publication.find(request.request_parameters['relation_selected'].to_i)
          puts "Custom update".red
          Publication.where(id: pg.publications.pluck(:id) - [pub.id]).update_all(active: false)
          # pub.active = true
          # pub.save! # does no work always ? e.g: when a pub is selected and u choose the same one it gets deactivated
          Publication.where(id: pub.id).update_all(active: true)
        end
				redirect_to admin_publication_group_path, notice: "Actualizado"
			end
		end
	end

  end


end
