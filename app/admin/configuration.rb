ActiveAdmin.register Configuration, as: "parameter" do

  menu false
  # menu parent: 'Configuración', priority: 5, label: proc{ t("active_admin.configurations") }
  
  # menu priority: 500, label: proc{ t("active_admin.configurations") }

  permit_params(
	:factor_a, :factor_b, :factor_c, :factor_d, :factor_e, :factor_f, :factor_g, 
	:has_label1_ss, :has_label2_ss, 
	:publisher_freq_ss, :many_updates_ss, :multiprod_labels,
	:disc_limit1, :disc_limit2, :disc_limit3,
	:has_new_label, :has_not_new_label, :run_cat ,:search_from, my_select: []
  )
# 
  # index false

  config.filters = false
  # config.clear_action_items!
  actions :all, :except => :destroy
  # actions :index, :edit, :update
  show do
		attributes_table do
			row :factor_a
			row :factor_b
			row :factor_c
			row :factor_d
			row :factor_e
			row :factor_f
			# row :factor_g
			row :has_label1_ss
			row :has_label2_ss
			row :publisher_freq_ss
			row :many_updates_ss
			row :multiprod_labels
		end
  end

  form do |f|
		f.semantic_errors # shows errors on :base
		
		panel "Selección de Cliente activo" do
			customers = Customer.all
			render partial: "client_active_select", locals: {customers: customers}
		end
		# f.inputs "Seleccionar tipo de búsqueda" do
		# 	f.input :search_from, :as => :select, :collection => search_choices()
		# end

		# f.inputs "Parámetros Fórmula Score" do
		# 	f.input :factor_a
		# 	f.input :disc_limit1
		# 	f.input :disc_limit2
		# 	f.input :disc_limit3		
		# 	f.input :factor_b
		# 	f.input :has_label1_ss
		# 	f.input :factor_c
		# 	f.input :has_label2_ss
		# 	f.input :factor_d
		# 	f.input :many_updates_ss 
		# 	f.input :factor_e
		# 	f.input :publisher_freq_ss
		# 	f.input :factor_f
		# 	# f.input :factor_g
		# end
		

		f.inputs "General" do
			f.input :multiprod_labels
			# f.input :has_new_label
			f.input :has_not_new_label
		end

		f.inputs "Sistema" do
			f.input :sys_scraped_at, as: :date_time_picker, label: "Último scraper"
		end

		f.actions
	end

	controller do
		def show 
		  super do |format|
				format.html do
				  config = Configuration.first
				  if config.nil?
						redirect_to new_admin_parameter_path
				  else
						redirect_to edit_admin_parameter_path config
				  end
				end
				format.json do
					return render json: collection[0].to_json
				end
			end
		end

		def update
			if request.request_parameters['my_select'].present?
				session[:customer_ids] = request.request_parameters['my_select'].map { |x| x.to_i }
			else
				session[:customer_ids] = nil
			end

			if request.request_parameters['my_search'].present?
				config = Configuration.first
				config.search_from = request.request_parameters['my_search']
				puts "#{config.search_from}".red
				config.save
			end	

		  update! do |format|
				format.html do 
					redirect_to edit_admin_parameter_path(Configuration.first), notice: "Actualizado"
				end
		  end
		end

		def create
		  create! do |format|
				format.html do 
				  redirect_to edit_admin_parameter_path(Configuration.first), notice: "Creado"
				end
		  end
		end

		def new
		  if Configuration.first.nil?
				super
		  else
				redirect_to edit_admin_parameter_path(Configuration.first)
		  end
		end
	end
	
end