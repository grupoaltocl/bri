include ApplicationHelper
ActiveAdmin.register Notification do
    menu parent: 'Configuración', priority: 9999999999999, label: "Notificaciones"
    # config.clear_action_items
    actions :all, :except => [:destroy,:edit,:new]
    member_action :set_is_read, method: :put do
        notification = Notification.find(params[:id])
        if notification.is_read == false
          notification.update_columns(is_read: true)
        else
          notification.update_columns(is_read: false)
        end
        return '{ "status": "OK"}'
      end

    index do
        selectable_column
        column "Fecha", :sortable => :created_at do |obj|
            r = obj.created_at.localtime.strftime("%d %B %Y, %H:%M")
            r.gsub!("January", "Enero")
            r.gsub!("February", "Febrero")
            r.gsub!("March", "Marzo")
            r.gsub!("April", "Abril")
            r.gsub!("May", "Mayo")
            r.gsub!("June", "Mayo")
            r.gsub!("July", "Julio")
            r.gsub!("August", "Agosto")
            r.gsub!("September", "Septiembre")
            r.gsub!("October", "Octubre")
            r.gsub!("November", "Noviembre")
            r.gsub!("August", "Agosto")
            r.gsub!("December", "Diciembre")
            r
        end        
        # column "Fecha" do |record|
        #     record.created_at
        # end
        column "Tipo" do |record|
            record.name
        end
        column "Descripción" do |record|
            record.description
        end
        # column :is_read
        column "uuid" do |resource|
            resource.process_uuid
        end
        column "Estado" do |resource|
            options = {true: '<span class="status_tag no">Leído</span>'.html_safe, false: '<span class="status_tag yes">No leído</span>'.html_safe}
            place_holder = resource.is_read? ? options[:true] : options[:false]
            best_in_place resource, :is_read, place_holder: place_holder, as: :checkbox, collection: options, url: set_is_read_admin_notification_path(resource)
        end


        actions defaults: false do  |record|
            item "Ver", admin_notification_path(record), class: 'member_link'
        end
    end
    def action_methods
        super  - ['new']
    end
end