include ApplicationHelper
ActiveAdmin.register Person do
  	menu priority: 5, label: proc{ I18n.t("active_admin.people") }
	permit_params(:uid, :first_name, :name, :last_name, :email1, :email2, 
					:phone1, :phone2, :status, :note, :county_raw,
					:state_raw, :city_raw,
					suitcase_ids: [], publisher_ids: [])
	  
	# filter :first_name
	filter :name
	filter :first_name
	filter :last_name
	filter :uid
	filter :email1
	filter :email2
	filter :phone1
	filter :phone2
	filter :county_raw
	filter :state_raw
	filter :city_raw
	filter :stolen_score
	filter :status, :as => :select, collection: -> { status_choices }
	filter :suitcases, as: :select, multiple: true, collection: -> { Suitcase.client_scoped(session[:customer_ids]).order(:name) }
	filter :created_at
	filter :updated_at
	filter :id

	index do
		selectable_column
		column :name
		column :first_name
		column :last_name
		column :stolen_score
		column :status do |record|
			status_tag(status_for(record.status), status_color(record.status))
		end  
			# column :status do |resource|
			# 	options = [ApplicationHelper::STATUS_CLEAN,
			# 	ApplicationHelper::STATUS_LOW,
			# 	ApplicationHelper::STATUS_MODERATE,
			# 	ApplicationHelper::STATUS_HIGH,
			# 	ApplicationHelper::STATUS_CRITICAL].map{ |x| [x, status_for(x)]}.to_h
			# 	best_in_place resource, :status, place_holder: options[ApplicationHelper::STATUS_CLEAN], as: :select, collection: options, url: admin_person_path(resource), :class => "status_tag #{status_color(resource.status)} pub_status_on_success"
			# end	     
		column :email1
		column :fb_page1 do |resource|
			options = {true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
			resource.fb_page1.present? ? link_to(options[:true], resource.fb_page1) : options[:false]
		end
		column :phone1
		actions
	end

	show do
		panel I18n.t("app.suitcase.other") do
			table_for person.suitcases do
				column "name" do |c|
					c.name
				end
			# column :appointment_date
			end
		end

		attributes_table do
		  row :id
		  row :name
		  row :first_name
		  row :last_name
		  row :uid
		  row :stolen_score
		  row :email1
		  row :email2  
		  row :phone1
		  row :phone2
		  row :county_raw
		  row :state_raw
		  row :city_raw
		  row :note
		  row :status do |record|
				status_tag(status_for(record.status), status_color(record.status))
		  end
		  row :created_at
		  row :updated_at
		end


		pubs = Publication.in_person(resource)
		render partial: "publisher_table", locals: {publishers: person.publishers.name_order}
		# render partial: "publication_common", locals: {pubs: pubs, resource: resource, params_filter: {}}
		render partial: "publication_common", locals: {pubs: pubs, resource: resource, params_filter: {publisher_person_id_in: [resource.id]}}
	  active_admin_comments
  
  end

  # sidebar "Details", only: :show do
  #   attributes_table_for person do
  #     row :first_name
  #     row :last_name
  #   end
  # end

  form do |f|
		f.semantic_errors # shows errors on :base

		f.inputs 'Details' do
			f.input :name, :input_html => { :readonly => true, :disabled => true }
			f.input :first_name
			f.input :last_name
			f.input :uid
			f.input :email1
			f.input :email2
			f.input :phone1
			f.input :phone2
			f.input :county_raw
			f.input :state_raw
			f.input :city_raw
			f.input :status, :as => :select, :collection => status_choices, :input_html => { :readonly => true, :disabled => true }
			f.input :note
			f.input :suitcases, :as => :select, :collection	=> Suitcase.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }  
		end
		

		f.actions
	end
  controller do 
    def scoped_collection
      people = Person.all
      if session[:customer_ids].present?
        return people.in_customers(session[:customer_ids])
      end
      people
	end
	def action_methods
		if session[:customer_ids].present?
			super - ['new'] 
		else
			super
		end
	end  	
	def update
		person = Person.find(params[:id])
		puts "UPDATING"
		if not person.publishers.empty?
			puts " NO ESTA VACÍO"
			publisher = person.publishers.max_by(&:status)
			person.name = publisher.name
			person.email1 = publisher.email1
			person.email2 = publisher.email2
			person.phone1 = publisher.phone1
			person.phone2 = publisher.phone2
			person.note = publisher.note
			person.watch = publisher.watch
			person.status = publisher.status
			person.county_raw = publisher.county_raw
			person.state_raw = publisher.state_raw
			person.city_raw = publisher.city_raw
			person.stolen_score = publisher.stolen_score
			person.save
			redirect_to admin_person_path, notice: "Actualizado"
		# if request.request_parameters['relation_selected'].present?
		# 	publisher = Publisher.find(request.request_parameters['relation_selected'].to_i)
		# 	if person.present?
		# 		puts "ENTREEEEE".red
		# 		person.name = publisher.name
		# 		person.email1 = publisher.email1
		# 		person.email2 = publisher.email2
		# 		person.phone1 = publisher.phone1
		# 		person.phone2 = publisher.phone2
		# 		person.note = publisher.note
		# 		person.watch = publisher.watch
		# 		person.status = publisher.status
		# 		person.county_raw = publisher.county_raw
		# 		person.state_raw = publisher.state_raw
		# 		person.city_raw = publisher.city_raw
		# 		person.stolen_score = publisher.stolen_score
		# 		person.save
		# 		redirect_to admin_person_path, notice: "Actualizado"
		# 	end
		else
			update! do |format|
				format.html do 
					redirect_to admin_person_path, notice: "Actualizado"
				end
			end
		end
	end
  end
end
