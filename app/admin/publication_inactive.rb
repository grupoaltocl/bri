include ApplicationHelper
ActiveAdmin.register Publication, as: "publication_inactive" do
  
  controller do
    before_filter { @page_title = t("active_admin.publications_inactive") } 
  end

  # scope proc { I18n.t('active_admin.inactive') }, :inactive, default: true
  scope proc { I18n.t('active_admin.inactive_valid') }, :inactive_valid, default: true
  scope proc { I18n.t('active_admin.inactive_expired') }, :inactive_expired
  scope I18n.t("app.all_inactive"), :inactive
  config.sort_order = ''

  menu parent: 'Configuración', priority: 1000, label: proc{ t("active_admin.publications_inactive") }

  xls(:header_style => {:weight => :bold, :color => :blue }) do 
    whitelist
    column :id
    column :name
    column :products do |record|
      x = []        
      record.products.in_customers(session[:customer_ids]).each do |prod|
        x << prod.name
      end
      x.join(",").html_safe
    end    
  #   column('Marca Scraper') do |record|
  #    x = []
  #    record.brands.in_customers(session[:customer_ids]).each do |b|
  #      x << b.name
  #    end
  #    x.join(",").html_safe
  #  end 
    column('Marca Producto') do |record|
      x = []
      record.products.in_customers(session[:customer_ids]).each do |prod|
        x << prod.brand.name
      end
      x.join(",").html_safe
    end 
    column ('Producto marca') do |record|
      x = []
      record.products.in_customers(session[:customer_ids]).each do |prod|
        x << prod.to_s
      end
      x.join(",").html_safe
    end
    column :condition do |record|
      condition_for(record.condition)
    end
    column :price do |record|
        record.price.to_i
    end
    column :price2 do |record|
        record.price2.to_i
    end
    column :stolen_score
    column :status
    column "Riesgo" do |record|
      status_for(record.status)
    end
    column "Revisado" do |record|
      record.reviewed?
    end
    column "Fecha revisión" do |record|
      "#{record.reviewed_at}"
    end
    column :multi_prod do |record|
      if record.multi_prod
        true
      else
        false
      end
    end
    column :manual_match
    column "Comuna" do |record|
      record.county_raw
    end
    column "Región" do |record|
      record.state_raw
    end
    column "Ciudad" do |record|
      record.city_raw
    end
    column :published_at
    column "Día publicación" do |record|
      record.published_at.day if !record.published_at.nil?
    end
    column "Día semana publicación" do |record|
      record.published_at.wday if !record.published_at.nil?
    end
    column "Mes publicación" do |record|
      record.published_at.month if !record.published_at.nil?
    end
    column "Año publicación" do |record|
      record.published_at.year if !record.published_at.nil?
    end
    column "Vigente" do |record|
      sys_scraped_at = Configuration.first.sys_scraped_at
      if record.last_seen_at >= sys_scraped_at 
        true
      else
        false
      end
    end
    column "Ultima vez visto" do |record|
      record.last_seen_at
    end
    column I18n.t("app.aprox_valid_time") do |record|
      if record.published_at.present? && record.last_seen_at.present?
        days = ((record.last_seen_at - record.published_at).to_f / 1.day).floor
        "#{days} día(s)"
      end
    end
    column :description
    column "ID Grupo" do |resource|
      group_id = ""
      group_id = "#{resource.publication_group.id}" if not resource.publication_group.nil?
      group_id      
    end
    column(:publication_group) do |resource|
      group_name = ""
      group_name = "#{resource.publication_group.name}" if not resource.publication_group.nil?
      group_name
    end
    column "ID Publicador" do |resource|
      "#{resource.publisher.id}"
    end
    column "Publicador" do |resource|
      publisher_name = t('app.unknown')
      publisher_name = "#{resource.publisher.name}" if not resource.publisher.nil?
      publisher_name
    end
    column("Valor riesgo publicador") do |resource|
      publisher_status = t('app.unknown')
      publisher_status = "#{resource.publisher.status}" if not resource.publisher.status.nil?
      publisher_status
    end    
    column("Riesgo publicador") do |resource|
      publisher_status = t('app.unknown')
      publisher_status = "#{status_for(resource.publisher.status)}" if not resource.publisher.status.nil?
      publisher_status
    end    
    column("Fuente publicador") do |resource|
      publisher_source = t('app.unknown')
      publisher_source = "#{resource.publisher.source.name}" if not resource.publisher.source.nil?
      publisher_source
    end
    column :url
    column "Snap" do |resource|
      snap = resource.publication_snapshots.first
      snap = "#{snap.snapshot.url}" if (snap.present? && snap.snapshot.present? && snap.snapshot.url.present?)
      snap
    end
    column :created_at
    column :updated_at
    column "Id caso" do |record|
      x = []        
      record.suitcases.each do |suitcase|
        x << suitcase.id
      end
      x.join("").html_safe      
      # suitcase_ids = "#{resource.suitcases.id}" if not resource.suitcases.present?
      # suitcase_ids
    end
    column "Caso" do |record|
      x = []        
      record.suitcases.each do |suitcase|
        x << suitcase.name
      end
      x.join("").html_safe        
      # suitcase_name = "#{resource.suitcases.name}" if not resource.suitcases.present?
      # suitcase_name
    end

  #    after_filter { |sheet|
  #      # todo
  #    }
  #
  #    before_filter do |sheet|
  #      # todo
  #    end
  end


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params(
    :name, :publisher_id, :published_at,
    :description, :status, :publication_group_id,
    :stolen_score, :condition, :publication_snapshot,
    :multi_prod, :manual_match, :reviewed_at, :destroy,
    publication_snapshots_attributes: [:id, :name, :price, :_destroy],
    product_ids: [], suitcase_ids: []
    )
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  member_action :toggle, method: :get do
    publication_inactive = Publication.find(params[:id])
    if publication_inactive.reviewed_at != nil
      publication_inactive.unreview!
      redirect_to admin_publication_inactive_path, notice: t('app.reviewed')
    else
      publication_inactive.review!
      redirect_to admin_publication_inactive_path, notice: t('app.unreviewed')
    end
  end

  member_action :toggle_reviewed, method: :put do
    publication_inactive = Publication.find(params[:id])
    if publication_inactive.reviewed_at != nil
      publication_inactive.unreview!
      return '{ "status": "OK"}'
      # redirect_to admin_publication_inactive_path, notice: t('app.reviewed')
    else
      publication_inactive.review!
      return '{ "status": "OK"}'
      # redirect_to admin_publication_inactive_path, notice: t('app.unreviewed')
    end
  end
  # member_action :watch_pub, method: :patch do
  #   publication_inactive = Publication.find(params[:id])
  #   if publication_inactive.watch == false || publication_inactive.watch == nil
  #     publication_inactive.watch!
  #     return '{ "status": "OK"}'
  #     # redirect_to admin_publication_inactive_path, notice: t('app.watched')
  #   else
  #     publication_inactive.unwatch!
  #     return '{ "status": "OK"}'
  #     # redirect_to admin_publication_inactive_path, notice: t('app.unwatched')
  #   end
  # end

  action_item :action_review, only: [:show, :edit] do
    publication_inactive = Publication.find(params[:id])
    if publication_inactive.reviewed_at != nil
      link_to t('app.unmark_reviewed'), toggle_admin_publication_inactive_path, class: :reviewed
    else 
      link_to t('app.mark_reviewed'), toggle_admin_publication_inactive_path, class: :not_reviewed
    end
    
  end

  member_action :action_inactive, method: :get do
    publication = Publication.find(params[:id])
    publication.deactivate!
    publication.manual!   
    
    redirect_to :back, 
    notice: t('app.notice_publication_inactivated',
              pub: "<a href=\"#{admin_publication_inactive_path(publication)}\">#{publication.id}</a>",
              undo: "<a href=\"#{action_active_admin_publication_path(publication)}\">Deshacer</a>"
            )
  end
  
  member_action :action_active, method: :get do
    publication = Publication.find(params[:id])
    publication.activate!
    publication.manual!   
    redirect_to :back, 
      notice: t('app.notice_publication_activated',
                pub: "<a href=\"#{admin_publication_path(publication)}\">#{publication.id}</a>",
                undo: "<a href=\"#{action_inactive_admin_publication_path(publication)}\">Deshacer</a>"
              )
  end

  # filter :name # filters: [:not_start,  :not_end, :not_cont]
	# filter :name_cont, label: "Contiene nombre" # filters: [:not_start,  :not_end, :not_cont]
	# filter :name_not_cont, label: "No contiene nombre"  
  # filter :publisher_source_id, as: :select, multiple: true, collection: -> { Source.all }, label: I18n.t('app.source.other')
  # filter :publisher_name, as: :string, label: I18n.t('app.publisher_name')
  # filter :status, :as => :select, multiple: true, collection: -> { status_choices }
  # filter :published_at
  # filter :multi_prod
  # filter :price
  # filter :stolen_score
  # filter :reviewed_at
  # filter :brands, multiple: true
  # filter :publisher_person_id, as: :select,  multiple: true, collection: -> { Person.all }, label: I18n.t('app.person.other')
  # filter :products
  # filter :suitcases
  # filter :county_raw
  # filter :state_raw
  # filter :city_raw
  # filter :created_at
  # filter :updated_at
  # filter :manual_match
  # filter :description
  # filter :publisher_id
	filter :name_cont, label: "Contiene nombre" # filters: [:not_start,  :not_end, :not_cont]
	filter :name_not_cont, label: "No contiene nombre"
	filter :description_cont, label: "Contiene descripción"
	filter :description_not_cont, label: "No contiene descripción"
  filter :publisher_name, as: :string, label: I18n.t('app.publisher_name')
  filter :publisher_status, as: :select, collection: -> { status_choices }
  filter :published_at
  filter :publication_valid_in, as: :boolean, :label => "Vigente",collection: -> { [["Si", 1],["No", 0]] }
  filter :multi_prod
  filter :price
  filter :stolen_score
  filter :status, :as => :select, multiple: true, collection: -> { status_choices }
  filter :reviewed_at_null, :as => :boolean, label: "Revisado"
  # filter :brands, as: :select,  multiple: true, label: I18n.t('app.brand.filter_brand'), collection: -> { 
  #   Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }
  # }
filter :products_brand_id, as: :select,  multiple: true, label: I18n.t('app.brand.filter_product'), collection: -> { 
  Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }
}
  filter :publisher_person_id, as: :select,  multiple: true, collection: -> { Person.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }, label: I18n.t('app.person.other')
  filter :products_base_product_id, as: :select, multiple: true, collection: -> { BaseProduct.client_scoped(session[:customer_ids]).order(:name) }
  filter :publication_group, :as => :select, multiple: true, collection: -> { PublicationGroup.client_scoped(session[:customer_ids]).order(:name) }
  filter :suitcases, as: :select, multiple: true, collection: -> { Suitcase.client_scoped(session[:customer_ids]).order(:name) }
  filter :county_raw
  filter :state_raw
  filter :city_raw
  filter :created_at
  filter :updated_at
  filter :manual_match
  filter :publisher_id  
  filter :publisher_source_id, as: :select, multiple: true, collection: -> { Source.all.order(:name) }, label: I18n.t('app.source.other')
  filter :condition, :as => :select, collection: -> { condition_choices }
  filter :id

  

  index do
    selectable_column
    column :name do |record|
      span :class => "name" do
        link_to "#{truncate record.name, :length   => 30, :separator => /\w/, :omission => "..."}", admin_publication_path(record)
      end
    end
    column :publication_group do |record|
      if record.publication_group
        publication_group = record.publication_group
      end
      if not publication_group.nil?
        link_to '<span class="status_tag yes">Sí</span>'.html_safe, admin_publication_group_path(publication_group)
      else
        '<span class="status_tag no">No</span>'.html_safe
      end
    end   
    column :publisher do |record|
      span link_to "#{record.publisher.to_s}", admin_publisher_path(record.publisher)
    end
    column :publisher_status do |record|
      if record.publisher and record.publisher.status
      publisher_status = record.publisher.status
      end
      if not publisher_status.nil?
      status_tag(status_for(publisher_status), status_color(publisher_status))
      end            
    end   
    column :published_at
    column :valid do |res|
      sys_scraped_at = Configuration.first.sys_scraped_at
      options = {true: '<span class="status_tag sí active_green">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}

      is_valid = false
      if res.last_seen_at.present? && sys_scraped_at.present?
        is_valid = res.last_seen_at > sys_scraped_at
      end
      is_valid ? options[:true] : options[:false]
    end
    column I18n.t('app.product_brand.other') do |record|
      x = []
      record.products.each do |prod|
        # if not prod.generic
        x << link_to("#{prod.to_s}", admin_product_path(prod))
        # end
      end
      x.join("").html_safe
      if x.count > 1
        "#{x[0]} (+#{x.count-1})".html_safe
      else
        x[0]
      end
    end    
    column :multi_prod do |resource|
      options = {true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
      place_holder = resource.multi? ? options[:true] : options[:false]
      best_in_place resource, :multi_prod, place_holder: place_holder, as: :checkbox, collection: options, url: set_multi_admin_publication_path(resource)
    end
    column :price do |record|
      number_to_currency record.price, locale: :es, separator: ",", delimiter: ".", precision: 0
    end
    column :stolen_score do |record|
      span :class => "score" do
        number_with_precision record.stolen_score, precision: 2
      end
    end
    column :status do |resource|
    # status_tag(status_for(record.status), status_color(record.status))
      options = [ApplicationHelper::STATUS_CLEAN,
      ApplicationHelper::STATUS_LOW,
      ApplicationHelper::STATUS_MODERATE,
      ApplicationHelper::STATUS_HIGH,
      ApplicationHelper::STATUS_CRITICAL].map{ |x| [x, status_for(x)]}.to_h
      best_in_place resource, :status, place_holder: options[ApplicationHelper::STATUS_CLEAN], as: :select, collection: options, url: toggle_admin_publication_path(resource), :class => "status_tag #{status_color(resource.status)} pub_status_on_success"
    end 
    column :reviewed_at do |res|
      options = {true: '<span class="status_tag sí active_green">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
      place_holder = res.reviewed? ? options[:true] : options[:false]
      best_in_place res, :reviewed_at, place_holder: place_holder, as: :checkbox, collection: options, url: toggle_reviewed_admin_publication_inactive_path(res), :class => "pub_reviewed_at_on_success"
    end      
    column :condition do |resource|
      status_tag(condition_for(resource.condition), status_color(resource.condition))
    end   
    column :url do |record|
      snap = record.publication_snapshots.first
      [
        record.url.present? ? link_to('Link', record.url) : 'link',
        (snap.present? && snap.snapshot.present? && snap.snapshot.url.present?) ? link_to(' Snap', snap.snapshot.url) : ''
      ].join("").html_safe
    end
    
    actions defaults: false do  |record|
      item "Ver", admin_publication_path(record), class: 'member_link'
      item "Editar", edit_admin_publication_path(record), class: 'member_link'
      item "Activar", action_active_admin_publication_inactive_path(record), class: 'member_link'
    end
  end

  show do
    panel t('app.suitcase.other') do
      table_for publication_inactive.suitcases do
        column "name" do |c|
          c.name
        end
        # column :appointment_date
      end
    end
    attributes_table do
      row :id
      row :name
      row :publisher do |record|
        if not record.publisher.nil?
          span do
            q = client_scoped(record.publisher.publications).where(condition: ApplicationHelper::USE_STATUS_NEW).count
            link_to "(#{q}) #{record.publisher.to_s}", admin_publisher_path(record.publisher)
          end
        end
      end
      row :url do |pub|
        div :class => "" do
          link_to pub.url, pub.url, target: "_blank"
        end
      end
      row :suitcase
      row :publication_group
      

      row :county_raw
      row :state_raw
      row :city_raw
      row :published_at
      row :condition do |record|
        div :class => "condition" do
          condition_for(record.condition)
        end
      end
      

      row :price do |record|
        number_to_currency record.price, locale: :es, separator: ",", delimiter: ".", precision: 0
      end

      row :stolen_score do |record|
        number_with_precision record.stolen_score, precision: 2
      end 

      row :status do |record|
        status_tag(status_for(record.status), status_color(record.status))
      end


      row :description

      row :reviewed_at
      # row :finished_at
      row t("app.valid") do |resource|
      options = {true: '<span class="status_tag sí active_green">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
      if resource.last_seen_at.present?
        options[:true]
      else
        options[:false]
      end
    end

      row :multi_prod
      row :manual_match

      row :created_at
      row :updated_at
    end



    panel t('app.product.other') do
      table_for publication_inactive.products.order(name: :asc) do
        column :brand do |product|
          link_to product.brand.name, admin_brand_path(product.brand)
        end
        column :product do |product|
          link_to product.name, admin_product_path(product)
        end
      end
    end
    panel t('app.snapshot.other') do
      table_for publication_inactive.publication_snapshots.order(updated_at: :desc) do
        column :updated_at
        column :name do |snapshot|
          span :class => "name" do
            truncate snapshot.name, :length   => 30, :separator => /\w/, :omission => "..."
          end
        end

        column :price do |snapshot|
          span :class => "price" do
          number_to_currency snapshot.price, locale: :es, separator: ",", delimiter: ".", precision: 0
          end
        end 
        column :description
        column :snapshot_at
        column :file do |snapshot|
          div :class => "url" do
            link_to t('app.download'), snapshot.snapshot.url if snapshot.snapshot.url
          end          
        end

      end
    end
    active_admin_comments
  end

  form do |f|
    f.semantic_errors # shows errors on :base

    f.inputs t('app.detail.other') do
      f.input :suitcases
      f.input :publication_group
      f.input :products
      f.input :name, :input_html => { :readonly => true, :disabled => true }
      f.input :url, :input_html => { :readonly => true, :disabled => true }
      f.input :publisher, as: :string, :input_html => { :readonly => true, :disabled => true }
      f.input :published_at, as: :datepicker, datepicker_options: { min_date: "2016-01-01", max_date: "+3Y" }, :input_html => { :readonly => true, :disabled => true }
      f.input :last_seen_at, as: :datepicker, datepicker_options: { min_date: "2016-01-01", max_date: "+3Y" }, :input_html => { :readonly => true, :disabled => true }
      f.input :county_raw, :input_html => { :readonly => true, :disabled => true }
      f.input :state_raw, :input_html => { :readonly => true, :disabled => true }
      f.input :city_raw, :input_html => { :readonly => true, :disabled => true }
      f.input :description, :input_html => { :readonly => true, :disabled => true }
      f.input :multi_prod, :input_html => { :readonly => true, :disabled => true }
      f.input :manual_match
    end

    f.inputs t("app.metric.other") do
      f.input :stolen_score, :input_html => { :readonly => true, :disabled => true }
      f.input :status, :as => :select, :collection => status_choices
    end
    f.actions
  end

  # batch_action :watch do |ids|
  #   batch_action_collection.find(ids).each do |pub|
  #     pub.watch!
  #   end
  #   redirect_to collection_path, alert: t("app.alert.batch_watch")
  # end

  # batch_action :unwatch do |ids|
  #   batch_action_collection.find(ids).each do |pub|
  #     pub.unwatch!
  #   end
  #   redirect_to collection_path, alert: t("app.alert.batch_unwatch")
  # end
  # scoped_collection_action :scoped_collection_update,
  #                         title: 'Acciones en masa',
  #                          form: -> do
  #                           {
  #                             condition: [['Si', "#{DateTime.now.strftime('%F %T')}"], ['No', nil]],
  #                             active: [['Activa', true], ['En Basurero', false]]
  #                           }
  #                         #  } do
  #   # scoped_collection_records.update_all(name: params[:changes][:name], reviewed_at: params[:changes][:review])
  #   # flash[:notice] = 'Name successfully changed.'
  #   # render nothing: true, status: :no_content, location: admin_publications_path
  # end

  scoped_collection_action :set_condition,
                            title: 'Marcar como Nuevas' do
    scoped_collection_records.update_all(condition: ApplicationHelper::USE_STATUS_NEW, active: true)
  end
  scoped_collection_action :activar,
                            title: 'Activar' do
    scoped_collection_records.update_all(active: true, manual_match: true)
  end
  
  batch_action :activate do |ids|
    batch_action_collection.find(ids).each do |pub|
      pub.activate!
    end
    redirect_to :back, alert: t("app.alert.activate")
  end
  
  batch_action :new do |ids|
    batch_action_collection.find(ids).each do |pub|
      pub.set_condition(ApplicationHelper::USE_STATUS_NEW)
      pub.activate!
    end
    redirect_to :back, alert: t("app.alert.new")
  end
  batch_action :to_publication_group, form: -> { 
    {
      publication_group: PublicationGroup.client_scoped(session[:customer_ids]).pluck(:name, :id)
    }} do |ids, inputs|
    pb = PublicationGroup.find(inputs["publication_group"])
    ids.each do |id|
      pb.publications << Publication.find(id)
    end
    redirect_to collection_path, notice: "Agregadas las publicaciones #{ids} a #{pb.name}"
  end
  batch_action :create_and_asign_to_publication_group, form: -> { 
    {name: :text} } do |ids, inputs|
    pb = PublicationGroup.find_or_initialize_by(name: inputs["name"])
    ids.each do |id|
      pb.publications << Publication.find(id)
    end
    pb.save!
    redirect_to collection_path, notice: "Agregadas las publicaciones #{ids} a #{pb.name}"
  end

  controller do 
    
    def scoped_collection
#       query = <<~HEREDOC
#       INNER JOIN `brands_publications` ON `brands_publications`.`publication_id` = `publications`.`id` 
#       INNER JOIN `brands` ON `brands`.`id` = `brands_publications`.`brand_id`
#       INNER JOIN `brands_customers` ON `brands`.`id` = `brands_customers`.`brand_id`
#       INNER JOIN `customers` ON `customers`.`id` = `brands_customers`.`customer_id`
# HEREDOC
      publications = Publication.inactive.all
      publications = client_scoped(publications)
      # if session[:customer_ids].present?
        
      #   # publications.joins(query).where('customers.id IN (?)', session[:customer_ids])
      # else
      #   publications
      # end
    end
    def action_methods
      if session[:customer_ids].present?
        super - ['new'] 
      else
        super
      end
    end      

  end

end