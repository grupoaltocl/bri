include ApplicationHelper
ActiveAdmin.register Source do
  menu false
  permit_params :name, :code, :active

  index do
    selectable_column
    column :name
    column :code
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :code
      row :created_at
      row :updated_at
    end

    active_admin_comments
  end

  
  controller do 
    def action_methods
      if session[:customer_ids].present?
        super - ['new'] 
      else
        super
      end
    end      
  end

end
