include ApplicationHelper
ActiveAdmin.register SearchEngine do

  menu parent: 'Configuración', priority: 3, label: proc{ t("active_admin.search_engine") }
  permit_params :name, :aliases, :excludes, :search_keywords, :search_excludes, :keywords_ml, :keywords_yp, :keywords_mp, :brand_ids, :active,customer_ids: []

  filter :name
#   filter :products, as: :select, collection: -> { Product.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }
  filter :created_at
	filter :updated_at
	filter :id

  member_action :set_active, method: :put do
    s_e = SearchEngine.find(params[:id])
    if s_e.active == false
      s_e.update_columns(active: true)
    else
      s_e.update_columns(active: false)
    end
    return '{ "status": "OK"}'
  end

  index do

	selectable_column
	# column :name do |record|
	#   link_to record.name, admin_brand_path(record)
    # end
    column :name
	column :search_keywords
	column :search_excludes
	column "Pertenece a" do |resource|
		case resource.parent_type
		when "#{ApplicationHelper::USE_TYPE_CATEGORY}" || ApplicationHelper::USE_TYPE_CATEGORY
			link_to("#{resource.category[0]} (Rubro)", admin_category_path(resource.category[0]))
		when "#{ApplicationHelper::USE_TYPE_BRAND}" || ApplicationHelper::USE_TYPE_BRAND
			brand = Brand.where(search_engine: resource).first
			link_to(brand.to_s + " (Marca)", admin_brand_path(brand.id))
		end
	end
  column :active do |resource|
    options = {true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
    place_holder = resource.active? ? options[:true] : options[:false]
    best_in_place resource, :active, place_holder: place_holder, as: :checkbox, collection: options, url: set_active_admin_search_engine_path(resource)
  end
	actions
  end

  show do
		# active_publications = brand.publications.active
		# inactive_publications = brand.publications.inactive
		
		attributes_table do
			row :id
      row "Nombre (No se incluye en búsqueda)" do |resource|
        resource.name
      end
			row :search_keywords
			row :search_excludes
			row :keywords_ml
			row :keywords_yp
			row :keywords_mp
			row "Marca/Rubro" do |resource|
        case resource.parent_type
				when "#{ApplicationHelper::USE_TYPE_CATEGORY}" || ApplicationHelper::USE_TYPE_CATEGORY
          link_to("#{resource.category[0]} (Rubro)", admin_category_path(resource.category[0]))
        when "#{ApplicationHelper::USE_TYPE_BRAND}" || ApplicationHelper::USE_TYPE_BRAND
          brand = Brand.where(search_engine: resource).first
          link_to(brand.to_s + " (Marca)", admin_brand_path(brand.id))
        end
			end
			row :active
			row :created_at
			row :updated_at
		end
		# panel t("app.customer.other") do
		# 	table_for brand.customers.order(name: :asc) do
		# 		column :name do |customer|
		# 		link_to customer.name, admin_customer_path(customer)
		# 		end
		# 	end
		# end
		
		# panel t("app.product.other") do
		# 	table_for brand.products.order(name: :asc) do
		# 		column :name do |product|
		# 			link_to product, admin_product_path(product)
		# 		end
		# 		column :price do |product|
		# 			number_to_currency product.price, locale: :es, separator: ",", delimiter: ".", precision: 0
		# 		end
		# 		column "Precio promedio" do |record|
		# 			number_to_currency record.publications.average(:price), locale: :es, separator: ",", delimiter: ".", precision: 0
		# 		end
		# 		column :updated_at
		# 		# column "records count" do |product|
		# 		#   product.publications.count
		# 		# end
		# 	end
		# end

		# # panel "Indicadores" do
		# # 	render partial: "dashboard", locals: {pubs: brand.publications}
		# # end
		# pubs = client_scoped(resource.publications)
		# render partial: "publication_common", locals: {pubs: pubs, resource: resource, params_filter: {brands_id_in: resource.id}}			
	end

	form do |f|
		f.semantic_errors # shows errors on :base

		f.inputs t("app.detail.other") do
		#   f.input :customers, as: :select, :collection => Customer.all.order(:name) 
		f.input :name, :label => "Nombre (No se incluye en búsqueda)"
		f.input :search_keywords
		f.input :search_excludes
		f.input :keywords_ml
		f.input :keywords_yp
		f.input :keywords_mp
		f.input :active
		f.input :brands, as: :select, :collection => Brand.all.order(:name) 
        #   f.fields_for :brands do |m|
        #     m.inputs do
        #       m.input :name
        #     end
        #   end
        #   f.has_many :brands, new_record: false do |m|
        #     m.input :name
        #     m.input(:_destroy, as: :boolean, required: false, label: 'Remove') if m.object.persisted?
        #   end
        #   f.inputs do
        #     f.has_many :brands, heading: 'Themes',
        #                             allow_destroy: true
        #                             # new_record: false do |a|
        #                             #     a.name
        #                             # end
        #   end
        #   f.input :brands, :as => :select, :collection	=> Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }  
		end

		f.actions
	end

  controller do 
    # nested_belongs_to :brands, :search_engine
    # def scoped_collection
    #   brands = Brand.all
    #   if session[:customer_ids].present?
    #     return brands.in_customers(session[:customer_ids])
	# 			# return brands.customer.where('customers.id IN (?)', session[:customer_ids])
				
    #   end
    #   brands
	# 	end
	# 	def action_methods
	# 		if session[:customer_ids].present?
	# 			super - ['new'] 
	# 		else
	# 			super
	# 		end
	# 	end  		
  end

end
