include ApplicationHelper
ActiveAdmin.register Product do
	menu parent: 'Configuración', priority: 6, label: proc{ I18n.t("active_admin.products") }
	permit_params :name, :price, :brand_id, :aliases, :excludes, :sku, :filter_by_brand, :status, :base_product_id

	filter :brand, collection: -> { Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }
	filter :name
	filter :price
	filter :status
	filter :created_at
	filter :updated_at
	filter :sku
	filter :generic
	filter :id

	member_action :filter_by_brand, method: :put do
		s_e = Product.find(params[:id])
		if s_e.filter_by_brand == false
		s_e.update_columns(filter_by_brand: true)
		else
		s_e.update_columns(filter_by_brand: false)
		end
		return '{ "status": "OK"}'
	end

	index do

		selectable_column
		column :name do |record|
			span record
		end
		column :brand
		column :base_product
		column :price do |product|
			div :class => "price" do
				number_to_currency product.price, locale: :es, separator: ",", delimiter: ".", precision: 0
			end
		end
		
		column "Precio promedio" do |record|
			number_to_currency record.publications.active.average(:price), locale: :es, separator: ",", delimiter: ".", precision: 0
		end

		# column :filter_by_brand
		column :filter_by_brand do |resource|
			options = {true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
			place_holder = resource.filter_by_brand? ? options[:true] : options[:false]
			best_in_place resource, :filter_by_brand, place_holder: place_holder, as: :checkbox, collection: options, url:  filter_by_brand_admin_product_path(resource)
		end		
		column :updated_at

		actions
	end

	show do
		attributes_table do
			row :id
			row :name
			row :aliases
			row :excludes
			row :brand
			row :filter_by_brand 
			row :price do |record|
				number_to_currency record.price, locale: :es, separator: ",", delimiter: ".", precision: 0
			end
			row :sku
			row "producto base" do |record|
				record.base_product.nil? ? "" : link_to(record.base_product.to_s, admin_base_product_path(record.base_product))
			end
			row :created_at
			row :updated_at
		end

		# div.panel t('app.stats.other') do
		#   attributes_table_for product do # FIXME: hack
		# 			row "Publicaciones vigentes" do |record|
		# 				record.publications.active.count
		# 			end
		# 			row "Monto total" do |record|
		# 				number_to_currency record.publications.active.sum(:price), locale: :es, separator: ",", delimiter: ".", precision: 0
		# 			end
		# 			row "Precio promedio" do |record|
		# 				number_to_currency record.publications.active.average(:price), locale: :es, separator: ",", delimiter: ".", precision: 0
		# 			end
		#   end                                                                  
		# end  

		# panel t("app.publication_new.other") do
		# # 	table_for product.publications.active_new.order(name: :asc) do
		# # 		column :name do |publication|
		# # 			link_to publication.name, admin_publication_path(publication)
		# # 		end
		# # 		column :source do |record|
		# # 			if record.publisher and record.publisher.source
		# # 				source = record.publisher.source
		# # 			end
		# # 			if not source.nil?
		# # 				link_to "#{source.name}", admin_source_path(source)
		# # 			end
		# # 		end
		# # 		column :publisher
		# # 		column :stolen_score do |publication|
		# # 			publication.stolen_score
		# # 		end
		# # 		column :status do |record|
		# # 			status_tag(status_for(record.status), status_color(record.status))
		# # 		end
		# # 	end
		# 	active_pub = product.publications.active.count
		# 	inactive_pub = product.publications.inactive.count
		# 	div do
		# 		span link_to "Ir a publicaciones activas (#{active_pub.to_i})", admin_publications_path(q: { product_id_eq: product.id})
		# 		span "-"
		# 		span link_to "Ir a publicaciones inactivas (#{inactive_pub.to_i})", admin_publication_inactives_path(q: { product_id_eq: product.id})
		# 		br
		# 		span link_to "Descargar publicaciones activas", admin_publications_path('xls', q: { product_id_eq: product.id})
		# 	end
		# 	paginated_collection product.publications.active.order(name: :asc).page(params[:pub_page]).per(10), entry_name: "Post", param_name: :pub_page, download_links: false do
		# 		table_for collection do
		# 			column :name do |record|
		# 				link_to record.name.truncate(20), admin_publication_path(record)
		# 			end
		# 			column :source do |record|
		# 				if record.publisher and record.publisher.source
		# 				source = record.publisher.source
		# 				end
		# 				if not source.nil?
		# 				link_to "#{source.code}", admin_source_path(source)
		# 				end
		# 			end
		# 			column :publication_group do |record|
		# 				if record.publication_group
		# 				publication_group = record.publication_group
		# 				end
		# 				if not publication_group.nil?
		# 				link_to '<span class="status_tag yes">Sí</span>'.html_safe, admin_publication_group_path(publication_group)
		# 				else
		# 				'<span class="status_tag no">No</span>'.html_safe
		# 				end
		# 			end

		# 			column :published_at do |record|
		# 				record.published_at
		# 			end
		
		# 			column :multi_prod      
		# 			column :condition do |record|
		# 				div :class => "condition" do
		# 				condition_for(record.condition)
		# 				end
		# 			end
		# 			column :price do |record|
		# 				number_to_currency record.price, locale: :es, separator: ",", delimiter: ".", precision: 0
		# 			end   
		# 			column :stolen_score do |record|
		# 				record.stolen_score
		# 			end
		# 			column :status do |record|
		# 				status_tag(status_for(record.status), status_color(record.status))
		# 			end   
		# 			column :url do |record|
		# 				div do 
		# 					span link_to t('app.goto'), record.url
		# 				end
		# 			end


		# 		end
		# 	end
		# end
		# panel "Indicadores" do
		# 	render partial: "dashboard", locals: {pubs: product.publications}
		# end
		pubs = resource.publications
		render partial: 'publication_common', locals: {pubs: pubs, resource: resource, params_filter: {products_id_equals: resource.id}}
	end

	form do |f|
		f.semantic_errors # shows errors on :base
		read_only = resource.generic ? true : false
		f.inputs t("app.detail.other") do
			f.input :brand, :as => :select, :collection => Brand.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }, :input_html => { :readonly => read_only, :disabled => read_only }
			f.input :name, :input_html => { :readonly => read_only, :disabled => read_only }
			f.input :aliases, :input_html => { :readonly => read_only, :disabled => read_only }
			f.input :excludes, :input_html => { :readonly => read_only, :disabled => read_only }
			f.input :price, :input_html => { :readonly => read_only, :disabled => read_only }
			f.input :status, :as => :select, :collection => Product.status_choices(), :input_html => { :readonly => read_only, :disabled => read_only }
			f.input :filter_by_brand, :as => :boolean, :input_html => { :checked => 'checked', :readonly => read_only, :disabled => read_only }
			f.input :base_product, include_blank: false, :as => :select, :collection => BaseProduct.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] }, hint: 'Selecciona un producto base', :input_html => { :readonly => read_only, :disabled => read_only }
			f.input :sku, :input_html => { :readonly => read_only, :disabled => read_only }
		end

		f.actions
	end

	controller do
		alias_method :delete_product, :destroy
		def destroy
			brand = resource.brand
			generic_product = Product.find_or_initialize_by(name: brand.name.chomp('Otros').strip + ' Otros')
			generic_product.save
			puts generic_product.name
			generic_product.publications << resource.publications
			brand.products << generic_product
			delete_product
		end		
		def scoped_collection
			products = Product.all
			# publications = Publication.all
			if session[:customer_ids].present?
				return products.in_customers(session[:customer_ids])
			end
			products			
		end
		def action_methods
			if session[:customer_ids].present?
				super - ['new'] 
			else
				super
			end
		end  		
	end

end
