ActiveAdmin.register Publisher do
  menu priority: 3, label: proc{ t('active_admin.publishers') }

	scope I18n.t('app.all'), :default_order, default: true
	
	actions :all, except: [:destroy]
  permit_params(
		:source_id, :person_id, :url, 
		:name, :stolen_score,
		:county_raw, :state_raw, :city_raw, :email1, 
		:email2, :phone1, :phone2, :website, :fb_page1, :fb_page2, :twitter,
		:instagram, :status, :note, :created_at, 
		:updated_at, suitcase_ids: []
  )

	filter :name
	filter :source
	filter :person, as: :select, collection: -> { Person.client_scoped(session[:customer_ids]).order(:name).map { |x| [x.to_s, x.id] } }, label: I18n.t('app.person.other')
	filter :stolen_score
	filter :status, :as => :select, collection: -> { status_choices }
	filter :suitcases, as: :select, multiple: true, collection: -> { Suitcase.client_scoped(session[:customer_ids]).order(:name) }
	# filter :suitcase, :as => :select, collection: -> { Suitcase.all.map { |x| [x.to_s, x.id] } }
	filter :email1
	filter :email2	
	filter :fb_page1
	filter :fb_page2
	filter :url
	filter :phone1
	filter :phone2
	filter :county_raw
	filter :state_raw
	filter :city_raw
	filter :website
	filter :instagram
	filter :twitter
	filter :created_at
	filter :updated_at
	filter :id

  before_save do |publisher|
	publisher.display_name = publisher.to_s
    if publisher.person_id_changed?
    	# NOTE: not necessary anymore. Customer didn't like it
    	# ...
    end
  end

  config.sort_order = ''

	index do
		selectable_column
		# column :count, sortable: 'publication_count' do |b| 
		#   b.publication_count
		# end
		column :name do |record|
			if record.display_name != ""
				link_to record.display_name.truncate(50), admin_publisher_path(record)
			else
				link_to record.to_s.truncate(50), admin_publisher_path(record)
			end
		end
		column :person do |record|
			if record.person and record.person.first_name
				person = record.person
			end
			if not person.nil?
				link_to person.first_name, admin_person_path(person)
			end
		end
		column :stolen_score
		column :status do |record|
			status_tag(status_for(record.status), status_color(record.status))
		end    
		# column :status do |resource|
		# 	options = [ApplicationHelper::STATUS_CLEAN,
		# 	ApplicationHelper::STATUS_LOW,
		# 	ApplicationHelper::STATUS_MODERATE,
		# 	ApplicationHelper::STATUS_HIGH,
		# 	ApplicationHelper::STATUS_CRITICAL].map{ |x| [x, status_for(x)]}.to_h
		# 	best_in_place resource, :status, place_holder: options[ApplicationHelper::STATUS_CLEAN], as: :select, collection: options, url: admin_publisher_path(resource), :class => "status_tag #{status_color(resource.status)} pub_status_on_success"
		# end		
		column :email1
		column :fb_page1 do |resource|
			options = { true: '<span class="status_tag yes">Sí</span>'.html_safe, false: '<span class="status_tag no">No</span>'.html_safe}
			resource.fb_page1.present? ? link_to(options[:true], resource.fb_page1) : options[:false]
		end
		column :phone1
		column :county_raw 
		actions
	end

  show do

		# puts "#{resource.count}".red
		attributes_table do
		  # row :suitcases
			row :id
			row :name
			row :source do |record|
				if record.source
					source = record.source
				end
				if not source.nil?
					link_to "#{source.code}", admin_source_path(source)
				end
			end	
			row :stolen_score	
			row :email1
			row :email2	
			row :website
			row :fb_page1 do |record|
				link_to record.fb_page1, record.fb_page1 if record.fb_page1.present?
			end
			
			row :fb_page2 do |record|
				link_to record.fb_page2, record.fb_page2 if record.fb_page2.present?
			end
			
			row :url
			row :county_raw
			row :state_raw
			row :city_raw

			row :phone1
			row :phone2

			row :twitter
			row :instagram
			
			row :status do |record|
				status_tag(status_for(record.status), status_color(record.status))
			end
			row :note
			row :person do |person|
				link_to publisher.person.to_s, admin_person_path(publisher.person) if publisher.person
			end			
			row :created_at
			row :updated_at
		end

		panel t("app.suitcase.other") do
		  table_for publisher.suitcases do
				column "name" do |c|
				  c.name
				end
		  end
		end
		
		# pubs = client_scoped(resource.publications)
		render partial: "publication_common", locals: { pubs: resource.publications, resource: resource, params_filter: {publisher_id_equals: resource.id}}
		active_admin_comments
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  form do |f|
	f.semantic_errors # shows errors on :base

	f.inputs 'Details' do
		f.input :source, include_blank: false, :collection => Source.all.order(:name)
		f.input :url, :input_html => { :readonly => true, :disabled => true }
		f.input :name
		f.input :stolen_score, :input_html => { :readonly => true, :disabled => true }
		f.input :county_raw
		f.input :state_raw
		f.input :city_raw
		f.input :email1
		f.input :email2
		f.input :phone1
		f.input :phone2
		f.input :website
		f.input :fb_page1
		f.input :fb_page2
		f.input :twitter
		f.input :instagram
		f.input :status, :as => :select, :collection => status_choices, :input_html => { :readonly => true, :disabled => true }
		f.input :note
		person = Person.all
		if session[:customer_ids].present?
			person = person.in_customers(session[:customer_ids])
		end
		f.input :person, :collection => person.order(:first_name).map{ |p| ["#{p.first_name}", p.id] }
		f.input :suitcases
	end
	f.actions
	end
	

	# batch_action :watch do |ids|
	# 		batch_action_collection.find(ids).each do |pub|
	# 		pub.watch!
	# 	end
	# 	redirect_to collection_path, alert: "The posts have been flagged."
	# end

	# batch_action :unwatch do |ids|
	# 		batch_action_collection.find(ids).each do |pub|
	# 		pub.unwatch!
	# 	end
	# 	redirect_to collection_path, alert: "The posts have been flagged."
	# end
	config.batch_actions = true
	batch_action :to_person, form: -> { 
		{ person: Person.client_scoped(session[:customer_ids]).map{ |p| [p.to_s, p.id] }} } do |ids, inputs|
			person = Person.find(inputs["person"])
			ids.each do |id|
				publisher = Publisher.find_by_id(id)
				next if publisher.nil?
				person.publishers << publisher
			end
			person.update_publishers
			person.save
			redirect_to admin_people_url, notice: "Agregados los publicadores #{ids} a #{person.name}"
		end	

	batch_action :to_new_person, form: -> { {first_name: :text, last_name: :text} } do |ids, inputs|
			person = Person.find_or_initialize_by(first_name: inputs["first_name"], last_name: inputs["last_name"])
			ids.each do |id|
				publisher = Publisher.find_by_id(id)
				next if publisher.nil?
				person.publishers << publisher
			end
			person.update_publishers
			person.save
			redirect_to admin_people_url, notice: "Agregados los publicadores #{ids} a #{person.name}"
	end	

	batch_action :force_scraper do |ids|
		batch_action_collection.find(ids).each do |publisher|
				publisher.publications.each do |pub|
					pub.update_attribute(:force_scraper, true)
				end
		end
		redirect_to :back, alert: t("app.alert.force_scraper")
	end
  	scoped_collection_action :force_scraper,  title: 'Forzar scraper' do 
		scoped_collection_records.each do |publisher|
			publisher.publications.each do |pub|
				pub.update_attribute(:force_scraper, true)
			end
		end
		# scoped_collection_records.update_all(reviewed_at: nil)
		# flash[:notice] = t("app.alert.reviewed_at")
	end
	# scoped_collection_action :scoped_collection_destroy


	controller do
		alias_method :update_publisher, :update
		def action_methods
			if session[:customer_ids].present?
				super - ['new'] 
			else
				super
			end
		end  		
    def scoped_collection
      publishers = Publisher.all
      if session[:customer_ids].present?
        return publishers.in_customers(session[:customer_ids])
      end
      publishers
		end	
		def update
			puts "Custom update, luego el normal"
			puts "#{params[:publisher][:person_id]}".red
			person_id = params[:publisher][:person_id]
			if not params[:publisher][:person_id].empty?
				person = Person.find(person_id)
				if not person.publishers.empty?
					publisher = person.publishers.max_by(&:status)
					puts "SET TO MAX STATUS"
					person.name = publisher.name
					person.email1 = publisher.email1
					person.email2 = publisher.email2
					person.phone1 = publisher.phone1
					person.phone2 = publisher.phone2
					person.note = publisher.note
					person.watch = publisher.watch
					person.status = publisher.status
					person.county_raw = publisher.county_raw
					person.state_raw = publisher.state_raw
					person.city_raw = publisher.city_raw
					person.stolen_score = publisher.stolen_score
					person.save
				else
					publisher = Publisher.find(params[:id])
					person.name = publisher.name
					person.email1 = publisher.email1
					person.email2 = publisher.email2
					person.phone1 = publisher.phone1
					person.phone2 = publisher.phone2
					person.note = publisher.note
					person.watch = publisher.watch
					person.status = publisher.status
					person.county_raw = publisher.county_raw
					person.state_raw = publisher.state_raw
					person.city_raw = publisher.city_raw
					person.stolen_score = publisher.stolen_score
					person.save
				end
			end	
			update_publisher
		end	
	end

end
