include ApplicationHelper
ActiveAdmin.register BaseProduct do
  menu parent: 'Configuración', priority: 9, label: proc{ t("active_admin.base_products") }
  permit_params :name, :aliases, :excludes

	
	index do 
		selectable_column
		column :name do |record|
			link_to "#{record.name}", admin_base_product_path(record)
		end
		column :aliases
		column :excludes
		column :updated_at
		actions
	end
	show do
		attributes_table do
		row :id
		row :name
		row :aliases
		row :excludes
		row :created_at
		row :updated_at
		end

		active_admin_comments
	end

	filter :name
	filter :aliases
	filter :excludes
	filter :created_at
	filter :updated_at
	filter :id
	
    controller do 
        def scoped_collection
			base_product = BaseProduct.all
			if session[:customer_ids].present?
				return base_product.in_customers(session[:customer_ids])
			end
			base_product
		end
		def action_methods
			if session[:customer_ids].present?
				super - ['new'] 
			else
				super
			end
		end  		
    end   	

end
