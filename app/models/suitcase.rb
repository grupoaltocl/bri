class Suitcase < ApplicationRecord

  has_and_belongs_to_many :people
  accepts_nested_attributes_for :people, :allow_destroy => true

  has_and_belongs_to_many :publishers
  accepts_nested_attributes_for :publishers, :allow_destroy => true

  has_and_belongs_to_many :publications
  accepts_nested_attributes_for :publications, :allow_destroy => true

  has_and_belongs_to_many :publication_groups
  accepts_nested_attributes_for :publication_groups, :allow_destroy => true

  has_many :case_files
  accepts_nested_attributes_for :case_files, :allow_destroy => true
  
  def to_s
    "#{self.name}"
  end

  def self.all_in_customers(customers)
    # via publisher
    suitcase_ids = Suitcase.joins(publishers: [{ publications: [{ products: [{ brand: :customers }] }] }])
                            .where(customers: { id: customers })
                            .pluck(:id)
    # via person
    suitcase_ids += Suitcase.joins(people: [{ publishers: [{ publications: [{ products: [{ brand: :customers }] }] }] }])
                            .where(customers: { id: customers })
                            .pluck(:id)
    # via publication
    suitcase_ids += Suitcase.joins(publications: [{ products: [{ brand: :customers }] }])
                            .where(customers: { id: customers })
                            .pluck(:id)

    # via publication_group
    suitcase_ids += Suitcase.joins(publication_groups: [{ publications: [{ products: [{ brand: :customers }] }] }])
                    .where(customers: { id: customers })
                    .pluck(:id)

    Suitcase.where('suitcases.id IN (?)', suitcase_ids).uniq
  end
  def self.client_scoped(cids)
    if cids.present?
      return self.all_in_customers(cids)
    end
    self.all
  end
end
