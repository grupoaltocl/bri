class Brand < ApplicationRecord

  belongs_to :categories
  belongs_to :search_engine, dependent: :destroy
  accepts_nested_attributes_for :search_engine, :allow_destroy => true, reject_if: proc { |attributes| attributes['name'].blank? }

  has_and_belongs_to_many :customers
  accepts_nested_attributes_for :customers, :allow_destroy => true
  
  has_many :products
  # has_and_belongs_to_many :publications

  default_scope { order('name ASC') }
  scope :in_customers, ->(customers) do
    joins(:customers).where('customers.id IN (?)', customers).uniq
  end

  def self.client_scoped(cids)
    if cids.present?
      return self.in_customers(cids)
    end
    self.all
  end

  def to_s
    r = ""
    r += "#{self.name}" if not self.name.nil?
    r
  end


  def self.client_scoped(cids)
    if cids.present?
      return self.in_customers cids
    end
    result = self.all
  end


end
