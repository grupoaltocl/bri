class Person < ApplicationRecord

  has_and_belongs_to_many :suitcases
  accepts_nested_attributes_for :suitcases, :allow_destroy => true

  has_many :publishers

  default_scope { order('name ASC').order('last_name ASC').order('first_name ASC') }
  scope :in_suitcases, ->(suitcases) do
    # joining this way because without alias a erros is being thrown
    joins('INNER JOIN `people_suitcases` AS ps ON `ps`.`person_id` = `people`.`id`').where('ps.suitcase_id IN (?)', suitcases).uniq
  end

  scope :in_customers, ->(customers) do
    joins(publishers: [{ publications: :products }])
    .joins('INNER JOIN brands AS brands_in_customer ON products.brand_id = brands_in_customer.id')
    .joins('INNER JOIN `brands_customers` AS bc ON `bc`.`brand_id` = `brands_in_customer`.`id`')
    .where('bc.customer_id IN (?)', customers).uniq
  end

  def self.client_scoped(cids)
    if cids.present?
      return self.in_customers(cids)
    end
    self.all
  end

  STATUS_CLEAN = 0
  STATUS_LOW = 1
  STATUS_MODERATE = 2
  STATUS_HIGH = 3
  STATUS_CRITICAL = 4

  def to_s
    if self.name.blank?
      "#{self.first_name} #{self.last_name}"
    else
      "#{self.name}"
    end
  end

  # Returns the corresponding status choices
  def Person.status_choices
    [Person::STATUS_CLEAN,
      Person::STATUS_LOW,
      Person::STATUS_MODERATE,
      Person::STATUS_HIGH,
      Person::STATUS_CRITICAL].map{ |x| [status_for(x), x]}.to_h
  end

  # Returns the corresponding status
  def Person.status_for(status)
    val = "Nulo"
    case status
      when Person::STATUS_CLEAN
        val = "Nulo"
      when Person::STATUS_LOW
        val = "Bajo"
      when Person::STATUS_MODERATE
        val = "Medio"
      when Person::STATUS_HIGH
        val = "Alto"
      when Person::STATUS_CRITICAL
        val = "Crítico"
    end
    val
  end

  # Returns the corresponding status color
  def Person.status_color(status)
    val = "green"
    case status
      when Person::STATUS_CLEAN
        val = "green"
      when Person::STATUS_LOW
        val = "yellow"
      when Person::STATUS_MODERATE
        val = "blue"
      when Person::STATUS_HIGH
        val = "magenta"
      when Person::STATUS_CRITICAL
        val = "red"
    end
    val
  end  

  def status!(status)
    update_attribute(:status, status)
  end

  def update_publishers
    publisher = self.publishers.max_by(&:status)
    self.name = publisher.name
    self.email1 = publisher.email1
    self.email2 = publisher.email2
    self.phone1 = publisher.phone1
    self.phone2 = publisher.phone2
    self.note = publisher.note
    self.watch = publisher.watch
    self.status = publisher.status
    self.county_raw = publisher.county_raw
    self.state_raw = publisher.state_raw
    self.city_raw = publisher.city_raw
    self.stolen_score = publisher.stolen_score
  end
end
