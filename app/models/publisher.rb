class Publisher < ApplicationRecord

  has_many :publications
  belongs_to :person
  belongs_to :source 

  scope :active, lambda { joins(:publications).where('publications.active = ?', true).uniq }
  
  scope :name_order, -> { 
    order('name ASC')
  }

  scope :default_order, -> { 
    active
      .order_by_default
  }

  scope :order_by_default, -> { 
    order('status DESC')
    .order('stolen_score DESC' )
  }

  scope :active_and_new, lambda { joins(:publications).where('publications.active = ? AND publications.condition = ?', true, 2).uniq }

  scope :in_suitcases, ->(suitcases) do
    # joining this way because without alias a erros is being thrown
    joins('INNER JOIN `publishers_suitcases` AS ps ON `ps`.`publisher_id` = `publishers`.`id`').where('ps.suitcase_id IN (?)', suitcases).uniq
  end

  scope :in_customers, ->(customers) do
    joins(publications: [{ products: [{ brand: :customers }] }])
    .where(customers: { id: customers }).uniq
  end

  scope :in_people, ->(people) do
    # joining this way because without alias a erros is being thrown
    joins('INNER JOIN `publishers_suitcases` AS ps ON `ps`.`publisher_id` = `publishers`.`id`').where('ps.suitcase_id IN (?)', people).uniq
  end


  has_and_belongs_to_many :suitcases
  accepts_nested_attributes_for :suitcases, :allow_destroy => true

  def self.client_scoped(cids)
    if cids.present?
      return self.in_customers(cids)
    end
    self.all
  end
  
  def to_s
    name = "[#{self.source.code}] #{self.name}" 
    name += " #{publisher_code(self)}"
    # name += " #{self.uid[0..5]}" if self.uid.present?
    # name.truncate(50)
  end
  

  def watch!
    update_attribute(:watch, true)
  end

  def unwatch!
    update_attribute(:watch, false)
  end

  def status!(status)
    update_attribute(:status, status)
  end

end
