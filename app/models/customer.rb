class Customer < ApplicationRecord
  has_and_belongs_to_many :brands
  accepts_nested_attributes_for :brands, :allow_destroy => true

  has_many :customer_products
  accepts_nested_attributes_for :customer_products, :allow_destroy => true
  
  has_many :products, through: :customer_products
  accepts_nested_attributes_for :products, :allow_destroy => true

  def to_s
    "#{self.name}"
  end

  scope :in_customers, ->(customers) do
    where('id IN (?)', customers).uniq
  end

  def self.client_scoped(cids)
    if cids.present?
      return self.in_customers(cids)
    end
    self.all
  end

end
