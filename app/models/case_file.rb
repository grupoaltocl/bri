class CaseFile < ApplicationRecord
    
    mount_uploader :file, CaseFilesUploader
    belongs_to :suitcase
end
