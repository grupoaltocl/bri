class SearchEngine < ApplicationRecord
    has_many :brands, dependent: :nullify
    has_many :categories_search_engine
    has_many :category, :through => :categories_search_engine
    has_many :publications
    accepts_nested_attributes_for :brands, :allow_destroy => true, reject_if: proc { |attributes| attributes['name'].blank? }
    accepts_nested_attributes_for :category, :allow_destroy => true, reject_if: proc { |attributes| attributes['name'].blank? }
    attr_accessor :brands

    def to_s
        r = ""
        r += "#{self.name}" if not self.name.nil?
        r
    end
end
