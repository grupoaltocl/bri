include ApplicationHelper
class Publication < ApplicationRecord

  has_and_belongs_to_many :products, -> { uniq }
  
  # has_and_belongs_to_many :brands, -> { uniq }
  accepts_nested_attributes_for :products, :allow_destroy => true

  # belongs_to :product
  belongs_to :publisher
  belongs_to :publication_group
  has_one :search_engine
  
  has_many :publication_snapshots
  accepts_nested_attributes_for :publication_snapshots, :allow_destroy => true

  has_and_belongs_to_many :suitcases
  accepts_nested_attributes_for :suitcases, :allow_destroy => true

  mount_uploader :image, PublicationImageUploader

  ransacker :publication_valid,
    formatter: proc { |selected|
      result = Publication.filter_valid(selected.to_i)
      results = result.map(&:id)
      results = results.present? ? results : nil
    }, splat_params: true do |parent|
    parent.table[:id]
  end

  def self.custom_reviewed_at_search(name)
    # This method should return an ActiveRecord::Relation Object with MyModel objects
    # Put in your complex search logic here
    if name == 0
      objects = Publication.where('reviewed_at IS NULL')
    else
      objects = Publication.where('reviewed_at IS NOT NULL')
    end
    return objects
  end

  
  scope :default_order, -> { 
    active_list
      .valid
      .order_by_default
  }

  scope :order_by_default, -> {
    joins('INNER JOIN `publishers` AS publishers_x ON `publications`.`publisher_id` = `publishers_x`.`id`')
    .order('reviewed_at ASC')
    .order('publishers_x.status DESC')
    .order('publishers_x.stolen_score DESC')
  }

  scope :all_suitcases_equals, ->(input){ self.all_in_suitcases([input]) }
  
  scope :in_person, ->(person) do
    # joining this way because without alias a erros is being thrown
    joins('INNER JOIN `publishers` AS publishers_x ON `publications`.`publisher_id` = `publishers_x`.`id`')
    .where('publishers_x.person_id = ?', person).uniq
  end  

  scope :count_active_by_day, ->() do
    # joining this way because without alias a erros is being thrown
    active.group_by_day(:published_at).count.sort 
  end

  scope :count_valid_by_day, ->() do
    # joining this way because without alias a erros is being thrown
    active.valid.group_by_day(:published_at).count.sort 
  end

  scope :count_all_by_day, ->() do
    # joining this way because without alias a erros is being thrown
    group_by_day(:published_at).count.sort 
  end

  scope :in_suitcases, ->(suitcases) do
    # joining this way because without alias a erros is being thrown
    joins('INNER JOIN `publications_suitcases` AS ps ON `ps`.`publication_id` = `publications`.`id`').where('ps.suitcase_id IN (?)', suitcases).uniq
  end

  def self.all_in_suitcases(suitcases)
    pub_ids = Publication.in_suitcases(suitcases).pluck(:id)

    pub_ids += Publication.where('publications.publication_group_id IN (?)', PublicationGroup.in_suitcases(suitcases).pluck(:id)).pluck(:id)

    publisher_ids = Publisher.in_suitcases(suitcases).pluck(:id)
    publisher_ids += Publisher.in_people(Person.in_suitcases(suitcases).pluck(:id)).pluck(:id)

    pub_ids += Publication.where('publications.publisher_id IN (?)', publisher_ids).pluck(:id)

    Publication.where('publications.id IN (?)', pub_ids).uniq
  end
  
#   scope :in_suitcase, ->(suitcase) do
#     # joining this way because without alias a erros is being thrown
#     query = <<~HEREDOC
#       OUTER JOIN "publications_suitcases" ON "publications_suitcases"."publication_id" = "publications"."id"
#       OUTER JOIN "publishers" ON "publications"."publisher_id" = "publishers"."id"
#       OUTER JOIN "people" ON "people"."id" = "publishers"."person_id"
# HEREDOC
#       joins(query).where("publications_suitcases.suitcase_id = ? OR publishers.suitcase_id = ? OR people.suitcase_id = ?", suitcase, suitcase, suitcase)
#   end

  scope :condition_new, -> { where(condition: ApplicationHelper::USE_STATUS_NEW) }
  scope :inactive, -> { where(active: false).uniq.order_by_default }
  scope :inactive_valid, -> { where(active: false).valid.uniq.order_by_default }
  scope :inactive_expired, -> { where(active: false).expired.uniq.order_by_default }
  scope :active, -> { where(active: true).uniq}
  scope :active_list, -> { where(active: true).uniq.order_by_default}
  scope :active_other, -> { where(other: true).uniq }
  scope :active_multi_prod, -> { where(multi_prod: true).uniq }
  scope :active_new, lambda { self.active.condition_new }
  scope :active_valid, lambda { self.active.valid }
  scope :active_expired, lambda { self.active.expired.order_by_default }

  def self.valid
    sys_scraped_at = Configuration.first.sys_scraped_at
    where('last_seen_at >= ?', sys_scraped_at)
  end

  def self.expired
    sys_scraped_at = Configuration.first.sys_scraped_at
    where('last_seen_at < ? OR last_seen_at IS NULL', sys_scraped_at)
  end

  def review!
    update_attribute(:reviewed_at, Time.zone.now)
  end

  def self.filter_valid(selected)
    sys_scraped_at = Configuration.first.sys_scraped_at
    if selected == 1
      self.where('last_seen_at >= ?', sys_scraped_at);
    elsif selected == 0
      self.where('last_seen_at < ? OR last_seen_at IS NULL', sys_scraped_at);
    else
      nil
    end
  end

  def unreview!
    update_attribute(:reviewed_at, nil)
  end  

  def watch!
    update_attribute(:watch, true)
  end

  def unwatch!
    update_attribute(:watch, false)
  end

  def reviewed?
    not self.reviewed_at.nil?
  end

  def set_status(status)
    update_attribute(:status, status)
  end

  def multi!
    update_attribute(:multi_prod, true)
  end

  def unmulti!
    update_attribute(:multi_prod, false)
  end

  def manual!
    update_attribute(:manual_match, true)
  end
  
  def set_condition(condition)
    update_attribute(:condition, condition)
  end

  def active=(x)
    if self.publication_group.nil?
      super(x)
    else
      super(self.active)
    end
  end

  def activate!
    update_column(:active, true)
  end

  def deactivate!
    update_column(:active, false)
  end

  def multi?
    not self.multi_prod.nil?
  end
  
  def watched?
    not self.watch.nil?
  end

  def products_changed?
    hsh = products.map { |x| x.id}.hash
    @products_hash.present? && (@products_hash != hsh)
  end
  
  def remember_products_hash
    @products_hash = products.map { |x| x.id}.hash
  end

  def to_s
    "#{self.name}"
  end

  def recategorize
    category = Category.joins(brands: {products: :publications}).where(publications: {id: self.id}).first
    puts "Before:".yellow
    puts "\t Category: #{category.name}".yellow
    before_products = ""
    before_brands = ""
    self.products.each do |prod|
      before_products << "#{prod.name},"
      before_brands << "#{prod.brand.name}," unless before_brands.include? "#{prod.brand.name},"
    end
    puts "\t Brand: #{before_brands}".yellow
    puts "\t Products: #{before_products}".yellow
    brand_in_category = set_category(category)
    publication_name = ''
    publication_name = self.name.strip.downcase unless self.name.nil?
    publication_description = ''
    publication_description = self.description.strip.downcase unless self.description.nil?
    checked_with_brand = check_with_brand(publication_name, publication_description, brand_in_category, category)
    pub_products = checked_with_brand[:pub_products]
    pub_brands = checked_with_brand[:pub_brands]
    has_products = set_products(self, pub_products, pub_brands, category)
    puts "\n"
    puts "After: ".red
    puts "Brand with aliases #{pub_brands}".red
    puts "products with aliases #{pub_products}".red
    if has_products
      after_products = ""
      after_brands = ""
      self.products.each do |prod|
        after_products << "#{prod.name},"
        after_brands << "#{prod.brand.name}," unless after_brands.include? "#{prod.brand.name},"
      end
      puts "\t Brand: #{after_brands}".red
      puts "\t Products: #{after_products}".red

      Notification.new(name: "Recategorización",
              description: "Antes: \n\tRubro: #{category.name} \n\tMarca: #{before_brands} \n\tProductos: #{before_products}\n || Despues: \n\tRubro: #{category.name} \n\tMarca: #{after_brands} \n\tProductos: #{after_products}",
              process_uuid: "ID publicación: #{self.id}").save
    else
      Notification.new(name: "Recategorización",
              description: "Antes: \n \tRubro: #{category.name} \n\tMarca: #{before_brands} \n\tProductos: #{before_products}\n|| Despues: Nada cambio",
              process_uuid: "ID publicación: #{self.id}").save
      puts "Nothing changed".blue
    end
  end
end
