class PublicationSnapshot < ApplicationRecord
  belongs_to :publication

  mount_uploader :snapshot, SnapshotUploader

end
