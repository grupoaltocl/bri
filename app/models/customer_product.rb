class CustomerProduct < ApplicationRecord
  belongs_to :customer
  belongs_to :product

  def to_s
    "<#{self.customer.to_s} - #{self.product.to_s}> #{self.is_priority} "
  end
end
