class BaseProduct < ApplicationRecord
    has_many :products
    # accepts_nested_attributes_for :products, :allow_destroy => true
    scope :in_customers, ->(customers) do
        joins(:products)
        .joins('INNER JOIN brands AS brands_in_customer ON products.brand_id = brands_in_customer.id')
        .joins('INNER JOIN `brands_customers` AS bc ON `bc`.`brand_id` = `brands_in_customer`.`id`').where('bc.customer_id IN (?)', customers).uniq
    end 
    def to_s
        r = ""
        r += "#{self.name}" if not self.name.nil?
        r
    end    

    def self.client_scoped(cids)
        if cids.present?
          return self.in_customers(cids)
        end
        self.all
      end
end
