class Category < ApplicationRecord
    has_many :brands
    has_many :categories_search_engine
    has_many :search_engine, :through => :categories_search_engine
    accepts_nested_attributes_for :brands, :allow_destroy => true, reject_if: :all_blank
    accepts_nested_attributes_for :search_engine, :allow_destroy => true, reject_if: proc { |attributes| attributes['name'].blank? }

    scope :in_customers, ->(customers) do
        joins(brands: :customers).where('customers.id IN (?)', customers).uniq
    end

    def to_s
        r = ""
        r += "#{self.name}" if not self.name.nil?
        r
    end
end
