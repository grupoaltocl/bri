class Product < ApplicationRecord
  belongs_to :brand
  belongs_to :base_product
  has_and_belongs_to_many :publications
  accepts_nested_attributes_for :publications, :allow_destroy => true
  
  has_many :customer_products
  has_many :customers, through: :customer_products

  scope :in_group, ->(group) do
    joins(:publications).where('publications.publication_group_id = ?', group).uniq
  end

  scope :in_customers, ->(customers) do
    joins(:brand)
    .joins('INNER JOIN `brands_customers` AS bc ON `bc`.`brand_id` = `brands`.`id`')
    .where('bc.customer_id IN (?)', customers)
    .uniq
  end

  def self.client_scoped(cids)
    if cids.present?
      return self.in_customers(cids)
    end
    self.all
  end
  
  STATUS_CLEAN = 0
  STATUS_LOW = 1
  STATUS_MODERATE = 2
  STATUS_HIGH = 3
  STATUS_CRITICAL = 4

  def to_s
    r = "#{self.name}"
    r += " #{self.brand.to_s}" if not self.brand.nil? && self.filter_by_brand
    r
  end

  # Returns the corresponding status choices
  def Product.status_choices
    [Product::STATUS_CLEAN,
      Product::STATUS_LOW,
      Product::STATUS_MODERATE,
      Product::STATUS_HIGH,
      Product::STATUS_CRITICAL].map{ |x| [status_for(x), x]}.to_h
  end

  # Returns the corresponding status
  def Product.status_for(status)
    val = "Limpio"
    case status
      when Product::STATUS_CLEAN
        val = "Limpio"
      when Product::STATUS_LOW
        val = "Bajo"
      when Product::STATUS_MODERATE
        val = "Moderado"
      when Product::STATUS_HIGH
        val = "Alto"
      when Product::STATUS_CRITICAL
        val = "Crítico"
    end
    val
  end

end
