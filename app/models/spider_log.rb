class Spider_log
  def self.debug(message=nil)
    @spider_log ||= Logger.new("#{Rails.root}/log/spider.log")
    @spider_log.debug(message) unless message.nil?
  end
end