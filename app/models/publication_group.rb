class PublicationGroup < ApplicationRecord
  belongs_to :publisher
  has_many :publications
  accepts_nested_attributes_for :publications

  has_and_belongs_to_many :suitcases
  accepts_nested_attributes_for :suitcases, :allow_destroy => true

  default_scope { order('name ASC') }
  scope :in_suitcases, ->(suitcases) do
    # joining this way because without alias a erros is being thrown
    joins('INNER JOIN `publication_groups_suitcases` AS ps ON `ps`.`publication_group_id` = `publication_groups`.`id`').where('ps.suitcase_id IN (?)', suitcases).uniq
  end
  scope :in_customers, ->(customers) do 
    joins(publications: [{ products: [{ brand: :customers }] }])
    .where(customers: { id: customers })
    .uniq
  end

  def self.client_scoped(cids)
    if cids.present?
      return self.in_customers(cids)
    end
    self.all
  end

  def to_s
    "#{self.name}"
  end

  def review!
    update_attribute(:reviewed_at, Time.zone.now)
  end

  def unreview!
    update_attribute(:reviewed_at, nil)
  end  

  def watch!
    update_attribute(:watch, true)
  end

  def unwatch!
    update_attribute(:watch, false)
  end

  def reviewed?
    not self.reviewed_at.nil?
  end

  def set_status(status)
    update_attribute(:status, status)
  end

  def multi!
    update_attribute(:multi_prod, true)
  end

  def unmulti!
    update_attribute(:multi_prod, false)
  end
  
  def watched?
    not self.watch.nil?
  end

  def update_publications_to_group
    puts "Desactivando todas".red
    self.publications.each do |pub|
      pub.deactivate!
    end
    publication = self.publications.max_by(&:published_at)
    puts "Activando la más nueva #{publication.id}".red
    publication.activate!
  end
  
end
