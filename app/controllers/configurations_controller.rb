class ConfigurationsController < InheritedResources::Base

  private

    def configuration_params
      params.require(:configuration).permit(:factor_a, :factor_e, :factor_f, :factor_g)
    end
end

