class PublicationSnapshotsController < InheritedResources::Base

  private

    def publication_snapshot_params
      params.require(:publication_snapshot).permit()
    end
end

