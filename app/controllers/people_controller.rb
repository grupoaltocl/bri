class PeopleController < InheritedResources::Base

  private

    def person_params
      params.require(:person).permit(:uid, :first_name, :last_name, :email, :phone, :note, :suitcase_id, :watch, :status)
    end
end

