class SuitcasesController < InheritedResources::Base

  private

    def case_params
      params.require(:suitcase).permit()
    end
end

