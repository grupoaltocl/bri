class BaseProductsController < InheritedResources::Base

  private

    def base_product_params
      params.require(:base_product).permit(:name, :aliases, :excludes)
    end
end

