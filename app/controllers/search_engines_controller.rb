class SearchEnginesController < InheritedResources::Base

  private

    def search_engine_params
      params.require(:search_engine).permit(:name, :search_keywords, :search_excludes)
    end
end

