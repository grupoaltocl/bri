class Api::V1::BrandsController < Api::V1::BaseController
  def show
    user = Brand.find(params[:id])

    render(json: Api::V1::BrandSerializer.new(user).to_json)
  end
end