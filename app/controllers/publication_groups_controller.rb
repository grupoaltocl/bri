class PublicationGroupsController < InheritedResources::Base

  private

    def publication_group_params
      params.require(:publication_group).permit(:name, :description)
    end
end

