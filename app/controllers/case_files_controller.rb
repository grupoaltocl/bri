class CaseFilesController < InheritedResources::Base

  private

    def case_file_params
      params.require(:case_file).permit(:file, :description)
    end
end

