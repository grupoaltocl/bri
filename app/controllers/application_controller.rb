class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method   :client_scoped,
                  :product_quantity_pie_chart,
                  :product_amount_pie_chart,
                  :brand_quantity_pie_chart,
                  :brand_amount_pie_chart,
                  :client_scoped_brands,
                  :client_scoped_products,
                  :category_quantity_pie_chart,
                  :category_amount_pie_chart
  include SessionsHelper

  def client_scoped(publications)
    ns = 'cs'
    publications = publications
                  # .joins(:products).uniq
                      # .joins("INNER JOIN brands_publications AS brands_publications_#{ns} ON brands_publications_#{ns}.publication_id = publications.id")
                      # .joins("INNER JOIN brands AS brands_#{ns} ON brands_publications_#{ns}.brand_id = brands_#{ns}.id")

    if session[:customer_ids].present?
      return publications.joins(products: [{ brand: :customers }])
                          .where(customers: { id: session[:customer_ids] })
    end
    publications
  end

  def client_scoped_brands(brands)
    ns = 'csb'
    if session[:customer_ids].present?
      return brands.joins("INNER JOIN brands_customers AS brands_customers_#{ns} ON brands.id = brands_customers_#{ns}.brand_id")
                  .where("brands_customers_#{ns}.customer_id IN (?)", session[:customer_ids])
    end
    return brands
  end

  def client_scoped_products(products)
    ns = 'csp'
    if session[:customer_ids].present?
      return products
        .joins("INNER JOIN brands AS brands_#{ns} ON brands_#{ns}.id = products.brand_id")
        .joins("INNER JOIN brands_customers AS brands_customers_#{ns} ON brands_#{ns}.id = brands_customers_#{ns}.brand_id")
        .where("brands_customers_#{ns}.customer_id IN (?)", session[:customer_ids])
    end
    return products
  end

def product_quantity_pie_chart(pubs)
    
    if session[:customer_ids].present?
      subquery = pubs.active
        .joins(products: [{brand: :customers}])
        .joins(:products)
        .joins("LEFT OUTER JOIN base_products ON products.base_product_id = base_products.id")
        .where(customers: {id: session[:customer_ids] })
        .select('publications.id pub_id', 'base_products.id base_product_id', 'AVG(publications.price) price').group(['pub_id', 'base_product_id']).to_sql
    else
      subquery = pubs.active
        .joins(:products)
        .joins("LEFT OUTER JOIN base_products ON products.base_product_id = base_products.id")
        .select('publications.id pub_id', 'base_products.id base_product_id', 'AVG(publications.price) price').group(['pub_id', 'base_product_id']).to_sql

    end

    _chart = ActiveRecord::Base.connection.execute("SELECT base_product_id, COUNT(pub_id) FROM (#{subquery}) T GROUP BY base_product_id")
    chart = []
    _others_count = 0
    _chart.each do |x| 
      if x[0].nil? 
        _others_count = x[1]
      else
        chart << [BaseProduct.find(x[0]).to_s, x[1].to_i]
      end
    end

    color_hash = {}
    sort_by_name = chart.sort
    sort_by_name.each_with_index do |item,index|
      size = $colors.size
      color_hash[item[0]] = $colors[(index%size)]
    end
    sort_by_value = chart.sort do |a,b|
      b[1] <=> a[1]
    end
    slices = []
    sort_by_value.each do |item|
      val = { color: color_hash[item[0]] }
      slices.append(val)
    end
    slices.append({ color: $colors[ (sort_by_value.size + 1)% $colors.size ]})
    sort_by_value << ["Otros", _others_count] if _others_count
    sort_by_value = [["Sin resultados", 1]] if chart.size.zero?
    return {'chart': sort_by_value, 'slices': slices}
  end

  def product_amount_pie_chart(pubs)

    if session[:customer_ids].present?
      subquery = pubs.active
        .joins(products: [{brand: :customers}])
        .joins(:products)
        .joins("LEFT OUTER JOIN base_products ON products.base_product_id = base_products.id")
        .where(customers: {id: session[:customer_ids] })
        .select('publications.id pub_id', 'base_products.id base_product_id', 'AVG(publications.price) price').group(['pub_id', 'base_product_id']).to_sql
    else
      subquery = pubs.active
        .joins(:products)
        .joins("LEFT OUTER JOIN base_products ON products.base_product_id = base_products.id")
        .select('publications.id pub_id', 'base_products.id base_product_id', 'AVG(publications.price) price').group(['pub_id', 'base_product_id']).to_sql
    end

    _chart = ActiveRecord::Base.connection.execute("SELECT base_product_id, SUM(price) FROM (#{subquery}) T GROUP BY base_product_id")
    chart = []
    _others_count = 0
    _chart.each do |x| 
      if x[0].nil? 
        _others_count = x[1].to_i
      else
        chart << [BaseProduct.find(x[0]).to_s, x[1].to_i]
      end
    end
    
    color_hash = {}
    sort_by_name = chart.sort
    sort_by_name.each_with_index do |item,index|
      size = $colors.size
      color_hash[item[0]] = $colors[(index%size)]
    end
    
    sort_by_value = chart.sort do |a,b|
      b[1] <=> a[1]
    end
    
    slices = []
    sort_by_value.each do |item|
      val = { color: color_hash[item[0]] }
      slices.append(val)
    end
    slices.append({ color: $colors[ (sort_by_value.size + 1)% $colors.size ]})

    
    sort_by_value << ["Otros", _others_count] if _others_count
    sort_by_value = [["Sin resultados", 1]] if chart.size.zero?
    return {'chart': sort_by_value, 'slices': slices}

  end
  
  def brand_quantity_pie_chart(pubs)
    slices = []
    if session[:customer_ids].present?
      subquery = pubs.active
        .joins(products: [{brand: :customers}])
        .where(customers: {id: session[:customer_ids] })
        .select('publications.id pub_id', 'brands.id brand_id', 'AVG(publications.price) price').group(['pub_id', 'brand_id']).to_sql
    else
      subquery = pubs.active
        .joins(products: :brand)
        .select('publications.id pub_id', 'brands.id brand_id', 'AVG(publications.price) price').group(['pub_id', 'brand_id']).to_sql
    end

    chart = ActiveRecord::Base.connection.execute("SELECT brand_id, COUNT(pub_id) FROM (#{subquery}) T GROUP BY brand_id")
    chart = chart.map { |x| [Brand.find(x[0]).to_s, x[1].to_i] }
    color_hash = {}
    sort_by_name = chart.sort
    sort_by_name.each_with_index do |item,index|
      size = $colors.size
      color_hash[item[0]] = $colors[(index%size)]
    end
    
    sort_by_value = chart.sort do |a,b|
      b[1] <=> a[1]
    end
    
    slices = []
    sort_by_value.each do |item|
      val = { color: color_hash[item[0]] }
      slices.append(val)
    end
    slices.append({ color: $colors[ (sort_by_value.size + 1)% $colors.size ]})
    sort_by_value = [["Sin resultados", 1]] if chart.size.zero?
    return {'chart': sort_by_value, 'slices': slices }
  end

  def brand_amount_pie_chart(pubs)
    slices = []
    if session[:customer_ids].present?
      subquery = pubs.active
        .joins(products: [{brand: :customers}])
        .where(customers: {id: session[:customer_ids] })
        .select('publications.id pub_id', 'brands.id brand_id', 'AVG(publications.price) price').group(['pub_id', 'brand_id']).to_sql
    else
      subquery = pubs.active
        .joins(products: :brand)
        .select('publications.id pub_id', 'brands.id brand_id', 'AVG(publications.price) price').group(['pub_id', 'brand_id']).to_sql
    end

    chart = ActiveRecord::Base.connection.execute("SELECT brand_id, SUM(price) FROM (#{subquery}) T GROUP BY brand_id")
    chart = chart.map { |x| [Brand.find(x[0]).to_s, x[1].to_i] }
    
    color_hash = {}
    sort_by_name = chart.sort
    sort_by_name.each_with_index do |item,index|
      size = $colors.size
      color_hash[item[0]] = $colors[(index%size)]
    end
    
    sort_by_value = chart.sort do |a,b|
      b[1] <=> a[1]
    end
    
    slices = []
    sort_by_value.each do |item|
      val = { color: color_hash[item[0]] }
      slices.append(val)
    end
    slices.append({ color: $colors[ (sort_by_value.size + 1)% $colors.size ]})
    count_sum = chart.map { |x| x[1] }.reduce(:+)
    count_sum = 0 if count_sum.nil?
    sort_by_value = [["Sin resultados", 1]] if chart.size.zero?
    return {'chart': sort_by_value, 'slices': slices }
  end

  def category_quantity_pie_chart(pubs)
    slices = []
    if session[:customer_ids].present?
      subquery = pubs.active
        .joins(products: [{brand: :customers}])
        .joins("INNER JOIN categories ON brands.category_id = categories.id")
        .where(customers: {id: session[:customer_ids] })
        .select('publications.id pub_id', 'categories.id cat_id', 'AVG(publications.price) price').group(['pub_id', 'cat_id']).to_sql
    else
      puts "RUNNING DEBUG .."
      subquery = pubs.active
        .joins(products: :brand)
        .joins("INNER JOIN categories ON brands.category_id = categories.id")
        .select('publications.id pub_id', 'categories.id cat_id', 'AVG(publications.price) price').group(['pub_id', 'cat_id']).to_sql
    end
    chart = ActiveRecord::Base.connection.execute("SELECT cat_id, COUNT(pub_id) FROM (#{subquery}) T GROUP BY cat_id")
    chart = chart.map { |x| [Category.find(x[0]).to_s, x[1].to_i] }
    color_hash = {}
    sort_by_name = chart.sort
    sort_by_name.each_with_index do |item,index|
      size = $colors.size
      color_hash[item[0]] = $colors[(index%size)]
    end
    
    sort_by_value = chart.sort do |a,b|
      b[1] <=> a[1]
    end
    
    slices = []
    sort_by_value.each do |item|
      val = { color: color_hash[item[0]] }
      slices.append(val)
    end
    slices.append({ color: $colors[ (sort_by_value.size + 1)% $colors.size ]})
    count_sum = chart.map { |x| x[1] }.reduce(:+)
    count_sum = 0 if count_sum.nil?
    sort_by_value = [["Sin resultados", 1]] if chart.size.zero?
    return {'chart': sort_by_value, 'slices': slices }
  end

  def category_amount_pie_chart(pubs)
    slices = []
    if session[:customer_ids].present?
      subquery = pubs.active
        .joins(products: [{brand: :customers}])
        .joins("INNER JOIN categories ON brands.category_id = categories.id")
        .where(customers: {id: session[:customer_ids] })
        .select('publications.id pub_id', 'categories.id cat_id', 'AVG(publications.price) price').group(['pub_id', 'cat_id']).to_sql
    else
      subquery = pubs.active
        .joins(products: :brand)
        .joins("INNER JOIN categories ON brands.category_id = categories.id")
        .select('publications.id pub_id', 'categories.id cat_id', 'AVG(publications.price) price').group(['pub_id', 'cat_id']).to_sql
    end
    chart = ActiveRecord::Base.connection.execute("SELECT cat_id, SUM(price) FROM (#{subquery}) T GROUP BY cat_id")
    chart = chart.map { |x| [Category.find(x[0]).to_s, x[1].to_i] }
    color_hash = {}
    sort_by_name = chart.sort
    sort_by_name.each_with_index do |item,index|
      size = $colors.size
      color_hash[item[0]] = $colors[(index%size)]
    end
    
    sort_by_value = chart.sort do |a,b|
      b[1] <=> a[1]
    end
    
    slices = []
    sort_by_value.each do |item|
      val = { color: color_hash[item[0]] }
      slices.append(val)
    end
    slices.append({ color: $colors[ (sort_by_value.size + 1)% $colors.size ]})
    count_sum = chart.map { |x| x[1] }.reduce(:+)
    count_sum = 0 if count_sum.nil?
    sort_by_value = [["Sin resultados", 1]] if chart.size.zero?
    return {'chart': sort_by_value, 'slices': slices }
  end

end
