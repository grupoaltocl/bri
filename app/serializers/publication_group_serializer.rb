class PublicationGroupSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
end
