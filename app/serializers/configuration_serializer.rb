class ConfigurationSerializer < ActiveModel::Serializer
  attributes :id, :factor_a, :factor_e, :factor_f, :factor_g
end
