class PersonSerializer < ActiveModel::Serializer
  attributes :id, :uid, :first_name, :last_name, :email, :phone, :note, :status
  has_one :suitcase
end
