class CaseFileSerializer < ActiveModel::Serializer
  attributes :id, :file, :description
end
