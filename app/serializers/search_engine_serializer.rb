class SearchEngineSerializer < ActiveModel::Serializer
  attributes :id, :name, :search_keywords, :search_excludes
end
