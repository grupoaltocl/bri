class BaseProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :aliases, :excludes
end
