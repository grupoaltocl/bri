$( function() {
  var cy = cytoscape({
    container: document.getElementById('cy'),
elements: [ // list of graph elements to start with
{ // node a
  data: { id: 'a' }
},
{ // node b
  data: { id: 'b' }
},
{ // edge ab
  data: { id: 'ab', source: 'a', target: 'b' }
}
],

style: [ // the stylesheet for the graph
{
  selector: 'node',
  style: {
    'background-color': '#666',
    'label': 'data(id)'
  }
},

{
  selector: 'edge',
  style: {
    'width': 3,
    'line-color': '#ccc',
    'target-arrow-color': '#ccc',
    'target-arrow-shape': 'triangle'
  }
}
],

layout: {
  name: 'grid',
  rows: 1
}
});


var dialog, form,

// From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
name = $( "#name" ),
email = $( "#email" ),
password = $( "#password" ),
allFields = $( [] ).add( name ).add( email ).add( password ),
tips = $( ".validateTips" );

function updateTips( t ) {
  tips
  .text( t )
  .addClass( "ui-state-highlight" );
  setTimeout(function() {
    tips.removeClass( "ui-state-highlight", 1500 );
  }, 500 );
}

function checkLength( o, n, min, max ) {
  if ( o.val().length > max || o.val().length < min ) {
    o.addClass( "ui-state-error" );
    updateTips( "Length of " + n + " must be between " +
      min + " and " + max + "." );
    return false;
  } else {
    return true;
  }
}

function checkRegexp( o, regexp, n ) {
  if ( !( regexp.test( o.val() ) ) ) {
    o.addClass( "ui-state-error" );
    updateTips( n );
    return false;
  } else {
    return true;
  }
}

function addUser() {
  var valid = true;
  allFields.removeClass( "ui-state-error" );

  // valid = valid && checkLength( name, "username", 3, 16 );
  // valid = valid && checkLength( email, "email", 6, 80 );
  // valid = valid && checkLength( password, "password", 5, 16 );

  // valid = valid && checkRegexp( name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
  // valid = valid && checkRegexp( email, emailRegex, "eg. ui@jquery.com" );
  // valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );

  var val = dialog.dialog( "option", "source-node-id" );
  var formVal = $( "#node_value" ).val();
  if ( val !== formVal) {
    var eles = cy.add([
      { group: "nodes", data: { id: formVal }, position: { x: 200, y: 200 } },
      { group: "edges", data: { id: val+"-"+formVal, source: val, target: formVal } }
    ]);

    dialog.dialog( "close" );
    return valid;
  }
  var formUrl = $( "#node_url" ).val();
  if (formUrl) {
    $.get( formUrl, function( data ) {
      console.log("data", data);
      var result = JSON.parse(data);
      var nodeValue = result.node.value;
      var eles = cy.add([
        { group: "nodes", data: { id: nodeValue }, position: { x: 200, y: 200 } },
        { group: "edges", data: { id: val+"-"+formVal, source: val, target: nodeValue } }
      ]);
    });
    dialog.dialog( "close" );
    return valid;
  }

  return valid;
}

dialog = $( "#dialog-form" ).dialog({
  autoOpen: false,
  height: 400,
  width: 350,
  modal: true,
  buttons: {
    "Create": addUser,
    Cancel: function() {
      dialog.dialog( "close" );
    }
  },
  close: function() {
    form[ 0 ].reset();
    allFields.removeClass( "ui-state-error" );
  },
  open: function(event, ui) {
    var val = dialog.dialog( "option", "source-node-id" );
    $( "#node_value" ).val(val);
  }
});

form = dialog.find( "form" ).on( "submit", function( event ) {
  event.preventDefault();
  addUser();
});

$( "#create-user" ).button().on( "click", function() {
  dialog.dialog( "open" );
});


cy.on('tap', 'node', { foo: 'bar' }, function(evt) {
  console.log( evt.data.foo ); // 'bar'
  var node = evt.cyTarget;
  console.log( 'tapped ' + node.id() );
  dialog.dialog( "option", "source-node-id", node.id() );
  dialog.dialog( "open" );
});

cy.on('tap', 'edge', { foo: 'bar' }, function(evt) {
  console.log( evt.data.foo ); // 'bar'
  var node = evt.cyTarget;
  console.log( 'tapped edge ' + node.id() );
});

} );