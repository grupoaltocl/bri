def hsv2rgb(hsv)
  h = hsv[:hue]
  s = hsv[:sat]
  v = hsv[:val]
  rgb, i, data = []
  if s == 0 then
    rgb = [v,v,v]
  else
    h = h / 60
    i = h.floor
    data = [v*(1-s), v*(1-s*(h-i)), v*(1-s*(1-(h-i)))]
    case i
      when 0
        rgb = [v, data[2], data[0]]
      when 1
        rgb = [data[1], v, data[0]]
      when 2
        rgb = [data[0], v, data[2]]
      when 3
        rgb = [data[0], data[1], v]
      when 4
        rgb = [data[2], data[0], v]
      else
        rgb = [v, data[0], data[1]]
    end
  end
  result = rgb.map do |x|
    "0" + (x*255).round.to_s(16).slice(-2)
  end
  '#' + result.join('')
end
grades = { "Jane Doe" => 10, "Jim Doe" => 6 }

input = {
  :hue => 0,
  :sat => 100,
  :val => 50
}
puts hsv2rgb(input)