require 'rubygems'
require 'date'
require 'securerandom'
$configuration = Configuration.first

$configuration.has_label1_ss ||= nil
$configuration.has_label2_ss ||= nil
$configuration.many_updates_ss ||= 2
$configuration.publisher_freq_ss ||= 2
$configuration.factor_a ||= 1.0
$configuration.factor_b ||= 1.0
$configuration.factor_c ||= 1.0
$configuration.factor_d ||= 1.0
$configuration.factor_e ||= 1.0
$configuration.factor_g ||= 1.0
$configuration.disc_limit1 ||= 1.0
$configuration.disc_limit2 ||= 1.0
$configuration.disc_limit3 ||= 1.0

$has_label1 = []
if $configuration.has_label1_ss && $configuration.has_label1_ss.strip.size > 0
    $has_label1 += $configuration.has_label1_ss.split(',').map { |x| x.strip.downcase }
end
$has_label2 = []
if $configuration.has_label2_ss && $configuration.has_label2_ss.strip.size > 0
    $has_label2 += $configuration.has_label2_ss.split(',').map { |x| x.strip.downcase }
end

def calculate_pdesc(product, publication, category)
    pprod = product.price
    ppub = publication.price    
    if pprod.nil? or ppub.nil?
        return 0.0
    end
    pdisc = 1.0*(pprod-ppub)/pprod

    if pdisc < 0
        return 0.0
    else
        pdisc_t1 = pdisc >= category.disc_limit1 ? 1 : 0
        pdisc_t2 = pdisc >= category.disc_limit2 ? 1 : 0
        pdisc_t3 = pdisc > category.disc_limit3 ? 1 : 0
        temp_new = (pdisc_t1.to_f + pdisc_t2.to_f + pdisc_t3.to_f)/3
        return temp_new.to_f
    end
end

def calculate_has_label1(publication, category)
    if category.has_label1_ss.blank?
        return false
    end
    $has_label1.each do |x|
        if publication.name.present? && publication.name.downcase.include?(x.downcase)
            return true
        end
        if publication.description.present? && publication.description.downcase.include?(x.downcase)
            return true
        end
    end
    return false
end

def calculate_has_label2(publication, category)
    if category.has_label2_ss.blank?
        return false
    end
    $has_label2.each do |x|
        if publication.name.present? && publication.name.downcase.include?(x.downcase)
            return true
        end
        if publication.description.present? && publication.description.downcase.include?(x.downcase)
            return true
        end
    end
    return false
end

def calculate_publisher_freq(publication)
    publisher = publication.publisher
    if publisher.nil?
        return 0
    end
    date_now = DateTime.now
    publisher.publications.active.where(['published_at > ?', DateTime.now - 2.month]).count
end
uuid = SecureRandom.uuid
puts "Calculando stolen score para las publicaciones".red
total_pubs = Publication.active.count
puts "Se calculará stolen score para #{total_pubs} publicaciones".red
begin
    Notification.new(name: "Stolen Score",
                    description: "Iniciando Stolen score para #{total_pubs} publicaciones",
                    process_uuid: uuid).save
    total = total_pubs/10000
    (1..(total+1)).each do |i|
        puts "EN PÁGINA #{i}".yellow
        Publication.active.valid.page(i).per(10000).each do |publication|
            
            if publication.condition != ApplicationHelper::USE_STATUS_NEW
                publication.stolen_score = 0
            elsif publication.multi_prod
                publication.stolen_score = 0
            else
                product = publication.products.first
                if product.nil?
                    publication.stolen_score = 0
                else
                    category = Category.joins(brands: { products: :publications })
                                    .where(publications: { id: publication.id })
                                    .first
                    pdesc = calculate_pdesc(product, publication, category)
                    has_label1 = calculate_has_label1(publication, category) ? 1 : 0
                    has_label2 = calculate_has_label2(publication, category) ? 1 : 0
                    many_updates = publication.publication_snapshots.count >= category.many_updates_ss ? 1 : 0
                    publisher_freq = calculate_publisher_freq(publication) > 0 ? 1 : 0
                    is_priority = product.customers.count > 0 ? 1 : 0
                    stolen_score =  pdesc * category.factor_a +
                                    has_label1 * category.factor_b +
                                    has_label2 * category.factor_c +
                                    many_updates * category.factor_d +
                                    publisher_freq * category.factor_e +
                                    is_priority * category.factor_g
                    puts "For publication #{publication.name} with #{publication.id} in category #{category.name}".red
                    puts "METRICS pdesc: #{pdesc}, has_label:#{has_label1}, has_label2:#{has_label2}, many_updates:#{many_updates}, publisher_freq:#{publisher_freq}, priority:#{is_priority} >>> #{stolen_score.to_s.red}".purple
                    publication.stolen_score = stolen_score
                    puts "Publication Stolen score is #{stolen_score}".blue
                end
                puts "Stolen score for '#{publication.name}(#{publication.id})'is #{publication.stolen_score.to_s.yellow}".green
            end
            publication.save
        end
    end
rescue
    puts "Error in stolen score publication"
    Notification.new(name: "Stolen Score",
                description: "Error en calculo de Stolen score para publicaciones",
                process_uuid: uuid).save
end

puts "Calculando stolen score para los publicadores".red
total_publishers = Publisher.active.count
puts "Se calculará stolen score para #{total_publishers} publicaciones".red
begin
    Notification.new(name: "Stolen Score",
                    description: "Iniciando Stolen score para #{total_publishers} publicadores",
                    process_uuid: uuid).save
    total = total_publishers/10000
    (1..(total+1)).each do |i|
        puts "EN PÁGINA #{i}".yellow
        Publisher.active.page(i).per(10000).each do |publisher|
            
                avg_score = publisher.publications.active.average(:stolen_score)
                publisher.stolen_score = avg_score
                publisher.save
                puts "Stolen Score for '#{publisher.name}(#{publisher.id})' is #{avg_score.to_s.yellow}".green      

        end
    end
rescue
    puts "Error in stolen score publisher "
    Notification.new(name: "Stolen Score",
            description: "Error en calculo de Stolen score para publicadores",
            process_uuid: uuid).save
end

begin
    puts "Calculando stolen score para los Personas".red
    Notification.new(name: "Stolen Score",
                description: "Iniciando Stolen score para personas",
                process_uuid: uuid).save
    Person.all.each do |person|
        
        person_score_sum = 0
        sum_score = 0
        count_score = 0
        person.publishers.each do |publisher|
            sum_score += publisher.publications.active.sum(:stolen_score)
            count_score += publisher.publications.active.count(:stolen_score)
        end
        avg_score = count_score == 0 ? 0 : 1.0*sum_score/count_score
        puts "sum,count,avg: #{sum_score},#{count_score},#{avg_score},".red
        person.stolen_score = avg_score
        person.save

    end
rescue
    puts "error in stolen score people"
    Notification.new(name: "Stolen Score",
        description: "Error en calculo de Stolen score para personas",
        process_uuid: uuid).save
end

Notification.new(name: "Stolen Score",
                description: "Stolen score finalizado con exito",
                process_uuid: uuid).save

