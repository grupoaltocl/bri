Publication.left_outer_joins(:products).where(products: {id: nil}).each do |publication|
  if publication.search_engine_id.nil?
    publication.delete
  else
    puts "tienen s_e"
    s_e = SearchEngine.find(publication.search_engine_id)
    if not s_e.category.empty?
      se_cat = s_e.category.first
      puts "FIND BY CATEGORY #{se_cat}"
      brand = Brand.find_or_initialize_by(name: se_cat.name.chomp('Otros').strip + ' Otros')
      product = Product.find_or_initialize_by(name: brand.name.chomp('Otros').strip + ' Otros')
      puts "WILL BE ASSIGN TO THE GENERIC PRODUCT: #{product.name} OF BRAND: #{brand.name}"
      brand.generic = true
      brand.save
      brand.products << product
      brand.save
      product.filter_by_brand = true
      product.generic = true
      product.save
      publication.products << product
      
    else
      brand = Brand.where(search_engine_id: s_e.id).first
      puts "FIND BY BRAND #{brand}"
      product = Product.find_or_initialize_by(name: brand.name.chomp('Otros').strip + ' Otros')
      puts "WILL BE ASSIGN TO THE GENERIC PRODUCT: #{product.name} OF BRAND: #{brand.name}"
      # brand.products << product
      product.filter_by_brand = true
      product.generic = true
      product.save
      publication.products << product
    end
  end
end