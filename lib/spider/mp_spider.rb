    require 'rubygems'
    require 'meli'
    # require './meli_cli'
    require './lib/spider/base_spider'
    require './lib/spider/spider_model'

    class MPSpider < BaseSpider
    @source = Source.find_or_create_by(name: "Facebook/Marketplace")
    @name = "Facebook/Marketplace"
    @cursor = ""
    @code = ""
    @current_pub
    @@item_dict = {}
    @@seller_dict = {}

    class << self
        attr_accessor :name
        attr_accessor :test
        attr_accessor :extract_name
    end

    def initialize(code)
        @code = code
        @@name = @name
        @proxy = ''
    end

    def name
        return @name
    end
    def query(current_page, per_page, search_by)
        cities = { "111806975511600"=> "Arica","109988942364338"=> "Iquique","106221866081430"=> "Calama","110061615681238"=> "Antofagasta","108711402493416"=> "Copiapó","105686679466017"=> "La Serena","111908478835911"=> "Valparaiso","112371848779363"=> "Santiago","110300362332319"=> "Rancagua","108445209187309"=> "Talca","109177112433420"=> "Concepcion","108019992553591" => "Temuco","108628905828413" => "Valdivia","115767185103622" => "Puerto Montt","108301162534806" => "Chaiten","111586475526184" => "Coyhaique","108224032538973" => "Cochrane","108817815816062" => "Puerto Natales","108424689189074" => "Punta Arenas"}    
        proxy_ip = YAML.load_file("#{Rails.root}/config/proxy.yml")
        request_retry = {}
        puts "Current page #{current_page} for #{search_by} in location #{cities[@code.to_s]}".blue
        # Spider_log.debug "Current page #{current_page} for #{search_by} in location #{cities[@code.to_s]}".blue
        puts "Per page #{per_page}".blue
        url = 'http://31.13.65.36/api/graphql/'
        if current_page == 1
            options = {
                followlocation: true,
                verbose: true,
                method: :post,
                proxy: proxy_ip["ip"].to_s,
                headers: {
                    "Host"=> "www.facebook.com",
                    "pragma"=> "no-cache",
                    "origin"=> "https://www.facebook.com",
                    "accept-language"=> "es",
                    "User-Agent"=> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
                    "Content-Type"=> "application/x-www-form-urlencoded",
                    "accept"=> "*/*",
                    "cache-control"=> "no-cache",
                    "authority"=> "www.facebook.com",
                    "X-Forwarded-For": "191.126.33.181"
                },
                # without cursor must be the first call to get the next cursor
                body: 'av=0&'+
                        '__user=0&'+
                        '__a=1&'+
                        '__dyn=5V8WXxaAcUmgDxKS5o9FE9XGiWF3ozzrrWpEeEqwXCxCaxubwTwyyoaUgDyUJqy8cWAAzppFXye4Xze2ei7E4ium2SaCxOu58nzEGjyEbo5yi9J0Px6ewZxy5FEzx2ih0WxS8wg8mwWwnEoxWexCqUpwzz9oyq4Hh8Fe15ByoB1qUKaxSUKnxyUaE98PxiGzVEgy8oyEhzUkU-8HgoUhw&'+
                        '__af=h0&'+
                        '__req=j&'+
                        '__be=-1&'+
                        '__pc=PHASED:DEFAULT&'+
                        '__rev=3359904&'+
                        'lsd=AVq5gxpT&'+
                        'fb_api_caller_class=RelayModern&'+
                        'variables={"count":"'+"#{per_page}"+'","query_arguments":{"bqf":"intersect(commerce_by_keyword('+search_by+'),commerce_by_price(0,214748364700),)","filtered_query_arguments":{"callsite":"browse:commerce:marketplace_www"},"people_filters":{"marketplace_id":null,"category_ids":[],"location_id":"'+@code.to_s+'"}},"MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194,"RENTAL_LEAD_GEN_PHOTO_HEIGHT_WIDTH":40,"MARKETPLACE_SAVED_SEARCH_ID":null,"MARKETPLACE_SAVED_SEARCH_ID_VALID":false,"viewerIsAnonymous":true,"isPogNavEnabled":true}&'+
                        'doc_id=1621766594521928'
            }
        else
            puts "**************************".red
            puts "WITH CURSOR WITH #{search_by} IN LOCATION #{cities[@code.to_s]}".red
            #Spider_log.debug "with cursor #{search_by}. location #{cities[@code.to_s]}"
            puts "**************************".red
            options = {
                followlocation: true,
                verbose: true,
                method: :post,
                proxy: proxy_ip["ip"].to_s,
                headers: {
                    "Host"=> "www.facebook.com",
                    "pragma"=> "no-cache",
                    "origin"=> "https://www.facebook.com",
                    "accept-language"=> "es",
                    "User-Agent"=> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
                    "Content-Type"=> "application/x-www-form-urlencoded",
                    "accept"=> "*/*",
                    "cache-control"=> "no-cache",
                    "authority"=> "www.facebook.com",
                    "X-Forwarded-For": "191.126.33.181"
                },
                # with cursor
                body: 
                    'av=0&'+
                    '__user=0&'+
                    '__a=1&'+
                    '__dyn=5V8WXxaAcUmgDxKS5o9FE9XGiWF3ozzrrWpEeEqwXCxCaxubwTwyyoaUgDyUJqy8cWAAzppFXye4Xze2ei7E4ium2SaCxOu58nzEGjyEbo5yi9J0Px6ewZxy5FEzx2ih0WxS8wg8mwWwnEoxWexCqUpwzz9oyq4Hh8Fe15ByoB1qUKaxSUKnxyUaE98PxiGzVEgy8oyEhzUkU-8HgoUhw&'+
                    '__af=h0&'+
                    '__req=j&'+
                    '__be=-1&'+
                    '__pc=PHASED:DEFAULT&'+
                    '__rev=3359904&'+
                    'lsd=AVq5gxpT&'+
                    'fb_api_caller_class=RelayModern&'+
                    'variables={"count":'+"#{per_page}"+',"cursor":"'+"#{@cursor}"+'","query_arguments":{"bqf":"intersect(commerce_by_keyword('+search_by+'),commerce_by_price(0,214748364700),)","filtered_query_arguments":{"callsite":"browse:commerce:marketplace_www"},"people_filters":{"marketplace_id":null,"category_ids":[],"location_id":"'+@code.to_s+'"}},"MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194,"RENTAL_LEAD_GEN_PHOTO_HEIGHT_WIDTH":40,"MARKETPLACE_SAVED_SEARCH_ID":null,"MARKETPLACE_SAVED_SEARCH_ID_VALID":false,"viewerIsAnonymous":true,"isPogNavEnabled":true}&'+
                    'doc_id=2239356606106911'
            }
        end
        hydra = Typhoeus::Hydra.new
        request = Typhoeus::Request.new(url, options)
        request_retry[request.hash] = 0
        request.on_complete do |response|
            puts "#{response.debug_info.text}".red
            # byebug
            # Spider_log.debug "Response code: #{response.code}"
            has_error = false
            response.debug_info.text.each do |value| 
                has_error = true if value.include? "Empty reply from server" 
                has_error = true if value.include? "Recv failure: Connection reset by peer"
            end
            if request_retry[request.hash] > 3
                Spider_log.debug "Third request surpassed for #{cities[@code.to_s]} searching #{search_by}"
                r = JSON.parse(response.body)
                if r["data"]["marketplace_search_query"]["marketplace_units"]["page_info"] != nil
                    next_page = r["data"]["marketplace_search_query"]["marketplace_units"]["page_info"]["has_next_page"]
                    Spider_log.debug "Has next page #{next_page}"
                end     
            elsif (has_error) && request_retry[request.hash] < 3
                puts "ERROR IN REQUEST RETRY"
                request_retry[request.hash] += 1
                puts "Retries #{request_retry[request.hash]}".yellow
                Spider_log.debug "Retries #{request_retry[request.hash]} for #{search_by}"
                hydra.queue(request)
            elsif response.code == 500 && request_retry[request.hash] < 3
                puts "ENCOLANDO"
                request_retry[request.hash] += 1
                hydra.queue(request)
                puts "Retries #{request_retry[request.hash]}".yellow
                Spider_log.debug "Retries #{request_retry[request.hash]} for #{search_by}"
            elsif response.code == 502 && request_retry[request.hash] < 3
                puts "ENCOLANDO"
                request_retry[request.hash] += 1
                hydra.queue(request)
                Spider_log.debug "Retries #{request_retry[request.hash]} for #{search_by}"
            elsif response.success?
                return nil if response.body.include? 'html'
                puts "Response #{response.code}"
                puts "Response #{response.body}".yellow
                r = JSON.parse(response.body)
                n_results = r["data"]["marketplace_search_query"]["marketplace_units"]["relaxed_request_params"]["original_request_result_count"]
                if  n_results == nil || n_results > 0
                    if r["data"]["marketplace_search_query"]["marketplace_units"]["edges"].empty?
                        fatal = 0
                        if r["errors"] != nil
                            r["errors"].each do |message|
                                if (message["severity"] == "ERROR") && (message["message"].include? "MarketplaceSearchFeedRefetchQuery")
                                    fatal += 1
                                end
                            end
                            if fatal != 0                                
                                Spider_log.debug "FATAL RESPONSE LOCKED BY FACEBOOK in #{cities[@code.to_s]} searching #{search_by}, proceed to retry #{request_retry[request.hash]}"
                                request_retry[request.hash] += 1
                                hydra.queue(request)
                                puts "Retries #{request_retry[request.hash]}".yellow
                                # raise 'Fatal response'
                            end
                            # exit(true)
                            # return nil
                        else
                            hydra.queue(request)
                        end
                        
                    end     
                end
                if r["data"]["marketplace_search_query"]["marketplace_units"]["page_info"] == nil
                    return nil
                end
                if r["data"]["marketplace_search_query"]["marketplace_units"]["page_info"]["has_next_page"]
                    @cursor = r["data"]["marketplace_search_query"]["marketplace_units"]["page_info"]["end_cursor"]
                    r = r["data"]["marketplace_search_query"]["marketplace_units"]["edges"]
                    if (r.nil? || r.empty?)
                        Spider_log.debug "No reply for #{cities[@code.to_s]} searching #{search_by}, proceed to retry #{request_retry[request.hash]}"
                        request_retry[request.hash] += 1
                        hydra.queue(request)
                        puts "Retries #{request_retry[request.hash]}".yellow                        
                    end
                    return response.body
                end
                if not r["data"]["marketplace_search_query"]["marketplace_units"]["page_info"]["has_next_page"]
                    return nil
                end
            
            else
                return nil
            end 
        end
        hydra.queue(request)
        hydra.run
    end
    def parse_list(response)
        puts "PARSING LIST"
        #   puts "Response #{response}".red
        result = []
        if response.nil?
            return result
        end
        r = JSON.parse(response)
        r = r["data"]["marketplace_search_query"]["marketplace_units"]["edges"]
        return result if r.nil?
        r.each do |value| 
            product_id = value["node"]["id"].nil? ? "-" : value["node"]["id"]
            if value["node"]["primary_photo"].nil?
                puts "FIND WITHOUT PRIMARY PHOTO ".yellow
                puts "https://www.facebook.com/marketplace/item/#{product_id}".yellow
                next
            end
            #   product_id = value["node"]["id"].nil? ? "-" : value["node"]["id"]
            seller = value["node"]["marketplace_post"].nil? ? "-" : value["node"]["marketplace_post"]["id"]
            unless @@item_dict.has_key?(product_id)
                seller_id = value["node"]["story"].nil? ? "-" : value["node"]["story"]["actors"].first
                pub = Spider::Publication.new
                pub.name = value["node"]["group_commerce_item_title"].nil? ? "-" : value["node"]["group_commerce_item_title"]
                pub.id = product_id
                pub.url = "https://www.facebook.com/marketplace/item/#{product_id}"
                pub.seller_id = seller_id["id"]
                pub.price = value["node"]["formatted_price"].nil? ? 0 : value["node"]["formatted_price"]["text"].gsub(/[^\d]/, '').to_i
                result << pub        
                @@item_dict[product_id] = seller        
            end
        end
        return result
    end

    def get_full_item node
        begin
        puts "GET FULL ITEMS"
        proxy_ip = YAML.load_file("#{Rails.root}/config/proxy.yml")
        result = nil
        url = 'http://www.facebook.com/api/graphql/'
        options = {
            followlocation: true,
            verbose: true,
            method: :post,
            proxy: proxy_ip["ip"].to_s,
            headers: {
                "Host"=> "www.facebook.com",
                "pragma"=> "no-cache",
                "origin"=> "https://www.facebook.com",
                "accept-language"=> "es",
                "User-Agent"=> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
                "Content-Type"=> "application/x-www-form-urlencoded",
                "accept"=> "*/*",
                "cache-control"=> "no-cache",
                "authority"=> "www.facebook.com",
                "X-Forwarded-For": "191.126.33.181"
            },
            # without cursor must be the first call to get the next cursor
            # body: 'av=0&__user=0&__a=1&__dyn=5V8WXxaAcUmgDxKS5o9FE9XGiWF3ozzrrWpEeEqwXCxCaxubwTwyyoaUgDyUJqy8cWAAzppFXye4Xze2ei7E4ium2SaCxOu58nzEGjyEbo5yi9J0Px6ewZxy5FEzx2ih0WxS8wg8mwWwnEoxWexCqUpwzz9oyq4Hh8Fe15ByoB1qUKaxSUKnxyUaE98PxiGzVEgy8oyEhzUkU-8HgoUhw&__af=h0&__req=5&__be=-1&__pc=PHASED:DEFAULT&__rev=3359904&lsd=AVq5gxpT&fb_api_caller_class=RelayModern&variables={"count":'+"#{per_page}"+',"query_arguments":{"bqf":"intersect(commerce_by_keyword('+@product.name+'),commerce_by_price(0,214748364700),)","filtered_query_arguments":{"callsite":"browse:commerce:marketplace_www"},"people_filters":{"marketplace_id":null,"category_ids":[],"location_id":"112371848779363"}},"MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194}&doc_id=1621766594521928'
            body: 'av=0&'+
            '__user=0&'+
            '__a=1&'+
            '__dyn=5V8WXxaAcUmgDxKS5o9FEbFbGAdyedJLFCwWxG3Kq6oG5UK3u2a9wHx2ubyRyUcWAAzppFXye4Xze2ei7E4ium2SaCxOu58nzEGjyEbo5yi9J0Px6ewZxy5FEzx2ih0WxS8wg8mwWwnEoxWexCqUpwzz9oyq4Hh8Fe15ByoB1qUKaxSUKnxyUaE98PxiGzVEgy8oyEhzUkU-bQ6e4o&'+
            '__af=h0&'+
            '__req=2&'+
            '__be=-1&'+
            '__pc=PHASED:DEFAULT&'+
            '__rev=3369849&'+
            'lsd=AVr3_jeP&'+
            'fb_api_caller_class=RelayModern&'+
            'variables={"count":1,"cursor":null,"product_id":'+"#{node.id}"+',"marketplaceID":null,"MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194}&'+
            'doc_id=1497760080307535'
        }
        
        response = Typhoeus::Request.new(url, options)
        response.on_complete do |response|
            if response.success?
                result = response.body
            end
        end
        response.run
        if result.nil?
            return nil
        else
            puts "#{result}".blue
            r = JSON.parse(result)
            return nil if r["data"]["groupCommerceProductItem"].nil?
            r = r["data"]["groupCommerceProductItem"]
            pub = Spider::Publication.new
            url = r["share_uri"].gsub('http://', 'https://')
            pub.city_raw = ""
            pub.county_raw = r["location"]["reverse_geocode"].nil? ? nil : r["location"]["reverse_geocode"]["city"]
            pub.description = r["redacted_description"]["text"]
            pub.image = r["photos"].empty? ? "": r["photos"][0]["image"]["uri"]
            pub.name = r["group_commerce_item_title"]
            pub.price = (r["item_price"]["offset_amount"].to_i)/100 > 100000000 ? 0 :  (r["item_price"]["offset_amount"].to_i)/100
            pub.published_at = r["story"].nil? ? nil : DateTime.strptime(r["story"]["creation_time"].to_s,'%s')
            pub.state_raw = r["location"]["reverse_geocode"].nil? ? nil : r["location"]["reverse_geocode"]["state"]
            pub.url = url
            condition = ApplicationHelper::USE_STATUS_UNDEFINED
            pub.condition = condition
            @current_pub = r
            return pub
        end
        rescue Exception => e
            puts "Error in full item (MP)"
            puts e.message
            return nil
        end
    end

    def get_publisher node
        begin
        r = @current_pub.to_json
        r = JSON.parse(r)
        publisher_temp = r["story"].nil? ? nil : r["story"]["actors"][0]
        publisher = Spider::Publisher.new
        publisher.name = publisher_temp.nil? ? nil : publisher_temp["name"]
        publisher.fb_page1 = publisher_temp.nil? ? nil : "https://www.facebook.com/#{publisher_temp["id"]}"
        publisher.county_raw = r["location"]["reverse_geocode"].nil? ? nil : r["location"]["reverse_geocode"]["city"]   
        return publisher
        rescue
            puts "Error in parse list (MP)"
            return nil
        end
    end

    def self.calculate_publisher_uid(pub)
        if not (pub.is_a?(Spider::Publisher))
        raise "Expected 'Spider::Publisher' param"
        end
        fb = pub.fb_page1.nil? ? "": pub.fb_page1
        Digest::SHA1.hexdigest "#{fb}"
    end

    def check_response
    end
end