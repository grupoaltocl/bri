#FACEBOOK
Notification.new(name: "Parche Publicadores",
                   description: "Parche Iniciado para publicadores.",
                   process_uuid: "Facebook").save
publisher_fixed = 0
begin
    Publisher.where(publishers: { source_id: 3 }).group('fb_page1').count.each do |pub|
        if pub[1] > 1
        publisher = Publisher.where(publishers: {fb_page1: pub[0], source_id: 3} ).first
        pubs_copy = Publisher.where(publishers: {fb_page1: pub[0], source_id: 3} )
        puts pubs_copy
        pubs_copy.each do |pub_copy|
            if pub_copy.id != publisher.id
                puts "another publisher".red
                pub_copy.publications.each do |pub|
                    publisher.publications << pub
                end
                pub_copy.delete
                publisher_fixed += 1
            end 
        end
        end
    end
    puts "FACEBOOK PUBLISHER FIXED: #{publisher_fixed}"
    Notification.new(name: "Parche Publicadores",
                    description: "Parche finalizado, se corrigieron #{publisher_fixed} publicadores.",
                    process_uuid: "Facebook").save
rescue
    puts "ERROR"
    Notification.new(name: "Parche Publicadores",
                description: "Parche finalizado con problemas",
                process_uuid: "Facebook").save
end
