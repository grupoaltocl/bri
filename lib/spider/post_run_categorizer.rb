require 'rubygems'

config = Configuration.first
multiprod_labels = []
if config.multiprod_labels && config.multiprod_labels.strip.size.size > 0
  multiprod_labels += config.multiprod_labels.split(',').map { |x| x.strip.downcase }
end

new_labels = []
if config.has_new_label && config.has_new_label.strip.size.size > 0
  new_labels += config.has_new_label.split(',').map { |x| x.strip.downcase }
end

not_new_labels = []
if config.has_not_new_label && config.has_not_new_label.strip.size.size > 0
  not_new_labels += config.has_not_new_label.split(',').map { |x| x.strip.downcase }
end

def copy_aliases(type,aliases)
  if type.aliases && type.aliases.strip.size.size > 0
    aliases += type.aliases.split(',').map { |x| x.strip.downcase }
  end
  return aliases
end
def copy_excludes(type,excludes)
  if type.excludes && type.excludes.strip.size.size > 0
    excludes += type.excludes.split(',').map { |x| x.strip.downcase }
  end
  return excludes
end

def check_save_condition(publication, category)
  if category.by_used && ( publication.condition == ApplicationHelper::USE_STATUS_USED || publication.condition == ApplicationHelper::USE_STATUS_NEW )
    puts "Publication can be activate by used, Publication condition #{publication.condition}".red
    return true
  else
    if publication.condition == ApplicationHelper::USE_STATUS_NEW
      puts "Publication cant be activate by used, Publication condition #{publication.condition}".blue
      return true
    else
      puts "Publication cant be activate by used, Publication condition #{publication.condition} USED OR UNDEFINED".blue
      return false
    end
  end
end


puts "Now Categorize by Brand"
Publication.where(publications: { manual_match: false })
           .update_all(active_cat: false,
                       other: false,
                       multi_prod: false)
Brand.all.each do |brand|
  # next if brand.cat_uuid == config.cat_uuid
  publications = Publication.joins(products: :brand)
                            .where(brands: { id: brand.id })
                            .where(publications: { manual_match: false })
                            .uniq
  puts "From brand: #{brand.name} to products, to publications".red
  puts "Will be categorized '#{publications.count}' publications from brand #{brand.name}"
  category = Category.joins(:brands).where(brands: { id: brand.id }).to_a[0]
  # puts category.methods
  # puts category.to_json
  #From brand aliases y excludes
  brand_aliases = []
  brand_excludes = []

  brand_aliases << brand.name
  brand_aliases = copy_aliases(brand,brand_aliases)
  brand_excludes = copy_excludes(brand,brand_excludes)

  # From products 
  products = brand.products
  products.each do |product|
    product_name = product.name.downcase.strip
    product_excludes = []
    product_aliases = []
    product_aliases << product_name
    product_aliases = copy_aliases(product,product_aliases)
    product_aliases = copy_excludes(product,product_aliases)
    base_product = product.base_product

    if base_product
      product_aliases = copy_aliases(base_product,product_aliases)
      product_excludes = copy_excludes(base_product,product_excludes)
    end
    if product.filter_by_brand
      product_aliases = copy_aliases(brand,product_aliases)
      product_excludes = copy_excludes(brand,product_excludes)
    end
    if brand.filter_by_category
      product_aliases = copy_aliases(category,product_aliases)
      product_excludes = copy_excludes(category,product_excludes)
    end

    puts "For product: #{product.name}".cyan
    p "Aliases: #{product_aliases}"
    p "Excludes: #{product_excludes}"

    publications.each do |publication|
      puts "Check #{publication}"
      publication_name = publication.name.nil? ? '' : publication.name.downcase.strip
      puts "NAME #{publication_name}"
      publication_description = ''
      publication_description = publication.description.strip.downcase if not publication.description.nil?
      puts "NAME #{publication_description}"

      if publication.condition == ApplicationHelper::USE_STATUS_UNDEFINED
        publication.condition = ApplicationHelper::USE_STATUS_USED
        new_labels.each do |x|
          if publication_name.include? x
            # has_new_label = true
            # puts "include en el nombre".red
            publication.condition = ApplicationHelper::USE_STATUS_NEW
          end
          if publication_description.include? x
            # has_new_label = true
            # puts "include en la descripción".blue
            publication.condition = ApplicationHelper::USE_STATUS_NEW
          end
        end

        not_new_labels.each do |x|
          if publication_name.include? x
            # has_used_label = true
            publication.condition = ApplicationHelper::USE_STATUS_USED
          end
          if publication_description.include? x
            # has_used_label = true
            publication.condition = ApplicationHelper::USE_STATUS_USED
          end
        end
      end      

      has_product_exclude = false
      has_product_exclude_description = false
      product_excludes.each do |x|
        if publication_name.include? x
          has_product_exclude = true
          break
        end
        if publication_description.include? x
          has_product_exclude_description = true
          break
        end
      end

      if !category.nil? && (category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION)
        if has_product_exclude_description || has_product_exclude
          next
        end
      else
        if has_product_exclude
          next
        end
      end

      has_product_alias = false
      has_product_alias_description = false
      product_aliases.each do |x|
        if !category.nil? && (category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION)
          if publication_description.include? x
            has_product_alias_description = true
            break
          end
        end
        if publication_name.include? x
          has_product_alias = true
          break
        end
      end
      # puts !category.nil?
      do_save = false
      if !category.nil?
        case category.by_description
        when ApplicationHelper::ACTIVATE_BY_NAME
          if has_product_alias
            puts "Publication #{publication_name.red} active and other because of #{product.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of product match in name".red
            publication.active_cat = true if check_save_condition(publication, category)
            do_save = true
          end
        when ApplicationHelper::ACTIVATE_BY_DESCRIPTION
          if has_product_alias_description
            puts "Publication #{publication_name.red} active and other because of #{product.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of product match in description".red
            publication.active_cat = true if check_save_condition(publication, category)
            do_save = true
          end
        when ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION
          if has_product_alias || has_product_alias_description
            puts "Publication #{publication_name.red} active and other because of #{product.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of brand match in name or description".red
            publication.active_cat = true if check_save_condition(publication, category)
            do_save = true
          end
        end
      else
        if has_product_alias
          puts "Publication #{publication_name.red} active and other because of #{product.name.cyan}"
          puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of product match in name".red
          publication.active_cat = true if check_save_condition(publication, category)
          do_save = true
        end
      end

      if do_save
        publication.products << product
        if publication.products.where(products: { generic: false }).size > 1
          publication.products.where(products: { generic: true }).each do |prod|
            prod.publications.delete(publication)
          end
          publication.multi_prod = true
        end
        publication.save
        product.save
      end
    end
  end
  brand.cat_uuid = config.cat_uuid
  brand.save
end

Publication.joins(products: :brand)
           .where(brands: { cat_uuid: config.cat_uuid })
           .where(publications: { manual_match: false })
           .update_all(active: false)
Publication.joins(products: :brand)
           .where(brands: { cat_uuid: config.cat_uuid })
           .where(publications: { active_cat: true, manual_match: false })
           .update_all(active: true)
