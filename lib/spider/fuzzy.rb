require 'rubygems'
require 'fuzzy_match'

treshold = 0.0000001
products = Product.all
records = Publication.all

def calculate_stolen_score(item, product)
  score = 0
  if item.nil? or product.nil?
    return 0
  end
  if item.price.nil? or product.price.nil?
    return 0
  end
  # ((item.price - product.price).abs/product.price.abs)
  (item.price.abs/product.price.abs)
end

records.each do |item| 
  products.each do |product|
    product_name = product.name
    item_name = item.name
    fz = FuzzyMatch.new([product_name])
    result, score = fz.find_with_score(item.name)
    puts "Score for '#{item_name}' matching '#{product_name}' is '#{score}'"
    if not score
      next
    end
    if item.match_score
      if score > item.match_score and (score-item.match_score).abs > treshold
        puts "Updating score for '#{item_name}' matching '#{product_name}' from '#{item.match_score}' to '#{score}'"
        item.match_score = score
        item.product = product
        item.stolen_score = calculate_stolen_score(item, product)
        if item.save :validate => false
          puts "Saved successfully"
        else
          puts "Save failed"
        end
      end
    else
      puts "Updating score for '#{item_name}' matching '#{product_name}' with score: '#{score}'"
      item.match_score = score
      item.product = product
      item.stolen_score = calculate_stolen_score(item, product)
      if item.save :validate => false
        puts "Saved successfully"
      else
        puts "Save failed"
      end
    end
  end
  puts "Final score for '#{item.name}' is '#{item.match_score}"
end

