require 'rubygems'
require 'mechanize'
require 'fuzzy_match'
require 'securerandom'
require 'cgi'
require 'meli'

require './lib/spider/base_spider'
require './lib/spider/yapo_spider'
require './lib/spider/ml_spider'
require './lib/spider/mp_spider'
require './lib/spider/engine'
# File.open(File.expand_path('../', __FILE__)+'/../../tmp/pids/scraper.pid', 'w') {|f| f.write Process.pid }
begin
  $meli = Meli.new()

  spider_classes = [MPSpider,YapoSpider,MLSpider]
  spiders = []
  # city_codes = [111806975511600,109988942364338,106221866081430,110061615681238,108711402493416,105686679466017,111908478835911,112371848779363,110300362332319,108445209187309,109177112433420,108019992553591,108628905828413,115767185103622,108301162534806,111586475526184,108224032538973,108817815816062,108424689189074]
  #city_codes = [112371848779363,110300362332319,108445209187309,109177112433420,108019992553591,108628905828413,115767185103622,108301162534806,111586475526184,108224032538973,108817815816062,108424689189074]
  sources = Source.all()
  city_codes = YAML.load_file("#{Rails.root}/config/cities.yml")

  spider_classes.each do |spider|
    source = Source.find_by(name: spider.name)
    if spider.name == MPSpider.name and source.active
      city_codes['cities'].each do |code|
        spiders << MPSpider.new(code[0])
      end
    else
      spiders << spider.new if source.active
    end
  end
  puts "#{spiders.count} searches will be made"
  persist = ENV["PERSIST"] || "false"
  if not persist.nil?
    persist = ['true','1'].any? { |word| persist.include?(word) }
  end

  brands = Brand.all
  categories = Category.all
  engine = Engine.new(spiders, brands, categories, persist)
  uuid = engine.instance_variable_get(:@uuid)
  config = Configuration.first

  now = Time.current

  puts uuid
  puts Time.now
  Spider_log.debug "Starting scraper #{uuid}"
  engine.by_category
  engine.by_brand
  Configuration.first.update_columns(sys_scraped_at: now)
  Notification.new(name: "Scraper",
                   description: "Scraper Finalizado correctamente",
                   process_uuid: uuid).save
  Spider_log.debug "Finished scraper #{uuid}"
  puts "Spider Finished".red
  puts Time.now
  # exit(true)
rescue Exception => e
  puts 'The scrapper failed unexpectitly:'
  puts e.message
  Spider_log.debug "Finished scraper with problems #{uuid}"
  if e.message == 'Fatal response'
    # puts "ERRRRRROOOOORRR DE FACEBOOOOOK".red
    Configuration.first.update_columns(sys_scraped_at: now)
    Notification.new(name: "Scraper",
                description: "Scraper finalizado con problemas, Bloqueo por Facebook",
                process_uuid: uuid).save
  elsif e.cause.to_s != "MySQL server has gone away"
  # puts e.backtrace.join("\n")
    Notification.new(name: "Scraper",
                    description: "Scraper finalizado con problemas",
                    process_uuid: uuid).save
  end
  # exit(false)
end