# require "typhoeus"

# # url = 'http://31.13.85.36/api/graphql/'

# # options = {
# #     followlocation: true,
# #     verbose: true,
# #     method: :post,

# #     headers: {
# #         "Host"=> "www.facebook.com",
# #         "pragma"=> "no-cache",
# #         "origin"=> "https://www.facebook.com",
# #         "accept-language"=> "es",
# #         "Content-Type"=> "application/x-www-form-urlencoded",
# #         "accept"=> "*/*",
# #         "cache-control"=> "no-cache",
# #         "authority"=> "www.facebook.com",
# #         "X-Forwarded-For": "191.126.33.181"
# #     },

# #     # without cursor must be the first call to get the next cursor
# #     # body: 'av=0&'+
# #     # '__user=0&'+
# #     # '__a=1&'+
# #     # '__dyn=5V8WXxaAcUmgDxKS5o9FE9XGiWF3ozzrrWpEeEqwXCxCaxubwTwyyoaUgDyUJqy8cWAAzppFXye4Xze2ei7E4ium2SaCxOu58nzEGjyEbo5yi9J0Px6ewZxy5FEzx2ih0WxS8wg8mwWwnEoxWexCqUpwzz9oyq4Hh8Fe15ByoB1qUKaxSUKnxyUaE98PxiGzVEgy8oyEhzUkU-8HgoUhw&'+
# #     # '__af=h0&'+
# #     # '__req=5&'+
# #     # '__be=-1&'+
# #     # '__pc=PHASED:DEFAULT&'+
# #     # '__rev=3359904&'+
# #     # 'lsd=AVq5gxpT&'+
# #     # 'fb_api_caller_class=RelayModern&'+
# #     # 'variables={
# #     #   "count":10,
# #     #   "query_arguments":{
# #     #     "bqf":"intersect(commerce_by_keyword(máscara),commerce_by_price(0,214748364700),)",
# #     #     "filtered_query_arguments":{"callsite":"browse:commerce:marketplace_www"},
# #     #     "people_filters":{"marketplace_id":null,"category_ids":[],"location_id":"112371848779363"}},
# #     #     "MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194}&'+
# #     # 'doc_id=1621766594521928'
# #     # with cursor
# #     body: 'av=0&'+
# #     '__user=0&'+
# #     '__a=1&'+
# #     '__dyn=5V8WXxaAcUmgDxKS5o9FE9XGiWF3ozzrrWpEeEqwXCxCaxubwTwyyoaUgDyUJqy8cWAAzppFXye4Xze2ei7E4ium2SaCxOu58nzEGjyEbo5yi9J0Px6ewZxy5FEzx2ih0WxS8wg8mwWwnEoxWexCqUpwzz9oyq4Hh8Fe15ByoB1qUKaxSUKnxyUaE98PxiGzVEgy8oyEhzUkU-8HgoUhw&'+
# #     '__af=h0&'+
# #     '__req=g&'+
# #     '__be=-1&'+
# #     '__pc=PHASED:DEFAULT&'+
# #     '__rev=3359904&'+
# #     'lsd=AVq5gxpT&'+
# #     'fb_api_caller_class=RelayModern&'+
# #     'variables={
# #       "count":10,
# #       "cursor":'+'"AbrNhGIIiCbB9nwZQKVoZIYomNBqI6-mCCDbaC_MpMKWoV0n7Nevkt5qpj3hNxZNuwk4bzh2dI7xVK6q0_Wg9-N1mG5388eBwz4y0MUBZYMqcfMLCGQsOZqO1Cg8wwgjEyU0iCatAbtcOUNuvEGD8oQKAbUJUBb706S6mjuqdEl9DiPFoP3p_YMO-GzQ0nJTVRYcJrr3W98LGfuCSceSWF2w-iQFoh0QrB3gK5xKguWIYhA2d4JdzjVGq2DrO_2EVimbLDgbaWuai54_BkE22fBr_vsnu96wrgRqOwurqqg03G4eaBrijBEsgGb6sedXJVKRNp2GSx5ySd3fUEiO-jY-OhZu4TjcCr0ZaE23JIarCnXI-CZ9jdAvFkCmikrUGAyPIdZJXRcLgWVOJyrmEfTh"'+',
# #       "query_arguments":{
# #         "bqf":"intersect(commerce_by_keyword(máscara),commerce_by_price(0,214748364700),)",
# #         "filtered_query_arguments":{"callsite":"browse:commerce:marketplace_www"},
# #       "people_filters":{"marketplace_id":null,"category_ids":[],"location_id":""}},
# #       "MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194}&'+
# #     'doc_id=1621766594521928'

# # }


# # req = Typhoeus::Request.new(url, options)
# # req.on_complete do |response|
# #   if response.success?
# #     puts 'Response #{response.code}'
# #     puts response.body

# #   elsif response.timed_out?
# #     puts 'Request Timed Out!'
# #   elsif response.code == 0
# #     # Could not get an http response, something's wrong.
# #     puts response.return_message
# #   else
# #     # Received a non-successful http response.
# #     puts 'HTTP request failed: ' + response.code.to_s
# #   end
# # end

# # req.run

# def query(current_page, per_page, search_by, code)
#     proxy_ip = YAML.load_file("#{Rails.root}/config/proxy.yml")
#     request_retry = {}
#     puts "Pagina actual #{current_page}".blue
#     puts "Por página #{per_page}".blue
#     puts 'variables={"count":"'+"#{per_page}"+'","query_arguments":{"bqf":"intersect(commerce_by_keyword('+search_by+'),commerce_by_price(0,214748364700),)","filtered_query_arguments":{"callsite":"browse:commerce:marketplace_www"},"people_filters":{"marketplace_id":null,"category_ids":[],"location_id":"'+code.to_s+'"}},"MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194}&'
#     url = 'http://www.facebook.com/api/graphql/'
#     options = {
#         followlocation: true,
#         verbose: true,
#         method: :post,
#         # proxy: proxy_ip["ip"].to_s,
#         headers: {
#             "Host"=> "www.facebook.com",
#             "pragma"=> "no-cache",

#             "origin"=> "https://www.facebook.com",
#             "accept-language"=> "es",
#             "User-Agent"=> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
#             "Content-Type"=> "application/x-www-form-urlencoded",
#             "accept"=> "*/*",
#             "cache-control"=> "no-cache",
#             "authority"=> "www.facebook.com",
#             "X-Forwarded-For": "191.126.33.181"
#         },
#         # without cursor must be the first call to get the next cursor
#         body: 'av=0&'+
#         '__user=0&'+
#         '__a=1&'+
#         '__dyn=5V8WXxaAcUmgDxKS5o9FE9XGiWF3ozzrrWpEeEqwXCxCaxubwTwyyoaUgDyUJqy8cWAAzppFXye4Xze2ei7E4ium2SaCxOu58nzEGjyEbo5yi9J0Px6ewZxy5FEzx2ih0WxS8wg8mwWwnEoxWexCqUpwzz9oyq4Hh8Fe15ByoB1qUKaxSUKnxyUaE98PxiGzVEgy8oyEhzUkU-8HgoUhw&'+
#         '__af=h0&'+
#         '__req=5&'+
#         '__be=-1&'+
#         '__pc=PHASED:DEFAULT&'+
#         '__rev=3359904&'+
#         'lsd=AVq5gxpT&'+
#         'fb_api_caller_class=RelayModern&'+
#         'variables={"count":"'+"#{per_page}"+'","query_arguments":{"bqf":"intersect(commerce_by_keyword('+search_by+'),commerce_by_price(0,214748364700),)","filtered_query_arguments":{"callsite":"browse:commerce:marketplace_www"},"people_filters":{"marketplace_id":null,"category_ids":[],"location_id":"'+code.to_s+'"}},"MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194}&'+
#         'doc_id=1621766594521928'
#     }
#     hydra = Typhoeus::Hydra.new
#     request = Typhoeus::Request.new(url, options)
#     request_retry[request.hash] = 0
#     request.on_complete do |response|
#         puts "#{response.debug_info.text}".red
#         puts "#{response.code}".red
#         puts "#{response.failure?}".red
#         puts "#{response.methods}".red
#         # byebug
#         if (response.debug_info.text.include? "transfer closed with outstanding read data remaining\n" || "Empty reply from server\n") && request_retry[request.hash] <3
#             puts "ERROR IN REQUEST RETRY"
#             request_retry[request.hash] += 1
#             puts "Retries #{request_retry[request.hash]}".yellow
#             hydra.queue(request)
#         elsif response.code == 403 && request_retry[request.hash] <3
#             puts "ENCOLANDO"
#             request_retry[request.hash] += 1
#             hydra.queue(request)
#             puts "Retries #{request_retry[request.hash]}".yellow
#         elsif response.success?
#             puts "Response #{response.code}"
#             puts "Response #{response.body}".yellow
#             r = JSON.parse(response.body)
#             n_results = r["data"]["marketplace_search_query"]["marketplace_units"]["relaxed_request_params"]["original_request_result_count"]
#             if  n_results == nil || n_results > 0
#                 if r["data"]["marketplace_search_query"]["marketplace_units"]["edges"].empty?
#                     fatal = 0
#                     if r["errors"] != nil
#                         r["errors"].each do |message|
#                             if (message["severity"] == "ERROR") && (message["message"].include? "MarketplaceSearchFeedRefetchQuery")
#                                 fatal += 1
#                             end
#                         end
#                         if fatal != 0
#                             raise 'Fatal response'
#                         end
#                         # exit(true)
#                         return nil
#                     end
#                 end     
#             end
#             if r["data"]["marketplace_search_query"]["marketplace_units"]["page_info"]["has_next_page"]
#                 cursor = r["data"]["marketplace_search_query"]["marketplace_units"]["page_info"]["end_cursor"]
#                 puts cursor
#                 return response.body
#             end
#             if not r["data"]["marketplace_search_query"]["marketplace_units"]["page_info"]["has_next_page"]
#                 return nil
#             end
#             return nil 
#         else
#             return nil
#         end
#     end
#     hydra.queue(request)
#     hydra.run
# end
# def get_full_item node
#     proxy_ip = YAML.load_file("#{Rails.root}/config/proxy.yml")
#     puts "GET FULL ITEMS"
#     result = nil
#     url = 'http://www.facebook.com/api/graphql/'
#     options = {
#         followlocation: true,
#         verbose: false,
#         method: :post,
#         # proxy: proxy_ip["ip"].to_s,
#         headers: {
#             "Host"=> "www.facebook.com",
#             "pragma"=> "no-cache",
#             "origin"=> "https://www.facebook.com",
#             "accept-language"=> "es",
#             "User-Agent"=> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
#             "Content-Type"=> "application/x-www-form-urlencoded",
#             "accept"=> "*/*",
#             "cache-control"=> "no-cache",
#             "authority"=> "www.facebook.com",
#             "X-Forwarded-For": "191.126.33.181"
#         },
#         # without cursor must be the first call to get the next cursor
#         # body: 'av=0&__user=0&__a=1&__dyn=5V8WXxaAcUmgDxKS5o9FE9XGiWF3ozzrrWpEeEqwXCxCaxubwTwyyoaUgDyUJqy8cWAAzppFXye4Xze2ei7E4ium2SaCxOu58nzEGjyEbo5yi9J0Px6ewZxy5FEzx2ih0WxS8wg8mwWwnEoxWexCqUpwzz9oyq4Hh8Fe15ByoB1qUKaxSUKnxyUaE98PxiGzVEgy8oyEhzUkU-8HgoUhw&__af=h0&__req=5&__be=-1&__pc=PHASED:DEFAULT&__rev=3359904&lsd=AVq5gxpT&fb_api_caller_class=RelayModern&variables={"count":'+"#{per_page}"+',"query_arguments":{"bqf":"intersect(commerce_by_keyword('+@product.name+'),commerce_by_price(0,214748364700),)","filtered_query_arguments":{"callsite":"browse:commerce:marketplace_www"},"people_filters":{"marketplace_id":null,"category_ids":[],"location_id":"112371848779363"}},"MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194}&doc_id=1621766594521928'
#         body: 'av=0&'+
#         '__user=0&'+
#         '__a=1&'+
#         '__dyn=5V8WXxaAcUmgDxKS5o9FEbFbGAdyedJLFCwWxG3Kq6oG5UK3u2a9wHx2ubyRyUcWAAzppFXye4Xze2ei7E4ium2SaCxOu58nzEGjyEbo5yi9J0Px6ewZxy5FEzx2ih0WxS8wg8mwWwnEoxWexCqUpwzz9oyq4Hh8Fe15ByoB1qUKaxSUKnxyUaE98PxiGzVEgy8oyEhzUkU-bQ6e4o&'+
#         '__af=h0&'+
#         '__req=2&'+
#         '__be=-1&'+
#         '__pc=PHASED:DEFAULT&'+
#         '__rev=3369849&'+
#         'lsd=AVr3_jeP&'+
#         'fb_api_caller_class=RelayModern&'+
#         'variables={"count":1,"cursor":null,"product_id":'+"#{node}"+',"marketplaceID":null,"MARKETPLACE_FEED_ITEM_IMAGE_WIDTH":194}&'+
#         'doc_id=1497760080307535'
#     }
#     hydra = Typhoeus::Hydra.new
#     request = Typhoeus::Request.new(url, options)
#     request.on_complete do |response|
#         puts response.code
#         puts response.debug_info.to_h
#         puts response.debug_info.to_json
#         if response.code == 500 && request.failures < 3
#             puts "ENCOLANDO"
#             hydra.queue(request)
#         elsif response.success?
#             result = response.body
#         end
#     end
#     hydra.queue(request)
#     hydra.run
#     # response.run
#     if result.nil?
#         return nil
#     else
#         puts "#{result}".blue
#         r = JSON.parse(result)
#         # r = r["data"]["groupCommerceProductItem"]
#         # pub = Spider::Publication.new
#         # url = r["share_uri"].gsub('http://', 'https://')
#         # pub.city_raw = ""
#         # pub.county_raw = r["location"]["reverse_geocode"].nil? ? nil : r["location"]["reverse_geocode"]["city"]
#         # pub.description = r["redacted_description"]["text"]
#         # pub.image = r["photos"].empty? ? "": r["photos"][0]["image"]["uri"]
#         # pub.name = r["group_commerce_item_title"]
#         # pub.price = (r["item_price"]["offset_amount"].to_i)/100
#         # pub.published_at = r["story"].nil? ? nil : DateTime.strptime(r["story"]["creation_time"].to_s,'%s')
#         # pub.state_raw = r["location"]["reverse_geocode"].nil? ? nil : r["location"]["reverse_geocode"]["state"]
#         # pub.url = url
#         # condition = ApplicationHelper::USE_STATUS_UNDEFINED
#         # pub.condition = condition
#         # @current_pub = r
#         # return pub
#     end
# end
# # get_full_item("515428415502586")
# # city_codes = [111806975511600,109988942364338,106221866081430,110061615681238,108711402493416,105686679466017,111908478835911,112371848779363,110300362332319,108445209187309,109177112433420,108019992553591,108628905828413,115767185103622,108301162534806,111586475526184,108224032538973,108817815816062,108424689189074]
# # city_name = ["Arica","Iquique","Calama","Antofagasta","Copiapó","La Serena","Valparaiso","Santiago","Rancagua","Talca","Concepcion","Temuco","Valdivia","Puerto Montt","Chaiten","Coyhaique","Cochrane","Puerto Natales","Punta Arenas"]
# city_name=["Madrid","Barcelona","Sevilla","Valencia","Bilbao","Malaga","Zaragoza","Granada","Córdoba","Toledo","Santiago de compostela","Palma de Mallorca","San sebastían","Salamanca","Alicante","Valladolid","Vigo","Murcia","Gijón","Pamplona","León","Vitoria","Santander","Cuenca","Tarragona"]
# city_codes=[106504859386230,106287062743373,111812165504902,271963079548530,110875455606568,114807985198048,106252896078918,110664458955950,108957072462073,108439919176942,106316556073266,106466316056863,104298006276926,108279102539860,110390408990276,108236755863231,111561865529958,110546248974344,106445742726598,114830741863254,112984052045516,108099185885552,104002292969382,107414562621685,110474335646794]
# cont = 0
# city_codes.each do |code|
#     puts code
#     puts "Searching with code #{code} and city #{city_name[cont]}".red
#     query(1,10,'nike',code)
#     cont = cont + 1
# end
# puts city_codes.count


require 'rubygems'
require 'mechanize'
require 'fuzzy_match'
require 'securerandom'
require 'cgi'
require 'meli'

require './lib/spider/base_spider'
require './lib/spider/mp_spider'
require './lib/spider/engine'
begin
  spider_classes = [MPSpider]
  spiders = []
  city_codes = [112371848779363]
  sources = Source.all()

  spider_classes.each do |spider|
    source = Source.find_by(name: spider.name)
    if spider.name == MPSpider.name and source.active
      city_codes.each do |code|
        spiders << MPSpider.new(code)
      end
    else
      spiders << spider.new if source.active
    end
  end
  puts "#{spiders.count} searches will be made"

  brands = [Brand.find(4)]
  categories = [Category.first]
  engine = Engine.new(spiders, brands, categories, false)
  uuid = engine.instance_variable_get(:@uuid)
  puts Time.now
  Spider_log.debug "Starting scraper #{uuid}"
#   engine.by_category
  engine.by_brand
  Spider_log.debug "Finished scraper #{uuid}"
  puts "Spider Finished".red
  puts Time.now
  # exit(true)
rescue Exception => e
  puts 'The scrapper failed unexpectitly:'
  puts e.message
end
