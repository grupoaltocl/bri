Notification.new(name: "Parche Publicadores",
                   description: "Parche Iniciado para publicadores.",
                   process_uuid: "Mercadolibre").save

Publisher.where(publishers: {source_id: 2, url: nil}).each do |publisher|
    publisher.url = "http://perfil.mercadolibre.cl/#{publisher.name}"
    publisher.save
end
publisher_fixed = 0
begin
    Publisher.where(publishers: { source_id: 2 }).group('url').count.each do |publisher|
        if publisher[1] > 1
            # pubs_copy = Publisher.where(fb_page1: publisher.fb_page1)
            publisher = Publisher.where(publishers: {url: publisher[0], source_id: 2} ).first
            pubs_copy = Publisher.where(publishers: {url: publisher.url, source_id: 2} )
            puts pubs_copy
            pubs_copy.each do |pub_copy|
                if pub_copy.id != publisher.id
                    puts "another publisher".red
                    pub_copy.publications.each do |pub|
                        publisher.publications << pub
                    end
                    pub_copy.delete
                    publisher_fixed += 1
                end 
            end
        end
    end
    puts "MERCADOLIBRE PUBLISHER FIXED: #{publisher_fixed}"
    Notification.new(name: "Parche Publicadores",
                    description: "Parche finalizado, se corrigieron #{publisher_fixed} publicadores.",
                    process_uuid: "Mercadolibre").save    
rescue
    puts "ERROR"
    Notification.new(name: "Parche Publicadores",
                description: "Parche finalizado con problemas",
                process_uuid: "Mercadolibre").save
end
