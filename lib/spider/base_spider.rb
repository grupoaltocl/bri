require 'pdfkit'
require 'digest/sha1'
require './lib/spider/spider_model'

include ApplicationHelper

class BaseSpider

  class << self
      # attr_accessor :base_url
      attr_accessor :setup
      attr_accessor :next_item
      # attr_accessor :name
  end 
  attr_accessor :current_page
  
  def initialize()
    @per_page = 50
    @current_page = 1
  end

  def setup(search_engine, source)
    @source = source
    @search_engine = search_engine
    @current_page = 1
    @per_page = 50

  end

  def next_item(search_by)
    # p "foo"
    # puts @search_keywords
    # # @search_keywords.each do |search_by|
    # puts "Searching by #{search_by} in #{@source}".red
    begin
      total = 0
      loop do
        puts "#{@source}"
        if @source == "Facebook/Marketplace"
          final_response = nil
          last_list = []
          current_list = []
          (0..2).each do |i|
            response = query(@current_page, @per_page, search_by)
            current_list = parse_list response
            puts "CURRENT LIST: #{current_list.size}".red
            puts "LAST LIST: #{last_list.size}".red
            Spider_log.debug "Current page #{@current_page}" if i == 0
            Spider_log.debug "Search by #{search_by} find #{current_list.size} in #{@source}"
            if current_list.size > last_list.size
              final_response = response
              last_list = current_list
            end
          end
          # list = parse_list final_response
          total += last_list.size
          break if (last_list.nil? || last_list.empty?)
          last_list.each do |item|
            yield item if do_keep_item item
          end
          @current_page += 1
        else
          response = query(@current_page, @per_page, search_by)
          break if response.nil?
          # puts response.methods
          list = parse_list response
          break if list.empty?
          Spider_log.debug "Search by #{search_by} find #{list.size} in #{@source}"
          total += list.size
          list.each do |item|
            yield item if do_keep_item item
            # yield item
          end
          @current_page += 1
        end
      end
      Spider_log.debug "Search by #{search_by} total #{total} in #{@source}"
    rescue Exception => e
      puts 'The scrapper failed in next item'
      puts e.message
      return nil
    end
    # end
  end

  def reset_query
    @current_page = 1
    @per_page = 50
  end

  def do_keep_item(list_item)
    if @search_engine.search_excludes and  @search_engine.search_excludes.size > 0 
      # puts list_item.name
      item_name = list_item.name.nil? ? "" : list_item.name.dup 
      item_name.downcase!
      item_name.strip!
      # item_description = list_item.description.dup
      # item_description.downcase!
      # item_description.strip!
      has_exclude = false
      excludes = @search_engine.search_excludes.split(',').each {|x| x.downcase! }
      excludes.each {|x| has_exclude = true if item_name.include? x }
      # excludes.each {|x| has_exclude = true if item_name.include? x }
      if has_exclude
        puts "Search engine has excludes in #{item_name}".red
        return false
      else
        puts "Search engine has excludes in #{item_name} but diding have any".blue
        return true
      end
    else
      puts "Search engine has not excludes".blue
      return true
    end
  end
# mike was here modafaka

  def parse_list(response)
    raise "Not implemented"
  end

  def query(current_page, per_page)
    raise "Not implemented"
  end

  def self.calculate_publisher_uid(pub)
    if not (pub.is_a?(Spider::Publisher))
        raise "Expected 'Spider::Publisher' param"
    end
    name = pub.name.nil? ? "": pub.name
    fb = pub.fb_page1.nil? ? "": pub.fb_page1
    ph = pub.phone1.nil? ? "": pub.phone1
    Digest::SHA1.hexdigest "#{name}.#{fb}.#{ph}"
  end

  def calculate_publisher_uid(pub)
    self.class.calculate_publisher_uid(pub)
  end

  def log(msg)
    puts "[#{@name}] #{msg}"
  end
end