require 'rubygems'
require 'mechanize'
require 'fuzzy_match'
require 'securerandom'
require 'cgi'
require 'meli'

require './lib/spider/base_spider'
require './lib/spider/yapo_spider'
require './lib/spider/ml_spider'
require './lib/spider/engine'
$meli = Meli.new()

def calculate_uid(pub)
    name = pub.name.nil? ? "": pub.name
    fb = pub.fb_page1.nil? ? "": pub.fb_page1
    ph = pub.phone1.nil? ? "": pub.phone1
    Digest::SHA1.hexdigest "#{name}.#{fb}.#{ph}"
end
brand_to_force = ARGV[0].to_i
persist = ENV["PERSIST"] || "false"
if not persist.nil?
  persist = ['true','1'].any? { |word| persist.include?(word) }
end
publications = Publication.joins(products: :brand)
                          .where(brands: { id: brand_to_force })
                          .where(publications: { manual_match: false })
                          .uniq
                          .count
puts "Forzing #{Brand.find(brand_to_force).name}"
Notification.new(name: "Parche Publicadores",
                   description: "Parche iniciado para publicadores de Yapo.cl",
                   process_uuid: "#{Brand.find(brand_to_force).name}").save
YP_spider = YapoSpider.new
ML_spider = MLSpider.new
def check_response(response)
    if response.nil?
        raise "NO VIGENTE"
    elsif response
        return response
    end
end
pubs_fixed = 0
total = publications/100
publisher_fixed = []
puts "Pages: #{total}"
(1..(total + 1)).each do |i|
    puts "In page #{i}"
    Publication.joins(products: :brand)
               .where(brands: { id: brand_to_force })
               .where(publications: { manual_match: false })
               .page(i)
               .per(100)
               .uniq
               .each do |publication|
        url = publication.url.to_s
        begin
            # if url.include? 'mercadolibre.cl'
            #     puts url
            #     response = ML_spider.fix_trash(url)
            #     current_condition = check_response(response)
            #     puts "ANTES  : #{condition_for(publication.condition)}"
            #     puts "DESPUES: #{condition_for(current_condition)}"
            #     if publication.condition == 2 and (current_condition == 1)
            #       pubs_fixed += 1
            #       publication.condition = current_condition
            #       puts publication.inspect.red
            #       publication.save if persist 
            #     end
            # end
            if url.include? 'yapo.cl'
                puts url
                response = YP_spider.fix_trash(url)
                publisher_temp = YP_spider.fix_publisher(url)
                if not publisher_temp.nil?
                    uid = calculate_uid(publisher_temp)
                    puts "#{uid}".yellow
                    source = Source.find_or_create_by(name: "yapo.cl")
                    publisher = Publisher.find_or_initialize_by(source: source, name: publisher_temp.name, uid: uid)
                    if publisher.new_record?
                        puts "New publisher fixed with #{uid}".red
                        publisher_fixed << {publisher: publisher, publication: publication}
                    end
                    publisher.fb_page1 = publisher_temp.fb_page1
                    publisher.phone1 = publisher_temp.phone1
                    publisher.save
                    publisher.publications << publication
                end
                current_condition = check_response(response)
                next if condition_for(current_condition) == "Desconocido"
                puts "ANTES  : #{condition_for(publication.condition)}"
                puts "DESPUES: #{condition_for(current_condition)}"
                if publication.condition == 2 and (current_condition == 1)
                  pubs_fixed += 1
                  publication.condition = current_condition
                  puts publication.inspect.red
                  publication.save if persist 
                end
            end
        rescue => e
            puts e.message
        end
    end
end
Notification.new(name: "Parche Publicadores",
                   description: "Parche finalizado, se corrigieron #{publisher_fixed.size} publicadores.",
                   process_uuid: "#{Brand.find(brand_to_force).name}").save
puts publisher_fixed
puts "Fixed pubs: #{pubs_fixed}".green
puts "Fixed publisher: #{publisher_fixed.size}".green
