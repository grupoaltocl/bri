Notification.new(name: "Parche Publicadores",
                   description: "Parche Iniciado para publicadores.",
                   process_uuid: "Yapo").save
publisher_fixed = 0
begin
    Publisher.where(publishers: { source_id: 1 }).group('uid').count.each do |pub|
        if pub[1] > 1
        publisher = Publisher.where(publishers: {uid: pub[0], source_id: 1} ).first
        pubs_copy = Publisher.where(publishers: {uid: pub[0], source_id: 1} )
        
        puts "#{publisher.id}".red
        pubs_copy.each do |pub_copy|
            if pub_copy.id != publisher.id
                puts "another publisher".red
                puts "#{pub_copy.id}".yellow
                
                pub_copy.publications.each do |pub|
                    publisher.publications << pub
                end
                pub_copy.delete
                publisher_fixed += 1
            end 
        end
        end
    end
    puts "YAPO PUBLISHER FIXED: #{publisher_fixed}"
    Notification.new(name: "Parche Publicadores",
                    description: "Parche finalizado, se corrigieron #{publisher_fixed} publicadores.",
                    process_uuid: "Yapo").save    
rescue
    puts "ERROR"
    Notification.new(name: "Parche Publicadores",
                description: "Parche finalizado con problemas",
                process_uuid: "Yapo").save
end
