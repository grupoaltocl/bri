require 'rubygems'

# sql = "DELETE FROM products_publications;"
# ActiveRecord::Base.connection.execute(sql)

# TODO: check if manual_match for all this fields
# Publication.where('publications.manual_match = ?',false).update_all(active: false, other: false, multi_prod: false)
# # Publication.where(manual_match: false).joins(products: :brand).select("publications.*, count(publication_id)").group('publications.id').having('count(publication_id) > 1').update(multi_prod: true)
# Publication.where(manual_match: false).joins(products: :brand).select("publications.*, count(products_publications.publication_id)").group('publications.id').having('count(products_publications.publication_id) > 1').update(multi_prod: true)
config = Configuration.first
multiprod_labels = []
if config.multiprod_labels && config.multiprod_labels.strip.size.size > 0
  multiprod_labels += config.multiprod_labels.split(',').map { |x| x.strip.downcase }
end

new_labels = []
if config.has_new_label && config.has_new_label.strip.size.size > 0
  new_labels += config.has_new_label.split(',').map { |x| x.strip.downcase }
end

not_new_labels = []
if config.has_not_new_label && config.has_not_new_label.strip.size.size > 0
  not_new_labels += config.has_not_new_label.split(',').map { |x| x.strip.downcase }
end

def copy_aliases(type,aliases)
  if type.aliases && type.aliases.strip.size.size > 0
    aliases += type.aliases.split(',').map { |x| x.strip.downcase }
  end
  return aliases
end
def copy_excludes(type,excludes)
  if type.excludes && type.excludes.strip.size.size > 0
    excludes += type.excludes.split(',').map { |x| x.strip.downcase }
  end
  return excludes
end

def check_save_condition(publication, category)
  if category.by_used && ( publication.condition == ApplicationHelper::USE_STATUS_USED || publication.condition == ApplicationHelper::USE_STATUS_NEW )
    puts "Publication can be activate by used, Publication condition #{publication.condition}".red
    return true
  else
    if publication.condition == ApplicationHelper::USE_STATUS_NEW
      puts "Publication cant be activate by used, Publication condition #{publication.condition} NEW".blue
      return true
    else
      puts "Publication cant be activate by used, Publication condition #{publication.condition} USED OR UNDEFINED".blue
      return false
    end
  end
end

# # #Asignando a cada marca de la categoria
Category.all.each do |category|
  next if category.id != 5
  puts "From category: #{category.name} change brand of publication".red
  publications = Publication.joins(products: :brand).where(brands: { category_id: category.id })
                                                    .where(publications: { manual_match: false} )
                                                    .uniq
  puts "Will be categorized '#{publications.count}' publications from category"
  category_aliases = []
  category_excludes = []
  category_aliases = copy_aliases(category,category_aliases)
  category_excludes = copy_excludes(category,category_excludes)

  category.brands.each do |brand|

    puts "Searching in #{brand.name}"
    brand_aliases = []
    brand_excludes = []
    brand_aliases = copy_aliases(brand,brand_aliases)
    brand_excludes = copy_excludes(brand,brand_excludes)
    puts "With aliases #{brand_aliases} and excludes#{brand_excludes}"
    next if brand_aliases.empty?
    publications.each do |publication|
      # puts "product of publication #{publication.id} #{publication.products}"
      publication_name = ''
      publication_name = publication.name.strip.downcase if not publication.name.nil?

      publication_description = ''
      publication_description = publication.description.strip.downcase if not publication.description.nil?

      has_brand_exclude = false
      has_brand_exclude_description = false
      brand_excludes.each do |x|
        if publication_name.include? x
          has_brand_exclude = true
          break
        end
        if publication_description.include? x
          has_brand_exclude_description = true
          break
        end
      end
      if brand.filter_by_category
        has_category_exclude = false
        has_category_exclude_description = false
        has_category_alias = false
        has_category_alias_description = false
        brand_excludes.each do |x|
          if publication_name.include? x
            has_category_exclude = true
          end
          if publication_description.include? x
            has_category_exclude_description = true
          end
        end

        brand_aliases.each do |x|
          if publication_description.include? x
            has_category_alias_description = true
          end
          if publication_name.include? x
            has_category_alias = true
          end
        end
      end
      if !category.nil? && (category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION)
        next if has_brand_exclude_description || has_brand_exclude
      else
        next if has_brand_exclude
      end

      has_brand_alias = false
      has_brand_alias_description = false
      brand_aliases.each do |x|
        if !category.nil? && (category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION)
          if publication_description.include? x
            has_brand_alias_description = true
            break
          end
        end
        if publication_name.include? x
          has_brand_alias = true
          break
        end
      end
      active_by_brand = false
      if publication.products.where.not(products: {filter_by_brand: nil}).count > 0
        active_by_brand = true
      end
      # puts "#{active_by_brand}  products: #{publication.products.where.not(products: {filter_by_brand: nil}).count}"
      do_save = false
      if !category.nil?
        case category.by_description
        when ApplicationHelper::ACTIVATE_BY_NAME
          if has_brand_alias
            # puts "Publication #{publication_name.red} active and other because of #{brand.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} MATCH with #{brand.name}, match in name".red
            # if active_by_brand
            #   publication.active = true 
            #   puts "Active by brand".red
            # end
            # if has_category_alias && !has_category_exclude
            #   publication.active = true
            #   puts "Activate by category"
            # end
            do_save = true
          end
        when ApplicationHelper::ACTIVATE_BY_DESCRIPTION
          if has_brand_alias_description
            # puts "Publication #{publication_name.red} active and other because of #{brand.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} MATCH with #{brand.name}, match in description".red
            # if active_by_brand
            #   publication.active = true 
            #   puts "Active by brand".red
            # end
            # if has_category_alias_description && !has_category_exclude_description
            #   publication.active = true
            #   puts "Activate by category"
            # end
            do_save = true
          end
        when ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION
          if has_brand_alias || has_brand_alias_description
            # puts "Publication #{publication_name.red} active and other because of #{brand.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} MATCH with #{brand.name}, match in name or description".red
            # if active_by_brand
            #   publication.active = true 
            #   puts "Active by brand".red
            # end
            # if ( has_category_alias_description || has_category_alias ) && ( !has_category_exclude_description || !has_category_exclude ) 
            #   publication.active = true
            #   puts "Activate by category"
            # end
            # if has_category_alias && !has_category_exclude
            #   publication.active = true
            #   puts "Activate by category"
            # end
            do_save = true
          end
        end
      else
        if has_brand_alias
          # puts "Publication #{publication_name.red} active and other because of #{brand.name.cyan}"
          puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} MATCH with #{brand.name}, match in name".red
          # if active_by_brand
          #   publication.active = true 
          #   puts "Active by brand".red
          # end
          do_save = true
        end
      end

      has_multiprod_label = false
      multiprod_labels.each do |x|
        if publication_name.include? x
          has_multiprod_label = true
          break
        end
        if publication_description.include? x
          has_multiprod_label = true
          break
        end
      end

      if do_save
        if has_multiprod_label
          publication.multi_prod = true
        end
        publication.products.each do |prod|
          prod.publications.delete(publication)
        end
        product = Product.find_or_initialize_by(name: brand.name + ' Otros')
        product.publications << publication
        publication.save
      end
    end
  end
end

# puts "Now Categorize by Brand"
Brand.all.each do |brand|
  
  puts "From brand: #{brand.name} to products, to publications".red
  
  category = Category.joins(:brands).where(brands: { id: brand.id }).to_a[0]
  puts "#{brand.name} #{brand.id} tiene categoria nula"
  next if category.nil? ||category.id != 5
  publications = Publication.joins(products: :brand)
                            .where(brands: { id: brand.id })
                            .where(publications: { manual_match: false })
                            .uniq
  puts "Will be categorized '#{publications.count}' publications from brand #{brand.name}"
  # puts category.methods
  # puts category.to_json
  #From brand aliases y excludes
  brand_aliases = []
  brand_excludes = []

  brand_aliases << brand.name
  brand_aliases = copy_aliases(brand,brand_aliases)
  brand_excludes = copy_excludes(brand,brand_excludes)

  # From products 
  products = brand.products
  products.each do |product|
    product_name = product.name.downcase.strip
    product_excludes = []
    product_aliases = []
    product_aliases << product_name
    product_aliases = copy_aliases(product,product_aliases)
    product_aliases = copy_excludes(product,product_aliases)
    base_product = product.base_product

    if base_product
      product_aliases = copy_aliases(base_product,product_aliases)
      product_excludes = copy_excludes(base_product,product_excludes)
    end
    if product.filter_by_brand
      product_aliases = copy_aliases(brand,product_aliases)
      product_excludes = copy_excludes(brand,product_excludes)
    end
    if brand.filter_by_category
      product_aliases = copy_aliases(category,product_aliases)
      product_excludes = copy_excludes(category,product_excludes)
    end

    puts "For product: #{product.name}".cyan
    p "Aliases: #{product_aliases}"
    p "Excludes: #{product_excludes}"

    publications.each do |publication|
      puts "Check #{publication}"
      publication_name = publication.name.nil? ? '' : publication.name.downcase.strip
      puts "NAME #{publication_name}"
      publication_description = ''
      publication_description = publication.description.strip.downcase if not publication.description.nil?
      puts "Description #{publication_description}"

      if publication.condition == ApplicationHelper::USE_STATUS_UNDEFINED
        publication.condition = ApplicationHelper::USE_STATUS_USED
        new_labels.each do |x|
          if publication_name.include? x
            # has_new_label = true
            # puts "include en el nombre".red
            publication.condition = ApplicationHelper::USE_STATUS_NEW
          end
          if publication_description.include? x
            # has_new_label = true
            # puts "include en la descripción".blue
            publication.condition = ApplicationHelper::USE_STATUS_NEW
          end
        end

        not_new_labels.each do |x|
          if publication_name.include? x
            # has_used_label = true
            publication.condition = ApplicationHelper::USE_STATUS_USED
          end
          if publication_description.include? x
            # has_used_label = true
            publication.condition = ApplicationHelper::USE_STATUS_USED
          end
        end
      end      

      has_product_exclude = false
      has_product_exclude_description = false
      product_excludes.each do |x|
        if publication_name.include? x
          has_product_exclude = true
          break
        end
        if publication_description.include? x
          has_product_exclude_description = true
          break
        end
      end

      if !category.nil? && (category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION)
        if has_product_exclude_description || has_product_exclude
          next
        end
      else
        if has_product_exclude
          next
        end
      end

      has_product_alias = false
      has_product_alias_description = false
      product_aliases.each do |x|
        if !category.nil? && (category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION)
          if publication_description.include? x
            has_product_alias_description = true
            break
          end
        end
        if publication_name.include? x
          has_product_alias = true
          break
        end
      end
      # puts !category.nil?
      do_save = false
      if !category.nil?
        case category.by_description
        when ApplicationHelper::ACTIVATE_BY_NAME
          if has_product_alias
            puts "Publication #{publication_name.red} active and other because of #{product.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of product match in name".red
            publication.active = true if check_save_condition(publication, category)
            do_save = true
          end
        when ApplicationHelper::ACTIVATE_BY_DESCRIPTION
          if has_product_alias_description
            puts "Publication #{publication_name.red} active and other because of #{product.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of product match in description".red
            publication.active = true if check_save_condition(publication, category)
            do_save = true
          end
        when ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION
          if has_product_alias || has_product_alias_description
            puts "Publication #{publication_name.red} active and other because of #{product.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of brand match in name or description".red
            publication.active = true if check_save_condition(publication, category)
            do_save = true
          end
        end
      else
        if has_product_alias
          puts "Publication #{publication_name.red} active and other because of #{product.name.cyan}"
          puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of product match in name".red
          publication.active = true if check_save_condition(publication, category)
          do_save = true
        end
      end

      if do_save
        publication.products << product
        if publication.products.where(products: { generic: false }).size > 1
          # publication.products.where(products: { generic: true }).each do |prod|
          #   prod.publications.delete(publication)
          # end
          publication.multi_prod = true
        end
        publication.save
        product.save
      end
    end
  end
end


# OLD CATEGORIZER IS DOWNS HERE, DON'T DELETE, IT'S PRETTY. FEEL OLD YET?

# require 'rubygems'

# # sql = "DELETE FROM products_publications;"
# # ActiveRecord::Base.connection.execute(sql)

# # TODO: check if manual_match for all this fields
# Publication.where('publications.manual_match = ?',false).update_all(active: false, other: false, multi_prod: false)
# # Publication.where(manual_match: false).joins(products: :brand).select("publications.*, count(publication_id)").group('publications.id').having('count(publication_id) > 1').update(multi_prod: true)
# Publication.where(manual_match: false).joins(products: :brand).select("publications.*, count(products_publications.publication_id)").group('publications.id').having('count(products_publications.publication_id) > 1').update(multi_prod: true)
# config = Configuration.first
# multiprod_labels = []
# if config.multiprod_labels && config.multiprod_labels.strip.size.size > 0
#     multiprod_labels += config.multiprod_labels.split(',').map { |x| x.strip.downcase }
# end

# new_labels = []
# has
# if config. = false && config. = false.strip.size.size > 0
# has
#     new_labels += config. = false.split(',').map { |x| x.strip.downcase }
# end

# not_new_labels = []
# if config.has_not_new_label && config.has_not_new_label.strip.size.size > 0
#     not_new_labels += config.has_not_new_label.split(',').map { |x| x.strip.downcase }
# end

# Brand.all.each do |brand|
#     puts "From brand: #{brand.name} to products, to publications".red

#     products = brand.products
#     publications = Publication.where(manual_match: false).joins(products: :brand).where(brands: {id: brand.id}).uniq
#     puts "Will be checked #{publications.count} publications".red
#     brand_name = brand.name.strip.downcase
    
#     brand_aliases = []
#     brand_excludes = []

#     brand_aliases << brand_name
#     if brand.aliases && brand.aliases.strip.size.size > 0
#         brand_aliases += brand.aliases.split(',').map { |x| x.strip.downcase }
#     end
    
#     if brand.excludes && brand.excludes.strip.size.size > 0
#         brand_excludes += brand.excludes.split(',').map { |x| x.strip.downcase }
#     end
    
    
#     products.each do |product|

#       product_name = product.name.downcase.strip
      
#       product_excludes = []
#       product_aliases = []

#       product_aliases << product_name
#       if product.aliases && product.aliases.strip.size > 0
#           product_aliases += product.aliases.split(',').map { |x| x.strip.downcase }
#       end
      
#       if product.excludes && product.excludes.strip.size > 0
#           product_excludes += product.excludes.split(',').map { |x| x.strip.downcase }
#       end
      
      
#       base_product = product.base_product
#       if base_product

#           if base_product.aliases && base_product.aliases.strip.size > 0
#               product_aliases += base_product.aliases.split(',').map { |x| x.strip.downcase }
#           end
          
#           if base_product.excludes && base_product.excludes.strip.size > 0
#               product_excludes += base_product.excludes.split(',').map { |x| x.strip.downcase }
#           end
#       end


#         puts "For product: #{product.name}".cyan
#         p "Aliases: #{product_aliases}"
#         p "Excludes: #{product_excludes}"
        
#         publications.each do |publication|
#             if publication.manual_match == true
#                 next
#             end
#             publication_name = publication.name.nil? ? "" : publication.name.downcase.strip

#             if product.filter_by_brand
#                 has_brand_alias = false
#                 brand_aliases.each do |x| 
#                     if publication_name.include? x
#                         has_brand_alias = true
#                         break
#                     end
#                 end
#                 next if not has_brand_alias
            
#                 # puts "TIENE ALIAS CON LA MARCA".blue
#                 has_brand_exclude = false
#                 brand_excludes.each do |x| 
#                     if publication_name.include? x
#                         has_brand_exclude = true
#                         break
#                     end
#                 end
#                 next if has_brand_exclude
#             end

#             has_product_alias = false
#             product_aliases.each do |x|
#                 if publication_name.include? x
#                     has_product_alias = true 
#                     break
#                 end
#             end
#             next if not has_product_alias
#             has_product_exclude = false
#             product_excludes.each do |x|
#                 if publication_name.include? x
#                     has_product_exclude = true 
#                     break
#                 end
#             end
#             next if has_product_exclude

#             puts "Publication #{publication_name.blue} matches #{product_name.red}"
#             if publication.condition == ApplicationHelper::USE_STATUS_NEW 
#                 publication.active = true
#             else
#                 publication.active = false
#             end
#             puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of product match".red
#             if publication.products.size > 0
#                 publication.multi_prod = true
#             end
#             publication.products << product
#             publication.save
#         end
#         # p "***************** SIGUIENTE PRODUCTO ************************"
#     end
#     # puts "***************** SIGUIENTE MARCA ************************".red
# end

# Brand.all.each do |brand|
#     puts "From brand: #{brand.name} to publications".red

#     brand_name = brand.name.strip.downcase

#     brand_aliases = []
#     brand_excludes = []

#     brand_aliases << brand_name
#     if brand.aliases && brand.aliases.strip.size > 0
#         brand_aliases += brand.aliases.split(',').map { |x| x.strip.downcase }
#     end
    
#     if brand.excludes && brand.excludes.strip.size > 0
#         brand_excludes += brand.excludes.split(',').map { |x| x.strip.downcase }
#     end

#     publications = Publication.joins(products: :brand).where(brands: {id: brand.id}).uniq
#     puts "For brand: #{brand_name}".cyan
#     p "Aliases: #{brand_aliases}"
#     p "Excludes: #{brand_excludes}"

#     publications.each do |publication|
#         if publication.manual_match == true
#             next
#            
#         # puts "***************** SIGUIENTE PUBLICACION ************************".yellow
#         publication_name = ""
#         publication_name = publication.name.strip.downcase if not publication.name.nil?

#         publication_description = ""
#         publication_description = publication.description.strip.downcase if not publication.description.nil?

#         next if publication.products.count > 1

#         has_brand_alias = false
#         brand_aliases.each do |x| 
#             if publication_name.include? x
#                 has_brand_alias = true
#                 break
#             end
#         end

#         has_brand_exclude = false
#         brand_excludes.each do |x| 
#             if publication_name.include? x
#                 has_brand_exclude = true
#                 break
#             end
#         end

#         if publication.condition == ApplicationHelper::USE_STATUS_UNDEFINED
#             publication.condition = ApplicationHelper::USE_STATUS_USED
            
#             new_labels.each do |x|
#                 if publication_name.include? x
#                     publication.condition = ApplicationHelper::USE_STATUS_NEW
#                 end
#                 if publication_description.include? x
#                     publication.condition = ApplicationHelper::USE_STATUS_NEW
#                 end
#             end

#             not_new_labels.each do |x|
#                 if publication_name.include? x
#                     publication.condition = ApplicationHelper::USE_STATUS_USED
#                 end
#                 if publication_description.include? x
#                     publication.condition = ApplicationHelper::USE_STATUS_USED
#                        
#             end
#         end
                

#         doSave = false
#         if has_brand_alias && (not has_brand_exclude)
#             puts "Publication #{publication_name.red} active and other because of #{brand_name.cyan}"
#             puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} ACTIVE because of brand match in name".red

#             publication.active = publication.condition != ApplicationHelper::USE_STATUS_USED
#             doSave = true
#         end
        
#         has_multiprod_label = false
#         multiprod_labels.each do |x| 
#             if publication_name.include? x
#                 has_multiprod_label = true
#                 break
#             end
#             if publication_description.include? x
#                 has_multiprod_label = true
#                 break
#             end
#         end

#         if has_multiprod_label
#             publication.multi_prod = true
#             doSave = true
#         end

#         if doSave
#             publication.save
#         end

#     end
#     # puts "***************** SIGUIENTE MARCA ************************".red
# end

