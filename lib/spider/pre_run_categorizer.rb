require 'rubygems'

config = Configuration.first

reset_cat_uuid = ENV["RESET"] || "false"
if not reset_cat_uuid.nil?
  reset_cat_uuid = ['true','1'].any? { |word| reset_cat_uuid.include?(word) }
end
if reset_cat_uuid
    config.cat_uuid = SecureRandom.uuid
    config.save
end
multiprod_labels = []
if config.multiprod_labels && config.multiprod_labels.strip.size.size > 0
  multiprod_labels += config.multiprod_labels.split(',').map { |x| x.strip.downcase }
end

new_labels = []
if config.has_new_label && config.has_new_label.strip.size.size > 0
  new_labels += config.has_new_label.split(',').map { |x| x.strip.downcase }
end

not_new_labels = []
if config.has_not_new_label && config.has_not_new_label.strip.size.size > 0
  not_new_labels += config.has_not_new_label.split(',').map { |x| x.strip.downcase }
end

def copy_aliases(type,aliases)
  if type.aliases && type.aliases.strip.size.size > 0
    aliases += type.aliases.split(',').map { |x| x.strip.downcase }
  end
  return aliases
end
def copy_excludes(type,excludes)
  if type.excludes && type.excludes.strip.size.size > 0
    excludes += type.excludes.split(',').map { |x| x.strip.downcase }
  end
  return excludes
end

def check_save_condition(publication, category)
  if category.by_used && ( publication.condition == ApplicationHelper::USE_STATUS_USED || publication.condition == ApplicationHelper::USE_STATUS_NEW )
    puts "Publication can be activate by used, Publication condition #{publication.condition}".red
    return true
  else
    if publication.condition == ApplicationHelper::USE_STATUS_NEW
      puts "Publication cant be activate by used, Publication condition #{publication.condition}".blue
      return true
    else
      puts "Publication cant be activate by used, Publication condition #{publication.condition} USED OR UNDEFINED".blue
      return false
    end
  end
end

# # #Asignando a cada marca de la categoria
Category.all.each do |category|
  puts "From category: #{category.name} change brand of publication".red
  publications = Publication.joins(products: :brand).where(brands: { category_id: category.id })
                                                    .where(publications: { manual_match: false} )
                                                    .uniq
  puts "Will be categorized '#{publications.count}' publications from category"
  category_aliases = []
  category_excludes = []
  category_aliases = copy_aliases(category,category_aliases)
  category_excludes = copy_excludes(category,category_excludes)

  category.brands.each do |brand|
    if brand.cat_uuid == config.cat_uuid
        puts "Already checked"
        next 
    end
    puts "Searching in #{brand.name}"
    brand_aliases = []
    brand_excludes = []
    brand_aliases = copy_aliases(brand,brand_aliases)
    brand_excludes = copy_excludes(brand,brand_excludes)
    puts "With aliases #{brand_aliases} and excludes#{brand_excludes}"
    if brand_aliases.empty?
        brand.cat_uuid = config.cat_uuid
        brand.save
        next
    end
    publications.each do |publication|
      # puts "product of publication #{publication.id} #{publication.products}"
      publication_name = ''
      publication_name = publication.name.strip.downcase if not publication.name.nil?

      publication_description = ''
      publication_description = publication.description.strip.downcase if not publication.description.nil?

      has_brand_exclude = false
      has_brand_exclude_description = false
      brand_excludes.each do |x|
        if publication_name.include? x
          has_brand_exclude = true
          break
        end
        if publication_description.include? x
          has_brand_exclude_description = true
          break
        end
      end
      if brand.filter_by_category
        has_category_exclude = false
        has_category_exclude_description = false
        has_category_alias = false
        has_category_alias_description = false
        brand_excludes.each do |x|
          if publication_name.include? x
            has_category_exclude = true
          end
          if publication_description.include? x
            has_category_exclude_description = true
          end
        end

        brand_aliases.each do |x|
          if publication_description.include? x
            has_category_alias_description = true
          end
          if publication_name.include? x
            has_category_alias = true
          end
        end
      end
      if !category.nil? && (category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION)
        next if has_brand_exclude_description || has_brand_exclude
      else
        next if has_brand_exclude
      end

      has_brand_alias = false
      has_brand_alias_description = false
      brand_aliases.each do |x|
        if !category.nil? && (category.by_description == ApplicationHelper::ACTIVATE_BY_DESCRIPTION || category.by_description == ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION)
          if publication_description.include? x
            has_brand_alias_description = true
            break
          end
        end
        if publication_name.include? x
          has_brand_alias = true
          break
        end
      end
      active_by_brand = false
      if publication.products.where.not(products: {filter_by_brand: nil}).count > 0
        active_by_brand = true
      end
      # puts "#{active_by_brand}  products: #{publication.products.where.not(products: {filter_by_brand: nil}).count}"
      do_save = false
      if !category.nil?
        case category.by_description
        when ApplicationHelper::ACTIVATE_BY_NAME
          if has_brand_alias
            puts "Publication #{publication_name.red} active and other because of #{brand.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} MATCH with #{brand.name}, match in name".red
            do_save = true
          end
        when ApplicationHelper::ACTIVATE_BY_DESCRIPTION
          if has_brand_alias_description
            puts "Publication #{publication_name.red} active and other because of #{brand.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} MATCH with #{brand.name}, match in description".red
            do_save = true
          end
        when ApplicationHelper::ACTIVATE_BY_NAME_AND_DESCRIPTION
          if has_brand_alias || has_brand_alias_description
            puts "Publication #{publication_name.red} active and other because of #{brand.name.cyan}"
            puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} MATCH with #{brand.name}, match in name or description".red
            do_save = true
          end
        end
      else
        if has_brand_alias
          puts "Publication #{publication_name.red} active and other because of #{brand.name.cyan}"
          puts "#{publication.id.to_s.green} #{publication.name.to_s.purple} MATCH with #{brand.name}, match in name".red
          do_save = true
        end
      end

      has_multiprod_label = false
      multiprod_labels.each do |x|
        if publication_name.include? x
          has_multiprod_label = true
          break
        end
        if publication_description.include? x
          has_multiprod_label = true
          break
        end
      end

      if do_save
        if has_multiprod_label
          publication.multi_prod = true
        end
        publication.products.each do |prod|
          prod.publications.delete(publication)
        end
        product = Product.find_or_initialize_by(name: brand.name + ' Otros')
        product.publications << publication
        publication.save
      end
    end
    brand.cat_uuid = config.cat_uuid
    brand.save
  end
end
