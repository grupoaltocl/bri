require 'rubygems'
require 'date'
require 'securerandom'
begin
    total_pubs = Publication.where("publications.last_seen_at < ?", (Time.now-3.months)).where(:active => false).count
    uuid = SecureRandom.uuid
    Notification.new(name: "Eliminando publicaciones antiguas",
                    description: "Se eliminarán #{total_pubs} publicaciones",
                    process_uuid: uuid).save    
    Publication.where("publications.last_seen_at < ?", (Time.now-3.months)).where(:active => false).delete_all
    Notification.new(name: "Publicaciones eliminadas correctamente",
                description: "Se eliminaron #{total_pubs} publicaciones",
                process_uuid: uuid).save
rescue => exception
    Notification.new(name: "Error al eliminar publicaciones",
            description: "No se pudieron eliminar",
            process_uuid: uuid).save
end