require 'rubygems'
require 'mechanize'
require 'fuzzy_match'
require 'securerandom'
require 'cgi'

agent = Mechanize.new

def currency_to_number(currency)
  currency.to_s.gsub(/[$.]/,'').to_f
end

# if File.exist?("cookies.yaml")
#   agent.cookie_jar.load("cookies.yaml")
# else
#   login_page = agent.get("/login")
#   form  = login_page.form_with(action: "/login")
#   form.username = login
#   form.password = password
#   home_page = agent.submit
# end

class Engine

  def initialize(spiders, brands, categories, persist=false)
    @spiders = spiders
    @brands = brands
    @persist = persist
    @categories = categories
    uuid = SecureRandom.uuid
    @uuid = uuid
    @pubs_per_search = 0
    Notification.new(name: "Scraper",
                     description: "Scraper inicializado para #{categories.size} categorias y #{brands.size} Marcas",
                     process_uuid: uuid).save
  end

  def get_snapshot(publication)
    snap = PublicationSnapshot.new
    snap.name = strip_emoji(publication.name.truncate(140))
    snap.description = strip_emoji(publication.description)
    snap.price = publication.price > 99999999 ? 0 : publication.price.to_i
    snap
  end

  def copy_to_publication(publication,publication_temp,search_engine)
    publication.city_raw = publication_temp.city_raw
    publication.county_raw = publication_temp.county_raw
    publication.description = strip_emoji(publication_temp.description)
    publication.finished_at = publication_temp.finished_at
    publication.image = publication_temp.image
    publication.multi_prod = publication_temp.multi_prod
    publication.name = strip_emoji(publication_temp.name.truncate(140))
    publication.price = publication_temp.price.to_i > 99999999 ? 0 : publication_temp.price.to_i
    publication.price2 = publication_temp.price.to_i > 99999999 ? 0 : publication_temp.price.to_i
    publication.published_at = publication_temp.published_at
    publication.state_raw = publication_temp.state_raw
    publication.url = publication_temp.url
    publication.condition = publication_temp.condition || ApplicationHelper::USE_STATUS_UNDEFINED
    publication.manual_match = false
    publication.last_seen_at = Time.zone.now
    # s_e = SearchEngine.find(search_engine)
    search_engine.publications << publication
    puts "Asignado search engine #{search_engine}"
    # publication.find_by = search_engine.id
    return publication
  end

  def copy_to_publisher(publisher_temp,source,spider)
    begin
      uid = spider.calculate_publisher_uid(publisher_temp)
      publisher = Publisher.find_or_initialize_by(source: source, name: publisher_temp.name, uid: uid)
    rescue Exception => e
      # sometimes the returned user has strange content "<!Document ..."
      puts e.message
      puts e.backtrace.inspect
      return nil
    end
    publisher.county_raw = publisher_temp.county_raw
    publisher.city_raw = publisher_temp.city_raw
    publisher.url = publisher_temp.url
    publisher.fb_page1 = publisher_temp.fb_page1
    publisher.phone1 = publisher_temp.phone1
    return publisher
  end

  def get_keywords(search_keywords,search_engine, source)
    search_engine.search_keywords.split(',').each do |search|
      search_keywords << search.strip.downcase
    end
    case source.name
    when "yapo.cl"
      unless search_engine.keywords_yp.nil?
        search_engine.keywords_yp.split(',').each do |search|
          search_keywords << search.strip.downcase
        end
      end
    when "mercadolibre.cl"
      unless search_engine.keywords_ml.nil?
        search_engine.keywords_ml.split(',').each do |search|
          search_keywords << search.strip.downcase
        end
      end
    when "Facebook/Marketplace"
      unless search_engine.keywords_mp.nil?
        search_engine.keywords_mp.split(',').each do |search|
          search_keywords << search.strip.downcase
        end
      end
    end
    search_keywords
  end

  def strip_emoji str
    if str.nil? || str.empty?
      str = ""
    end
    # str = str.force_encoding('utf-8').encode
    # clean_text = ""

    # # emoticons  1F601 - 1F64F
    # regex = /[\u{1f600}-\u{1f64f}]/
    # clean_text = str.gsub regex, ''

    # #dingbats 2702 - 27B0
    # regex = /[\u{2702}-\u{27b0}]/
    # clean_text = clean_text.gsub regex, ''

    # # transport/map symbols
    # regex = /[\u{1f680}-\u{1f6ff}]/
    # clean_text = clean_text.gsub regex, ''

    # # enclosed chars  24C2 - 1F251
    # regex = /[\u{24C2}-\u{1F251}]/
    # clean_text = clean_text.gsub regex, ''

    # # symbols & pics
    # regex = /[\u{1f300}-\u{1f5ff}]/
    # clean_text = clean_text.gsub regex, ''
    clean_text = str.each_char.select { |char| char.bytesize < 4 }.join
  end

  def by_brand
    @spiders.each do |spider|
      source = Source.find_or_create_by(name: spider.class.name)
      @brands.each do |brand|
        next if brand.search_engine.nil?
        next if !brand.search_engine.nil? && !brand.search_engine.active
        puts "Searching with #{spider} brand: '#{brand.name}'".blue
        # unless brand.search_engine.active?
        #   next
        # end
        # Creating a new generic producto or search for it
        # product = Product.find_or_initialize_by(name: brand.name+" Otros")
        product = Product.find_or_initialize_by(name: brand.name.chomp('Otros').strip + ' Otros')
        unless product.new_record? 
          puts "Generic product for #{brand.name} created"
        end
        search_engine = brand.search_engine
        search_engine.brands = brand
        search_engine.save
        if search_engine.search_keywords.nil?
          search_engine.search_keywords = search_engine.name
          search_engine.save
        end
        search_keywords = []
        search_keywords = get_keywords(search_keywords, search_engine, source)
        spider.setup(search_engine, source.name)
        puts search_keywords
        search_keywords.each do |search_by|
          spider.next_item(search_by) do |item|
            next if item.nil?
            break if item.url == 'https://www.yapo.cl/ayuda/.htm'
            @pubs_per_search += 1
            puts "Analyzing #{item.name}".cyan
            puts "With #{item.url}".cyan
            publication = Publication.find_or_initialize_by(url: item.url)
            publication.last_seen_at = Time.zone.now

            # If mobile yapo scraping has no name, uncomment this.
            # if publication.name.empty?
            #   publication_temp = spider.get_full_item(item)
            #   publication.name = publication_temp.name
            # end
            unless publication.new_record? 
              if publication.manual_match == true
                puts "Publication #{publication.id} skip for manual match".purple
                next
              end
              if publication.force_scraper
                puts "Forzing scraper to publicacion with id: #{publication.id}"
                publication_temp = spider.get_full_item(item)
                next if publication_temp.nil?
                publisher_temp = spider.get_publisher(item)
                next if publisher_temp.nil?
                copy_to_publication(publication, publication_temp, search_engine)
                publication.force_scraper = false
                publisher = copy_to_publisher(publisher_temp,source,spider)
                snap = get_snapshot(publication_temp)
                snap.publication = publication
                publisher.save if @persist
              else
                nameChanged = item.name.truncate(140) != publication.name
                priceChanged = item.price.to_i != publication.price2.to_i
                if (nameChanged || priceChanged) && !item.name.truncate(140).empty?
                  puts "Publicatin Changed".yellow
                  publication.name = strip_emoji(item.name.truncate(140))
                  publication.save if @persist
                  puts "Taking a new snapshot".yellow
                  publication_temp = spider.get_full_item(item)
                  next if publication_temp.nil?
                  snap = get_snapshot(publication_temp)
                  snap.publication = publication
                  snap.save if @persist
                else
                  puts "Skipping item with id #{publication.id}. Already in DB and did not change!".purple
                  if publication.publication_snapshots.count == 0
                    puts "There were no snapshots for publication. Creating one.".yellow
                    publication_temp = spider.get_full_item(item)
                    snap = get_snapshot(publication_temp)
                    snap.publication = publication
                    snap.save if @persist
                  end
                end
              end
              puts "Adding product #{product} to the pub: #{publication.id}"
              product.publications << publication rescue ActiveRecord::RecordNotUnique
              brand.products << product rescue ActiveRecord::RecordNotUnique
              brand.search_engine.publications << publication
              publication.save if @persist
              product.save if @persist
              brand.save if @persist
              next
            end

            # create or update
            puts 'creando publicación'

            publication_temp = spider.get_full_item(item)
            if publication_temp.nil?
              next
            end
            publisher_temp = spider.get_publisher(item)
            # puts "#{publisher_temp}".red
            publisher = copy_to_publisher(publisher_temp, source,spider)
            copy_to_publication(publication,publication_temp, search_engine)
            snap = get_snapshot(publication_temp)
            snap.publication = publication

            publication.scraper_uid = @uuid if publication.changed?
            begin
              if @persist
                publisher.save
                puts "PUBLISHER"
                puts publisher.to_s
                publisher.display_name = publisher.to_s
                publisher.save
                publication.save
                product.save
                brand.save
                puts "Publication #{publication.id} saved".blue

                snap.save
                publisher.publications << publication
                product.publications << publication rescue ActiveRecord::RecordNotUnique
                brand.products << product rescue ActiveRecord::RecordNotUnique
              end
            rescue Exception => e
              # sometimes the returned user has strange content "<!Document ..."
              puts e.message
              puts e.backtrace.inspect
              next
            end
            
          end
          
          p spider.reset_query
          search_keywords = []
        end
      end
    end
  end

  def by_category
    @spiders.each do |spider|
      source = Source.find_or_create_by(name: spider.class.name)
      SearchEngine.joins(:category).where('search_engines.active = ?',true).each do |search_engine|

        category = Category.find(search_engine.category_ids[0])
        # brand = Brand.find_or_initialize_by(name: category.name+" Otros")
        brand = Brand.find_or_initialize_by(name: category.name.chomp('Otros').strip + ' Otros')
        # product = Product.find_or_initialize_by(name: category.name+" Otros")
        product = Product.find_or_initialize_by(name: category.name.chomp('Otros').strip + ' Otros')
        brand.generic = true
        product.generic = true
        puts "Searching with #{spider} search engine: '#{search_engine.name}'".blue
        search_keywords = []
        search_keywords = get_keywords(search_keywords,search_engine, source)

        # next if search_keywords.nil?
        spider.setup(search_engine, source.name)
        puts search_keywords
        search_keywords.each do |search_by|
          spider.next_item(search_by) do |item|
            next if item.nil?
            break if item.url == 'https://www.yapo.cl/ayuda/.htm'
            puts "Analyzing #{item.name}".cyan
            puts "With #{item.url}".cyan
            publication = Publication.find_or_initialize_by(url: item.url)
            publication.last_seen_at = Time.zone.now
            unless publication.new_record? 
              if publication.manual_match == true
                puts "Publication #{publication.id} skip for manual match".purple
                next
              end
              if publication.force_scraper
                puts "Forzing scraper to publicacion with id: #{publication.id}"
                publication_temp = spider.get_full_item(item)
                next if publication_temp.nil?
                publisher_temp = spider.get_publisher(item)
                next if publisher_temp.nil?
                copy_to_publication(publication, publication_temp, search_engine)
                publisher = copy_to_publisher(publisher_temp,source,spider)
                publication.force_scraper = false
                snap = get_snapshot(publication_temp)
                snap.publication = publication
                # publication.save if @persist
                publisher.save if @persist
              else
                nameChanged = item.name.truncate(140) != publication.name
                priceChanged = item.price.to_i != publication.price2.to_i
                if (nameChanged || priceChanged) && !item.name.truncate(140).empty?
                  puts "Publicatin Changed".yellow
                  publication.name = strip_emoji(item.name.truncate(140))
                  publication.price2 = item.price.to_i > 99999999 ? 0 : item.price.to_i
                  publication.save if @persist
                  puts "Taking a new snapshot".yellow
                  publication_temp = spider.get_full_item(item)
                  next if publication_temp.nil?
                  snap = get_snapshot(publication_temp)
                  snap.publication = publication
                  snap.save if @persist
                else
                  puts "Skipping item with id #{publication.id}. Already in DB and did not change!".purple
                  if publication.publication_snapshots.count == 0
                    puts "There were no snapshots for publication. Creating one.".yellow
                    publication_temp = spider.get_full_item(item)
                    snap = get_snapshot(publication_temp)
                    snap.publication = publication
                    snap.save if @persist
                  end
                end
              end
              # always save to store last seen value
              puts "Adding product #{product} to the pub: #{publication.id}"
              product.publications << publication rescue ActiveRecord::RecordNotUnique
              brand.products << product rescue ActiveRecord::RecordNotUnique
              product.brand_id = brand.id
              # publication.find_by = 
              search_engine.publications << publication
              publication.save if @persist
              product.save if @persist
              brand.save if @persist
              next
            end

            # create or update
            # puts "#{item}"
            puts "creating publication for #{item.id}"

            publication_temp = spider.get_full_item(item)
            next if publication_temp.nil?

            publisher_temp = spider.get_publisher(item)
            # puts "#{publisher_temp}".red
            # puts "#{publication_temp}".red
            publisher = copy_to_publisher(publisher_temp,source,spider)
            # puts "#{publisher}".red
            copy_to_publication(publication,publication_temp,search_engine)
            # puts "#{publication}".red
            snap = get_snapshot(publication_temp)
            snap.publication = publication

            snap = get_snapshot(publication_temp)
            snap.publication = publication

            publication.scraper_uid = @uuid if publication.changed?
            begin
              if @persist
                publisher.publications << publication
                product.publications << publication rescue ActiveRecord::RecordNotUnique
                # product.brand << product rescue ActiveRecord::RecordNotUnique
                brand.products << product rescue ActiveRecord::RecordNotUnique
                category.brands << brand rescue ActiveRecord::RecordNotUnique
                publisher.save
                puts "PUBLISHER"
                puts publisher.to_s
                publisher.display_name = publisher.to_s
                publisher.save                
                publication.save
                snap.save
                product.save
                brand.save
                category.save
                puts "Publication #{publication.id} saved".blue
                # puts "Persisting".cyan
              end
            rescue Exception => e
              # sometimes the returned user has strange content "<!Document ..."
              puts e.message
              puts e.backtrace.inspect
              next
            end
          end
          p spider.reset_query
          search_keywords = []
        end
      end
    end
  end

end