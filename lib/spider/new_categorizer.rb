require 'rubygems'
include ApplicationHelper
config = Configuration.first
config.cat_uuid = SecureRandom.uuid
config.save
# reset_cat_uuid = ENV["RESET"] || "false"
# if not reset_cat_uuid.nil?
#   reset_cat_uuid = ['true','1'].any? { |word| reset_cat_uuid.include?(word) }
# end
# if reset_cat_uuid
#   config.cat_uuid = SecureRandom.uuid
#   config.save
# end
$multiprod_labels = []
if config.multiprod_labels && config.multiprod_labels.strip.size > 0
  $multiprod_labels += config.multiprod_labels.split(',').map { |x| x.strip.downcase }
end

$new_labels = []
if config.has_new_label && config.has_new_label.strip.size > 0
  $new_labels += config.has_new_label.split(',').map { |x| x.strip.downcase }
end

$not_new_labels = []
if config.has_not_new_label && config.has_not_new_label.strip.size > 0
  $not_new_labels += config.has_not_new_label.split(',').map { |x| x.strip.downcase }
end

# def copy_aliases(type,aliases)
#   if type.aliases && type.aliases.strip.size > 0
#     aliases += type.aliases.strip.split(',').map { |x| x.strip.downcase }
#   end
#   return aliases
# end
# def copy_excludes(type,excludes)
#   if type.excludes && type.excludes.strip.size > 0
#     excludes += type.excludes.strip.split(',').map { |x| x.strip.downcase }
#   end
#   return excludes
# end

def check_save_condition(publication, category)
  if category.by_used && ( publication.condition == ApplicationHelper::USE_STATUS_USED || publication.condition == ApplicationHelper::USE_STATUS_NEW )
    puts "Publication can be activate by used, Publication condition #{publication.condition}".red
    return true
  else
    if publication.condition == ApplicationHelper::USE_STATUS_NEW
      puts "Publication can be activate by new, Publication condition NEW".blue
      return true
    else
      puts "Publication cant be activate by used, Publication condition #{publication.condition} USED OR UNDEFINED".blue
      return false
    end
  end
end

def set_condition(publication, publication_name, publication_description)
  if publication.url.include? 'www.facebook.com'
    puts "Source facebook, condition as new"
    publication.condition = ApplicationHelper::USE_STATUS_NEW
  else
    if publication.condition == ApplicationHelper::USE_STATUS_UNDEFINED
      publication.condition = ApplicationHelper::USE_STATUS_NEW
    end
  end
  if publication.condition == ApplicationHelper::USE_STATUS_NEW
    $not_new_labels.each do |x|
      if publication_name.downcase.include? x
        puts "Has used label in name"
        publication.condition = ApplicationHelper::USE_STATUS_USED
      end
      if publication_description.downcase.include? x
        puts "Has used label in description"
        publication.condition = ApplicationHelper::USE_STATUS_USED
      end
    end
  end
end

def set_multi(publication_name,publication_description)
  has_multiprod_label = false
  $multiprod_labels.each do |x|
    if publication_name.include? x
      has_multiprod_label = true
      # break
    end
    if publication_description.include? x
      has_multiprod_label = true
      # break
    end
  end
  return has_multiprod_label
end

begin
  config.run_cat = true
  config.save
  Publication.where(publications: {active_cat: true}).update_all(active_cat: false)
  puts Time.now
  Notification.new(name: "Categorizador",
                   description: "Categorizador inicializado",
                   process_uuid: config.cat_uuid).save
  # # #Asignando a cada marca de la categoria
  all_pubs = 0
  all_categories = 0
  Category.all.each do |category|
    puts "From category: #{category.name} change brand of publication".red
    # next if not category.id == 3
    publications = Publication.joins(products: :brand).where(brands: { category_id: category.id })
                              .where(publications: { manual_match: false, active_cat: false })
                              .uniq
                              .count
    puts "Will be categorized '#{publications}' publications from category"
    all_pubs += publications
    all_categories += 1
    puts "Setting up brands in #{category}"
    brand_in_category = set_category(category)
    puts "All brands checked"
    puts "Category Aliases empty".red if category.aliases.empty?
    # puts "#{brand_in_category}"
    total = publications/10000
    (1..(total+1)).each do |i|
      pubs = Publication.joins(products: :brand)
                        .where(brands: { category_id: category.id })
                        .where(publications: { manual_match: false,
                                               active_cat: false })
                        .page(i)
                        .per(10000)
                        .uniq
      publi = []
      publi << pubs
      puts "Procesing #{pubs.count} in page #{i}".red
      Parallel.map(publi, in_threads: 1) do | parallel_pubs |
        ActiveRecord::Base.connection.reconnect!
        puts "Threading #{parallel_pubs.size} publications".red
        parallel_pubs.each do |publication|
          puts "Checking #{publication} with id #{publication.id}".blue
          publication_name = ''
          publication_name = publication.name.strip.downcase if not publication.name.nil?

          publication_description = ''
          publication_description = publication.description.strip.downcase if not publication.description.nil?
          set_condition(publication, publication_name, publication_description)
          has_multi_prod_label = set_multi(publication_name,publication_description)
          publication.multi_prod if has_multi_prod_label
          puts "Check with brands"
          checked_with_brand = check_with_brand(publication_name, publication_description, brand_in_category, category)
          pub_products = checked_with_brand[:pub_products]
          pub_brands = checked_with_brand[:pub_brands]
          puts "Brand with aliases #{pub_brands}"
          puts "products with aliases #{pub_products}"
          publication.products.delete_all
          has_products = set_products(publication, pub_products, pub_brands, category)
          if publication.products.empty?
            puts "PUBLICATION WITHOUT ANY PRODUCT ".red
            s_e = SearchEngine.find_by_id(publication.search_engine_id)
            if not s_e.nil?
              puts "HAVE SEARCH ENGINE #{s_e.name} #{s_e.id}"
              if not s_e.nil?
                puts "#{s_e}"
                if not s_e.category.empty?
                  se_cat = s_e.category.first
                  puts "FIND BY CATEGORY #{se_cat}"
                  brand = Brand.find_or_initialize_by(name: se_cat.name.chomp('Otros').strip + ' Otros')
                  product = Product.find_or_initialize_by(name: brand.name.chomp('Otros').strip + ' Otros')
                  puts "WILL BE ASSIGN TO THE GENERIC PRODUCT: #{product.name} OF BRAND: #{brand.name}"
                  brand.generic = true
                  brand.save
                  brand.products << product
                  brand.save
                  product.filter_by_brand = true
                  product.generic = true
                  product.save
                  publication.products << product
                else
                  brand = Brand.where(search_engine_id: s_e.id).first
                  puts "FIND BY BRAND #{brand}"
                  product = Product.find_or_initialize_by(name: brand.name.chomp('Otros').strip + ' Otros')
                  puts "WILL BE ASSIGN TO THE GENERIC PRODUCT: #{product.name} OF BRAND: #{brand.name}"
                  brand.products << product
                  product.filter_by_brand = true
                  product.generic = true
                  product.save
                  publication.products << product
                end
              else
                product = Product.find_or_initialize_by(name: category.name.chomp('Otros').strip + ' Otros')
                puts "WILL BE ASSIGN TO THE GENERIC PRODUCT: #{product.name} OF CATEGORY: #{category.name}"
                if product.brand.nil?
                  brand = Brand.find_or_initialize_by(name: category.name + ' Otros')
                  brand.generic = true
                  brand.products << product
                  brand.save
                  category.brands << brand
                end
                product.filter_by_brand = true
                product.generic = true
                product.save
                publication.products << product
              end
            else
              product = Product.find_or_initialize_by(name: category.name.chomp('Otros').strip + ' Otros')
              puts "WILL BE ASSIGN TO THE GENERIC PRODUCT: #{product.name} OF CATEGORY: #{category.name}"
              if product.brand.nil?
                brand = Brand.find_or_initialize_by(name: category.name + ' Otros')
                brand.generic = true
                brand.products << product
                brand.save
                category.brands << brand
              end
              product.filter_by_brand = true
              product.generic = true
              product.save
              publication.products << product
            end
          end
          if not pub_products.empty?
            if has_products && check_save_condition(publication, category)
              puts "has products"
              publication.active_cat = true
            else
              puts "without any product"
              publication.active_cat = false
            end
            if publication.products.where(products: {generic: false}).size > 0
              publication.multi_prod = true
              publication.products.where(products: {generic: true}).each do |prod|
                prod.publications.delete(publication)
              end
            end
          end
          puts "ACTIVE?: #{publication.active_cat}"
          publication.save
        end
        puts "Updating publications"
        publications = Publication.where(id: parallel_pubs.map(&:id))
        puts "Deactivate publications"
        publications.update_all(active: false)
        puts "Activate publications"
        publications.where(publications: { active_cat: true })
              .update_all(active: true, active_cat: false)
      end
    end
    # puts "Updating publications"
    # # publications = Publication.joins(products: :brand)
    # # 						  .where(brands: { category_id: category.id })
    # # 						  .where(publications: { manual_match: false } )
    # # 						  .uniq
    # publications.update_all(active: false)
    # publications.where(publications: { active_cat: true })
    # 			.update_all(active: true, active_cat: false)
    puts "All publications updated"
    puts "NEXT CATEGORY".red
  end
  Notification.new(name: "Categorizador", 
                   description: "Categorizador finalizado correctamente para #{all_categories} Rubros y #{all_pubs} publicaciones",
                   process_uuid: config.cat_uuid).save
  puts "Categorizer finished".red
  puts Time.now

rescue Exception => e
  Notification.new(name: "Categorizador",
                   description: "Categorizador finalizado con problemas",
                   process_uuid: config.cat_uuid).save
  puts 'The categorizer failed unexpectitly:'
  puts e.message
ensure
  config.run_cat = false
  config.save
  Publication.where(publications: {active_cat: true}).update_all(active_cat: false)
end