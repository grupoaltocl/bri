require 'rubygems'
require 'securerandom'
# def to_pdf(path, url)
#     invoke = command(path)
#     invoke = invoke + " --print-to-pdf=#{path} #{url}"
#     puts "#{invoke}".yellow
#     begin
#         Timeout::timeout(20) {
#             @popen_result = IO.popen(invoke, "wb+")
#             Process.wait @popen_result.pid
#             @popen_result.close_write
#             @popen_result.gets(nil) if path.nil?
#             if $?.exitstatus == 0
#                 return { "pid" => $?.pid, "existstatus"=> $?.exitstatus}
#             end
#             # end

#         }
#     rescue Timeout::Error
#         Process.kill 9, @popen_result.pid
#         Process.wait @popen_result.pid 
#         puts "La obtención del snapshot de #{url} tardó demasiado. Se mató el proceso.".red
#         return {}
#     end
#     # exec(invoke) if fork == nil
#     # exec(invoke) if fork == nil
#     # exec("/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome --headless --disable-gpu --print-to-pdf=#{path} #{url}") if fork == nil
#     # puts "#{result}".red
# end

def to_pdf_face(path,url)
	invoke = "node "+File.expand_path(File.dirname(File.dirname(__FILE__)))+"/spider/node_snapshot.js #{url} #{path}"
	
	puts invoke
	begin
		Timeout::timeout(30) {
				@popen_result = IO.popen(invoke, "wb+")
				Process.wait @popen_result.pid
				@popen_result.close_write
				@popen_result.gets(nil) if path.nil?
				
				if $?.exitstatus == 0
					begin
						Process.kill 9, @popen_result.pid
						Process.wait @popen_result.pid						
					rescue
						puts "No process with id"
					ensure
						return { "pid" => $?.pid, "existstatus"=> $?.exitstatus}
					end
				else
					begin
						Process.kill 9, @popen_result.pid
						Process.wait @popen_result.pid
					rescue
						puts "no process in else"
					end
				end
				# end

		}
	rescue Timeout::Error
			Process.kill 9, @popen_result.pid
			Process.wait @popen_result.pid 
			puts "La obtención del snapshot de #{url} tardó demasiado. Se mató el proceso.".red
			return {}
	end
end
total_pubs = Publication.active.valid.count
uuid = SecureRandom.uuid
puts "Publication with snapshot #{total_pubs}".red
counter = 0
total = total_pubs/10000
Notification.new(name: "Snapshot",
			description: "Iniciando Snapshot para #{total_pubs} publicaciones",
			process_uuid: uuid).save
(1..(total+1)).each do |i|
	Publication.active.valid.page(i).per(10000).each do |publication|
		# break if counter > 100
		# next if publication.url.include? 'facebook'
		snap = publication.publication_snapshots.order(updated_at: :desc).first
		next if snap.nil? || (not snap.snapshot.file.nil?)
		
		puts "Obteniendo imagen para #{publication.name} id: #{publication.id}"
		
		# pdf_snap = PDFKit.new(publication.url, :quiet => true)  
		path = Dir.pwd+"/tmp/bri_snap_#{publication.id}.pdf"
		snapshot_result = {}
		source = publication.publisher.source.name
		next if source.nil?
		# next if source != "Facebook/Marketplace"

		snapshot_result = to_pdf_face(path, publication.url)
		# # puts "#{snapshot_result.empty?}".red
		if !snapshot_result.empty?
			if not File.zero?(path)
				begin
					snap.snapshot_at = DateTime.now
					if publication.url.include? 'facebook'
						doc = HexaPDF::Document.open(path)
						canvas = doc.pages[0].canvas(type: :overlay)
						canvas.font('Helvetica', size: 20)
						text = "Publicador: #{publication.publisher.name}\nLink publicador: #{publication.publisher.fb_page1}\nFecha obtencion: #{snap.snapshot_at}\nFecha publicacion: #{publication.published_at}"
						canvas.text(text, at: [10, 350])
						doc.write(path, validate: false)
					end
					File.open(path) do |f|
						snap.snapshot = f
					end        
					
					snap.save!
					puts "Snapshot de #{publication.id} guardado".blue
				rescue Exception => e
					puts "Error en obtención de snapshot".red
				end
			else
				puts "No se pudo encontrar el directorio #{path} del snapshot".red
			end
		else
			puts "No se pudo obtener el snapshot de #{publication.id}".red
		end
		# counter += 1
	end
end
Notification.new(name: "Snapshot",
                description: "Snapshot finalizado con exito",
                process_uuid: uuid).save
# else
#     puts "No Chrome headless"
# end

