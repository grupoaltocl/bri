require 'rubygems'
require 'mechanize'
require 'fuzzy_match'
require 'securerandom'
require 'cgi'
require './lib/spider/base_spider'
require './lib/spider/spider_model'
require 'digest/sha1'

# if File.exist?("cookies.yaml")
#   agent.cookie_jar.load("cookies.yaml")
# else
#   login_page = agent.get("/login")
#   form  = login_page.form_with(action: "/login")
#   form.publisher = login
#   form.password = password
#   home_page = agent.submit
# extract_description



class YapoSpider < BaseSpider

  @source = Source.find_or_create_by(name: "yapo.cl")
  @base_url = "http://example.com"
  @name = "yapo.cl"

  class << self
    attr_accessor :name
    attr_accessor :base_url
  end

  # def initialize(start_url="", persist=false)
  #   source_str = "yapo.cl"
    
  #   @agent = Mechanize.new
  #   @persist = persist

  # end
  def initialize
    @agent = Mechanize.new
    super
  end

  # def query(current_page, per_page)

  # end

  def query(current_page, per_page, search_by)
    url = "http://m.yapo.cl/chile?q=#{CGI.escape(search_by)}&o=#{current_page}"
    puts "Checking url: #{url} in YapoSpider".red
    begin
      return @agent.get(url)
    rescue Mechanize::ResponseCodeError => e
      return nil
    end
  end

  def currency_to_number(currency)
    currency.to_s.gsub(/[$.]/,'').to_f
  end

  def extract_description(node)
    r = node.search(".//div[contains(@class,'textBox')]/p").first
    r = r.inner_html if r
    r
  end

  def extract_thumbnail(node)
    false
  end

  def extract_main_image(node)
    r = node.search(".//div[contains(@class,'gallery')]/div/span/img").first
    r = r.attributes["src"] if r
    r
  end

  def extract_all_images(node)
    false
  end

  def extract_name(node)
    r = node.search(".//h3[contains(@class,'title')]/text()").first
    r = r.text.strip if r
    r
  end

  def extract_county(node)
    r = node.search(".//h2[contains(@class,'commune')]/text()").first
    r = r.text.strip if r
    r
  end

  def extract_city(node)
    r = node.search(".//h2[contains(@class,'region')]/text()").first
    r = r.text.strip if r
    r
  end

  def extract_price(node)
    r = node.search(".//p[contains(@class,'price')]").first
    r = currency_to_number(r.text.strip) if r
    r
  end

  def extract_phone(node)
    r = node.search(".//div[contains(@class,'call-show-number text-center')]/text()").first
    r = r.text.strip if r
    r
  end

  def extract_id(node)
    r = node.search(".//a/@id").first
    r = r.value if r
    r
  end

  def extract_url(node)
    r = node.search(".//a/@href").first
    r = r.value if r
    id = extract_id(node)
    link = r.split('//')[1]
    county = link.split('/')[1]
    url = "https://www.yapo.cl/#{county}/#{id}.htm"
    url
  end

  def extract_all(node)
    false
  end

  def extract_publisher(node)
    r = nil
    node.search(".//div[contains(@class,'user-info')]").each do |detail_node|
      r = detail_node.search(".//h3[contains(@class,'name')]").first
    end
    r = r.text.strip if r
    r
  end

  def extract_fb_page(node)
    r = node.search(".//div[contains(@class,'frame-rounded')]/img").first
    if not r.blank?
      r = r.attributes["src"]
      r = r.text.strip
      m = /facebook.com\/(\d+)\//.match(r)
      if not m.nil?
        fbid = m[1]
        r = "https://www.facebook.com/#{fbid}"
      end
    end
    r
  end

  def extract_published_at(node)
    r = node.search(".//div[contains(@class,'date')]")
    r = r.text.strip if r
    r.gsub!("Enero", "january")
    r.gsub!("Febrero", "February")
    r.gsub!("Marzo", "march")
    r.gsub!("Abril", "april")
    r.gsub!("Mayo", "may")
    r.gsub!("Mayo", "june")
    r.gsub!("Julio", "july")
    r.gsub!("Agosto", "august")
    r.gsub!("Septiembre", "september")
    r.gsub!("Octubre", "october")
    r.gsub!("Noviembre","november")
    r.gsub!("Agosto","august") # This is for translation problems with date parser in ruby
    r.gsub!("Diciembre","december")
    puts "date #{r}"
    r
  end

  def deep_name_extract(node)
    # puts "TESTING DEEP NAME SEARCH"
    r = node.search(".//div[contains(@class,'daview-title box')]/h1")
    r = r.text.strip if r
    # puts r
    r
  end

  def extract_next_page(node)
    r = node.search("//span[contains(@class, 'nohistory')]/a[contains(text(),'Próxima página')]/@href").first;
    r = r.text.to_s if r
    r
  end

  def guess_condition(node_detail)
    # tagged as new
    r = node_detail.search(".//li[contains(@class,'box-two ad-param')]/div[contains(@class,'box-right')]").first
    if not r.nil? then 
      if r.text and not r.text.to_s.match(/nuev[oa][s]?/i).nil? then
        puts "Found a new item based on tag!".red
        return ApplicationHelper::USE_STATUS_NEW
      end
      if r.text and r.text.to_s.match(/usad[oa][s]?/i)  
        puts "Found a new used item based on details!".yellow
        return ApplicationHelper::USE_STATUS_USED
      end 
    end

    node_detail.search(".//div[contains(@class,'details')]/table/tbody/tr").each do |n|
      r = n.search("./th")
      r.each do |value|
        if value.text and value.text.to_s == "Nuevo/Usado" then
          result = n.search("./td")
          if result.text and result.text.to_s.match(/nuev[oa][s]?/i) then
            puts "Found a new item based on details!".red
            return ApplicationHelper::USE_STATUS_NEW
          else 
            if result.text and result.text.to_s.match(/usad[oa][s]?/i)  
              puts "Found a new used item based on details!".yellow
              return ApplicationHelper::USE_STATUS_USED
            end
          end
        end
      end
    end
    return ApplicationHelper::USE_STATUS_UNDEFINED
  end


  def get_url(url, color=:cyan)
    color_url = url.send color
    puts "Getting url '#{url}' ..."
    @agent.get(url)
  end

  def show_record(record)
    puts "o--".blue
    puts "  User: #{record.publisher}".green
    puts "Nombre: #{record.name}".green
    puts "Precio: #{record.price}".green
    # puts "Nuevo?: #{false}".green
    puts "   URL: #{record.url}".green
    # puts "ImgURL: #{record.name}".green
    puts "--o".blue
  end


  def parse_list(response)
    result = []
    begin
      # puts response.body
      response.search("//li[contains(@class,'ad')]").each do |node|
        item = Spider::Publication.new
        item.name = extract_name(node)
        item.price = extract_price(node)
        # url = extract_url(node)
        item.url = extract_url(node)
        result << item
      end
      result
    rescue
      puts "Error in parse list (YP)"
      return result
    end
  end

  def get_full_item item
    begin
      detail_page = @agent.get(item.url.sub('www.','m.'))
      if item.name == ""
        item.name = deep_name_extract(detail_page)
      end
      item.description = extract_description(detail_page)
      item.condition = guess_condition(detail_page)
      item.published_at = Date.parse(extract_published_at(detail_page).to_s)
      item.county_raw = extract_county(detail_page)
      item.city_raw = extract_city(detail_page)
      # puts item
      item      
    rescue
      puts "Error in full item (YP)"
      return nil
    end

  end

  def fix_trash url
    begin
      detail_page = @agent.get(url.sub('www.','m.'))
      return guess_condition(detail_page)
    rescue
      return nil
    end
  end

  def fix_publisher url
    begin
      detail_page = @agent.get(url.sub('www.','m.'))
      publisher = Spider::Publisher.new
      publisher.name = extract_publisher(detail_page)
      publisher.phone1 = extract_phone(detail_page)
      publisher.fb_page1 = extract_fb_page(detail_page)
      puts publisher.name
      puts publisher.phone1
      puts publisher.fb_page1
      return publisher
    rescue
      puts "Error in fix publisher (YP)"
      return nil
    end
  end
  def get_publisher item
    begin
      detail_page = @agent.get(item.url.sub('www.','m.'))
      publisher = Spider::Publisher.new
      publisher.name = extract_publisher(detail_page)
      publisher.phone1 = extract_phone(detail_page)
      publisher.fb_page1 = extract_fb_page(detail_page)
      publisher.county_raw = extract_county(detail_page)
      # publisher.county_raw = r["country_id"]
      # publisher.city_raw = r["address"]["city"]
      publisher.url = item.url
      return publisher
      # item.publisher = extract_publisher(detail_page)
      # item
    rescue
      puts "Error in get publisher (YP)"
      return nil
    end
  end

  def self.calculate_publisher_uid(pub)
    if not (pub.is_a?(Spider::Publisher))
      raise "Expected 'Spider::Publisher' param"
    end
    name = pub.name.nil? ? "": pub.name
    fb = pub.fb_page1.nil? ? "": pub.fb_page1
    ph = pub.phone1.nil? ? "": pub.phone1
    if pub.fb_page1.nil? && pub.phone1.nil?
      county = pub.county_raw.nil? ? "": pub.county_raw
      Digest::SHA1.hexdigest "#{name}.#{county}"
    else
      Digest::SHA1.hexdigest "#{name}.#{fb}.#{ph}"
    end
  end

end