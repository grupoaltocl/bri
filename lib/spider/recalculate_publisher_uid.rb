require './lib/spider/yapo_spider'
require './lib/spider/ml_spider'
require './lib/spider/mp_spider'
require './lib/spider/spider_model'

puts ENV["PERSIST"] 
persist = ENV["PERSIST"] || false
if not persist.nil?
  # persist = ['true','1'].any? { |word| persist.include? (word) }
end

if persist == false
  puts "Will NOT persist".green
else
  puts "WARNING will PERSIST".red
end

sleep 3
total_publishers = Publisher.all.count
puts "Se calculará stolen score para #{total_publishers} publicaciones".red
total = total_publishers/10000
(1..(total+1)).each do |i|
  puts "EN PÁGINA #{i}".yellow
  Publisher.page(i).per(10000).each do |pub|
      if pub.source.name == "yapo.cl"
          spiderClass = YapoSpider
      elsif pub.source.name == "mercadolibre.cl"
          spiderClass = MLSpider
      elsif pub.source_id = 3
        spiderClass = MPSpider
      end
      item = Spider::Publisher.new
      item.fb_page1 = pub.fb_page1
      item.name = pub.name
      item.phone1 = pub.phone1
      item.url = pub.url
      begin
        pub.uid = spiderClass.calculate_publisher_uid(item)
        puts "#{pub.name.blue} \ncalculated uid: #{pub.uid.yellow}".green
        # pub.save
        if persist
          pub.save
        end      
      rescue Exception => e
        puts "Error in publisher #{pub.id}".red
                      puts e.message
              puts e.backtrace.inspect
      end

  end
end
