publications = Publication.where("publications.url LIKE ?", "https://www.yapo.cl%").count
total = publications/10000
(1..(total+1)).each do |i|
    pubs = Publication.where("publications.url LIKE ?", "https://www.yapo.cl%").page(i).per(10000).uniq
    pubs.each do |publication|
        url = publication.url
        publication.url = url.gsub('http://', 'https://')
        publication.save!
        clone_pubs = Publication.where(:url => publication.url).order(:id)
        if clone_pubs.size > 1
            to_delete = []
            clone_pubs.each_with_index{
                |val,idx|
                if idx == 0
                    next
                else
                    to_delete << val.id
                end
            }
            Publication.delete(to_delete)
        end
    end
end
def clean_url(id)
    code = id
    'http://articulo.mercadolibre.cl/'+code+'-_JM'
end
publications = Publication.where("publications.url LIKE ?", "http://articulo.mercadolibre.cl/%").count
total = publications/10000
(1..(total+1)).each do |i|
    pubs = Publication.where("publications.url LIKE ?", "http://articulo.mercadolibre.cl/%").page(i).per(10000).uniq
    pubs.each do |publication|
        puts "For publication #{publication.id} with #{publication.url}"
        url = publication.url
        publication.url = clean_url(url.match(/((MLC)-\d)\w+/)[0])
        puts "To #{publication.url}".red
        publication.save!
        clone_pubs = Publication.where(:url => publication.url).order(:id)
        if clone_pubs.size > 1
            to_delete = []
            clone_pubs.each_with_index{
                |val,idx|
                if idx == 0
                    next
                else
                    puts "Deleting #{val.id} duplicated"
                    to_delete << val.id
                end
            }
            Publication.delete(to_delete)
        end
    end
end
publications = Publication.where("publications.url LIKE ?", "https://www.facebook.com%").count
total = publications/10000
(1..(total+1)).each do |i|
    pubs = Publication.where("publications.url LIKE ?", "https://www.facebook.com%").page(i).per(10000).uniq
    pubs.each do |publication|
        puts "checking #{publication.id}"
        url = publication.url
        clone_pubs = Publication.where(:url => publication.url).order(:id)
        puts "with #{clone_pubs.count} copies"
        if clone_pubs.size > 1
            to_delete = []
            clone_pubs.each_with_index{
                |val,idx|
                if idx == 0
                    next
                else
                    to_delete << val.id
                end
            }
            Publication.delete(to_delete)
        end
    end
end