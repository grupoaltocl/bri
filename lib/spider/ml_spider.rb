require 'rubygems'
require 'meli'
# require './meli_cli'
require './lib/spider/base_spider'
require './lib/spider/spider_model'

class MLSpider < BaseSpider
    @source = Source.find_or_create_by(name: "mercadolibre.cl")
    @name = "mercadolibre.cl"
    @@item_dict = {}
    @@seller_dict = {}

    class << self
        attr_accessor :name
        attr_accessor :test
        attr_accessor :extract_name
    end    

    # def initialize()
    #     @@name = @name
    # end
    # def name
    #     return @name
    # end
    # @meli = Meli.new()
    def query(current_page, per_page, search_by)
        offset = (current_page-1)*per_page
        url = "/sites/MLC/search?q=#{search_by}&limit=#{per_page}&offset=#{offset}"
        puts "Checking url: #{url} in MLSpider".red
        begin
            response = $meli.get(url)
        rescue
            puts "Error in query (ML)"
            return nil
        end
    end
    def clean_url(id)
        if id.include? 'MLC'
            code = id.split('MLC')[1]
            'http://articulo.mercadolibre.cl/MLC-'+code+'-_JM'
        else
            nil
        end
    end
    def parse_list(response)
        result = []
        begin
            r = JSON.parse(response.body)
            r["results"].each do |value| 
                puts "#{value}".red
                product_id = value["id"]
                product_permalink = clean_url(value["id"])
                puts "#{product_permalink}".red
                puts "#{product_id}".red
                unless @@item_dict.has_key?(product_id)
                    # p value
                    pub = Spider::Publication.new
                    pub.name = value["title"]
                    pub.id = value["id"]
                    pub.url = clean_url(value["id"])
                    pub.seller_id = value["seller"]["id"]
                    pub.price = value["price"].nil? ? 0 : value["price"].to_i
                    # byebug
                    result << pub        
                    @@item_dict[product_id] = product_permalink
                end
            end
            result
        rescue
            puts "Error in parse list (ML)"
            return result
        end
    end


    def get_full_item node
        begin
            description = $meli.get("/items/#{node.id}/description")
            response = $meli.get("/items/#{node.id}")
            # p response.body
            r = JSON.parse(response.body)
            d = JSON.parse(description.body)
            r.merge!(d)
            puts "#{r}".yellow
            pub = Spider::Publication.new
            pub.city_raw = ""
            pub.county_raw = r["seller_address"]["city"]["name"]
            pub.description = r["text"].nil? ? "" : r["text"]
            pub.finished_at = r["stop_time"].nil? ? nil : r["stop_time"]
            pub.image = r["thumbnail"]
            pub.multi_prod = r["available_quantity"].to_i < 2 ? false : true
            pub.name = r["title"].nil? ? "" : r["title"]
            pub.price = r["price"].nil? ? 0 : r["price"]
            pub.published_at = r["start_time"].nil? ? Time.now : r["start_time"]
            pub.state_raw = r["seller_address"]["state"]["name"]
            pub.url = clean_url(r["id"])
            condition = ""
            if r["condition"] == "new"
                condition = ApplicationHelper::USE_STATUS_NEW
            elsif r["condition"]=="used" 
                condition = ApplicationHelper::USE_STATUS_USED
            else 
                condition = ApplicationHelper::USE_STATUS_UNDEFINED
            end
            pub.condition = condition
            return pub
        rescue
            puts "Error in full item (ML)"
            return nil
        end
    end

    def get_publisher node
        begin
            seller_id = node.seller_id
            unless @@seller_dict.has_key?(seller_id)
                response = $meli.get("/users/#{seller_id}")
                r = JSON.parse(response.body)
                
                # puts "> #{r}".red
                publisher = Spider::Publisher.new
                publisher.name = r["nickname"]
                publisher.county_raw = r["address"]["city"]
                publisher.city_raw = r["country_id"]
                publisher.url = r["permalink"]   
                @@seller_dict[seller_id] = publisher         
                return publisher
            else
                return @@seller_dict[seller_id]       
            end
        rescue
            puts "Error in get publisher (ML)"
            return nil
        end
    end

    def search_by_nickname(nickname)
        # p @@seller_dict
        @@seller_dict.detect { |seller| seller[1]["nickname"]==nickname}
    end
    def extract_finished_at(node)
        return node["stop_time"]
    end

    def extract_start_at(node)
        begin
            response = $meli.get("/items/#{node["id"]}")
            # p response.body
            r = JSON.parse(response.body)
            return r["start_time"]
        rescue
            return nil
        end
    end

    def extract_multiproduct(node)
        # p node["available_quantity"].to_i 
        if node["available_quantity"].to_i < 2
            return false
        else
            return true
        end
    end    

    def extract_description(node)
        begin
            product_id = node["id"]
            response = $meli.get("/items/#{product_id}/description")
            r = JSON.parse(response.body)
            return r["plain_text"]
        rescue
            puts "Error in get description (ML)"
            return nil
        end
    end

    def extract_name(node)
        return node["title"]
    end

    def extract_price(node)
        return node["price"]
    end

    def extract_link(node)
        return node["permalink"]
    end

    def extract_id(node)
        return node["id"]
    end

    def extract_publication_state(node)
        return node["address"]["state_name"]
    end

    def extract_publication_city(node)
        return node["address"]["city_name"]
    end
    def self.calculate_publisher_uid(pub)
        if not (pub.is_a?(Spider::Publisher))
            raise "Expected 'Spider::Publisher' param"
        end
        url = pub.url.nil? ? "": pub.url
        # puts "pa mercaolibre #{pub.url}"
        Digest::SHA1.hexdigest "#{url}"
    end
    def fix_trash url
        begin
            code = url.split('-')[1]
            response = $meli.get("/items/MLC#{code}")
            r = JSON.parse(response.body)
            condition = ""
            if r["condition"] == "new"
                condition = ApplicationHelper::USE_STATUS_NEW
            elsif r["condition"]=="used" 
                condition = ApplicationHelper::USE_STATUS_USED
            else 
                condition = ApplicationHelper::USE_STATUS_UNDEFINED
            end
            
            return condition
        rescue
            return nil
        end
    end
end