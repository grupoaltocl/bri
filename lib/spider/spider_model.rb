
module Spider
  class Publication
    @city_raw
    @county_raw
    @description
    @finished_at
    @image
    @multi_prod
    @name
    @price
    @price2
    @published_at
    @state_raw
    @url
    @condition
    @id
    @seller_id

    attr_accessor :city_raw, :county_raw, :description, :finished_at, :image, :multi_prod, :name, :price, :price2, :published_at, :state_raw, :url, :condition, :id, :seller_id
  end

  class Publisher
    @name
    @fb_page1
    @county_raw
    @city_raw
    @url
    @phone1
    attr_accessor :county_raw, :city_raw, :url, :name, :fb_page1, :phone1
  end
end
