const puppeteer = require('puppeteer');

var argvs = process.argv.slice(2);
console.log('argvs: ', argvs);
console.log(argvs[0])
console.log(argvs[1])
let url = argvs[0].toString()
let path = argvs[1].toString()
let snapshotFace = (async (url,path) => {
    try {
        // const browser = await puppeteer.launch();
        const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] })
        const page = await browser.newPage();
        mediaType = "screen";
        page.emulateMedia(mediaType);
        await page.goto(argvs[0].toString(), { waitUntil: 'networkidle2' });
        // await page.waitForSelector('body > div > div > div > div > div > div > div > div > div > div > div > div > img')
        console.log('First URL with image: ');
        await page.pdf({ path: argvs[1].toString(), printBackground: true });

        await browser.close();
        
    } catch (error) {
        console.log(error)
        return {}
    }
})();

