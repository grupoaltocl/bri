require 'active_admin/views/index_as_table'

module ActiveAdmin
  module Views
    class IndexAsCustomDashboard < IndexAsTable

      def build(page_presenter, collection)
        if group_by_attribute = page_presenter[:group_by_attribute]
          line_chart1 = collection.active.group_by_day(:published_at).count.sort
          line_chart2 = collection.group_by_day(:published_at).count.sort

          line_chart1 = [
            {name: "activas", data: line_chart1},
            {name: "total", data: line_chart2}
          ]
          
          # line_chart2 = collection.group_by_day(:published_at).where('publication'.'condition' = '2').count.sort

          pie_chart1 = collection.select("publications.name").joins(:products).group('products.id').uniq.count.map { |x| [Product.find(x[0]).name, x[1]] }
          pie_chart1 = [["Sin resultados", 1]] if pie_chart1.size.zero?
          pie_chart2 = collection.select("publications.name").joins(:products).group('products.id').uniq.sum(:price).map { |x| [Product.find(x[0]).name, x[1]] }
          pie_chart2 = [["Sin resultados", 1]] if pie_chart2.size.zero?          
          # line_chart2 = collection.group_by_day(:published_at).where(condition)
          render partial: "dashboard", locals: {line_chart1: line_chart1,line_chart2: line_chart2, pie_chart1: pie_chart1, pie_chart2: pie_chart2 }
        else
          super
        end
      end

    end
  end
end