class AddSourceToPublisher < ActiveRecord::Migration[5.0]
  def change
    add_reference :publishers, :source, foreign_key: true
  end
end
