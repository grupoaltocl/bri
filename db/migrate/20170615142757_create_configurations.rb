class CreateConfigurations < ActiveRecord::Migration[5.0]
  def change
    create_table :configurations do |t|
      t.decimal :factor_a
      t.decimal :factor_b 
      t.decimal :factor_c 
      t.decimal :factor_d 
      t.decimal :factor_e
      t.decimal :factor_f
      t.decimal :factor_g
      t.string :has_label1_ss 
      t.string :has_label2_ss 
      t.integer :publisher_freq_ss
      t.integer :many_updates_ss

      t.timestamps
    end
  end
end
