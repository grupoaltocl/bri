class AddScrapedAtToConfigurations < ActiveRecord::Migration[5.0]
  def change
    add_column :configurations, :sys_scraped_at, :datetime
  end
end
