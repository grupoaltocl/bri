class RenameItemRecordFieldsRecordsToPublication < ActiveRecord::Migration[5.0]
  def change
    rename_column :publication_snapshots, :item_record_id, :publication_id
  end
end
