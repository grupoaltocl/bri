class AddMissingFieldsToBrand < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :aliases, :string
    add_column :brands, :excludes, :string
  end
end
