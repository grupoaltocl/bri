class ChangeDecimalsPrecisionField < ActiveRecord::Migration[5.0]
  def change
    change_column :configurations, :factor_a, :decimal, :precision => 5, :scale => 2
    change_column :configurations, :factor_b, :decimal, :precision => 5, :scale => 2
    change_column :configurations, :factor_c, :decimal, :precision => 5, :scale => 2
    change_column :configurations, :factor_d, :decimal, :precision => 5, :scale => 2
    change_column :configurations, :factor_e, :decimal, :precision => 5, :scale => 2
    change_column :configurations, :factor_f, :decimal, :precision => 5, :scale => 2
    change_column :configurations, :factor_g, :decimal, :precision => 5, :scale => 2
    change_column :products, :price, :decimal, :precision => 10, :scale => 2
    change_column :publication_snapshots, :price, :decimal, :precision => 10, :scale => 2
    change_column :publication_groups, :price, :decimal, :precision => 10, :scale => 2
    change_column :publications, :price, :decimal, :precision => 10, :scale => 2
    change_column :publications, :match_score, :decimal, :precision => 5, :scale => 2
    change_column :publishers, :max_stolen_score, :decimal, :precision => 5, :scale => 2
  end
end
