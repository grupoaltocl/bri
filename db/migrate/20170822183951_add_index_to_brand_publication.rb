class AddIndexToBrandPublication < ActiveRecord::Migration[5.0]
  def change
    remove_index :brands_publications, [:brand_id, :publication_id] if index_exists?(:brands_publications, [:brand_id, :publication_id])
    execute <<-HEREDOC
CREATE TABLE temp AS 
  SELECT DISTINCT brand_id, publication_id FROM brands_publications;
HEREDOC
  execute <<-HEREDOC  
TRUNCATE TABLE brands_publications;
HEREDOC
execute <<-HEREDOC
INSERT INTO brands_publications(brand_id,publication_id)
  SELECT brand_id,publication_id FROM temp;
HEREDOC
execute <<-HEREDOC
DROP TABLE temp;
HEREDOC
    add_index :brands_publications, [:brand_id, :publication_id], :unique => true
    add_index :brands_publications, :brand_id
  end
end
