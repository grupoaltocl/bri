class AddCopyUrlToItemRecords < ActiveRecord::Migration[5.0]
  def up
    add_column :item_records, :url, :string
    remove_reference :item_records, :url, index: true
  end
 
  def down
    # pending
  end
end
