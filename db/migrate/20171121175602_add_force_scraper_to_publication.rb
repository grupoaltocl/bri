class AddForceScraperToPublication < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :force_scraper, :bool, default: false
  end
end
