class AddCategorizerUuidToBrand < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :cat_uuid, :string, default: ""
    add_column :configurations, :cat_uuid, :string, default: ""
  end
end
