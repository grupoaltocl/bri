class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :uid
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone
      t.text :note
      t.references :suitcase, foreign_key: true
      t.boolean :watch
      t.string :status

      t.timestamps
    end
  end
end
