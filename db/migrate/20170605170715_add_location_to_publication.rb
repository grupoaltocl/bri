class AddLocationToPublication < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :county_raw, :string
    add_column :publications, :state_raw, :string
    add_column :publications, :city_raw, :string
  end
end
