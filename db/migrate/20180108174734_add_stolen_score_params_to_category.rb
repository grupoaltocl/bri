class AddStolenScoreParamsToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :factor_a, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :disc_limit1, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :disc_limit2, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :disc_limit3, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :factor_b, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :factor_c, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :factor_d, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :many_updates_ss , :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :factor_e, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :publisher_freq_ss, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :factor_f, :decimal, :precision => 5, :scale => 2, default: 0
    add_column :categories, :has_label1_ss, :string
    add_column :categories, :has_label2_ss, :string
  end
end


