class AddMultiprodLabelsToConfiguration < ActiveRecord::Migration[5.0]
  def change
    add_column :configurations, :multiprod_labels, :string
  end
end
