class CreateJoinTableSuitcasePublicationGroup < ActiveRecord::Migration[5.0]
  def change
    create_join_table :suitcases, :publication_groups do |t|
      t.index [:suitcase_id, :publication_group_id], :name => 'index_cases_p_groups_on_suitcase_id_and_p_group_id'
      t.index [:publication_group_id, :suitcase_id], :name => 'index_cases_p_groups_on_p_group_id_and_suitcase_id'
    end
  end
end

