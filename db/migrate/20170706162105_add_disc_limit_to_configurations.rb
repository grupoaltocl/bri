class AddDiscLimitToConfigurations < ActiveRecord::Migration[5.0]
  def change
    add_column :configurations, :disc_limit1, :decimal, :precision => 5, :scale => 2
    add_column :configurations, :disc_limit2, :decimal, :precision => 5, :scale => 2
    add_column :configurations, :disc_limit3, :decimal, :precision => 5, :scale => 2
  end
end
