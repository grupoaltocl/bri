class CreateJoinTableSuitcasePublication < ActiveRecord::Migration[5.0]
  def change
    create_join_table :suitcases, :publications do |t|
      t.index [:suitcase_id, :publication_id]
      t.index [:publication_id, :suitcase_id]
    end
  end
end
