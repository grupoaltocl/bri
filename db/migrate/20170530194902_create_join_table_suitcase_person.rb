class CreateJoinTableSuitcasePerson < ActiveRecord::Migration[5.0]
  def change
    create_join_table :suitcases, :people do |t|
      t.index [:suitcase_id, :person_id]
      t.index [:person_id, :suitcase_id]
    end
  end
end
