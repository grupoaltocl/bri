class ChangeStatusTypeToInteger < ActiveRecord::Migration[5.0]
  def change
    
    execute "UPDATE publishers SET status='0'"
    execute "UPDATE publications SET status='0'";
    execute "UPDATE people SET status='0'";
    execute "UPDATE products SET status='0'";
    
    change_column :people, :status, :integer, default: 0, null: false
    change_column :publishers, :status, :integer, default: 0, null: false
    change_column :products, :status, :integer, default: 0, null: false
    change_column :publications, :status, :integer, default: 0, null: false
  end
end
