class AddSuitcaseToItemRecords < ActiveRecord::Migration[5.0]
  def change
    add_reference :item_records, :suitcase, foreign_key: true
  end
end
