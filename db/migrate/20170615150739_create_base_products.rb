class CreateBaseProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :base_products do |t|
      t.string :name
      t.string :aliases
      t.string :excludes

      t.timestamps
    end
  end
end
