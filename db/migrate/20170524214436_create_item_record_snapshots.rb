class CreateItemRecordSnapshots < ActiveRecord::Migration[5.0]
  def change
    create_table :item_record_snapshots do |t|

      t.timestamps
    end
  end
end
