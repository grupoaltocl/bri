class CreateJoinTableSearchEngineCategory < ActiveRecord::Migration[5.0]
  def change
    create_join_table :categories, :search_engines do |t|
      t.index [:category_id, :search_engine_id],:name => "index_cat_search_engines_on_cat_id_and_search_engine_id"
      t.index [:search_engine_id, :category_id],:name => "index_cat_search_engines_on_search_engine_id_and_cat_id"
    end
  end
end
