class AddMissingFieldsToPublisher < ActiveRecord::Migration[5.0]
  def change
    add_column :publishers, :url, :string
    add_column :publishers, :email1, :string
    add_column :publishers, :email2, :string
    add_column :publishers, :phone1, :string
    add_column :publishers, :phone2, :string
    add_column :publishers, :county_raw, :string
    add_column :publishers, :state_raw, :string
    add_column :publishers, :city_raw, :string
    add_column :publishers, :website, :string
    add_column :publishers, :fb_page, :string
    add_column :publishers, :instagram, :string
    add_column :publishers, :twitter, :string
    add_column :publishers, :max_stolen_score, :decimal
    add_column :publishers, :update_times, :integer
    add_column :publishers, :deleted_at, :datetime
  end
end
