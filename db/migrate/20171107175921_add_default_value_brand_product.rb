class AddDefaultValueBrandProduct < ActiveRecord::Migration[5.0]
  def change
    change_column :products, :generic, :boolean, default: false
    change_column :brands, :generic, :boolean, default: false
  end
end
