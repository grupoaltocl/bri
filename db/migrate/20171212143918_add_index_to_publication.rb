class AddIndexToPublication < ActiveRecord::Migration[5.0]
  def change
    # add_column :publications, :url, :string
    add_index :publications, :url
  end
end
