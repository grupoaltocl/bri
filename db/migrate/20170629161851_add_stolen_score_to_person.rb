class AddStolenScoreToPerson < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :stolen_score, :decimal
  end
end
