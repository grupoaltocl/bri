class AddActivationParamsToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :by_description, :bool, default: false
    add_column :categories, :by_used, :bool, default: false
  end
end
