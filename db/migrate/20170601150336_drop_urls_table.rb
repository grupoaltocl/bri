class DropUrlsTable < ActiveRecord::Migration[5.0]
  def up
    drop_table :urls
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
