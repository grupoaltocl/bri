class AddSuitcaseToCaseFiles < ActiveRecord::Migration[5.0]
  def change
    add_reference :case_files, :suitcase, foreign_key: true
  end
end
