class ChangeTextToStringToProduct < ActiveRecord::Migration[5.0]
  def up
      change_column :products, :aliases, :string
      change_column :products, :exludes, :string
      rename_column :products, :exludes, :excludes
  end
  def down
      # This might cause trouble if you have strings longer
      # than 255 characters.
      rename_column :products, :excludes, :exludes
      change_column :products, :exludes, :text
      change_column :products, :aliases, :text
  end
end
