class AddStatusToPublishers < ActiveRecord::Migration[5.0]
  def change
    add_column :publishers, :status, :string
  end
end
