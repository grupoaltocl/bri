class AddFieldsToItemRecordSnapshots < ActiveRecord::Migration[5.0]
  def change
    add_column :item_record_snapshots, :name, :string
    add_column :item_record_snapshots, :price, :decimal
    add_column :item_record_snapshots, :description, :text
    add_reference :item_record_snapshots, :item_record, foreign_key: true
  end
end
