class AddNewsFieldsToPublishers < ActiveRecord::Migration[5.0]
  def change
    add_column :publishers, :stolen_score, :decimal
    add_column :publishers, :note, :text
    add_column :publishers, :watch, :boolean
    add_reference :publishers, :suitcase, foreign_key: true
  end
end
