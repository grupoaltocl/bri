class AddSearchFromToConfiguration < ActiveRecord::Migration[5.0]
  def change
    add_column :configurations, :search_from, :integer, default: 0
  end
end
