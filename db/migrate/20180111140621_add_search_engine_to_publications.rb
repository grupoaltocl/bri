class AddSearchEngineToPublications < ActiveRecord::Migration[5.0]
  def change
    add_reference :publications, :search_engine, foreign_key: true
  end
end
