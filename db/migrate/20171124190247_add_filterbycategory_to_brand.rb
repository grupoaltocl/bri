class AddFilterbycategoryToBrand < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :filter_by_category, :bool, default: true
  end
end
