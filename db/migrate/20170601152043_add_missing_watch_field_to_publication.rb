class AddMissingWatchFieldToPublication < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :watch, :boolean
  end
end
