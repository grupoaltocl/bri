class AddSourceToItemRecords < ActiveRecord::Migration[5.0]
  def change
    add_reference :item_records, :source, foreign_key: true
  end
end
