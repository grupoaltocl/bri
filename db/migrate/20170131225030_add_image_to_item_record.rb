class AddImageToItemRecord < ActiveRecord::Migration[5.0]
  def change
    add_column :item_records, :image, :string
  end
end
