class AddNewsFieldsToSuitcases < ActiveRecord::Migration[5.0]
  def change
    add_column :suitcases, :name, :string
    add_column :suitcases, :description, :text
    add_column :suitcases, :note, :text
  end
end
