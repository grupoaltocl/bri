class AddGroupToPublication < ActiveRecord::Migration[5.0]
  def change
    add_reference :publications, :publication_group, foreign_key: true
  end
end
