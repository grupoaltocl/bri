class AddPersonToPublishers < ActiveRecord::Migration[5.0]
  def change
    add_reference :publishers, :person, foreign_key: true
  end
end
