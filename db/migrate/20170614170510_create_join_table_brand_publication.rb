class CreateJoinTableBrandPublication < ActiveRecord::Migration[5.0]
  def change
    create_join_table :brands, :publications do |t|
      t.index [:brand_id, :publication_id]
      t.index [:publication_id, :brand_id]
    end
  end
end
