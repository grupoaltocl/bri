class CreateCustomerProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :customer_products do |t|
      t.references :customer, foreign_key: true
      t.references :product, foreign_key: true
      t.boolean :is_priority
    end
  end
end
