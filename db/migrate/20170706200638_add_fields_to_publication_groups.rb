class AddFieldsToPublicationGroups < ActiveRecord::Migration[5.0]
  def change
    add_reference :publication_groups, :product, foreign_key: true
    add_column :publication_groups, :published_at, :datetime
    add_reference :publication_groups, :publisher, foreign_key: true
    add_column :publication_groups, :price, :decimal, precision: 5, scale: 2
    add_column :publication_groups, :stolen_score, :decimal, precision: 5, scale: 2
    add_column :publication_groups, :condition, :integer
    add_column :publication_groups, :image, :string
    add_column :publication_groups, :status, :integer
    add_column :publication_groups, :url, :string
    add_column :publication_groups, :multi_prod, :boolean
    add_column :publication_groups, :update_times, :integer
    add_column :publication_groups, :finished_at, :datetime
    add_column :publication_groups, :reviewed_at, :datetime
    add_column :publication_groups, :watch, :boolean
    add_column :publication_groups, :county_raw, :string
    add_column :publication_groups, :state_raw, :string
    add_column :publication_groups, :city_raw, :string
    add_column :publication_groups, :other, :boolean
    add_column :publication_groups, :last_seen_at, :datetime
  end
end
