class AddColumnSearchEngine < ActiveRecord::Migration[5.0]
  def change
    add_column :search_engines, :parent_type, :string
  end
end