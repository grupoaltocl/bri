class ChangeTypeByDescriptionToIntCategory < ActiveRecord::Migration[5.0]
  def change
    change_column :categories, :by_description, :integer, default: 0
  end
end