class CreateItemRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :item_records do |t|
      t.string :name
      t.references :product, foreign_key: true
      t.datetime :published_at
      t.references :publisher, foreign_key: true
      t.decimal :price
      t.references :url, foreign_key: true

      t.timestamps
    end
  end
end
