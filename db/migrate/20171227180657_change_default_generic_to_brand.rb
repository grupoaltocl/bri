class ChangeDefaultGenericToBrand < ActiveRecord::Migration[5.0]
  def change
    change_column :brands, :generic, :bool, default: 0
  end
end
