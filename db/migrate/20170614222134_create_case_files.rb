class CreateCaseFiles < ActiveRecord::Migration[5.0]
  def change
    create_table :case_files do |t|
      t.string :file
      t.string :description

      t.timestamps
    end
  end
end
