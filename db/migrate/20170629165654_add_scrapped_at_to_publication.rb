class AddScrappedAtToPublication < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :last_seen_at, :datetime
    add_column :publications, :scraper_uid, :string
  end
end
