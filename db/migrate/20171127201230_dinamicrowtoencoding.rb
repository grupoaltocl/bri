class Dinamicrowtoencoding < ActiveRecord::Migration[5.0]
  def change
    # execute("ALTER TABLE active_admin_comments MODIFY column_name VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;")
    config = Rails.configuration.database_configuration
    db_name = config[Rails.env]["database"]
    collate = 'utf8mb4_polish_ci'
    char_set = 'utf8mb4'
    row_format = 'DYNAMIC'
    execute("ALTER DATABASE #{db_name} CHARACTER SET #{char_set} COLLATE #{collate};")
    
    ActiveRecord::Base.connection.tables.each do |table|
      next if table = "active_admin_comments"
      execute("ALTER TABLE #{table} ROW_FORMAT=#{row_format};")
      execute("ALTER TABLE #{table} CONVERT TO CHARACTER SET #{char_set} COLLATE #{collate};")
    end
  end
end
