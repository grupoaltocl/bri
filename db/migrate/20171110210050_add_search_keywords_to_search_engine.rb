class AddSearchKeywordsToSearchEngine < ActiveRecord::Migration[5.0]
  def change
    add_column :search_engines, :keywords_ml, :string
    add_column :search_engines, :keywords_yp, :string
    add_column :search_engines, :keywords_mp, :string
    add_column :publications, :find_by, :integer
  end
end
