class AddMatchScoreToItemRecords < ActiveRecord::Migration[5.0]
  def change
    add_column :item_records, :match_score, :decimal
  end
end
