class AddRunCategorizerToConfigurations < ActiveRecord::Migration[5.0]
  def change
    add_column :configurations, :run_cat, :bool, default: 0
  end
end
