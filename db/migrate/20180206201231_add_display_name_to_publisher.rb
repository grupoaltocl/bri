class AddDisplayNameToPublisher < ActiveRecord::Migration[5.0]
  def change
    add_column :publishers, :display_name, :string, default: ""
  end
end
