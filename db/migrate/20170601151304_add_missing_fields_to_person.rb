class AddMissingFieldsToPerson < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :email2, :string
    add_column :people, :phone2, :string
    add_column :people, :county_raw, :string
    add_column :people, :state_raw, :string
    add_column :people, :city_raw, :string
  end
end
