class CreateSearchEngines < ActiveRecord::Migration[5.0]
  def change
    create_table :search_engines do |t|
      t.string :name
      t.string :search_keywords
      t.string :search_excludes

      t.timestamps
    end
  end
end
