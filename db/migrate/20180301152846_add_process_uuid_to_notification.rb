class AddProcessUuidToNotification < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :process_uuid, :string, default: ''
  end
end
