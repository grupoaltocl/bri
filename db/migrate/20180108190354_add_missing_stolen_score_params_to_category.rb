class AddMissingStolenScoreParamsToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :factor_g, :decimal, :precision => 5, :scale => 2, default: 0
  end
end
