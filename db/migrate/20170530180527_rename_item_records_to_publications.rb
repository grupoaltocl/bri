class RenameItemRecordsToPublications < ActiveRecord::Migration[5.0]
  def change
      rename_table :item_records, :publications
  end
end
