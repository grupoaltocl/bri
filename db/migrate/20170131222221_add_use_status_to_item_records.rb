class AddUseStatusToItemRecords < ActiveRecord::Migration[5.0]
  def change
    add_column :item_records, :use_status, :integer, default: 0, null: false
  end
end
