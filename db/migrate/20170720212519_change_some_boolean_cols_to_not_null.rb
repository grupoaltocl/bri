class ChangeSomeBooleanColsToNotNull < ActiveRecord::Migration[5.0]
  def change
    change_column :publications, :manual_match, :boolean, null: false
  end
end
