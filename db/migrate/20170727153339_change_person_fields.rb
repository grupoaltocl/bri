class ChangePersonFields < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :url, :string
    add_column :people, :website, :string
    add_column :people, :fb_page1, :string
    add_column :people, :fb_page2, :string
    add_column :people, :instagram, :string
    add_column :people, :twitter, :string
    rename_column :people, :email, :email1
    rename_column :people, :phone, :phone1
  end
end
