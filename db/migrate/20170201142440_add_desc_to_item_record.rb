class AddDescToItemRecord < ActiveRecord::Migration[5.0]
  def change
    add_column :item_records, :description, :text
  end
end
