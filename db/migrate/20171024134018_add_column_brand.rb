class AddColumnBrand < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :generic, :bool
  end
end
