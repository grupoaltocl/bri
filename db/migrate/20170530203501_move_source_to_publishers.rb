class MoveSourceToPublishers < ActiveRecord::Migration[5.0]
  def up
    # Publisher.joins(:publications).update_all('publishers.source_id = publications.source_id');
    # execute <<~HEREDOC
    #   UPDATE publishers
    #   SET source_id = (SELECT publications.source_id FROM publications WHERE publications.publisher_id = publishers.id )
    #   WHERE EXISTS (SELECT * FROM publications WHERE publishers.id = publications.publisher_id)
    # HEREDOC
    remove_reference(:publications, :source, index: true)
  end
 
  def down
    # pending
  end
end
