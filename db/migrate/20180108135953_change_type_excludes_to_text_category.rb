class ChangeTypeExcludesToTextCategory < ActiveRecord::Migration[5.0]
  def change
    change_column :brands, :excludes, :text
    change_column :categories, :excludes, :text
    change_column :products, :excludes, :text
    change_column :search_engines, :search_excludes, :text
    change_column :base_products, :excludes, :text
  end
end
