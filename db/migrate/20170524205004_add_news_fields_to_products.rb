class AddNewsFieldsToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :aliases, :text
    add_column :products, :exludes, :text
    add_column :products, :sku, :string
    add_column :products, :watch, :boolean
  end
end
