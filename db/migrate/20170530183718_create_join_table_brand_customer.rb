class CreateJoinTableBrandCustomer < ActiveRecord::Migration[5.0]
  def change
    create_join_table :brands, :customers do |t|
      t.index [:brand_id, :customer_id]
      t.index [:customer_id, :brand_id]
    end
  end
end
