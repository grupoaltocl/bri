class AddPrice2ToPublications < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :price2, :decimal, :precision => 10, :scale => 2, default: 0
  end
end
