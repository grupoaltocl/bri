class MergePublicationProductsToM2m < ActiveRecord::Migration[5.0]
  def up
    pubs = Publication.all();
    pubs.each do |p|
      p.products << p.product if not p.product.nil?
    end
  end

  def down
  end
end
