class AddStolenScoreToItemRecords < ActiveRecord::Migration[5.0]
  def change
    add_column :item_records, :stolen_score, :decimal
  end
end
