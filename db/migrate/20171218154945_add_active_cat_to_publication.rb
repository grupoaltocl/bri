class AddActiveCatToPublication < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :active_cat, :bool, default: false
  end
end
