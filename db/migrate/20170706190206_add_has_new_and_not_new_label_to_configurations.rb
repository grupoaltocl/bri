class AddHasNewAndNotNewLabelToConfigurations < ActiveRecord::Migration[5.0]
  def change
    add_column :configurations, :has_new_label, :string
    add_column :configurations, :has_not_new_label, :string
  end
end
