class CreateJoinTableSuitcasePublisher < ActiveRecord::Migration[5.0]
  def change
    create_join_table :suitcases, :publishers do |t|
      # t.index [:suitcase_id, :publisher_id]
      # t.index [:publisher_id, :suitcase_id]
    end
  end
end
