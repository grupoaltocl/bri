class RemoveSuitcaseFromPublication < ActiveRecord::Migration[5.0]
  def change
    remove_reference(:publications, :suitcase, index: true, foreign_key: true)
  end
end
