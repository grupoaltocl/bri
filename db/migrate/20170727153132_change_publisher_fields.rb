class ChangePublisherFields < ActiveRecord::Migration[5.0]
  def change
    rename_column :publishers, :fb_page, :fb_page1
    add_column :publishers, :fb_page2, :string
    add_column :publishers, :first_name, :string
    add_column :publishers, :last_name, :string
  end
end
