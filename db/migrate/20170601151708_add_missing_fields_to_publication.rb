class AddMissingFieldsToPublication < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :manual_match, :boolean
    add_column :publications, :multi_prod, :boolean
    add_column :publications, :update_times, :integer
    add_column :publications, :finished_at, :datetime
    add_column :publications, :reviewed_at, :datetime
  end
end
