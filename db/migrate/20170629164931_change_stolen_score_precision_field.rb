class ChangeStolenScorePrecisionField < ActiveRecord::Migration[5.0]
  def change
    change_column :publications, :stolen_score, :decimal, :precision => 5, :scale => 2
    change_column :publishers, :stolen_score, :decimal, :precision => 5, :scale => 2
    change_column :people, :stolen_score, :decimal, :precision => 5, :scale => 2
  end
end
