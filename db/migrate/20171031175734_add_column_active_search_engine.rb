class AddColumnActiveSearchEngine < ActiveRecord::Migration[5.0]
  def change
    add_column :search_engines, :active, :bool
  end
end
