class AddUidToPublisher < ActiveRecord::Migration[5.0]
  def change
    add_column :publishers, :uid, :string
    add_index :publishers, :uid
  end
end
