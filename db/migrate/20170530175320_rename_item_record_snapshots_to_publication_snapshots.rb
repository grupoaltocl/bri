class RenameItemRecordSnapshotsToPublicationSnapshots < ActiveRecord::Migration[5.0]
  def change
    rename_table :item_record_snapshots, :publication_snapshots
  end
end
