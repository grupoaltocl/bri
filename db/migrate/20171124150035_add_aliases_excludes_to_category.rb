class AddAliasesExcludesToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :aliases, :string
    add_column :categories, :excludes, :string
  end
end
