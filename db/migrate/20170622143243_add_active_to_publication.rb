class AddActiveToPublication < ActiveRecord::Migration[5.0]
  def change
    add_column :publications, :active, :boolean, :default => false
    add_column :publications, :other, :boolean, :default => false
  end
end
