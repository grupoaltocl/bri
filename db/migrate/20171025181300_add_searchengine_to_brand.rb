class AddSearchengineToBrand < ActiveRecord::Migration[5.0]
  def change
    add_reference :brands, :search_engine, foreign_key: true
  end
end
