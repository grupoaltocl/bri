class AddCodeToCustomer < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :code, :string
  end
end
