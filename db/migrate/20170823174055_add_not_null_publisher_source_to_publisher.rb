class AddNotNullPublisherSourceToPublisher < ActiveRecord::Migration[5.0]
  def change
    change_column_null :publishers, :source_id, false
  end
end
