class RemoveSuitcaseFromPerson < ActiveRecord::Migration[5.0]
  def change
    remove_reference(:people, :suitcase, index: true, foreign_key: true)
  end
end
