class AddBaseProductToProduct < ActiveRecord::Migration[5.0]
  def change
    add_reference :products, :base_product, foreign_key: true
  end
end
