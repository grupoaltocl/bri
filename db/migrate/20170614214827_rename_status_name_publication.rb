class RenameStatusNamePublication < ActiveRecord::Migration[5.0]
  def up
      rename_column :publications, :use_status, :condition
  end
  def down
      # This might cause trouble if you have strings longer
      # than 255 characters.
      rename_column :publications, :condition, :use_status
  end
end
