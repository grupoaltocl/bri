class AddStatusToItemRecords < ActiveRecord::Migration[5.0]
  def change
    add_column :item_records, :status, :string
  end
end
