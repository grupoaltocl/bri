class AddExcludeRegionToConfigurations < ActiveRecord::Migration[5.0]
  def change
    add_column :configurations, :exclude_regions, :string
  end
end
