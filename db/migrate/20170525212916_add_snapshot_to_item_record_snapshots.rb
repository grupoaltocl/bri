class AddSnapshotToItemRecordSnapshots < ActiveRecord::Migration[5.0]
  def change
    add_column :item_record_snapshots, :snapshot, :string
  end
end
