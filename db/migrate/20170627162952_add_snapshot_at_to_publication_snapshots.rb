class AddSnapshotAtToPublicationSnapshots < ActiveRecord::Migration[5.0]
  def change
    add_column :publication_snapshots, :snapshot_at, :datetime
  end
end
