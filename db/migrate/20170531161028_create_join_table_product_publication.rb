class CreateJoinTableProductPublication < ActiveRecord::Migration[5.0]
  def change
    create_join_table :products, :publications do |t|
      t.index [:product_id, :publication_id]
      t.index [:publication_id, :product_id]
    end
  end
end
