class AddActiveToSource < ActiveRecord::Migration[5.0]
  def change
    add_column :sources, :active, :bool
  end
end
