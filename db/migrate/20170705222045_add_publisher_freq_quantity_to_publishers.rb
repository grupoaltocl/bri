class AddPublisherFreqQuantityToPublishers < ActiveRecord::Migration[5.0]
  def change
    add_column :publishers, :publisher_freq_quantity, :string
  end
end
