-- MySQL dump 10.13  Distrib 5.5.42, for osx10.6 (i386)
--
-- Host: 127.0.0.1    Database: foo
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_admin_comments`
--

DROP TABLE IF EXISTS `active_admin_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_admin_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) DEFAULT NULL,
  `body` text,
  `resource_id` varchar(255) NOT NULL,
  `resource_type` varchar(255) NOT NULL,
  `author_type` varchar(255) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_active_admin_comments_on_author_type_and_author_id` (`author_type`,`author_id`),
  KEY `index_active_admin_comments_on_namespace` (`namespace`),
  KEY `index_active_admin_comments_on_resource_type_and_resource_id` (`resource_type`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admin_users_on_email` (`email`),
  UNIQUE KEY `index_admin_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ar_internal_metadata`
--

DROP TABLE IF EXISTS `ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_products`
--

DROP TABLE IF EXISTS `base_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `aliases` varchar(255) DEFAULT NULL,
  `excludes` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `aliases` varchar(255) DEFAULT NULL,
  `excludes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brands_customers`
--

DROP TABLE IF EXISTS `brands_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands_customers` (
  `brand_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  KEY `index_brands_customers_on_brand_id_and_customer_id` (`brand_id`,`customer_id`),
  KEY `index_brands_customers_on_customer_id_and_brand_id` (`customer_id`,`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brands_publications`
--

DROP TABLE IF EXISTS `brands_publications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands_publications` (
  `brand_id` int(11) NOT NULL,
  `publication_id` int(11) NOT NULL,
  KEY `index_brands_publications_on_brand_id_and_publication_id` (`brand_id`,`publication_id`),
  KEY `index_brands_publications_on_publication_id_and_brand_id` (`publication_id`,`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_files`
--

DROP TABLE IF EXISTS `case_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `suitcase_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_case_files_on_suitcase_id` (`suitcase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configurations`
--

DROP TABLE IF EXISTS `configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factor_a` decimal(5,2) DEFAULT NULL,
  `factor_b` decimal(5,2) DEFAULT NULL,
  `factor_c` decimal(5,2) DEFAULT NULL,
  `factor_d` decimal(5,2) DEFAULT NULL,
  `factor_e` decimal(5,2) DEFAULT NULL,
  `factor_f` decimal(5,2) DEFAULT NULL,
  `factor_g` decimal(5,2) DEFAULT NULL,
  `has_label1_ss` varchar(255) DEFAULT NULL,
  `has_label2_ss` varchar(255) DEFAULT NULL,
  `publisher_freq_ss` int(11) DEFAULT NULL,
  `many_updates_ss` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `multiprod_labels` varchar(255) DEFAULT NULL,
  `disc_limit1` decimal(5,2) DEFAULT NULL,
  `disc_limit2` decimal(5,2) DEFAULT NULL,
  `disc_limit3` decimal(5,2) DEFAULT NULL,
  `has_new_label` varchar(255) DEFAULT NULL,
  `has_not_new_label` varchar(255) DEFAULT NULL,
  `sys_scraped_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_products`
--

DROP TABLE IF EXISTS `customer_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `is_priority` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_customer_products_on_customer_id` (`customer_id`),
  KEY `index_customer_products_on_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `people`
--

DROP TABLE IF EXISTS `people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email1` varchar(255) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `note` text,
  `watch` tinyint(1) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `county_raw` varchar(255) DEFAULT NULL,
  `state_raw` varchar(255) DEFAULT NULL,
  `city_raw` varchar(255) DEFAULT NULL,
  `stolen_score` decimal(5,2) DEFAULT NULL,
  `scraper_uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `fb_page1` varchar(255) DEFAULT NULL,
  `fb_page2` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `people_suitcases`
--

DROP TABLE IF EXISTS `people_suitcases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_suitcases` (
  `suitcase_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  KEY `index_people_suitcases_on_person_id_and_suitcase_id` (`person_id`,`suitcase_id`),
  KEY `index_people_suitcases_on_suitcase_id_and_person_id` (`suitcase_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `aliases` varchar(255) DEFAULT NULL,
  `excludes` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `watch` tinyint(1) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `filter_by_brand` tinyint(1) DEFAULT NULL,
  `base_product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_products_on_base_product_id` (`base_product_id`),
  KEY `index_products_on_brand_id` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products_publications`
--

DROP TABLE IF EXISTS `products_publications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_publications` (
  `product_id` int(11) NOT NULL,
  `publication_id` int(11) NOT NULL,
  KEY `index_products_publications_on_product_id_and_publication_id` (`product_id`,`publication_id`),
  KEY `index_products_publications_on_publication_id_and_product_id` (`publication_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publication_groups`
--

DROP TABLE IF EXISTS `publication_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `publisher_id` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `stolen_score` decimal(5,2) DEFAULT NULL,
  `condition` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `multi_prod` tinyint(1) DEFAULT NULL,
  `update_times` int(11) DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `reviewed_at` datetime DEFAULT NULL,
  `watch` tinyint(1) DEFAULT NULL,
  `county_raw` varchar(255) DEFAULT NULL,
  `state_raw` varchar(255) DEFAULT NULL,
  `city_raw` varchar(255) DEFAULT NULL,
  `other` tinyint(1) DEFAULT NULL,
  `last_seen_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_publication_groups_on_product_id` (`product_id`),
  KEY `index_publication_groups_on_publisher_id` (`publisher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publication_groups_suitcases`
--

DROP TABLE IF EXISTS `publication_groups_suitcases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication_groups_suitcases` (
  `suitcase_id` int(11) NOT NULL,
  `publication_group_id` int(11) NOT NULL,
  KEY `index_cases_p_groups_on_p_group_id_and_suitcase_id` (`publication_group_id`,`suitcase_id`),
  KEY `index_cases_p_groups_on_suitcase_id_and_p_group_id` (`suitcase_id`,`publication_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publication_snapshots`
--

DROP TABLE IF EXISTS `publication_snapshots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication_snapshots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `snapshot` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `description` text,
  `publication_id` int(11) DEFAULT NULL,
  `snapshot_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_publication_snapshots_on_publication_id` (`publication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publications`
--

DROP TABLE IF EXISTS `publications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `publisher_id` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `match_score` decimal(5,2) DEFAULT NULL,
  `stolen_score` decimal(5,2) DEFAULT NULL,
  `condition` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `publication_group_id` int(11) DEFAULT NULL,
  `manual_match` tinyint(1) NOT NULL,
  `multi_prod` tinyint(1) DEFAULT NULL,
  `update_times` int(11) DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `reviewed_at` datetime DEFAULT NULL,
  `watch` tinyint(1) DEFAULT NULL,
  `county_raw` varchar(255) DEFAULT NULL,
  `state_raw` varchar(255) DEFAULT NULL,
  `city_raw` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `other` tinyint(1) DEFAULT '0',
  `last_seen_at` datetime DEFAULT NULL,
  `scraper_uid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_publications_on_product_id` (`product_id`),
  KEY `index_publications_on_publication_group_id` (`publication_group_id`),
  KEY `index_publications_on_publisher_id` (`publisher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publications_suitcases`
--

DROP TABLE IF EXISTS `publications_suitcases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publications_suitcases` (
  `suitcase_id` int(11) NOT NULL,
  `publication_id` int(11) NOT NULL,
  KEY `index_publications_suitcases_on_publication_id_and_suitcase_id` (`publication_id`,`suitcase_id`),
  KEY `index_publications_suitcases_on_suitcase_id_and_publication_id` (`suitcase_id`,`publication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publishers`
--

DROP TABLE IF EXISTS `publishers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `stolen_score` decimal(5,2) DEFAULT NULL,
  `note` text,
  `watch` tinyint(1) DEFAULT NULL,
  `suitcase_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `person_id` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `email1` varchar(255) DEFAULT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `county_raw` varchar(255) DEFAULT NULL,
  `state_raw` varchar(255) DEFAULT NULL,
  `city_raw` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `fb_page1` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `max_stolen_score` decimal(5,2) DEFAULT NULL,
  `update_times` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `publisher_freq_quantity` varchar(255) DEFAULT NULL,
  `fb_page2` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_publishers_on_person_id` (`person_id`),
  KEY `index_publishers_on_source_id` (`source_id`),
  KEY `index_publishers_on_suitcase_id` (`suitcase_id`),
  KEY `index_publishers_on_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `publishers_suitcases`
--

DROP TABLE IF EXISTS `publishers_suitcases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publishers_suitcases` (
  `suitcase_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sources`
--

DROP TABLE IF EXISTS `sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `suitcases`
--

DROP TABLE IF EXISTS `suitcases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suitcases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `note` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `password_digest` varchar(255) DEFAULT NULL,
  `remember_digest` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT '0',
  `activation_digest` varchar(255) DEFAULT NULL,
  `activated` tinyint(1) DEFAULT '0',
  `activated_at` datetime DEFAULT NULL,
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'foo'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-11 19:29:53
INSERT INTO schema_migrations (version) VALUES ('20160915204628'), ('20160915205510'), ('20160915205755'), ('20170105043148'), ('20170110051006'), ('20170113044910'), ('20170119042626'), ('20170119042743'), ('20170119043015'), ('20170119044020'), ('20170119044039'), ('20170119045422'), ('20170119045437'), ('20170119054141'), ('20170119070148'), ('20170120031625'), ('20170120033144'), ('20170120033145'), ('20170131222221'), ('20170131225030'), ('20170201142440'), ('20170524205004'), ('20170524213700'), ('20170524214217'), ('20170524214436'), ('20170524224149'), ('20170524224549'), ('20170525212357'), ('20170525212916'), ('20170525212917'), ('20170525212918'), ('20170525212920'), ('20170525213901'), ('20170526155735'), ('20170530142551'), ('20170530175320'), ('20170530180527'), ('20170530181831'), ('20170530183012'), ('20170530183718'), ('20170530191754'), ('20170530193624'), ('20170530193755'), ('20170530194902'), ('20170530195745'), ('20170530201218'), ('20170530201340'), ('20170530201635'), ('20170530203427'), ('20170530203501'), ('20170531161028'), ('20170531163418'), ('20170601150336'), ('20170601151016'), ('20170601151304'), ('20170601151708'), ('20170601151822'), ('20170601152003'), ('20170601152043'), ('20170601152137'), ('20170602220332'), ('20170605170715'), ('20170606150445'), ('20170614170510'), ('20170614214827'), ('20170614222134'), ('20170614222727'), ('20170615142757'), ('20170615150739'), ('20170615185955'), ('20170622143243'), ('20170622182549'), ('20170627162952'), ('20170627223631'), ('20170629161851'), ('20170629164931'), ('20170629165654'), ('20170705222045'), ('20170706162105'), ('20170706190206'), ('20170706193039'), ('20170706193116'), ('20170706200638'), ('20170707000827'), ('20170707191540'), ('20170720212519'), ('20170724195221'), ('20170727153132'), ('20170727153339');


