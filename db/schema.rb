# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180301152846) do

  create_table "active_admin_comments", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=latin1" do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.string   "author_type",   limit: 255
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "admin_users", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "base_products", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",       limit: 255
    t.string   "aliases",    limit: 255
    t.text     "excludes",   limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "brands", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",               limit: 255
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "aliases",            limit: 255
    t.text     "excludes",           limit: 65535
    t.integer  "category_id"
    t.boolean  "generic",                          default: false
    t.integer  "search_engine_id"
    t.boolean  "filter_by_category",               default: true
    t.string   "cat_uuid",                         default: ""
    t.index ["category_id"], name: "index_brands_on_category_id", using: :btree
    t.index ["search_engine_id"], name: "index_brands_on_search_engine_id", using: :btree
  end

  create_table "brands_customers", id: false, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer "brand_id",    null: false
    t.integer "customer_id", null: false
    t.index ["brand_id", "customer_id"], name: "index_brands_customers_on_brand_id_and_customer_id", using: :btree
    t.index ["customer_id", "brand_id"], name: "index_brands_customers_on_customer_id_and_brand_id", using: :btree
  end

  create_table "brands_publications", id: false, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer "brand_id",       null: false
    t.integer "publication_id", null: false
    t.index ["brand_id", "publication_id"], name: "index_brands_publications_on_brand_id_and_publication_id", unique: true, using: :btree
    t.index ["brand_id"], name: "index_brands_publications_on_brand_id", using: :btree
    t.index ["publication_id", "brand_id"], name: "index_brands_publications_on_publication_id_and_brand_id", using: :btree
  end

  create_table "case_files", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "file",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "suitcase_id"
    t.index ["suitcase_id"], name: "index_case_files_on_suitcase_id", using: :btree
  end

  create_table "categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.datetime "created_at",                                                              null: false
    t.datetime "updated_at",                                                              null: false
    t.integer  "by_description",                                          default: 0
    t.boolean  "by_used",                                                 default: false
    t.string   "aliases"
    t.text     "excludes",          limit: 65535
    t.decimal  "factor_a",                        precision: 5, scale: 2, default: "0.0"
    t.decimal  "disc_limit1",                     precision: 5, scale: 2, default: "0.0"
    t.decimal  "disc_limit2",                     precision: 5, scale: 2, default: "0.0"
    t.decimal  "disc_limit3",                     precision: 5, scale: 2, default: "0.0"
    t.decimal  "factor_b",                        precision: 5, scale: 2, default: "0.0"
    t.decimal  "factor_c",                        precision: 5, scale: 2, default: "0.0"
    t.decimal  "factor_d",                        precision: 5, scale: 2, default: "0.0"
    t.decimal  "many_updates_ss",                 precision: 5, scale: 2, default: "0.0"
    t.decimal  "factor_e",                        precision: 5, scale: 2, default: "0.0"
    t.decimal  "publisher_freq_ss",               precision: 5, scale: 2, default: "0.0"
    t.decimal  "factor_f",                        precision: 5, scale: 2, default: "0.0"
    t.string   "has_label1_ss"
    t.string   "has_label2_ss"
    t.decimal  "factor_g",                        precision: 5, scale: 2, default: "0.0"
  end

  create_table "categories_search_engines", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "category_id",      null: false
    t.integer "search_engine_id", null: false
    t.index ["category_id", "search_engine_id"], name: "index_cat_search_engines_on_cat_id_and_search_engine_id", using: :btree
    t.index ["search_engine_id", "category_id"], name: "index_cat_search_engines_on_search_engine_id_and_cat_id", using: :btree
  end

  create_table "configurations", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.decimal  "factor_a",                      precision: 5, scale: 2
    t.decimal  "factor_b",                      precision: 5, scale: 2
    t.decimal  "factor_c",                      precision: 5, scale: 2
    t.decimal  "factor_d",                      precision: 5, scale: 2
    t.decimal  "factor_e",                      precision: 5, scale: 2
    t.decimal  "factor_f",                      precision: 5, scale: 2
    t.decimal  "factor_g",                      precision: 5, scale: 2
    t.string   "has_label1_ss",     limit: 255
    t.string   "has_label2_ss",     limit: 255
    t.integer  "publisher_freq_ss"
    t.integer  "many_updates_ss"
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.string   "multiprod_labels",  limit: 255
    t.decimal  "disc_limit1",                   precision: 5, scale: 2
    t.decimal  "disc_limit2",                   precision: 5, scale: 2
    t.decimal  "disc_limit3",                   precision: 5, scale: 2
    t.string   "has_new_label",     limit: 255
    t.string   "has_not_new_label", limit: 255
    t.datetime "sys_scraped_at"
    t.integer  "search_from",                                           default: 0
    t.string   "exclude_regions"
    t.string   "cat_uuid",                                              default: ""
    t.boolean  "run_cat",                                               default: false
  end

  create_table "customer_products", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer "customer_id"
    t.integer "product_id"
    t.boolean "is_priority"
    t.index ["customer_id"], name: "index_customer_products_on_customer_id", using: :btree
    t.index ["product_id"], name: "index_customer_products_on_product_id", using: :btree
  end

  create_table "customers", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "code",        limit: 255
  end

  create_table "notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci" do |t|
    t.string   "name",                       default: ""
    t.text     "description",  limit: 65535
    t.boolean  "is_read",                    default: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "process_uuid",               default: ""
  end

  create_table "people", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "uid",          limit: 255
    t.string   "first_name",   limit: 255
    t.string   "last_name",    limit: 255
    t.string   "email1",       limit: 255
    t.string   "phone1",       limit: 255
    t.text     "note",         limit: 65535
    t.boolean  "watch"
    t.integer  "status",                                             default: 0, null: false
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.string   "email2",       limit: 255
    t.string   "phone2",       limit: 255
    t.string   "county_raw",   limit: 255
    t.string   "state_raw",    limit: 255
    t.string   "city_raw",     limit: 255
    t.decimal  "stolen_score",               precision: 5, scale: 2
    t.string   "scraper_uid",  limit: 255
    t.string   "name",         limit: 255
    t.string   "url",          limit: 255
    t.string   "website",      limit: 255
    t.string   "fb_page1",     limit: 255
    t.string   "fb_page2",     limit: 255
    t.string   "instagram",    limit: 255
    t.string   "twitter",      limit: 255
  end

  create_table "people_suitcases", id: false, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer "suitcase_id", null: false
    t.integer "person_id",   null: false
    t.index ["person_id", "suitcase_id"], name: "index_people_suitcases_on_person_id_and_suitcase_id", using: :btree
    t.index ["suitcase_id", "person_id"], name: "index_people_suitcases_on_suitcase_id_and_person_id", using: :btree
  end

  create_table "products", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",            limit: 255
    t.integer  "brand_id"
    t.decimal  "price",                         precision: 10, scale: 2
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.string   "aliases",         limit: 255
    t.text     "excludes",        limit: 65535
    t.string   "sku",             limit: 255
    t.boolean  "watch"
    t.integer  "status",                                                 default: 0,     null: false
    t.boolean  "filter_by_brand"
    t.integer  "base_product_id"
    t.boolean  "generic",                                                default: false
    t.index ["base_product_id"], name: "index_products_on_base_product_id", using: :btree
    t.index ["brand_id"], name: "index_products_on_brand_id", using: :btree
  end

  create_table "products_publications", id: false, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer "product_id",     null: false
    t.integer "publication_id", null: false
    t.index ["product_id", "publication_id"], name: "index_products_publications_on_product_id_and_publication_id", using: :btree
    t.index ["publication_id", "product_id"], name: "index_products_publications_on_publication_id_and_product_id", using: :btree
  end

  create_table "publication_groups", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",         limit: 255
    t.text     "description",  limit: 65535
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.integer  "product_id"
    t.datetime "published_at"
    t.integer  "publisher_id"
    t.decimal  "price",                      precision: 10, scale: 2
    t.decimal  "stolen_score",               precision: 5,  scale: 2
    t.integer  "condition"
    t.string   "image",        limit: 255
    t.integer  "status"
    t.string   "url",          limit: 255
    t.boolean  "multi_prod"
    t.integer  "update_times"
    t.datetime "finished_at"
    t.datetime "reviewed_at"
    t.boolean  "watch"
    t.string   "county_raw",   limit: 255
    t.string   "state_raw",    limit: 255
    t.string   "city_raw",     limit: 255
    t.boolean  "other"
    t.datetime "last_seen_at"
    t.index ["product_id"], name: "index_publication_groups_on_product_id", using: :btree
    t.index ["publisher_id"], name: "index_publication_groups_on_publisher_id", using: :btree
  end

  create_table "publication_groups_suitcases", id: false, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer "suitcase_id",          null: false
    t.integer "publication_group_id", null: false
    t.index ["publication_group_id", "suitcase_id"], name: "index_cases_p_groups_on_p_group_id_and_suitcase_id", using: :btree
    t.index ["suitcase_id", "publication_group_id"], name: "index_cases_p_groups_on_suitcase_id_and_p_group_id", using: :btree
  end

  create_table "publication_snapshots", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "snapshot",       limit: 255
    t.string   "name",           limit: 255
    t.decimal  "price",                        precision: 10, scale: 2
    t.text     "description",    limit: 65535
    t.integer  "publication_id"
    t.datetime "snapshot_at"
    t.index ["publication_id"], name: "index_publication_snapshots_on_publication_id", using: :btree
  end

  create_table "publications", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                 limit: 255
    t.integer  "product_id"
    t.datetime "published_at"
    t.integer  "publisher_id"
    t.decimal  "price",                              precision: 10, scale: 2
    t.datetime "created_at",                                                                  null: false
    t.datetime "updated_at",                                                                  null: false
    t.decimal  "match_score",                        precision: 5,  scale: 2
    t.decimal  "stolen_score",                       precision: 5,  scale: 2
    t.integer  "condition",                                                   default: 0,     null: false
    t.string   "image",                limit: 255
    t.text     "description",          limit: 65535
    t.integer  "status",                                                      default: 0,     null: false
    t.string   "url",                  limit: 255
    t.integer  "publication_group_id"
    t.boolean  "manual_match",                                                                null: false
    t.boolean  "multi_prod"
    t.integer  "update_times"
    t.datetime "finished_at"
    t.datetime "reviewed_at"
    t.boolean  "watch"
    t.string   "county_raw",           limit: 255
    t.string   "state_raw",            limit: 255
    t.string   "city_raw",             limit: 255
    t.boolean  "active",                                                      default: false
    t.boolean  "other",                                                       default: false
    t.datetime "last_seen_at"
    t.string   "scraper_uid",          limit: 255
    t.integer  "find_by"
    t.boolean  "force_scraper",                                               default: false
    t.boolean  "active_cat",                                                  default: false
    t.integer  "search_engine_id"
    t.decimal  "price2",                             precision: 10, scale: 2, default: "0.0"
    t.index ["product_id"], name: "index_publications_on_product_id", using: :btree
    t.index ["publication_group_id"], name: "index_publications_on_publication_group_id", using: :btree
    t.index ["publisher_id"], name: "index_publications_on_publisher_id", using: :btree
    t.index ["search_engine_id"], name: "index_publications_on_search_engine_id", using: :btree
    t.index ["url"], name: "index_publications_on_url", using: :btree
  end

  create_table "publications_suitcases", id: false, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer "suitcase_id",    null: false
    t.integer "publication_id", null: false
    t.index ["publication_id", "suitcase_id"], name: "index_publications_suitcases_on_publication_id_and_suitcase_id", using: :btree
    t.index ["suitcase_id", "publication_id"], name: "index_publications_suitcases_on_suitcase_id_and_publication_id", using: :btree
  end

  create_table "publishers", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                    limit: 255
    t.datetime "created_at",                                                                 null: false
    t.datetime "updated_at",                                                                 null: false
    t.decimal  "stolen_score",                          precision: 5, scale: 2
    t.text     "note",                    limit: 65535
    t.boolean  "watch"
    t.integer  "suitcase_id"
    t.integer  "status",                                                        default: 0,  null: false
    t.integer  "person_id"
    t.integer  "source_id",                                                                  null: false
    t.string   "url",                     limit: 255
    t.string   "email1",                  limit: 255
    t.string   "email2",                  limit: 255
    t.string   "phone1",                  limit: 255
    t.string   "phone2",                  limit: 255
    t.string   "county_raw",              limit: 255
    t.string   "state_raw",               limit: 255
    t.string   "city_raw",                limit: 255
    t.string   "website",                 limit: 255
    t.string   "fb_page1",                limit: 255
    t.string   "instagram",               limit: 255
    t.string   "twitter",                 limit: 255
    t.decimal  "max_stolen_score",                      precision: 5, scale: 2
    t.integer  "update_times"
    t.datetime "deleted_at"
    t.string   "uid",                     limit: 255
    t.string   "publisher_freq_quantity", limit: 255
    t.string   "fb_page2",                limit: 255
    t.string   "first_name",              limit: 255
    t.string   "last_name",               limit: 255
    t.string   "display_name",                                                  default: ""
    t.index ["person_id"], name: "index_publishers_on_person_id", using: :btree
    t.index ["source_id"], name: "index_publishers_on_source_id", using: :btree
    t.index ["suitcase_id"], name: "index_publishers_on_suitcase_id", using: :btree
    t.index ["uid"], name: "index_publishers_on_uid", using: :btree
  end

  create_table "publishers_suitcases", id: false, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer "suitcase_id",  null: false
    t.integer "publisher_id", null: false
  end

  create_table "search_engines", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.string   "search_keywords"
    t.text     "search_excludes", limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "parent_type"
    t.boolean  "active"
    t.string   "keywords_ml"
    t.string   "keywords_yp"
    t.string   "keywords_mp"
  end

  create_table "sources", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "code",       limit: 255
    t.boolean  "active"
  end

  create_table "suitcases", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.text     "note",        limit: 65535
  end

  create_table "users", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                   limit: 255
    t.string   "email",                  limit: 255, default: "",    null: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "password_digest",        limit: 255
    t.string   "remember_digest",        limit: 255
    t.boolean  "admin",                              default: false
    t.string   "activation_digest",      limit: 255
    t.boolean  "activated",                          default: false
    t.datetime "activated_at"
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
