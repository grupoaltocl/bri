#!/bin/bash

trap "exit" INT # exit all after Ctrl+C
DATE=`date +%Y%m%d`

# load rvm ruby
source /usr/local/rvm/environments/ruby-2.3.0@ig

bundle install

process=$1
PID_FILE="/var/www/bri/staging/current/tmp/pids/scraper.pid"
case $process in
    start)
        echo "Starting scraper $DATE"
        export PERSIST=1
        export RAILS_ENV=production
        nohup bundle exec rails r /var/www/bri/staging/current/lib/spider/run.rb >> "/var/www/bri/staging/current/log/${DATE}_spider_run.log" 2>&1 &
        echo $! > $PID_FILE
        ;;

    stop)
        kill -9 $(cat $PID_FILE)
        rm $PID_FILE
        ;;

    *)
        echo "INVALID OPTION"
        ;;
esac