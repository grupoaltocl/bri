#!/bin/bash

trap "exit" INT # exit all after Ctrl+C
DATE=`date +%Y%m%d`

# load rvm ruby
source /usr/local/rvm/environments/ruby-2.3.0@ig


# bundle install
  
export PERSIST=1
export RESET=1
export RAILS_ENV=production
bundle exec rails r lib/spider/run.rb >> "./log/${DATE}_spider_run.log" 2>&1 && \
bundle exec rails r lib/spider/new_categorizer.rb >> "./log/${DATE}_spider_cat.log" 2>&1 && \
bundle exec rails r lib/spider/snapshot.rb >> "./log/${DATE}_spider_snap.log" 2>&1 && \
bundle exec rails r lib/spider/fix_publications.rb >> >> "./log/fix_publications.log" 2>&1
