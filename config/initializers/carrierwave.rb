# config/initializers/carrierwave.rb
# This file is not created by default so you might have to create it yourself.
# require 'carrierwave/storage/fog'
# CarrierWave.configure do |config|
#     access_token = YAML.load_file("#{Rails.root}/config/fog.yml")
#     config.fog_provider = 'fog/aws'                        # required
#     config.fog_credentials = {
#       provider:              'AWS',                        # required
#       aws_access_key_id:     access_token['default']['aws_access_key_id'],                        # required
#       aws_secret_access_key: access_token['default']['aws_secret_access_key'],                        # required
#       region:                'us-east-1',                  # optional, defaults to 'us-east-1'
#     }
#     config.fog_directory  = 'grupo-altos-rds-baks/ArchivosIncidentesChile/BRI/demo'                          # required
#     config.fog_public     = false                                        # optional, defaults to true
#     config.fog_attributes = { cache_control: "public, max-age=#{365.day.to_i}" } # optional, defaults to {}
# end

CarrierWave.configure do |config|
    access_token = YAML.load_file("#{Rails.root}/config/fog.yml")

    # Use local storage if in development or test
    config.fog_provider = 'fog/aws' 
    config.fog_credentials = {
        :provider               => 'AWS',                             # required
        :aws_access_key_id      => access_token['default']['aws_access_key_id'],            # required
        :aws_secret_access_key  => access_token['default']['aws_secret_access_key'],     # required
        :region                 => access_token['default']['aws_region']                       # optional, defaults to 'us-east-1'
      }
    config.storage = :fog
    config.fog_authenticated_url_expiration = 604799
    # if Rails.env.development? || Rails.env.test?
    #   CarrierWave.configure do |config|
    #     config.fog_directory = 'ArchivosIncidentesChile/BRI/demo/'
    #   end
    # end
    
    # # # Use AWS storage if in production
    # if Rails.env.production?
    #   CarrierWave.configure do |config|
    #     config.fog_directory = 'ArchivosIncidentesChile/BRI/prod/'
    #   end
    # end
    

    config.fog_directory = access_token['production']['directory']
    config.fog_public     = false
    # config.fog_prefix = 'ArchivosIncidentesChile/BRI/demo'
    # config.fog_host       = 'https://s3.console.aws.amazon.com/s3/buckets/grupo-altos-rds-baks/ArchivosIncidentesChile/BRI/demo/'           # optional, defaults to nil
    #config.fog_public     = false                                  # optional, defaults to true
    config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}  # optional, defaults to {}
  end