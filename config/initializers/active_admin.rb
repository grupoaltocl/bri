module ActiveAdmin
  module Views
    module Pages
      class Show < Base

        def attributes_table2(*args, &block)
          opts = args.extract_options!
          table_title = if opts.has_key?(:title)
            render_or_call_method_or_proc_on(resource, opts[:title])
          else
            ActiveAdmin::Localizers.resource(active_admin_config).t(:details)
          end
          panel(table_title) do
            attributes_table_for resource, *args, &block
          end
        end
        
      end
    end
  end
end

module ActiveAdmin
  module Views
    class Header < Component

      def build(namespace, menu)
        super(id: "header")

        @namespace = namespace
        @menu = menu
        @utility_menu = @namespace.fetch_menu(:utility_navigation)

        build_site_title
        build_global_navigation
        custom_customer_tags
        build_utility_navigation
      end


      def custom_customer_tags
        if session[:customer_ids].present?
          ul class: "header-item tabs" do
            li style: "display: table" do 
              session[:customer_ids].map do |x| 
                span Customer.find(x).code.html_safe, style: "background: #6090DB;
                                                              color: #fff;
                                                              text-transform: uppercase;
                                                              letter-spacing: 0.15em;
                                                              padding: 3px 5px 2px 5px;
                                                              font-size: 0.8em;"
              end
            end
          end
        end
        ul class: "header-item tabs" do 
          li style: "display: table" do 
            # p class: "something" "".html_safe
            # if Configuration.first.run_cat == true
              div "".html_safe, class: "categorizer", style: "width: 10px;height: 10px;border-radius: 50%;
                                        margin-left: 10px;"
          end            
        end        
      end
    end
  end
end

module ActiveAdmin
  module Views
    module Pages
      class Base < Arbre::HTML::Document

        def build_flash_messages
          div class: 'flashes' do
            flash_messages.each do |type, message|
              div message.html_safe, class: "flash flash_#{type}" # REVIEW!
            end
          end
        end

      end
    end
  end
end
module ActiveAdmin
  class ResourceController
    module DataAccess
      # needed for current active admin master
      def max_per_page
        10000000
      end

      def per_page
        return 10000000 if %w(text/csv application/xml application/json).include?(request.format)

        return max_per_page if active_admin_config.paginate == false

        @per_page || active_admin_config.per_page
      end
    end
  end
end

module ActiveAdmin
  module Inputs
    module Filters
      class StringInput < ::Formtastic::Inputs::StringInput
        filter :not_cont
        filter :blank
      end
    end
  end
end

ActiveAdmin.setup do |config|
  # == Site Title
  #
  # Set the title that is displayed on the main layout
  # for each of the active admin pages.
  #
  config.site_title = "Grupo Alto BRI"

  # Set the link url for the title. For example, to take
  # users to your main site. Defaults to no link.
  #
  # config.site_title_link = "/"

  # Set an optional image to be displayed for the header
  # instead of a string (overrides :site_title)
  #
  # Note: Aim for an image that's 21px high so it fits in the header.
  #
  # config.site_title_image = "logo.png"

  # == Default Namespace
  #
  # Set the default namespace each administration resource
  # will be added to.
  #
  # eg:
  #   config.default_namespace = :hello_world
  #
  # This will create resources in the HelloWorld module and
  # will namespace routes to /hello_world/*
  #
  # To set no namespace by default, use:
  #   config.default_namespace = false
  #
  # Default:
  # config.default_namespace = :admin
  #
  # You can customize the settings for each namespace by using
  # a namespace block. For example, to change the site title
  # within a namespace:
  #
  #   config.namespace :admin do |admin|
  #     admin.site_title = "Custom Admin Title"
  #   end
  #
  # This will ONLY change the title for the admin section. Other
  # namespaces will continue to use the main "site_title" configuration.

  # == User Authentication
  #
  # Active Admin will automatically call an authentication
  # method in a before filter of all controller actions to
  # ensure that there is a currently logged in admin user.
  #
  # This setting changes the method which Active Admin calls
  # within the application controller.
  config.authentication_method = :authenticate_admin_user!

  # == User Authorization
  #
  # Active Admin will automatically call an authorization
  # method in a before filter of all controller actions to
  # ensure that there is a user with proper rights. You can use
  # CanCanAdapter or make your own. Please refer to documentation.
  # config.authorization_adapter = ActiveAdmin::CanCanAdapter

  # In case you prefer Pundit over other solutions you can here pass
  # the name of default policy class. This policy will be used in every
  # case when Pundit is unable to find suitable policy.
  # config.pundit_default_policy = "MyDefaultPunditPolicy"

  # You can customize your CanCan Ability class name here.
  # config.cancan_ability_class = "Ability"

  # You can specify a method to be called on unauthorized access.
  # This is necessary in order to prevent a redirect loop which happens
  # because, by default, user gets redirected to Dashboard. If user
  # doesn't have access to Dashboard, he'll end up in a redirect loop.
  # Method provided here should be defined in application_controller.rb.
  # config.on_unauthorized_access = :access_denied

  # == Current User
  #
  # Active Admin will associate actions with the current
  # user performing them.
  #
  # This setting changes the method which Active Admin calls
  # (within the application controller) to return the currently logged in user.
  config.current_user_method = :current_admin_user

  # == Logging Out
  #
  # Active Admin displays a logout link on each screen. These
  # settings configure the location and method used for the link.
  #
  # This setting changes the path where the link points to. If it's
  # a string, the strings is used as the path. If it's a Symbol, we
  # will call the method to return the path.
  #
  # Default:
  config.logout_link_path = :destroy_admin_user_session_path

  # This setting changes the http method used when rendering the
  # link. For example :get, :delete, :put, etc..
  #
  # Default:
  # config.logout_link_method = :get

  # == Root
  #
  # Set the action to call for the root path. You can set different
  # roots for each namespace.
  #
  # Default:
  config.root_to = 'ranking#index'

  # == Admin Comments
  #
  # This allows your users to comment on any resource registered with Active Admin.
  #
  # You can completely disable comments:
  # config.comments = false
  #
  # You can change the name under which comments are registered:
  # config.comments_registration_name = 'AdminComment'
  #
  # You can change the order for the comments and you can change the column
  # to be used for ordering:
  # config.comments_order = 'created_at ASC'
  #
  # You can disable the menu item for the comments index page:
  config.comments_menu = false
  #
  # You can customize the comment menu:
  # config.comments_menu = { parent: 'Admin', priority: 1 }

  # == Batch Actions
  #
  # Enable and disable Batch Actions
  #
  config.batch_actions = true

  # == Controller Filters
  #
  # You can add before, after and around filters to all of your
  # Active Admin resources and pages from here.
  #
  # config.before_action :do_something_awesome

  # == Localize Date/Time Format
  #
  # Set the localize format to display dates and times.
  # To understand how to localize your app with I18n, read more at
  # https://github.com/svenfuchs/i18n/blob/master/lib%2Fi18n%2Fbackend%2Fbase.rb#L52
  #
  config.localize_format = :long

  # == Setting a Favicon
  #
  # config.favicon = 'favicon.ico'

  # == Meta Tags
  #
  # Add additional meta tags to the head element of active admin pages.
  #
  # Add tags to all pages logged in users see:
  #   config.meta_tags = { author: 'My Company' }

  # By default, sign up/sign in/recover password pages are excluded
  # from showing up in search engine results by adding a robots meta
  # tag. You can reset the hash of meta tags included in logged out
  # pages:
  #   config.meta_tags_for_logged_out_pages = {}

  # == Removing Breadcrumbs
  #
  # Breadcrumbs are enabled by default. You can customize them for individual
  # resources or you can disable them globally from here.
  #
  # config.breadcrumb = false

  # == Register Stylesheets & Javascripts
  #
  # We recommend using the built in Active Admin layout and loading
  # up your own stylesheets / javascripts to customize the look
  # and feel.
  #
  # To load a stylesheet:
  #   config.register_stylesheet 'my_stylesheet.css'
  #
  # You can provide an options hash for more control, which is passed along to stylesheet_link_tag():
  #   config.register_stylesheet 'my_print_stylesheet.css', media: :print
  #
  # To load a javascript file:
  #   config.register_javascript 'my_javascript.js'
  config.register_javascript 'https://www.gstatic.com/charts/loader.js'
  config.register_javascript 'custom.js'

  # == CSV options
  #
  # Set the CSV builder separator
  # config.csv_options = { col_sep: ';' }
  #
  # Force the use of quotes
  # config.csv_options = { force_quotes: true }

  # == Menu System
  #
  # You can add a navigation menu to be used in your application, or configure a provided menu
  #
  # To change the default utility navigation to show a link to your website & a logout btn
  #
  #   config.namespace :admin do |admin|
  #     admin.build_menu :utility_navigation do |menu|
  #       menu.add label: "My Great Website", url: "http://www.mygreatwebsite.com", html_options: { target: :blank }
  #       admin.add_logout_button_to_menu menu
  #     end
  #   end
  #
  # If you wanted to add a static menu item to the default menu provided:
  #
  #   config.namespace :admin do |admin|
  #     admin.build_menu :default do |menu|
  #       menu.add label: "My Great Website", url: "http://www.mygreatwebsite.com", html_options: { target: :blank }
  #     end
  #   end

  # == Download Links
  #
  # You can disable download links on resource listing pages,
  # or customize the formats shown per namespace/globally
  #
  # To disable/customize for the :admin namespace:
  #
  #   config.namespace :admin do |admin|
  #
  #     # Disable the links entirely
  #     admin.download_links = false
  #
  #     # Only show XML & PDF options
  #     admin.download_links = [:xml, :pdf]
  #
  #     # Enable/disable the links based on block
  #     #   (for example, with cancan)
  #     admin.download_links = proc { can?(:view_download_links) }
  #
  #   end

  # == Pagination
  #
  # Pagination is enabled by default for all resources.
  # You can control the default per page count for all resources here.
  #
  # config.default_per_page = 30
  #
  # You can control the max per page count too.
  #
  # config.max_per_page = 10_000

  # == Filters
  #
  # By default the index screen includes a "Filters" sidebar on the right
  # hand side with a filter for each attribute of the registered model.
  # You can enable or disable them for all resources here.
  #
  # config.filters = true
  #
  # By default the filters include associations in a select, which means
  # that every record will be loaded for each association.
  # You can enabled or disable the inclusion
  # of those filters by default here.
  #
  # config.include_default_association_filters = true
  config.namespace :admin do |admin|
    admin.build_menu do |menu|
      menu.add label: 'Configuración', priority: 9999999 do |pages|
        pages.add label: 'Parámetros', priority: 0 , url: -> {
          config = Configuration.first
          if config.nil?
            new_admin_parameter_path
          else
            edit_admin_parameter_path config
          end          
        }
      end
          
    end
    admin.build_menu :utility_navigation do |menu|
      menu.add :label => proc {
        count = Notification.where(notifications: {is_read: false}).count
        if count.to_i == 0
          link_to fa_icon('bell'), admin_notifications_path
        else
          link_to fa_icon('exclamation-triangle'), admin_notifications_path
        end
        },
        :id     => 'currrent_notification',
        :if     => proc{ current_active_admin_user? },
        priority: 0


      menu.add :label => proc {
        "Último scraper: #{Configuration.first.sys_scraped_at.strftime('%Y-%m-%d')}".html_safe },
        :id     => 'current_scraped',
        :if     => proc{ current_active_admin_user? },
        priority: 1
        
      menu.add  :label  => proc{ display_name current_active_admin_user },
        :url    =>  proc{  edit_admin_admin_user_path(current_active_admin_user) }  ,#link_to current_active_admin_user,
        :id     => 'current_user',
        :if     => proc{ current_active_admin_user? },
        priority: 2  
      
      # menu.add add_logout_button_to_menu
      # menu.add  :label  => proc{ " 
      # <div class='header-item tabs' style='display: inline-table;'>
      # #{display_name current_active_admin_user}
      # <br>
      # Último scrapper: #{Configuration.first.sys_scraped_at.strftime('%Y-%m-%d')}
      # </div>
      # ".html_safe},
      #   :url    =>  proc{  edit_admin_admin_user_path(current_active_admin_user) }  ,#link_to current_active_admin_user,
      #   :id     => 'current_user',
      #   :if     => proc{ current_active_admin_user? }

      
      #  menu.add :label => "<span id='active_customers'></span>".html_safe
      #   if ActionController::Base.session[:customer_ids].present?
      # #   session[:customer_ids].each do |customer|
      # #     labels = '<span class="status_tag yes">#{customer}</span>'
      #     menu.add :label => "<span>#{session[:customer_ids]}</span>".html_safe
      #   end


      admin.add_logout_button_to_menu menu   # can also pass priority & html_options for link_to to use
    end    

  end
end