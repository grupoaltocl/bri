# config valid only for current version of Capistrano
lock "3.8.1"

set :application, "bri"
set :repo_url, "git@bitbucket.org:devaid/alto.git"
set :ssh_options, { :forward_agent => true }


# If the environment differs from the stage name
set :rails_env, 'production'


# Defaults to :db role
# set :migration_role, :db
set :migration_role, :app

# Defaults to the primary :db server
set :migration_servers, -> { primary(fetch(:migration_role)) }

# Defaults to false
# Skip migration if files in db/migrate were not modified
set :conditionally_migrate, true

# Defaults to [:web]
set :assets_roles, [:web, :app]

# Defaults to 'assets'
# This should match config.assets.prefix in your rails config/application.rb
set :assets_prefix, 'assets'

# RAILS_GROUPS env value for the assets:precompile task. Default to nil.
set :rails_assets_groups, :assets

# If you need to touch public/images, public/javascripts, and public/stylesheets on each deploy
set :normalize_asset_timestamps, %w{public/images public/javascripts public/stylesheets}

# Defaults to nil (no asset cleanup is performed)
# If you use Rails 4+ and you'd like to clean up old assets after each deploy,
# set this to the number of versions to keep
set :keep_assets, 2

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'config/proxy.yml', 'config/fog.yml')

# namespace :deploy do
#     task :fix_absent_manifest_bug do
#     on roles(:web) do
#       within release_path do  execute :touch,
#         release_path.join('public', fetch(:assets_prefix), 'manifest.tmp')
#       end
#     end
#   end
#   before :updated, 'deploy:fix_absent_manifest_bug'
# end