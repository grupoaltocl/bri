require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.

Bundler.require(*Rails.groups)

module SampleApp
  class Application < Rails::Application

    config.autoload_paths << config.root.join('app/lib')
    config.eager_load_paths << config.root.join('app/lib')

    config.eager_load = true
    config.action_dispatch.default_headers = {
      'Access-Control-Expose-Headers' => 'Location'
    }

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
