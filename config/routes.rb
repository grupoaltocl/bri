Rails.application.routes.draw do
  
  resources :search_engines
  resources :categories
  resources :base_products
  resources :configurations
  resources :case_files
  resources :publication_groups
  resources :customers
  resources :people
  resources :publication_snapshots
  resources :suitcases
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self) rescue ActiveAdmin::DatabaseHitDuringLoad
  devise_for :users
  resources :publishers
  resources :products
  resources :publications
  get  '/help', to: 'static_pages#help'
  get  '/about', to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  resources :users
  resources :account_activations, only: [:edit]

  root :to => redirect('/admin')

  # api
  namespace :api do
    namespace :v1 do
      resources :users, only: [:index, :create, :show, :update, :destroy]
      resources :brands, only: [:index, :create, :show, :update, :destroy]
      resources :configurations, only: [:show]
    end
  end
end
