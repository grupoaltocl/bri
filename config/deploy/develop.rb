set :stage, :develop
set :branch, :develop

set :deploy_to, -> { "/var/www/#{fetch(:application)}/#{fetch(:stage)}" }
server "bri.rocket.devaid.cl", user: "deployer", roles: %w{app db web}