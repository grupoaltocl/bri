
set :stage, :staging
set :branch, :staging

set :deploy_to, -> { "/var/www/#{fetch(:application)}/#{fetch(:stage)}" }

server "bri.rocket.devaid.cl", user: "deployer", roles: %w{app db web}
