require 'test_helper'

class PublicationGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @publication_group = publication_groups(:one)
  end

  test "should get index" do
    get publication_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_publication_group_url
    assert_response :success
  end

  test "should create publication_group" do
    assert_difference('PublicationGroup.count') do
      post publication_groups_url, params: { publication_group: { description: @publication_group.description, name: @publication_group.name } }
    end

    assert_redirected_to publication_group_url(PublicationGroup.last)
  end

  test "should show publication_group" do
    get publication_group_url(@publication_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_publication_group_url(@publication_group)
    assert_response :success
  end

  test "should update publication_group" do
    patch publication_group_url(@publication_group), params: { publication_group: { description: @publication_group.description, name: @publication_group.name } }
    assert_redirected_to publication_group_url(@publication_group)
  end

  test "should destroy publication_group" do
    assert_difference('PublicationGroup.count', -1) do
      delete publication_group_url(@publication_group)
    end

    assert_redirected_to publication_groups_url
  end
end
