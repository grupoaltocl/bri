require 'test_helper'

class PublicationSnapshotsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @publication_snapshot = publication_snapshots(:one)
  end

  test "should get index" do
    get publication_snapshots_url
    assert_response :success
  end

  test "should get new" do
    get new_publication_snapshot_url
    assert_response :success
  end

  test "should create publication_snapshot" do
    assert_difference('PublicationSnapshot.count') do
      post publication_snapshots_url, params: { publication_snapshot: {  } }
    end

    assert_redirected_to publication_snapshot_url(PublicationSnapshot.last)
  end

  test "should show publication_snapshot" do
    get publication_snapshot_url(@publication_snapshot)
    assert_response :success
  end

  test "should get edit" do
    get edit_publication_snapshot_url(@publication_snapshot)
    assert_response :success
  end

  test "should update publication_snapshot" do
    patch publication_snapshot_url(@publication_snapshot), params: { publication_snapshot: {  } }
    assert_redirected_to publication_snapshot_url(@publication_snapshot)
  end

  test "should destroy publication_snapshot" do
    assert_difference('PublicationSnapshot.count', -1) do
      delete publication_snapshot_url(@publication_snapshot)
    end

    assert_redirected_to publication_snapshots_url
  end
end
