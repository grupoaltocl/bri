require 'test_helper'

class SearchEnginesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @search_engine = search_engines(:one)
  end

  test "should get index" do
    get search_engines_url
    assert_response :success
  end

  test "should get new" do
    get new_search_engine_url
    assert_response :success
  end

  test "should create search_engine" do
    assert_difference('SearchEngine.count') do
      post search_engines_url, params: { search_engine: { name: @search_engine.name, search_excludes: @search_engine.search_excludes, search_keywords: @search_engine.search_keywords } }
    end

    assert_redirected_to search_engine_url(SearchEngine.last)
  end

  test "should show search_engine" do
    get search_engine_url(@search_engine)
    assert_response :success
  end

  test "should get edit" do
    get edit_search_engine_url(@search_engine)
    assert_response :success
  end

  test "should update search_engine" do
    patch search_engine_url(@search_engine), params: { search_engine: { name: @search_engine.name, search_excludes: @search_engine.search_excludes, search_keywords: @search_engine.search_keywords } }
    assert_redirected_to search_engine_url(@search_engine)
  end

  test "should destroy search_engine" do
    assert_difference('SearchEngine.count', -1) do
      delete search_engine_url(@search_engine)
    end

    assert_redirected_to search_engines_url
  end
end
